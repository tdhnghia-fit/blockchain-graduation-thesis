Blockchain
===


- [Kiến trúc của Blockchain, ví dụ kiến trúc Blockchain (nền tảng Hyperledger)](./architecture.md)


- [Các hình thức huy động vốn ở Việt Nam hiện nay](./capital.md)

- [Các ứng dụng Blockchain hiện nay](./applications.md)

- [Bài viết so sánh các nền tảng công nghệ sổ cái](https://tek4.vn/so-sanh-ethereum-hyperledger-fabric-va-corda/)
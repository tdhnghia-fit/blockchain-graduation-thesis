CÁC ỨNG DỤNG BLOCKCHAIN HIỆN NAY
===
# ỨNG DỤNG BẢO HIỂM LIAN
Áp dụng công nghệ blockchain trong việc mã hoá thông tin khách hàng nhằm bảo mật.	

# BỆNH ÁN ĐIỆN TỬ
* Là phần mềm quản lý về hồ sơ sức khoẻ của công dân do Trung tâm Công nghệ lõi Viettel – Tổng công ty giải pháp doanh nghiệp Viettel nghiên cứu phát triển.
* Blockchain có thể giúp điện tử hoá hồ sơ y tế: tất cả mọi thông tin của người bệnh như thông tin cá nhân, nhóm máu, lịch sử bệnh án, lịch sử khám chữa bệnh… đều sẽ được lưu trữ trong một hồ sơ được xây dựng dưới công nghệ Blockchain.
* Rút ngắn thời gian khám chữa bệnh: nhờ có hồ sơ bệnh án điện tử, mà các bác sĩ có thể tiếp cận bệnh án của bệnh nhân một cách nhanh chóng hơn. Từ đó, rút ngắn được thời gian khám chữa bệnh, cũng như tăng cường tính hiệu quả cho công tác khám chữa bệnh.
* Không cần trung gian quản lý: hồ sơ bệnh án được lưu trữ tại Blockchain sẽ do chính bệnh nhân sở hữu, quản lý, không cần trung gian thứ 3. Điều này giúp người bệnh có thể theo dõi hiệu quả bệnh án của mình. Đồng thời, các cơ sở y tế cũng tiết kiệm được chi phí khi không cần nhân sự cho việc quản lý bệnh án.

Blockchain giúp cho hoạt động tương tác, phối hợp giữa người bệnh, các cơ sở y tế, bệnh viện, cửa hàng thuốc và các Bộ ban ngành liên quan diễn ra nhịp nhàng hơn. Đặc biệt, công nghệ này giúp gia tăng sự tin tưởng giữa các bên do loại bỏ được các yếu tố nghi ngờ lẫn nhau, không công bằng, thiếu minh bạch.

Ví dụ: “Cứ mỗi lần đến một cơ sở y tế, bệnh viện, người bệnh lại phải xếp hàng khai báo thông tin cá nhân từ đầu, thậm chí là làm lại những xét nghiệm căn bản mà ở các bệnh viện khác đã làm rồi. Thực tế những việc này khiến tiêu tốn mất 1-2 ngày”.

Điều này khiến người bệnh tốn thời gian và chi phí để lặp lại những hành động đã có. Ngoài ra, người Việt Nam không có thói quen giữ lại hồ sơ bệnh án sau mỗi lần điều trị, gây nên khó khăn cho các bác sĩ khi theo dõi bệnh tình.

# e-Health: Hệ thống chăm sóc sức khoẻ của chính phủ Estonia
* Mỗi người ở Estonia đã đến thăm bác sĩ đều có hồ sơ Sức khỏe điện tử (e-Health Record)) trực tuyến  có thể được theo dõi. Được xác định bằng thẻ ID điện tử (ID-card), thông tin sức khỏe được giữ an toàn tuyệt đối và đồng thời có thể truy cập được đối với các cá nhân được ủy quyền.

* Hồ sơ sức khỏe điện tử (Hồ sơ sức khỏe điện tử) là một hệ thống toàn quốc tích hợp dữ liệu từ các nhà cung cấp dịch vụ chăm sóc sức khỏe khác nhau của Estonia để tạo một hồ sơ chung mà mọi bệnh nhân có thể truy cập trực tuyến.

* Hoạt động rất giống như một cơ sở dữ liệu quốc gia tập trung, e-Health Record thực sự lấy dữ liệu khi cần thiết từ các nhà cung cấp khác nhau, những người có thể đang sử dụng các hệ thống khác nhau và trình bày nó ở định dạng chuẩn thông qua cổng thông tin Bệnh nhân điện tử . Một công cụ mạnh mẽ cho các bác sĩ cho phép họ truy cập hồ sơ của bệnh nhân một cách dễ dàng từ một tệp điện tử, các bác sĩ có thể đọc kết quả xét nghiệm khi họ nhập, bao gồm các tệp hình ảnh như tia X ngay cả từ các bệnh viện từ xa.

* Để đảm bảo tính toàn vẹn của hồ sơ y tế điện tử cũng như nhật ký truy cập hệ thống, công nghệ chuỗi khối KSI đang được sử dụng.

Ví dụ, trong tình huống khẩn cấp, bác sĩ có thể sử dụng mã ID của bệnh nhân để đọc thông tin quan trọng về thời gian, chẳng hạn như nhóm máu, dị ứng, phương pháp điều trị gần đây, thuốc đang sử dụng hoặc mang thai. Hệ thống này cũng tổng hợp dữ liệu cho các số liệu thống kê quốc gia, vì vậy Bộ có thể đo lường xu hướng y tế, theo dõi dịch bệnh và đảm bảo rằng các nguồn lực y tế của nó đang được chi tiêu một cách khôn ngoan.
Bệnh nhân có quyền truy cập vào hồ sơ của chính họ, cũng như của những đứa trẻ chưa đủ tuổi và những người đã cho phép họ truy cập. Bằng cách đăng nhập vào cổng thông tin Bệnh nhân điện tử bằng thẻ ID điện tử, bệnh nhân có thể xem lại các lần khám bác sĩ và đơn thuốc hiện tại, và kiểm tra xem bác sĩ nào đã truy cập vào tệp của họ.

Tổng quan về dịch vụ chăm sóc sức khoẻ của chính phủ Estonia: https://www.youtube.com/watch?v=H4QLzQGMI3k

Link : https://e-estonia.com/solutions/healthcare/e-health-record/

# Tracr
* Nền tảng sử dụng công nghệ Blockchain để theo dõi thành công một viên kim cương thông qua chuỗi giá trị, cung cấp sự đảm bảo truy xuất nguồn gốc

* Hệ thống sẽ đưa tất cả các doanh nghiệp nhỏ và không có tổ chức vào một nền tảng minh bạch duy nhất. Từ khai thác đến cắt và đánh bóng và sau đó bán lẻ, một viên kim cương sẽ được theo dõi thông qua số duy nhất được đăng ký trên nền tảng blockchain.

* Mỗi lần nó đi từ giai đoạn này sang giai đoạn khác, mỗi mục sẽ phản ánh trên hệ thống blockchain, theo dõi nó qua tất cả các giai đoạn. Để làm cho hệ thống không có các yếu tố bất chính, mỗi thành viên tham gia hội đồng sẽ phải trải qua quy trình KYC (Biết khách hàng của bạn).

* De Beers đã yêu cầu tất cả những người chơi trong ngành, bao gồm cả thợ mỏ và thương nhân, trở thành một phần của hệ thống. Các nhà giao dịch kim cương cũng tin rằng việc tích hợp mọi đơn vị kim cương trên một nền tảng là một ý tưởng tốt, miễn là nó giúp mang lại sự minh bạch và giảm bớt giấy tờ.

Link : https://www.tracr.com/

## Tài liệu tham khảo
1. http://giaiphapviettel.vn/tin-tuc/148/ap-dung-quy-dinh-ho-so-benh-an-dien-tu.html
2. http://cand.com.vn/y-te/Ung-dung-thanh-cong-Blockchain-vao-quan-ly-ho-so-suc-khoe-ca-nhan-515699/
3. https://devteam.mobi/ung-dung-blockchain-trong-y-te/
4. https://forbesvietnam.com.vn/cong-nghe/30-ung-dung-thiet-thuc-cua-cong-nghe-blockchain-trong-doi-song-3726.html
5. https://builtin.com/blockchain/blockchain-healthcare-applications-companies
6. https://medium.com/blockchainbistro/top-5-use-cases-of-blockchain-in-pharma-and-healthcare-that-you-should-know-about-77ccdd76369b
7. https://www.businesstoday.in/current/corporate/de-beers-blockchain-based-tracr-to-integrate-all-diamond-businesses-on-one-platform/story/280941.html
8. http://ciem.org.vn/Content/files/2018/vnep2018/C%C4%90%2013%20-%20Blockchain%20v%C3%A0%20CP%20s%E1%BB%91-converted.pdf










Blockchain Architecture
===

Kiến trúc của blockchain thông thường gồm có 6 lớp [10]:

- Lớp dữ liệu (data layer): lớp này bao gồm nền tảng các khối dữ liệu, các thông
điệp mã hóa, ấn thời gian,...
- Lớp mạng lưới (network layer): mạng lưới blockchain mang tính chất bình
đẳng, tự trị và phân tán.
- Lớp đồng thuận (consensus layer): như cái tên đã chỉ ra, lớp này bao gồm
nhiều giao thức đồng thuận khác nhau được sử dụng để giải quyết vấn đề về
độ tin cậy thấp đối với các thành viên tham gia, thường thấy nhất là thuật toán
Proof of Work (PoW), Proof of Stake (PoS), Practical Byzantine Fault
Tolerance (PBFT), ...
- Lớp khuyến khích (incentive layer): cơ chế khuyến khích góp phần giúp cho
blockchain đảm bảo các nút trong mạng lưới hướng về chung một mục tiêu đó
là độ an toàn và hiệu quả của hệ sinh thái blockchain. Lấy ví dụ từ Bitcoin, ta
có cơ chế trả công cho nút giải được một khối và trả công khi xác nhận giao
dịch.
- Lớp hợp đồng (contract layer): bao gồm các đoạn mã, thuật toán và các hợp
đồng thông minh tinh vi được lập trình để thực thi khi thỏa mãn các điều
khoản.
- Lớp ứng dụng (application layer): ứng dụng cụ thể được xây dựng trên nền
của blockchain như ứng dụng tiền ảo phân tán của Bitcoin, Ethereum hay ứng
dụng hướng doanh nghiệp của Hyperledger Fabric.

![](https://i0.wp.com/www.blockchain-utopia.com/wp-content/uploads/2016/12/blockchainarch.jpg)



Nguồn : Khoá luận tốt nghiệp K15

#

## Kiến trúc Hyperledger


Kiến trúc Hyperledger theo logical structure được tham chiếu theo 4 phần: 

- Identity services 
- Policy services
- Blockchain 
- Smart-contracts


![](https://www.blockchains-expert.com/wp-content/uploads/2018/11/Hyperledger-architecture-1.png)

`Identity services` có chức năng quản lý danh tính của các thực thể, người tham gia và đối tượng sổ cái như tài sản hay hợp đồng thông minh

`Policy services` quản lý quyền truy cập, bảo mật, luật hiệp hội/công ty/nội bộ, luật đồng thuận

`Blockchain services` quản lý sổ cái phân tán thông qua giao thức P2P. Cấu trúc dữ liệu  được tối ưu hóa để cung cấp các chương trình hiệu quả để duy trì trạng thái công khai được nhân rộng ở nhiều người tham gia.  Các thuật toán đồng thuận khác nhau đảm bảo tính nhất quán mạnh mẽ có thể được kích hoạt và định cấu hình cho mỗi lần triển khai.

`Smart-contract services` là một cách an toàn và dễ dàng để thực thi hợp đồng trên các nodes xác thực. 

## Identity Services

![](media/1.png)

- Identity (Danh tính) là một yêu cầu phổ biến và quan trọng của giao thức Hyperledger
- Identity Services quản lý danh tính cho các tổ chức, người xác nhận và người tham gia giao dịch, các đối tượng được chứa trong sổ cái như tài sản hay hợp đồng thông minh,
các thành phần hệ thống như mạng, server và môi trường thực thi.
- Identity Services cũng sẽ đại diện các vai trò khác nhau trong sổ cái

## Policy Services

![](media/2.png)

- Policy Services có chức năng cho phép cấu hình và quản lý cái chính sách của hệ thống. Bao gồm quyền kiểm soát truy cập và uỷ quyền, chính sách của tập đoàn, những điều này được mã hoá và tuân theo những quy luật dành cho việc thành viên on-boarding hay off-boarding, đăng ký nhận dạng, chính sách xác minh, chính sách bảo mật, chính sách kế toán và chính sách đồng thuận

## BLOCKCHAIN Services

![](media/3.png)

- Blockchain Services bao gồm 3 thành phần quan trọng: Giao thức P2P, Sổ cái phân tán và quản lý sự đồng thuận

- P2P Protocol cung cấp nhiều hình thức như bidirectional streaming, flow control và multiplexing requests over a single connection.Quan trọng nhất, nó hoạt động với cơ sở hạ tầng Internet hiện có, bao gồm tường lửa, proxy và bảo mật. Thành phần này xác định các thông điệp được sử dụng bởi các nút ngang hàng, từ điểm tới điểm đến phát đa hướng.

- Distributed ledger quản lý blockchain và trạng thái toàn mạng lưới bằng cách xử lý và xác thực các giao dịch, cập nhật và duy trì trạng thái của đối tượng sổ cái. Distributed ledger cũng cung cấp một số khía cạnh thiết yếu, phi chức năng như:
	- Tính toán hiệu quả một hàm băm mật mã của toàn bộ tập dữ liệu sau mỗi khối.
	- Truyền một cách hiệu quả một "delta" thay đổi tối thiểu cho tập dữ liệu, khi một node peer không đồng bộ và cần phải "bắt kịp".
	- Giảm thiểu lượng dữ liệu được lưu trữ cần thiết cho mỗi máy ngang hàng để hoạt động.

Distributed ledger sử dụng kho lưu trữ dữ liệu để persist tập dữ liệu và xây dựng cấu trúc dữ liệu nội bộ để thể hiện trạng thái thỏa mãn ba thuộc tính. Các tệp lớn (tài liệu, v.v.) được lưu trữ trong bộ lưu trữ ngoài chuỗi (off-chain), không phải trên sổ cái. Hashes của họ có thể được lưu trữ trên chuỗi như một phần của giao dịch, được yêu cầu để duy trì tính toàn vẹn của các tệp.

- Consencus module  có trách nhiệm xác nhận tính chính xác của tất cả các giao dịch trong một khối được đề xuất, theo các chính sách xác nhận và đồng thuận. Chức năng đồng thuận đạt được thỏa thuận (giữa các nút trên mạng) về thứ tự và tính chính xác của các giao dịch trong một khối. Chức năng này giao tiếp và phụ thuộc vào mô-đun hợp đồng thông minh để xác minh tính chính xác của các giao dịch.

## Smart-Contract

![](media/4.png)

- Như được định nghĩa trong các phần trước, hợp đồng thông minh là một chương trình giao dịch phi tập trung, chạy trên các nút xác thực.
- Dịch vụ hợp đồng thông minh bao gồm môi trường thời gian chạy an toàn, đăng ký hợp đồng thông minh và quản lý vòng đời.
- Nền tảng Hyperledger cung cấp khả năng chỉ định các hợp đồng thông minh theo cách thức bất khả tri về ngôn ngữ. Khả năng tương tác của các hợp đồng thông minh trên một mạng nhất định, không phụ thuộc vào ngôn ngữ, cung cấp một nền tảng thực sự mở rộng.
Các hình thức huy động vốn ở Việt Nam hiện nay
===

## Vay vốn từ ngân hàng
* Ưu điểm: sử dụng vốn vay Ngân hàng đem lại cho doanh nghiệp nhiều thuận lợi. Doanh nghiệp có thể huy động được khối lượng vốn lớn trong ngắn hạn hoặc dài hạn, do vậy đáp ứng nhu cầu về vốn của doanh nghiệp cho các mục tiêu khác nhau.
Thêm vào đó, lãi vay ngân hàng được xem là chi phí của doanh nghiệp, do đó khi sử dụng vốn vay ngân hàng doanh nghiệp được giảm một phần thuế thu nhập doanh nghiệp

* Nhược điểm: Để vay được vốn ngân hàng, các doanh nghiệp phải có bản báo cáo kế hoạch sử dụng vốn cụ thể để ngân hàng thẩm định cũng như cần tài sản để bảo đảm cho khoản vay đó. Ngoài ra doanh nghiệp phải tuân thủ các quy định do ngân hàng đề ra trong việc sử dụng vốn vay. Kết quả là doanh nghiệp giảm sự chủ động trong việc vay và sử dụng vốn vay vì còn phụ thuộc vào đánh giá của ngân hàng cũng như những quy định của tổ chức tín dụng đề ra.
Bên cạnh đó thì thủ tục phức tạp và mất thời gian có thể làm cho doanh nghiệp mất đi cơ hội kinh doanh do không có vốn một cách kịp thời.

# Gọi vốn từ nhà đầu tư thiên thần - Angel Investor

* Ưu điểm: Nhà đầu tư thiên thần cung cấp vốn và tư vấn cho công ty khởi nghiệp.
Họ sẵn sàng chấp nhận rủi ro cho ý tưởng kinh doanh vì họ tin tưởng vào nguồn lợi tiềm năng từ doanh nghiệp của bạn.

* Nhược điểm: Các nhà đầu tư thiên thần cung cấp vốn đầu tư thấp hơn so với các nhà đầu tư mạo hiểm

## Gọi vốn từ Quỹ đầu tư mạo hiểm - Venture Capital
* Ưu điểm: Các quỹ đầu tư mạo hiểm theo dõi tiến trình phát triển của một công ty mà họ đã đầu tư rất sát sao bởi họ muốn đảm bảo tính bền vững và tăng trưởng của khoản đầu tư đã bỏ ra. Họ thường kết hợp hoạt động cố vấn chuyên môn và nguồn vốn lớn để hỗ trợ một cách hiệu quả cho doanh nghiệp mới.

* Nhược điểm: Quỹ mạo hiểm sẽ vẫn trung thành với doanh nghiệp đến khi họ lấy lại được vốn và lợi nhuận. Sự gắn bó của họ thường diễn ra trong khung thời gian 3 - 5 năm. Doanh nhân có xu hướng mất quyền kiểm soát doanh nghiệp vì bạn đang dành một phần lớn cổ phần công ty cho các nhà đầu tư mạo hiểm

## Gọi vốn cộng đồng -  Crowdfunding
* Ưu điểm: giúp marketing miễn phí về kế hoạch kinh doanh mà còn cung cấp nguồn tài chính cần thiết.
Gọi vốn cộng đồng loại bỏ những rắc rối hoặc nguy cơ phải đặt doanh nghiệp của bạn vào tay một nhà đầu tư hoặc một nhà môi giới xa lạ và các nền tảng gây quỹ cộng đồng hiện nay cũng khá đơn giản

* Nhược điểm: Sự cạnh tranh gay gắt trong các nền tảng gây quỹ cộng đồng có thể sẽ rất khó khăn nếu ai đó cũng đưa ra ý tưởng kinh doanh giống nhau. Nếu nền tảng kinh doanh không vững chắc như đối thủ thì có khả năng ý tưởng kinh doanh sẽ bị bỏ qua hoặc từ chối ngay.

## Phát hành cổ phiếu

* Ưu điểm: Phát hành cổ phiếu như một công cụ giúp doanh nghiệp thu được lượng vốn lớn để mở rộng và phát triển doanh nghiệp. Hình thức này giúp doanh nghiệp tăng lượng vốn đối ứng để thực hiện các dự án có quy mô lớn hơn, cũng như nâng cao khả năng vay vốn của doanh nghiệp.
DN không phải trả lại tiền gốc cũng như không bắt buộc phải trả cổ tức nếu như doanh nghiệp làm ăn không có lãi bởi cổ tức của doanh nghiệp được chia từ lợi nhuận sau thuế.

* Nhược điểm: quyền sở hữu bị chia nhỏ ra. Các cổ đông sẽ có những ảnh hưởng nhất định đối với đời sống của công ty. Việc nghiên cứu thị trường và quá trình phát hành cổ phiếu IPO khá tốn kém đối với các chủ doanh nghiệp,các chi phí này là một phần của những rủi ro mà các chủ doanh nghiệp phải chịu trong quá trình chào bán cổ phiếu.

## Phát hành trái phiếu

* Ưu điểm: giúp doanh nghiệp chủ động linh hoạt hơn trong việc tiếp cận được những nguồn vốn lớn. Trái chủ không có quyền can thiệp vào công ty.

* Nhược điểm: trái phiếu không làm tăng vốn cho công ty, Đối với trái phiếu, trừ trái phiếu chuyển đổi thành cổ phiếu doanh nghiệp phải trả lại số tiền gốc khi trái phiếu đáo hạn, điều này có thể sẽ ảnh hưởng rất mạnh đến dòng tiền và  tình hình tài chính của doanh nghiệp. Phải trả lợi tức cho người chủ sở hữu trái phiếu với tỷ lệ nhất định không phụ thuộc vào tình hình kinh doanh công ty.


## Huy động vốn từ lợi nhuận không chia.

* Ưu điểm: vốn lợi nhuận không chia là vốn do chủ doanh nghiệp sở hữu, doanh nghiệp có quyền sở hữu lâu dài, không cần chịu gánh nặng nợ vay. Doanh nghiệp có thể sử dụng vốn tuỳ mục đích không chịu sự quản lý hay ràng buộc bởi bất kỳ ai. Hình thức tự tài trợ bằng lợi nhuận không chia có ưu điểm là nó tác động rất lớn đến nguồn vốn kinh doanh, tạo cơ hội cho công ty thu được lợi nhuận cao hơn trong các năm tiếp theo. Đồng thời giúp doanh nghiệp tự chủ trong vấn đề tài chính, dễ dàng hơn trong quan hệ tín dụng với Ngân hàng, tổ chức tín dụng và các cổ đông.

* Nhược điểm: Bất lợi: gây mâu thuẫn về quyền lợi giữa các nhà quản lý và cổ đông, giảm tính hấp dẫn của cổ phiếu và thời gian đầu. Khi doanh nghiệp trong trả cổ tức cho cổ đông mà giữ lại lợi nhuận có thể làm cho giá cổ phiếu trên thị trường giảm, ảnh hưởng xấu đến doanh nghiệp.

### TÀI LIỆU THAM KHẢO

1. https://vietnambiz.vn/uu-va-nhuoc-diem-cua-10-cach-goi-von-pho-bien-nhat-phan-1-118429.htm
2. https://luanvanviet.com/cac-hinh-thuc-huy-dong-von-cua-doanh-nghiep-pho-bien-hien-nay/#2_Huy_2737897ng_v7889n_t7915_l7907i_nhu7853n_khocircng_chia
3. https://123doc.org//document/2046865-tieu-luan-uu-va-nhuoc-diem-tung-nguon-von.htm




Blockchain
===


- [Các bước quan trọng để triển khai một dự án Blockchain](./step-by-step-blockchain.md)


- [Kiến trúc](./architecture.md)

- [Các nền tảng công nghệ sổ cái](./ledger.md)


- [Thực trạng việc kêu gọi vốn ở Việt Nam](./thuc-trang.md)


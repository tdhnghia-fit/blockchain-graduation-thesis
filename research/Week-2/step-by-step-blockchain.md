Các bước triển khai một dự án Blockchain
===

Từ việc khởi tạo một mạng lưới để thực thi một hợp đồng thông minh (smart contract)
và chọn lựa các công cụ cần thiết, dưới đây là các bước chính để triển khai một ứng
dụng blockchain.

Công nghệ Blockchain đã đi qua giai đoạn truyền bá. Một loại sổ cái tài khoản phi
tập trung (nên rất khó bị làm sai lệch), nó cho phép xác thực các giao dịch gần như
real-time mà không cần thông qua bên thứ 3. Các ứng dụng là vô vàng và nhiều công ty
đã chuyển từ PoC (Proof of concept) - bằng chứng về khái niệm - sang các thành tựu cụ thể.

Các công nghệ nền tảng cơ bản có sẵn trong các mã nguồn mở, bất kỳ một nhà phát triển nào
khao khát cũng có thể triển khai một blochain riêng nhỏ trên máy trạm của mình và chạy nó
trên một vài máy. Cơ hội học hỏi thông qua các việc thực hành các khái niệm đằng sau một
môi trường như vậy, như là khai thác hoặc thực thi một "hợp đồng thông minh". Với quan điểm
như các giai đoạn của một dự án  

## 1. Chọn platform

Mạng Blockchain của Ethereum là một nền tảng đồng nhất. Không chỉ nhờ vào sự năng động và khả năng đáp ứng
với cộng đồng người đang sử dụng mà còn sở hữu nguồn tài liệu dồi dào, với sứ mệnh của Blockchain là các
đối tượng sẽ trở nên hoàn toàn tự chủ và thuộc về chính nó. Chúng có thể thực thi mã: để đổi lấy
tiền thưởng (một dạng mã), rồi sẽ xuất ra một quyền truy cập cho nó (thông qua mã) để có thể sử dụng
trong thời gian được cho phép.

Môi trường phát triển Ethereum từ dựa trên các ngôn ngữ phổ biến nhất như C ++ (Cpp-ethereum),
 Haskell (ethereumH), JavaScript (EthereumJS-lib) hoặc Python (Pyethapp). 
Ngôn ngữ dựa trên ngôn ngữ Go đang được sở hữu bởi Go-ethereum hay Geth).
Nó là ngôn ngữ được sử dụng nhiều nhất trong thế giới của Ethereum. Mặc định, nó kết nối với
Homestead, mạng chính của nền tàng. Bước đầu tiên là cài đặt Geth trên máy trạm của mình (nó hỗ trợ
cho Linux, iOS, Android, macOS và Windows).
 
## 2. Khởi tạo Blockchain

Ðể khởi tạo blockchain, chỉ cần tạo block đàu tiên một cách thủ công. Block dó
Phải chứa toàn bộ các đặc tính của chuỗi. Chúng sau đó sẽ được chia sẻ đến toàn bộ
các nodes của mạng lưới. Ðể định nghĩa block này, bạn phải tạo một file với định dạng JSON. Một vài tham số phải được chỉ định: "nonce" ( băm mật mã thành một giá
trị ngẫu nhiên), "timestamp" ( xác nhận thời gian giữa 2 khốii liên tiếp),... Sau
khi hoàn thành file JSON này, dựa vào ứng dụng client Geth dể tạo thư mục chứa
blockchain (chaindata) và khởi tạo nó.

Ðể đảm bảo việc truyền bá của chuong trình, cần phải có tiền điện tử.


Mục tiêu là sao chép các lệnh nhiều lần vào các nodes của mạng,
cái sau được thiết lập theo thoả thuận với cái đầu tiên. Ðể chúng giao tiếp được với
nhau trong blockchain, cần phải kết nối chúng với nhau thêm một lần nữa. Ðể forGeth 
kết nối với một nút trong mạng và điều phối bộ nodes này, nó phải truy xuất mã
định danh của mình được gọi là enode trên Ethereum.
Để đảm bảo việc truyền bá chương trình node-to-node trên Ethereum, cần phải có tiền điện tử trong Gas để có được sức mạnh tính toán cần thiết từ các tác nhân của mạng.
## 3. Chọn lựa giao thức đồng thuận
Một giao thức sẽ bao gồm việc yêu cầu  giải quyết một vấn đề toán học đòi hỏi một lượng tính toán lớn. Khi một “thợ đào” tìm kiếm được một giải pháp, nó phải dễ dàng được kiểm chứng bởi tất cả. Người đầu tiên tìm ra được giải pháp sẽ được quyền viết khối (block) tiếp theo. Nhưng vấn đề là phải điểu chỉnh được thời gian thực theo tổng công suất của mạng lưới. Do đó, các khối được viết một cách đều đặn. Hệ thống này làm cho các âm mưu hack trở nên khó khăn ( bởi vì trở thành máy tính có khả năng tính toán ra đầu tiên cực kỳ tốn kém) và bảo vệ chống lại các nỗ lực spam để làm quá tải mạng. Vì việc xác định “thợ đào hải tặc” rất dễ dàng, việc làm sai lệch blockchain (bằng cách thu thập hơn 50% tổng sức mạnh tính toán) tương đương với việc phá hủy đầu tư phần cứng của nó và loại trừ chính nó khỏi mạng.
Hashcash là một sự đồng thuận nổi tiếng của Proof of Work. Chỉ cần mã hóa một tin nhắn thông qua chức năng băm. Việc tìm khóa giải mã là không thể về mặt toán học: không có cách nào khác để tạo khóa ngẫu nhiên và thử từng cái một để tìm thông điệp gốc. Nỗ lực này đòi hỏi sức mạnh tính toán, nó là bằng chứng của công việc (PoW).
## 4. Thực thi hợp đồng thông minh đầu tiên của bạn
Việc gắn kết một Blockchain chỉ được quan tâm nếu bạn có thể thực hiện một “Hợp đồng thông minh” (Smart Contract). Đó lầ một “Hợp đồng thông minh” nó tự thực thi từ các ngưỡng đã được định nghĩa từ trước như là một ngày, một lượng hay một sự kiện xác thực hợp lệ nào. Trong trường hợp Blockchain công khai, khái niệm này đã tạo nên thành công của Ethereum.
Ngôn ngữ tham khảo để phát triển các ứng dụng như vậy trên Ethereum là Solidity. Ngôn ngữ này tương đối đơn giản và tiếp cận một môi trường lập trình hướng đối tượng với các khái niệm về lớp, thuộc tính, hàm. Ngoài các đặc tính của Ethereum, ví dụ, khi một chức năng được sử dụng, mỗi giao dịch có một công ty phát hành, chi phí liên quan. Mã cũng vì thế mà nhạy cảm hơn, lỗi nhỏ nhất đều có hậu quả.
“Hợp đồng thông minh” như một ứng dụng web.
Ngoài kiến thức về JavaScript, một dự án như vậy sẽ liên quan đến việc có sự hiểu biết toàn diện về blockchain, triết lý và các ràng buộc của nó. Trước khi bắt tay vào thực hiện, điều quan trọng trước tiên là đặt câu hỏi về mức độ phù hợp của DApp (hoặc ứng dụng phi tập trung), điều này chỉ hữu ích để giải quyết vấn đề về niềm tin giữa các chủ thể.
## 5. Debug và Scale
Việc gỡ lỗi của một blockchain dường như đã đánh dấu tâm trí của những người thực hành nó. Không giống như một chương trình được chạy bởi một máy tính, một khối được thực thi trên một tập hợp các nút hoặc kết thúc mạng. Bạn nên biết rằng mỗi người phải dẫn đến một điều trị tương đương với những người khác. Quá trình làm cho việc gỡ lỗi ứng dụng vô cùng phức tạp.
Để khắc phục sự cố, bạn sẽ phải tạo một cái mới và sau đó chờ chuỗi truyền bá các thay đổi. Cuối cùng, trong trường hợp các nền tảng mã nguồn mở, mã sẽ không ngừng phát triển.
Vì tất cả những lý do này, nên kiểm tra hợp đồng thông minh của bạn bởi một chuyên gia. Một giai đoạn sẽ là cần thiết hơn tất cả trong trường hợp hợp đồng được triển khai trên blockchain công khai cho mục đích kinh doanh nhắm đến mục tiêu khách hàng. Về điểm này, trường hợp của tổ chức DAO bị đánh cắp tương đương 50 triệu đô la trong Ethers vì một lỗi là do con người.


Kiến trúc Blockchain
===
## Các thành phần trong hệ thống blockchain
- `Sổ cái (Ledger)`: bao gồm trạng thái hiện tại của cuốn sổ cái và các thông tin giao dịch của blockchain
- `Transaction`:  hồ sơ, thông tin, giao dịch, ... phục vụ như mục đích của blockchain
Giao dịch trong Bitcoin thường bao gồm địa chỉ người nhận, địa chỉ người gửi và giá trị. Điều này không quá khác biệt so với giao dịch tiêu chuẩn mà bạn sẽ tìm thấy trên bảng sao kê thẻ tín dụng.
Các giao dịch được đóng gói và gửi đến từng nút dưới dạng một khối (block). Vì các giao dịch mới được phân phối trên toàn mạng, nên chúng được xác minh và "xử lý" một cách độc lập bởi mỗi nút. 

    Tuỳ vào loại Bitcoin, thì các loại giao dịch sẽ khác nhau.
	
- `Block` - một cấu trúc dữ liệu được sử dụng để giữ một tập hợp các giao dịch được phân phối cho tất cả các nút trong mạng
Các khối là các cấu trúc dữ liệu đóng gói các giao dịch và phân phối cho tất cả các nút trong mạng. Các khối được tạo bởi miners 
Các khối chứa một block header, là siêu dữ liệu giúp xác minh tính hợp lệ của một khối.
Một `block header` bao gồm :
    - version - phiên bản hiện tại của khối
    - hàm băm của header khối trước đó 
    - hàm băm của tất cả giao dịch của khối này
    thời gian khối được tạo ra
    - nonce - một giá trị ngẫu nhiên mà người tạo ra khối được phép thao tác


    ![](https://raw.githubusercontent.com/pluralsight/guides/master/images/f874e6a3-d3b0-49b2-86e4-83ed29ef5d09.png)


    Những thuộc tính này, tạo nên một header của một khối. Phần còn lại của khối bao gồm các giao dịch (transaction) mà  miners tạo ra.

    Mỗi người dùng blockchain được phép hành động theo cách họ muốn trong hệ thống blockchain này. Các quy tắc đồng thuận chỉ ra rằng chỉ những thay đổi hợp lệ mới được người khác chấp nhận. Điều này dẫn đến một hệ thống đảm bảo về mặt kinh tế, chỉ các khối hợp lệ mới được xử lý, gửi lên mạng và được hệ thống các user chấp nhận.
    
    
- `Chain` : một chuỗi các khối theo thứ tự cụ thể
Miners - các nút cụ thể thực hiện quy trình xác minh khối trước khi thêm bất cứ điều gì vào cấu trúc blockchain
Mining là quá trình đưa công việc trong thế giới thực vào một khối hợp lệ và khối này sẽ được chấp nhận bởi các thành phần còn lại trong hệ thống mạng blockchain. 
- `Miner` : thực hiện các giao dịch đang chờ xử lý, xác minh rằng chúng chính xác về mật mã và đóng gói chúng thành các khối sẽ được lưu trữ trên blockchain
Quá trình mining bao gồm việc băm một khối , kiểm tra xem kết quả băm có phù hợp với các quy tắc hiện tại hay không , và nếu không, thay đổi nonce trong tiêu đề khối và băm lại khối.
- `Consensus` (giao thức đồng thuận) : một bộ quy tắc và sắp xếp để thực hiện các hoạt động blockchain
Tất cả các khái niệm trước đây về các nút kiểm tra độc lập và xác minh tính hợp lệ của các giao dịch và khối được gọi là sự đồng thuận 

    Sự đồng thuận của một hệ thống blockchain được hiện thực trong một bộ quy tắc được mã hóa mà mọi người tuân thủ theo. Các quy tắc này là hoàn toàn tự thực thi. Khi hệ thống blockchain phát triển lớn hơn và có nhiều nút tham gia vào hệ thống thì sự đồng thuận cũng tăng lên mạnh mẽ do số lượng miners đang thực thi các quy tắc của riêng họ (mà mọi người khác cũng đang thực thi).

    Toàn bộ hệ thống mạng đồng ý một cách độc lập. Nếu bạn muốn giao dịch hoặc khối của bạn được mạng chấp nhận, bạn phải tuân theo các quy tắc giống như mọi người trong mạng, nếu không, bạn sẽ gặp rủi ro không phù hợp với các tiêu chuẩn đồng thuận và hệ thống bỏ qua xác minh của bạn. Do đó, sự đồng thuận bổ sung một lớp bảo mật khác cho các giao dịch blockchain.

- `Hasing`: 
Các hàm băm có một vài thuộc tính quan trọng có thể làm  `bằng chứng công việc`.

    - Các hàm băm biến một phần dữ liệu lớn tùy ý thành đầu ra băm có độ dài cố định

    - one-to-one : cùng một đầu vào sẽ luôn cung cấp cùng một đầu ra băm
    -  one-way functions: không thể "làm việc ngược" và xây dựng lại đầu vào cho đầu ra băm. 




## Cấu trúc hệ thống blockchain
Hệ thống blockchain được thể hiện bằng một danh sách các khối (block) được liên kết với nhau theo một thứ tự cụ thể.

Hai cấu trúc dữ liệu quan trọng trong hệ thống blockchain bao gồm:

- Con trỏ: biến giữ thông tin của một biến khác
- Danh sách liên kết: là một tập hợp các khối (Block), trong đó mỗi khối chứa các dữ liệu cụ thể, và được liên kết lại với nhau nhờ con trỏ

![](https://d32myzxfxyl12w.cloudfront.net/assets/images/article_images/93f0e4d07f57f6fc2eaa4abb0dab819ead8470de.jpg?154893656)


### Đặc điểm chính của kiến ​​trúc Blockchain
Kiến trúc Blockchain sở hữu rất nhiều lợi ích cho doanh nghiệp. Dưới đây là một số đặc điểm nhúng:
- Mật mã học - các giao dịch blockchain được xác thực và đáng tin cậy do các tính toán phức tạp và bằng chứng mật mã giữa các bên liên quan
- Tính không thay đổi - mọi bản ghi được tạo trong blockchain không thể thay đổi hoặc xóa
- Chứng minh - đề cập đến thực tế là có thể theo dõi nguồn gốc của mọi giao dịch bên trong sổ cái blockchain
- Phân cấp - mỗi thành viên của cấu trúc blockchain có quyền truy cập vào toàn bộ cơ sở dữ liệu phân tán. Trái ngược với hệ thống dựa trên trung tâm, thuật toán đồng thuận cho phép kiểm soát mạng
- Tính ẩn danh - mỗi người tham gia mạng blockchain có một địa chỉ được tạo, không phải danh tính người dùng. Điều này giữ cho tính ẩn danh của người dùng, đặc biệt là trong cấu trúc blockchain công khai
- Tính minh bạch - hệ thống blockchain không thể bị hỏng. Điều này rất khó xảy ra, vì nó đòi hỏi sức mạnh tính toán khổng lồ để ghi đè hoàn toàn mạng blockchain
### Có ba loại kiến trúc blockchain

#### Kiến trúc blockchain công khai ( public blockchain)

Kiến trúc blockchain công khai có nghĩa là dữ liệu và quyền truy cập vào hệ thống có sẳn cho bất kỳ ai tham gia vào hệ thống.

Đặc tính của loại blockchain này là công khai tất cả các giao dịch cá nhân trên hệ thống, bất kỳ ai tham gia vào hệ thống cũng có thể nhìn thấy. Mỗi một node trên hệ thống có quyền hạn như nhau.

#### Kiến trúc blockchain riêng tư ( private blockchain)

Trái với blockchain công khai, hệ thống blockchain riêng tư chỉ được kiểm soát bởi một số người dùng từ một tổ chức cụ thể hoặc người dùng được ủy quyền. 
Chỉ có những cá nhân sử dụng blockchain này mới có quyền truy cập, người tham gia sử dụng cơ chế riêng tư này, sẽ tự chọn cơ chế bảo mật riêng tư cho mình dựa trên nguyên tắc `Pre-approved participants`.

Tốc độ xử lý giao dịch trong blockchain nhanh hơn và dễ dàng hơn, chi phí giao dịch rẻ hơn so với public blockchain.

Mất private key thì mất vĩnh viễn.

Nếu private phải mất chi phí đầu tư để thiết kế, khởi tạo và quản lý thì public blockchain bạn sẽ không phải chi thêm khoản phí khởi tạo và thiết kế. Chỉ cần tham gia vào hệ thống public, mọi dữ liệu về blockchain của bạn sẽ được công khai với cộng đồng.


![](./media/media-1.png)



#### Kiến trúc blockchain trộn ( Consortium blockchain)
Là sự kết hợp giữa 2 loại blockchain trên. Nghĩa là dữ liệu sẽ công khai hết cho tất cả mọi người nhưng chỉ cho phép một người có quyền để thêm dữ liệu vào.


	



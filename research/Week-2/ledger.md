Các công nghệ sổ cái
=== 

## Nền tảng sổ cái phân tán nổi bật

- Hiện nay có các nền tảng sổ cái phân tán được phát triển và áp dụng vào những
miền mục đích khác nhau, trong đó có thể kể đến: Tài chính, ngân hàng, chuỗi
cung ứng, bảo hiểm, chăm sóc sức khỏe, trong các nghành công nghiệp khác nhau,
thậm chí nó còn được sử dụng trong lĩnh vực văn hóa, nghệ thuật,...

- Hiện nay có ba công nghệ sổ cái nổi bật nhất: Hyperledger Fabric, R3 Corda và Ethereum.


### Tổng quan chung

- Tổng quan mà nói thì cả ba công nghệ Hyperledger Fabric, R3 Corda và
Ethereum có tư tưởng thiết kế và mục tiêu khác nhau. Cả Fabric và Corda đều
hướng đến việc áp dụng công nghệ Blockchain vào một mục đích cụ thể. Điều
này được minh chứng bằng việc Corda được thiết kế để giải quyết các vấn đề xuất
phát trong nghành tài chính. Hơi khác một chút so với Corda là Fabric, với kiến
trúc mô-đun, khả năng mở rộng cao là ưu điểm của nền tảng này và do đó
mục đích của nó là sử dụng trong các lĩnh vực công nghiệp khác nhau, từ bảo hiểm đến chăm sóc sức khỏe, từ ngân hàng cho đến các chuỗi cung ứng (Supply
chain). Với chung mục đích là sử dụng tổng quát cho các lĩnh vực cụ thể,
Ethereum đang có điểm chung với Fabric, tuy nhiên, trái ngược hoàn toàn với
Fabric, Ethereum không có kiến trúc mô-đun, không sử dụng mô-đun bên ngoài
mà Ethereum là một nền tảng chung cung cấp cho tất cả các loại giao dịch và ứng
dụng.

- Một trong những điểm chung đáng nói nhất của cả ba nền tảng trên đó là
đều hỗ trợ Hợp đồng thông minh (Smart Contract).

### Sự đồng thuận

- Do kiến trúc và mục tiêu hướng đến của 3 nền tảng kể trên là khác nhau
cho nên cơ chế đảm bảo tính đồng thuận cũng khác nhau.

- Ethereum sử dụng cơ chế đồng thuận giống với Bitcoin đó là Proof of
Work (PoW). Những node tham gia vào mạng lưới đều chấp thuận một sổ
cái chung và bất kỳ node nào cũng có thể thấy được các bản ghi trong nó và vai
trò của các node là giống nhau và cơ chế đồng thuật đạt được ở mức sổ cái (ledger
level). Cơ chế đồng thuận này có điểm bất lợi đó là hiệu năng bị ảnh hưởng
bởi quá trình xử lý giao dịch. Và thêm một điều nữa là các giao dịch trên sổ cái
đều được ẩn danh, tuy nhiên nó có thể được truy cập bởi tất cả người tham gia,
do vậy những ứng dụng đề cao tính riêng tư có thể không phù hợp khi sử dụng
nền tảng này.

- Trái ngược với Ethereum, bài toán đồng thuận của Fabric và Corda không
dựa vào PoW và cơ chế mining. Fabric đảm bảo tính đồng thuận xuyên suốt quá
trình giao dịch, từ khi đề xuất giao dịch đến khi giao dịch được cam kết ghi nhận.
Các node tham gia chỉ có thể truy cập được các giao dịch tại channel mà nó kết
nối và không thể biết được các giao dịch của channel khác. Với cách tiếp cận này, giao dịch chỉ được hạn chế cho các bên liên quan, do đó cơ chế đồng thuận đạt
được ở mức giao dịch (transaction level).

- Cũng giống Fabric, cơ chế đồng thuận của Corda đạt được tại mức giao
dịch.

### Hợp đồng thông minh

- Smart contract là một thuật ngữ mô tả một giao thức đặc biệt, được viết
bằng ngôn ngữ lập trình, được thực thi bởi máy tính. Đoạn mã của Smart contract
quy định những điều khoản, điều kiện bắt buộc phải thỏa mãn khi một giao dịch
xảy ra, hợp đồng thông minh được triển khai nhờ vào công nghệ blockchain. Khi
được cài đặt trên mạng blockchain, toàn bộ hoạt động của smart contract được
thực hiện một cách tự động và không có bất cứ sự can thiệp nào từ bên ngoài, do
đó đảm bảo được tính minh bạch và chính xác của dữ liệu.
- Với Ethereum ngôn ngữ thực hiện các hợp đồng thông minh có tên là
Solidity được phát triển trên ngôn ngữ C++, Python và Java Scripts và ngôn ngữ
này được dịch trên máy ảo Ethereum (EVM). Ưu điểm là trong sáng, dễ hiểu, dễ
thực hiện.

- Với Hyperledger Fabric, hợp đồng thông minh được triển khai dưới cái tên
là Chaincode. Hyperledger hỗ trợ viết hợp đồng thông minh bằng các ngôn ngữ
như Java, Golang hay Javascripts. Ưu điểm là nhà phát triển không cần phải học
thêm một ngôn ngữ mới nào khi sử dụng nền tảng này, bởi các ngôn ngữ hỗ trợ
hợp đồng thông mình đều là ngôn ngữ có mục đích chung (General-purpose
programming language) và phổ biến nhất.

- Với Corda thì hợp đồng thông minh có thể được viết bằng ngôn ngữ Kotlin,
đây là ngôn ngữ do JetBrains phát triển được biên dịch thông qua JVM (Java Virtual Machine), với ưu điểm là mức độ tích hợp cao, mô hình lập trình đa dạng.

## Tổng kết

- Ethereum, Corda, Fabric là ba nền tảng sổ cái phân tán nổi bật nhất, mục
tiêu hướng tới của các nền tảng là không giống nhau. Với Smart Contract thì
Ethereum có tham vọng là một nền tảng phù hợp với bất kỳ ứng dụng nào, tuy
nhiên một nhược điểm lớn của Ethereum đó là tính minh bạch hoàn toàn, do đó
việc áp dụng vào các lĩnh vực cần tính riêng tư thì đây không phải là một lựa chọn
phù hợp.
- Corda được xây dựng bởi các tổ chức tài chính và ngân hàng với mục đích
chính được nhắm đến là giải quyết các vấn đề quyền riêng tư đối mặt bởi các ngân
hàng cụ thể.
- Với Hyperledger Fabric, nó khắc phục được nhược điểm của Ethereum vì
nó có cơ chế kiểm soát truy cập, các bên tham gia phải được cấp phép với được
tham gia vào mạng lưới. Cùng với đó là kiến trúc mô-đun giúp mở rộng linh hoạt
về các giao thức đồng thuận, do đó Hyperledger Fabric phù hợp hơn với các ứng
dụng ở mức doanh nghiệp hơn và tập hợp các lĩnh vực áp dụng rộng rãi.


## REF
-  D. Yang, P. Treleaven and R. Gendal Brown, "Blockchain Technology in Finance,"
in Computer, vol. 50, no. 9, pp. 14-17, 2017.

-  Blockchain application and outlook in the banking industry  - https://jfin-
swufe.springeropen.com/articles/10.1186/s40854-016-0034-9.

- Blockchain ready manufacturing supply chain using distributed ledger. Authors:
Abeyratne, Saveen A Monfared, Radmehr P.
- Hyperledger vs Ethereum – Which Blockchain Platform Will Benefit Your
Business?  - https://www.edureka.co/blog/hyperledger-vs-ethereum/.
- Hyperledger Fabric - Architecture, Components & Transactional Flow  - https://www.linkedin.com/pulse/hyperledger-fabric-architecture-components-flow-
chandrasekaran.

- Ethereum Architecture  - https://www.zastrin.com/courses/ethereum-
primer/lessons/1-5.

- Smart contract showdown: Hyperledger Fabric vs MultiChain vs Ethereum vs
Corda - https://www.multichain.com/blog/2018/12/smart-contract-showdown/.

- Proof of Work  - https://en.wikipedia.org/wiki/Proof_of_works

- Ethereum Consensus and Scalability (Blockchain series — Part III)  - https://medium.com/bethereum/ethereum-consensus-and-scalability-blockchain-series-
part-iii-4acd78d0eb41.

- Consensus and notaries  - https://docs.corda.net/releases/release-M9.2/key-
concepts-consensus-notaries.html.

- Consensus in Hyperledger Fabric  -
https://www.skcript.com/svr/consensus-hyperledger-fabric/.
- Solidity  - https://solidity.readthedocs.io/en/v0.5.3/.

- Hyperledger Fabric Smart Contracts and Chaincode  - https://hyperledger-
fabric.readthedocs.io/en/release-1.4/smartcontract/smartcontract.html.

- Smart Contract in Corda  - https://docs.corda.net/releases/release-
M1.0/tutorial-contract.html.
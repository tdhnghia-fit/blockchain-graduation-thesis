Thực trạng việc gọi vốn tại Việt Nam
===

Đối với các doanh nghiệp khởi nghiệp tại Việt Nam gọi vốn vẫn là một câu chuyên đau đầu. Theo số liệu từ Cyber Agency, tổng giá trị đầu tư vào các dự án của khu vực Đông Nam Á là 1,5 tỷ đô la. Nhưng chỉ chưa đầy 100 triệu đô la Mỹ được đổ vào Việt Nam được biết 80% số tiền còn lại là chuyển qua các starup khác được chuyển qua Singapore và Indonesia.

## Thực trang về việc huy động vốn của các starup Việt Nam

- Mỗi năm thị trường VN lại đón nhận không ít doanh nghiệp mới được thành lập. Năm 2017, số lượng doanh nghiệp thành lập mới đạt mức kỹ luật gần 12.700 doanh nghiệp tăng 15% so với năm 2016 theo số liệu do Cục quản lý đăng ký kinh doanh và Bộ kế hoạch đầu tư. Vậy lý do vì sao các doanh nghiệp starup Việt Nam lại chậm chân trong việc nhận nguồn vốn đầu tư so với các quốc gia khác trong cùng khu vực. Trên thực tế nền kinh tế tại Singapore hay Indonesia vốn đã tập trung rất đông các quỹ đầu tư lớn nên các quỹ đầu tư và các starup dễ dàng gặp nhau hơn. Bên cạnh đó, ở Việt Nam doanh nghiệp vẫn chưa có cơ hội để tiếp cận với các nguồn quỹ lớn hoạt động đầu tư cho starup vẫn đang diễn ra và còn nhỏ lẻ.

- Đây là lí do khách quan nhưng lúi do chủ quan vẫn là doanh nghiệp của chúng ta đang thiếu thốn nhiều mặt, đặc biệt là khả năng ngoại ngữ còn hạn chế.

## Phương thức gọi vốn bằng công nghệ Blockchain

- Trong thời gian qua thủ tục pháp lý cho việc gọi vốn cũng là lí do khiến nhiều quỹ đầu tư và starup khó lòng gặp nhau. Trước đây khi nhắc đến nhà đầu tư starup thường nghĩ đến hai kênh chính nhà đầu tư thiên thần và quỹ đầu tư khởi nghiệp. Hai kênh đầu tư này thường có dòng tiền lớn nhưng bản thân họ cũng có những quy định rất khắc khe khi lựa chọn một dự án để rót tiền. Điều dáng mừng là hiện nay starup Việt Nam cũng đang được tiếp cận với nhiều kênh đầu tư mới. Đặc biệt là phương thức gọi vốn bằng công nghệ Blockchain đây là kênh đầu tư kêt hợp vs công nghệ Blockchain và phương thực gọi vốn cộng đồng.

- Nói một cách dễ hiểu Blockchain tạo nên một nền tảng, một mạng lưới đầu tư mà ở đó nhà đầu tư có thể gặp gở starup trước đây các nhà đầu tư cá nhân khi tìm kênh đầu tư thường chỉ nghĩ đến chúng khoán, bất động sản hay gửi ngân hàng nhưng nhờ Blockchain họ có thể tìm đến một kênh khác là đầu tư cho các dự án khởi nghiệp.

```
Bên cạnh đó, starup cũng cần lưu ý khi chọn hình thức gọi vốn bằng Blockchain thì đồng nghĩa với việc phải minh bạch hoạt động của doanh nghiệp, minh bạch các thông tin về sản phẩm, về đườn hướng, cơ hội tăng trưởng. Đây rõ ràng vẫn là điểm yêu của các starup Việt Nam  ở thời điển hiện tại.
```

- Sự hấp dẫn từ phương pháp đầu tư này đã khiến không ít doanh nghiệp tại nhiều quốc gia trên thế giới vào cuộc. Trong vòng 2 năm trở lại đây thì trường gọi vốn bằng Blockchain khá lộn xộn khi xuất hiện nhiều dự án lừa đảo. Điều nay cũng gây ra bất lợi cho các starup khi có nhu cầu gọi vốn thật muốn phát triển doanh nghiệp thật, Tuy nhiên điều đáng mừng là các thị trường đang dần bình ổn trở ại nhà đầu tư cũng trở nên khắc khe hơn khi lựa chọn dự án đổ tiền. Chính phủ của các quốc gia cũng đang nghiên cứu khung pháp lý để quản lý phương thực đầu tư rất mới này.
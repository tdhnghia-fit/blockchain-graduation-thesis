# Các vấn đề khi gọi vốn cộng đồng
## 1.  Thiếu trách nhiệm 
Khi những người sáng tạo gây quỹ thành công trong một chiến dịch gây quỹ, họ sẽ tiêu tiền theo cách họ thấy phù hợp. Nếu thời hạn đã hứa đến và đi mà không có kết quả, những người ủng hộ có thể làm rất ít nhưng chờ đợi và hy vọng. Nền tảng là đồng lõa trong tỷ lệ thất bại cao này bằng cách không giữ người sáng tạo chịu trách nhiệm với lời hứa của họ. Điều này là do làm như vậy đặt gánh nặng trách nhiệm lên các nền tảng như trọng tài tranh chấp hoặc đại lý ký quỹ, phát sinh trách nhiệm hòa giải và chi phí trên không. Thay vào đó, cam kết được dán nhãn là quyên góp của Cameron, khiến những người ủng hộ gần như không thể thu lại khoản lỗ của mình thông qua các biện pháp hợp pháp. Người sáng tạo có thể làm cạn kiệt toàn bộ quỹ, cho dù đó là do quản lý kém hoặc thiếu sự chuẩn bị, và chỉ đơn giản là tiếp tục. Không có gì ngạc nhiên khi những người sáng tạo có thể cung cấp quá mức và phân phối dưới mức mà không có hậu quả, kết quả là sự chậm trễ và thất bại thường xuyên. Điều này dẫn đến sự thất vọng và niềm tin bị tổn hại trong hệ sinh thái.

## 2.  Thiếu minh bạch 
Các chiến dịch gây quỹ cộng đồng kết thúc sau một khoảng thời gian xác định, điều này khuyến khích người sáng tạo ưu tiên tiếp thị và tăng càng nhiều tiền trong khi thời gian có sẵn. Một trang mô tả và video tiếp thị khéo léo có hiệu quả trong việc gây quỹ, vì vậy việc tiết lộ bản đồ và công ty được ưu tiên thứ hai. Những người ủng hộ thường được để lại để phân tích các chiến dịch có thị trường cao mà không có thông tin thực sự từ những người sáng tạo. Điều này dẫn đến sự bất cân xứng thông tin giữa người tạo và người ủng hộ ngăn người ủng hộ tự bảo vệ mình. Vì Kickstarter xác minh một cách riêng tư thông tin cá nhân của người sáng tạo, những người ủng hộ có thể xem đây là xác thực tính xác thực của một chiến dịch tiếp thị tuyên bố bằng cách cho phép dự án liệt kê. Tuy nhiên Kickstarter không xác nhận như vậy với quy trình niêm yết của họ. Các nhà sáng tạo khuyến khích cung cấp các tiết lộ thích hợp hơn xung quanh danh tính và hoạt động kinh doanh sẽ giảm rủi ro cho những người ủng hộ đầu tư và tăng niềm tin vào mô hình.

## 3.  Tính tập trung 
Việc tập trung hóa đặt ra các ràng buộc bổ sung đối với khả năng mở rộng quy mô cộng đồng. Hiện tại, những người sáng tạo Kickstarter phải cư trú tại một trong 22 quốc gia, tương đương với chỉ 11% các quốc gia trên thế giới. Các vấn đề xoay quanh việc xử lý nhiều loại tiền tệ và quy trình sàng lọc tập trung để liệt kê chi phí lớn và hậu cần để đạt được ở quy mô toàn cầu. Ngoài ra, tập trung hóa có nghĩa là các bên trong tầm kiểm soát có thể thực hiện bất kỳ hạn chế và chính sách loại trừ nào mà họ muốn có vấn đề với đầu tư tổ chức mà việc gây quỹ cộng đồng xuất hiện ngay từ đầu.

# Các nền tảng gọi vốn cộng đồng truyền thống

## 1.  FundStart - Nền tảng gọi vốn cộng đồng trả thưởng

Fundstart là trung gian chung để bạn đăng dự án kêu gọi vốn. Với hình thức gọi vốn cộng đồng trả thưởng trên Fundstart, bạn sẽ trả thưởng hoặc quà tặng bằng hiện vật hoặc dịch vụ cho những người ủng hộ. Trả thưởng bằng tiền, lãi hoặc cổ phần không được chấp nhận đối với hình thức này. 

- Các dịch vụ cung cấp
    - Tư vấn doanh nghiệp:
        Sau khi đã ký hợp đồng, Fundstart sẽ phân tích - đánh giá doanh nghiệp cũng như sản phẩm của nhà sáng tạo định giới thiệu trong dự án. Đồng thời, Fundstart sẽ tư vấn chiến lược marketing, cách quản lý dự án và doanh nghiệp. 
    - Môi giới đầu tư: Kết nối chủ dự án đến những nhà đầu tư.
    - Truyền thông tổ chức, sự kiện:
        Fundstart sẽ hỗ trợ Thực hiện các hoạt động PR – Quảng cáo cho các chiến dịch gọi vốn cộng đồng; tổ chức sự kiện dành cho start-up và các dự án gọi vốn.
- Quy trình thẩm định dự án:
    - Thẩm định thông tin về chủ dự án và công ty,
    - Thẩm định thông tin về dự án và sản phẩm mẫu (nếu đã có   sản phẩm cụ thể)
    - Đánh giá kế hoạch kinh doanh và kế hoạch sử dụng nguồn    vốn.
    - Thẩm định cơ cấu và kế hoạch trả thưởng cho người góp     vốn.
    - Qua các khâu thẩm định, các dự án trước khi được công bố   trên FundStart đều phải có Hợp đồng pháp lý giữa            FundStart và chủ dự án.
- Quy trình gọi vốn:

    1. Chủ dự án gửi ý tưởng và thông tin dự án cần huy động vốn trên trang fundstart.vn hoặc qua email, fanpage của FundStart.
    2. FundStart tiến hành đánh giá sơ bộ và tư vấn cho chủ dự án về hình thức gọi vốn phù hợp.

        - Thời gian: 3-5 ngày làm việc
        - Trong trường hợp dự án phù hợp với hình thức gọi vốn cộng đồng trả thưởng, dự án sẽ được chuyển tới các bước kế tiếp.
    3. Chủ dự án và FundStart thảo luận về việc hợp tác gọi vốn cộng đồng cho dự án, tư vấn các thủ tục và nội dung cần chuẩn bị…Thời gian dự kiến: tối thiểu 3 ngày làm việc, tùy tình trạng dự án.
    4. Chủ dự án và FundStart ký kết hợp đồng hỗ trợ gọi vốn. Chủ Dự án đóng một khoản tiền cọc tại thời điểm này.
    5. Thông tin về dự án được công bố chính thức trên website FundStart và bắt đầu chạy chiến dịch gọi vốn cộng đồng theo các thông tin trên hợp đồng đã ký kết.
    6. Chủ dự án thực hiện các hoạt động truyền thông, marketing cho dự án để kêu gọi vốn từ cộng đồng. FundStart hỗ trợ hoạt động này thông qua các kênh truyền thông, báo chí và đối tác của FundStart.
    7. Kết thúc thời gian huy động vốn

        - `Dự án kêu gọi thất bại`:

            - Tiền vốn được FundStart hoàn lại cho người ủng hộ.
            - Tiền cọc của Chủ Dự án được hoàn trả trong vòng 10 ngày làm việc sau khi hai bên hoàn tất các thủ tục thanh lý hợp đồng.
            
        - `Dự án kêu gọi thành công`

            - Tiền vốn từ cộng đồng và tiền cọc được FundStart chuyển cho chủ dự án trong vòng 10 ngày làm việc sau khi hai bên hoàn tất các thủ tục thanh lý hợp đồng.
            - Chủ dự án đảm bảo quyền lợi của Người Ủng hộ và thực hiện báo cáo tiến độ dự án định kỳ hàng tháng cho tới khi hoàn thành cam kết và việc trả thưởng.

        Gọi vốn thành công là khi hết thời hạn gọi vốn, số tiền kêu gọi được đạt mức BẰNG hoặc LỚN HƠN số vốn mục tiêu. Gọi vốn thất bại là khi hết thời hạn gói vốn, số tiền kêu gọi KHÔNG ĐẠT 100% số vốn mục tiêu.

## 2.  Betado.com - Nền tảng gọi vốn cộng đồng cho các dự án văn hoá - nghệ thuật
- Quy trình gọi vốn:

    - Tác giả liên hệ Betado để được hỗ trợ trong việc đưa sản phẩm của mình lên website (các tác phẩm cần phải tuân theo quy định của Betado: nếu là 1 cuốn sách, tác giả phải có trong tay bản thảo của tác phẩm. Nếu là một bộ phim, tác giả phải có những tác phẩm từ trước đã hoàn thiện, ...) 
    - Nếu sản phẩm đạt tiêu chuẩn của Betado, tác phẩm sẽ được đăng lên website để kêu gọi vốn
    - Betado cung cấp các dịch vụ sau cho chủ chiến dịch muốn tăng sự ủng hộ cho sản phẩm của mình
    - Kết thúc quá trình gọi vốn:

        - Nếu số tiền góp không đủ để xuất bản, thì dự án sẽ trả lại tiền cho người quyên góp (Trừ đi 1% giao dịch phí qua cổng thanh toán, và phí chuyển khoản ngân hàng)
        - Nếu số tiền góp thành công:

            - Betado sẽ giữ 9% doanh số thu được tại thời điểm kết thúc chiến dịch.
            - Toàn bộ chỗ tiền còn lại sẽ được gỡ đóng băng, và chủ chiến dịch có thể rút về
- Cách thức quyên góp vốn:

    Các độc giả sẽ là người kiểm tra chất lượng của sản phẩm qua website, sau đó sẽ quyết định ủng hộ tác phẩm bằng cách thanh toán chuyển khoản cho Betado, đây xem như là việc đặt cọc trước tác phẩm.

## 3.  Kickstarter - Nền tảng gọi vốn cồng động trả thưởng

### Quy trình hoạt động:

- Kickstarter là nền tảng crowdfunding , cho phép những nhà kinh doanh có khả năng phát triển sản phẩm trình bày dự án của mình, nhằm gọi vốn từ người dùng trên Kickstarter ở phạm vi toàn cầu.
- Khi một dự án được đưa lên Kickstarter để kêu gọi vốn, dự án bắt buộc phải xác định mức vốn đầu tư cần có và thời gian thực hiện chiến dịch gọi vốn cho dự án. Khoảng thời gian từ khi bắt đầu đến kết thúc dự án (khoảng 30 – 40 ngày), số tiền nhận được phải bằng hoặc lớn hơn mức vốn đặt ra ban đầu.
- Để có thể nhận được tiền đầu tư trên Kickstarter, người gọi vốn sẽ có những ý tưởng để thu hút sự quan tâm của các nhà đầu tư cộng đồng, bằng cách trình bày ý tưởng qua đoạn văn bản hoặc video ngắn thật chỉn chu để thuyết phục nhà đầu tư và tận dụng tối đa mạng xã hội để thu hút cộng đồng.
- Theo đó, chủ dự án sẽ phải đặt ra nhiều gói phần thưởng và ưu đãi khác nhau, tương ứng với số vốn của nhà đầu tư.
- Trong suốt thời gian kêu gọi vốn, chủ dự án có thể nhận ý kiến phản hồi của cộng đồng, thay đổi chi tiết dự án và các phần thưởng cho nhà đầu tư. Ngược lại, nhà đầu tư cũng có thể tăng mức vốn để nhận ưu đãi nhiều hơn và tham gia trực tiếp vào việc phát triển dự án.
- Khi dự án thành công, Kickstarter thu 5% trên tổng số tiền huy động được, số tiền còn lại sẽ được chuyển cho chủ dự án. Nếu dự án không thành công, tiền ủng hộ sẽ được hoàn trả cho chủ đầu từ.

### QUY TRÌNH CHUNG CỦA GỌI VỐN CỘNG ĐỒNG

![](./media/CRP.png)

## 4. Pledgecamp - Nền tảng gọi vốn cộng đồng phi tập trung sử dụng blockchain

### Quy trình gọi vốn:

`1. Người gọi vốn đặt một khoản tiền gửi đảm bảo hoàn lại, khoản tiền này tạo ra một hình phạt tài chính đối với các thư rác trên mạng, khuyến khích người tạo minh bạch hơn trong việc quảng bá dự án của họ.`

Tiền gửi chiến dịch được hoàn trả đầy đủ với điều kiện người gọi vốn thực hiện đủ các nhiệm vụ minh bạch, bao gồm, nhưng không giới hạn, tải lên các tài liệu đăng ký kinh doanh, bằng chứng hợp đồng, đăng ký tài sản trí tuệ, kho lưu trữ mã, tài liệu tham khảo cá nhân hoặc thậm chí trả lời câu hỏi trên video trực tiếp. Mỗi hành động hoàn lại một khoản tiền gửi cố định cho đến khi hoàn trả đầy đủ.

Nếu người gọi vốn từ chối thực hiện các tác vụ này, mọi khoản tiền gửi Chiến dịch chưa hoàn lại sẽ bị hủy bỏ.

`2. Những người sáng tạo cung cấp các tài liệu, lộ trình chi tiết với các mốc quan trọng và xác định mức bảo hiểm Người mua phù hợp cho dự án của họ.`

Bảo hiểm người mua là một tính năng bảo mật duy nhất cho Pledgecamp, thực thi trách nhiệm bằng cách trao quyền cho những nhà đầu tư giám sát việc sử dụng các khoản đóng góp của họ. Một hợp đồng thông minh nắm giữ một tỷ lệ phần trăm của quỹ chiến dịch trong ký quỹ, được phát hành cho người gọi vốn khi các mốc dự án được đáp ứng. Các mốc quan trọng được xác định trước khi các quỹ được huy động để kỳ vọng giữa người gọi vốn và người đầu tư rõ ràng ngay từ đầu.

![](./media/backer_insurance.png)

Những người đầu tư xác minh những cột mốc này thông qua bỏ phiếu dân chủ. Khi một ngày bỏ phiếu tiếp cận, những người gọi vốn có thể chứng minh bằng chứng về sự tiến bộ và biện minh cho việc sử dụng vốn của họ.

`3. Người điều hành thực hiện quy trình kiểm duyệt`

Người điều hành thực hiện các chức năng chính duy trì sức khỏe của hệ thống bằng cách xóa danh sách và hành vi của người dùng vi phạm Điều khoản dịch vụ.

Trách nhiệm đầu tiên để phát hiện hành vi có hại nằm ở đám đông. Bất kỳ người dùng nào nghi ngờ các trường hợp gian lận, lạm dụng hoặc hoạt động bất hợp pháp khác đều có thể gắn cờ vi phạm bị cáo buộc. Vi phạm được xác định bởi Điều khoản dịch vụ của Pledgecamp và được cộng đồng đặt ra. Khi số lượng hoặc tỷ lệ cờ trở nên có ý nghĩa thống kê, vấn đề sẽ được giải quyết bởi một ban giám khảo của Người điều hành. Ban giám khảo gồm 12 Người điều hành sẽ được chỉ định ngẫu nhiên để quyết định vấn đề trong vòng 24 giờ. Danh tính của họ sẽ được giữ bí mật để ngăn chặn sự thông đồng. Cần có sự đồng thuận của ít nhất 7 bồi thẩm để kích hoạt một hành động khắc phục. Nếu cần thiết, Người điều hành bổ sung sẽ được chỉ định cho đến khi ghi được tối thiểu 7 phiếu.

Sau khi đạt được sự đồng thuận, danh tính của Người điều hành và các quyết định của họ sẽ được công khai để phơi bày mọi hành vi đáng ngờ. Người điều hành bị nghi ngờ lạm dụng vị trí của họ có thể tự gắn cờ và đặt trước Người điều hành khác. Ngoài ra, Người điều hành không bỏ phiếu khi được triệu tập sẽ mất tư cách sau ba lần đình công và khoản bồi thường của họ sẽ bị mất cho Người điều hành còn lại.

`4. Dự án được tài trợ thành công`

Bảo hiểm Người mua sẽ tự động được giữ trong một hợp đồng thông minh được mã hóa với các mốc đã được thống nhất và ngày bỏ phiếu.

Người gọi vốn sẽ thực hiện các KPI, các target mà họ đặt ra đối với từng cột mốc quan trọng, khi thời gian mỗi cột mốc đến gần, họ phải trình bày, đưa ra những tài liệu để chứng minh về việc họ đã làm được những gì và sử dụng vốn như thế nào.

Các nhà đầu tư sẽ kiểm tra và xem xét liệu rằng người gọi vốn có thực hiện đúng quy trình đã đặt ra hay không bằng cách bỏ phiếu đồng thuận. Trong trường hợp, số phiếu đồng thuận trên 50% thì nhà đầu tư sẽ phát hành số tiền còn lại trong Bảo hiểm người mua. Trường hợp số phiếu không đồng thuận vượt quá 50%, các nhà đầu tư từ chối, quyết định không đầu tư nữa, thì họ sẽ được nhận lại một phần theo số tiền bảo hiểm người mua.

![](./media/pledgecamp_raising.png)

## 5.  Kyper Network - Nền tảng giao dịch phân cấp cho phép trao đổi tức thì giữa các loại tiền điện tử
Kyber Network (KNC) là một nền tảng giao dịch phân cấp mới đáng tin cậy, cho phép giao dịch gần như ngay lập tức và chuyển đổi giữa bất kỳ tài sản kỹ thuật số nào với nhau. Kyber Network hứa hẹn sẽ cung cấp các API thanh toán đa dạng và một chiếc ví contract mới cho phép nhận được thanh toán từ bất kỳ token nào và đảm bảo tính thanh khoản cao.

Kyber Network giúp người dùng có thể giảm thiểu rủi ro trong thế giới cryptocurrency thông qua việc sử dụng giao dịch phát sinh của Kyber Network.

`Tổng quát về Network`

Kyber Network không chỉ là một sàn giao dịch phân quyền mà nó còn được sử dụng làm công cụ để chuyển token giữa các người dùng với nhau. Kyber phù hợp với các giao dịch P2P và cả các dự án ICO. Các token gửi đi không bắt buộc phải giống với token mà người nhận muốn. 

Hệ sinh thái của Kyber Network bao gồm:

- `Người dùng`: Cá nhân, doanh nghiệp và các tài khoản hợp đồng thông minh đều có thể gửi và nhận token trên Kyber Network.
- `Tổ chức gây vốn`: Đây là bên mang đến tính thanh khoản cho Kyber Network. Họ có thể đến từ nội bộ công ty hoặc từ bên ngoài. Tùy thuộc vào khoản đóng góp, các tổ chức này sẽ được phân loại là đại chúng hay tư nhân.
- `Người đóng góp`: Cung cấp tiền cho các tổ chức gây vốn. Họ được liên kết với các tổ chức này và cùng nhau chia sẻ lợi nhuận.
- `Người quản lý vốn`: Nhóm này có trách nhiệm duy trì hoạt động của quỹ, tính tỷ giá hối đoái và đưa vào hệ thống mạng của Kyber Network.
- `Nhà điều hành`: Có thể thêm hoặc loại bỏ các tổ chức gây vốn và kiểm soát các token. Ban đầu, đội ngũ phát triển của Kyber đảm nhận vị trí này, sau đó chuyển sang một hệ thống mạng quản lý phân tán phù hợp.

`Tính năng nổi bật của Kyber Network`
- Tin cậy và an toàn: Mọi hoạt động trên Kyber Network được x lý bởi hợp đồng thông minh (Smart Contract). Sự tin cậy là cần thiết. Kyber Network không bao giờ giữ tiền của người dùng.
- Tính thanh khoản cao: Kyber Network tuyên bố sẽ dự trữ một lượng lớn Token để cung cấp tính thanh khoản cao. Bạn có thể nhận được token của bạn ngay lập tức khi bạn giao dịch.
- Giao dịch nhanh chóng: Không cần phải chờ xác nhận và cũng không yêu cầu phải có tiền đặt cọc (deposit). Bạn có thể nhận được Token của bạn ngay lập tức khi giao dịch của bạn có trong Blockchain.
- Khả năng tương thích: Kyber Network tương thích với các hợp đồng hiện tại, do đó bạn không cần thay đổi để tích hợp với Kyber Network.

`Cách thức hoạt động bên trong của Kyber Network`

- Giao thức của Kyber Network được xây dựng dựa trên 3 nguyên lý cốt lõi:

    - `HỖ TRỢ ĐA NỀN TẢNG`: Khả năng hỗ trợ đa nền tảng cho phép Kyber Network có thể kết nối với mọi ứng dụng và giao thức mà không bị giới hạn bởi sự khác biệt về công nghệ hay hệ sinh thái.
    - `GIAO DỊCH TỨC THÌ VÀ AN TOÀN`: Giao thức của kyber giúp hiện thực hóa các giao dịch thương mại và các sản phẩm tài chính phi tập trung bằng cách cho phép xử lý các giao dịch một cách tức thì và an toàn giữa các token. Đây cũng là điểm then chốt đối với rất nhiều ứng dụng.
    - `DỄ DÀNG TÍCH HỢP`: Không chỉ vận hành hoàn toàn trên blockchain để đảm bảo tính minh bạch, Kyber còn được thiết kế với khả năng tương thích cao và thân thiện đối với lập trình viên, giúp nhiều ứng dụng khác nhau dễ dàng tích hợp.

`Quy trình trao đổi token trong kyper network`
- `Takers` : có thể hiểu là những đối tượng sẽ có nhu cầu thanh khoản token của mình sang một loại token khác. Takers có thể là người dùng cuối, là các ứng dụng chuyển đổi phi tập trung hoặc có thể là smart contract.
- `Reserves`: là những nhà cung cấp thanh khoản. Những người này sẽ quyết định số lượng thanh khoản và giá của các loại token có trên network.

Ví dụ ở đây là một taker đang có token ETH và có nhu cầu muốn thanh khoản sang token BAT, lúc này khi taker thực hiện yêu cầu giao dịch thanh khoản cho smart contract, nó sẽ tìm kiếm trong list reserves xem có reserve nào chấp nhận thanh khoản nhận về ETH và trả lại BAT hay không. Trong trường hợp này thì có nhiều hơn 2 Reserves chấp nhận thanh khoản. Sau khi đã xác định được danh sách các reserves chấp nhận thanh khoản, smart contract sẽ sử dụng vòng lặp để duyệt qua các reserves này để tìm ra tỷ lệ rate tốt nhất. Và cuối cùng nó sẽ thực hiện chuyển đổi. Đây cũng có thể hiểu là kiểu trade trực tiếp, giống như ví dụ ở trên là có reserve chấp nhận chuyển đổi trực tiếp từ EHT sang BAT. 

Vậy nếu không có reserve nào chấp nhận chuyển đổi trực tiếp thì sao?

Taker muốn thanh khoản từ 50 BAT sang DAI. Nhưng tiếc là không có Reserves nào chấp nhận thanh khoản trực tiếp từ BAT sang DAI , nên lúc này smart contract sẽ tìm và chuyển lượng BAT kia sang ETH với lượng tốt nhất, sau đó nó sẽ chuyển từ ETH sang DAI . Và cuối cùng Taker vẫn sẽ nhận được token DAI như mình mong muốn. Như vậy nó đang chuyển đổi gián tiếp qua một loại token trung gian nên ta có thể coi nó là kiểu trao đổi gián tiếp.












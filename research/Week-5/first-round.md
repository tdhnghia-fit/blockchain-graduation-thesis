Quy trình huy động vốn
===

## Quy trình huy động vốn:

### Các bên tham gia quá trình huy động vốn :

- Start - Up
- Nhà đầu tư - Investors 

### Quy trình

1. Start - Up đăng thông tin dự án (Profile) cần kêu gọi vốn
2. Thông qua hệ thống nền tảng Blockchain phát hành tiền ảo 
3. Nhà đầu tư nếu đầu tư tiến hành IBO/ICO
4. Khi đạt đủ vốn nhất định số tiền được chuyển sang cho Start-Up
5. Chuyển sang giai đoạn quản lý vốn

(Có thể bổ sung thêm một bên có trách nhiệm đánh giá profile dự án)

### Mô tả

![](./media/first-round.png)
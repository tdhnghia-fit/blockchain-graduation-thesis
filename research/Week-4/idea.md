Ý tưởng áp dụng blockchain vào việc gọi vốn từ đó xây dựng phương thức, mô hình gọi vốn
===

Với ý tưởng tận dụng tính minh bạch, bất biến dữ liệu của công nghệ Blockchain vào quy trình huy động 
và quản lý vốn cho doanh nghiệp, nhằm mục đích tạo niềm tin, sự tin tưởng của các nhà đầu tư với dự án và
doanh nghiệp. Từ đó, giúp cho doanh nghiệp với hoạt động minh bạch, rõ ràng sẽ dễ dàng chiếm được niềm tin của
các nhà đầu tư và dễ dàng có đạt được mục tiêu gọi vốn của doanh nghiệp.

Trong hệ thống quản lý và huy động vốn áp dụng công nghệ Blockchain, mấu chốt sẽ là Blockchain Network. Trong đó,
các nodes sẽ đặt máy ở những bên có liên quan đến hệ thống và có vái trò quan trọng trong các quá trình xác thực như
các cơ quan nhà nước, ngân hàng, nhà đầu tư trung gian, doanh nghiệp,... Đối với từng lĩnh vực cụ thể sẽ có thêm các bên
liên quan và mỗi bên liên quan sẽ kiểm soát một nodes trong mạng lưới Blockchain.

Mỗi giao dịch diễn ra trong dự án bao gồm giao dịch chuyển tiền, xác thực các văn bản hành chính hay các bằng chứng khác nhau cần có
sự xác thực giữa các bên. Giao dịch đó sẽ được các bên đồng thuận theo một giao thức trước khi cập nhật sổ cái Blockchain.
Và các giao dịch đó dựa vào tính chất của Blockchain là hoàn toàn minh bạch và bất biến.
Nhờ vậy, các giao dịch sẽ rất khó để gian lận bởi một cá nhân, tổ chức nào hay rủi ro khi bị tấn công bởi tin tặc.

Các mô hình gọi vốn, phương thức gọi vốn được tích hợp trong hệ thống:
 - Vay vốn ngân hàng
 - Vốn cộng đồng
 - Angel Investor
 - Vốn đầu tư mạo hiểm (*)
 - Vốn từ các quỹ khởi nghiệp (*)
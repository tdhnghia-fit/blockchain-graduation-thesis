Các vấn đề khi gọi vốn cộng đồng
=

## 1. Thiếu trách nhiệm 
Khi những người sáng tạo gây quỹ thành công trong một chiến dịch gây quỹ, họ sẽ tiêu tiền theo cách họ thấy phù hợp. Nếu thời hạn đã hứa đến và đi mà không có kết quả, những người ủng hộ có thể làm rất ít nhưng chờ đợi và hy vọng. Nền tảng là đồng lõa trong tỷ lệ thất bại cao này bằng cách không giữ người sáng tạo chịu trách nhiệm với lời hứa của họ. Điều này là do làm như vậy đặt gánh nặng trách nhiệm lên các nền tảng như trọng tài tranh chấp hoặc đại lý ký quỹ, phát sinh trách nhiệm hòa giải và chi phí trên không. Thay vào đó, cam kết được dán nhãn là quyên góp của Cameron, khiến những người ủng hộ gần như không thể thu lại khoản lỗ của mình thông qua các biện pháp hợp pháp. Người sáng tạo có thể làm cạn kiệt toàn bộ quỹ, cho dù đó là do quản lý kém hoặc thiếu sự chuẩn bị, và chỉ đơn giản là tiếp tục. Không có gì ngạc nhiên khi những người sáng tạo có thể cung cấp quá mức và phân phối dưới mức mà không có hậu quả, kết quả là sự chậm trễ và thất bại thường xuyên. Điều này dẫn đến sự thất vọng và niềm tin bị tổn hại trong hệ sinh thái.

## 2. Thiếu minh bạch 
Các chiến dịch gây quỹ cộng đồng kết thúc sau một khoảng thời gian xác định, điều này khuyến khích người sáng tạo ưu tiên tiếp thị và tăng càng nhiều tiền trong khi thời gian có sẵn. Một trang mô tả và video tiếp thị khéo léo có hiệu quả trong việc gây quỹ, vì vậy việc tiết lộ bản đồ và công ty được ưu tiên thứ hai. Những người ủng hộ thường được để lại để phân tích các chiến dịch có thị trường cao mà không có thông tin thực sự từ những người sáng tạo. Điều này dẫn đến sự bất cân xứng thông tin giữa người tạo và người ủng hộ ngăn người ủng hộ tự bảo vệ mình. Vì Kickstarter xác minh một cách riêng tư thông tin cá nhân của người sáng tạo, những người ủng hộ có thể xem đây là xác thực tính xác thực của một chiến dịch tiếp thị tuyên bố bằng cách cho phép dự án liệt kê. Tuy nhiên Kickstarter không xác nhận như vậy với quy trình niêm yết của họ. Các nhà sáng tạo khuyến khích cung cấp các tiết lộ thích hợp hơn xung quanh danh tính và hoạt động kinh doanh sẽ giảm rủi ro cho những người ủng hộ đầu tư và tăng niềm tin vào mô hình.

## 3. Tính tập trung 
Việc tập trung hóa đặt ra các ràng buộc bổ sung đối với khả năng mở rộng quy mô cộng đồng. Hiện tại, những người sáng tạo Kickstarter phải cư trú tại một trong 22 quốc gia, tương đương với chỉ 11% các quốc gia trên thế giới. Các vấn đề xoay quanh việc xử lý nhiều loại tiền tệ và quy trình sàng lọc tập trung để liệt kê chi phí lớn và hậu cần để đạt được ở quy mô toàn cầu. Ngoài ra, tập trung hóa có nghĩa là các bên trong tầm kiểm soát có thể thực hiện bất kỳ hạn chế và chính sách loại trừ nào mà họ muốn có vấn đề với đầu tư tổ chức mà việc gây quỹ cộng đồng xuất hiện ngay từ đầu.


# Pledgecamp - Nền tảng gọi vốn cộng đồng phi tập trung sử dụng blockchain

## 1. Backer Insurance - Bảo hiểm người mua
Bảo hiểm người mua là một tính năng bảo mật duy nhất cho Pledgecamp, thực thi trách nhiệm bằng cách trao quyền cho những nhà đầu tư giám sát việc sử dụng các khoản đóng góp của họ. Một hợp đồng thông minh nắm giữ một tỷ lệ phần trăm của quỹ chiến dịch trong ký quỹ, được phát hành cho người gọi vốn khi các mốc dự án được đáp ứng. Các mốc quan trọng được xác định trước khi các quỹ được huy động để kỳ vọng giữa người gọi vốn và người đầu tư rõ ràng ngay từ đầu.
Những người đầu tư xác minh những cột mốc này thông qua bỏ phiếu dân chủ. Điều quan trọng cần lưu ý là những người ủng hộ không có quyền biểu quyết quyết định quản lý của người gọi vốn. Việc bỏ phiếu đặc biệt là một phương pháp xác minh và hòa giải phân tán.

![](./media/backer_insurance.png)

Blockchain và hợp đồng thông minh là cần thiết để thực hiện Bảo hiểm Người mua. Thay vì yêu cầu đội ngũ luật sư, việc tạo hợp đồng thông minh về cơ bản là miễn phí, phán quyết được tự động hóa và việc thực thi được đảm bảo. Không có bên tập trung nào đảm nhận việc giám sát các quỹ hoặc kế thừa trách nhiệm hòa giải cho các tranh chấp của người dùng. Trách nhiệm thuộc về những người ủng hộ có tiền đang bị đe dọa.

Trước khi khởi chạy một chiến dịch mới, những người gọi vốn cung cấp một lộ trình chi tiết với các mốc quan trọng và xác định mức bảo hiểm Người mua phù hợp cho dự án của họ.

Khi một chiến dịch được tài trợ thành công, Bảo hiểm Người mua sẽ tự động được giữ trong một hợp đồng thông minh được mã hóa với các mốc đã được thống nhất và ngày bỏ phiếu. Khi một ngày bỏ phiếu tiếp cận, những người gọi vốn có thể chứng minh bằng chứng về sự tiến bộ và biện minh cho việc sử dụng vốn của họ.
Chiến dịch sẽ tiếp tục hoàn thành trừ khi những người đầu tư quyết định hủy chiến dịch nếu dự án dự án không đạt được target. Trong trường hợp này, những người đầu tư sẽ được hoàn lại một phần theo số tiền Bảo hiểm Người mua.. Hệ thống bỏ phiếu cho phép người gọi vốn có cơ hội trình bày và tiếp tục các dự án của họ, ngay cả khi bị trì hoãn. 

`Ví dụ :`
Carol đang phát triển một robot gấp quần áo mới. Cô đã cam kết 40% tiền cho Bảo hiểm người mua và hứa sẽ xây dựng một nguyên mẫu có thể gấp quần trong sáu tháng. Việc gây quỹ thành công và sau sáu tháng, Carol chia sẻ một đoạn video về chiếc quần gấp nguyên mẫu như đã hứa. Nhờ bằng chứng này, những người ủng hộ tự tin rằng cô ấy có thể giao sản phẩm cuối cùng và đồng ý phát hành số tiền Bảo hiểm người mua còn lại cho Carol. Carol sử dụng khoản tiền cuối cùng này để chuyển giao các robot đã hoàn thành như đã hứa. Những người ủng hộ rất vui và Carol có thể bắt đầu công việc kinh doanh mới của mình. 

`Một giả thuyết thay thế: `
Cột mốc đầu tiên đến và Carol không thể chứng minh bất kỳ khoản tiền nào đã được chi cho nguyên mẫu và những người ủng hộ phát hiện ra rằng đối tác sản xuất chính của cô đã bỏ rơi cô vì không nhận được khoản thanh toán. Hơn 51% người ủng hộ bỏ phiếu rằng cô ấy đã không thực hiện nghĩa vụ của mình và quyết định thu hồi Bảo hiểm Người mua. Bởi vì Carol cung cấp 40% Bảo hiểm cho Người ủng hộ, tất cả những người ủng hộ đều nhận lại 40% đóng góp ban đầu của họ. Trong trường hợp xấu nhất này, những người ủng hộ bị thua lỗ nhưng ít nhất có quyền kiểm soát kết quả và hoàn lại một phần.

## 2. Chiến dịch gửi tiền

Trước khi bắt đầu một chiến dịch mới tại Pledgecamp, người gọi vốn phải đảm bảo hoàn lại tiền gửi trước khi niêm yết. Chiến dịch tiền gửi được gây quỹ đầy đủ với điều kiện nhà sáng lập phải thực hiện đủ “những công việc minh bạch” không giới hạn bao gồm đăng tải tài liệu đăng kí hoạt động doanh nghiệp, chứng minh nhân dân, bằng chứng hợp đồng, bản đăng kí sở hữu trí tuệ, kho lưu trữ mã, thư giới thiệu cá nhân, hoặc trả lời câu hỏi trên video trực tiếp.

Trước khi bắt đầu một chiến dịch mới trên Pledgecamp, người gọi vốn phải đặt một khoản tiền gửi bảo đảm hoàn lại trước khi niêm yết. Khoản tiền gửi Chiến dịch này tạo ra một hình phạt tài chính đối với thư rác trên mạng và khuyến khích người tạo minh bạch hơn trong khi quảng bá dự án của họ. Tiền gửi chiến dịch được hoàn trả đầy đủ với điều kiện người gọi vốn phải thực hiện đủ các nhiệm vụ minh bạch, bao gồm, nhưng không giới hạn, tải lên các tài liệu đăng ký kinh doanh, nhận dạng, bằng chứng hợp đồng, đăng ký tài sản trí tuệ, kho lưu trữ mã, tài liệu tham khảo cá nhân hoặc thậm chí trả lời câu hỏi trên video trực tiếp. Mỗi hành động sẽ được hoàn lại một khoản tiền gửi cố định cho đến khi hoàn trả đầy đủ. 

Ví dụ: người gọi vốn có thể chọn năm phù hợp nhất để quảng bá với dự án của họ và lấy lại tiền ký gửi của họ. Mặc dù blockchain không thể tự động xác minh rằng những tiết lộ này là thật hay có giá trị, nhưng những thông tin này sẽ công khai cho phép mọi người xem xét và đánh giá chúng. Những người đầu tư có thể sử dụng thông tin gia tăng này để đặt thêm câu hỏi và đưa ra quyết định có học thức. Hoàn trả một phần hoặc toàn bộ Tiền gửi Chiến dịch được trả lại sau khi hết thời gian cấp vốn.

## 3.	Phân cấp hệ điều hành

Người điều hành phân tích hành vi người dùng trên sàn giao dịch để đổi lấy phí nền tảng.

Trên Pledgecamp, người dùng cuối cùng chịu trách nhiệm về việc liệt kê, giám tuyển và kết quả của các chiến dịch. Do đó, trách nhiệm cho sự thành công của nền tảng được trải rộng trên toàn bộ mạng lưới người tham gia và yêu cầu kiểm duyệt phân tán.

Để trở thành người điều hành, bạn cần phải cam kết hơn 100.000 PLG để đổi lấy CS. Nếu bạn cần thu hồi quyền hạn của người điều hành, đổi lại CS thành PLG theo tỷ lệ 1: 1, bạn cần đợi 30 ngày. 

Người điều hành thực hiện các chức năng chính duy trì sức khỏe của hệ thống bằng cách xóa danh sách và hành vi của người dùng vi phạm Điều khoản dịch vụ. Người điều hành có quyền thực hiện công việc này bằng cách chứng minh cổ phần trong nền tảng, đảm bảo rằng lợi ích cá nhân của họ phù hợp với toàn bộ nền tảng. Bằng cách này, Người điều hành được thúc đẩy để đạt được sự đồng thuận về sự thật và chống tham nhũng. 

Người điều hành nhận được phí niêm yết được tạo bởi nền tảng dưới dạng bồi thường cho vai trò của họ. Khi nền tảng phát triển thành công hơn thông qua các hành động của họ, phí niêm yết sẽ tăng theo tỷ lệ tương tự. Theo cách này, bồi thường của Moderator gắn liền với sức khỏe và thành công của nền tảng.

`Quy trình kiểm duyệt:`

Việc liệt kê dự án, xem xét thông tin và xác minh cột mốc sẽ được thực hiện bởi người điều hành. Trách nhiệm đầu tiên để phát hiện hành vi có hại nằm ở đám đông. Bất kỳ người dùng nào nghi ngờ các trường hợp gian lận, lạm dụng hoặc hoạt động bất hợp pháp khác đều có thể gắn cờ vi phạm bị cáo buộc. Vi phạm được xác định bởi Điều khoản dịch vụ của Pledgecamp và được cộng đồng đặt ra. Khi số lượng hoặc tỷ lệ cờ trở nên có ý nghĩa thống kê, vấn đề sẽ được giải quyết bởi một ban giám khảo của Người điều hành.
Ban giám khảo gồm 12 người điều hành sẽ được chỉ định ngẫu nhiên để quyết định vấn đề trong vòng 24 giờ. Và cần có sự đồng thuận của ít nhất 7 bồi thẩm để kích hoạt hành động khắc phục. 

Sau khi đạt được sự đồng thuận, danh tính của Người điều hành và các quyết định của họ sẽ được công khai để phơi bày mọi hành vi đáng ngờ. Người điều hành bị nghi ngờ lạm dụng vị trí của họ có thể tự gắn cờ và đặt trước Người điều hành khác. Ngoài ra, nếu bạn trở thành người điều hành và không tham gia quản trị cộng đồng hoặc đưa ra phán quyết không công bằng, bạn sẽ bị phạt mà không được bồi thường khi được báo cáo bởi người điều hành khác.

Người điều hành thực hiện một vai trò quan trọng đối với sức khỏe đang diễn ra của nền tảng, đặc biệt là sự thay thế cho kiểm soát và quản trị tập trung. Như vậy, bồi thường phải tương xứng với tầm quan trọng của vai trò của họ. Phí niêm yết là một lựa chọn hợp lý cho bồi thường vì chúng thể hiện lợi ích kinh tế thực sự được tạo ra dưới sự quản trị của họ và sẽ phát triển cùng với sự thành công và tác động của nền tảng.

![](./media/formula.png)

## 4.	Token Economy

Mô hình 2 token: PLG – trao đổi giá trị và CS (Camp Shares) – dùng trong hệ sinh thái người điều hành

Pledgecamp sử dụng cơ chế kinh tế hai mã thông báo, nền kinh tế hai mã thông báo liên quan đến Pledge Coins (PLG) và Camp Shares (CS) cung cấp năng lượng cho hệ sinh thái Pledgecamp. Việc sử dụng hai mã thông báo này là khác nhau. 

Pledge Token (PLG)
Nó được sử dụng với các mục đích payment, security, reward và staking bởi những người sử dụng giải pháp của Pledgecamp.

CS (Camp Share)
Nó được sử dụng để trao thưởng cho nhà điều hành trong việc quản lý hệ thống. CS thường được quy đổi từ PLG, giúp người dùng trở thành trạng thái người điều hành. Và họ sẽ nhận được quyền lợi từ phí listing fee của các dự án tham gia Pledgecamp.

## 5.	HỆ SINH THÁI PLEDGECAMP

Trong nhiều trường hợp, những doanh nhân lần đầu tiên này không có mạng lưới chuyên nghiệp rộng lớn hoặc khả năng đánh giá ai là đối tác phù hợp với họ. Trên Pledgecamp, bất kỳ doanh nhân nào cũng sẽ có thể tìm kiếm sự giúp đỡ đáng tin cậy một cách an toàn và phi tập trung. Với các ưu đãi Token, người tham gia trong các thị trường này có thể tương tác theo cách mở và phi tập trung

Hệ sinh thái của Pledgecamp bao gồm 2 nhóm chính: Market network và Knowledge Center

`Market Network (mạng lưới thị trường)` là một thị trường dịch vụ phi tập chung cho phép dự án có thể tìm kiếm chuyên gia, nhân tài làm việc cho dự án và trả thưởng bằng PLG token. Hợp đồng thông minh cho phép các dư án nhìn thấy thông tin người dùng công khai cũng như cho phép các nhà đầu tư theo dõi được tiến trình sử dụng vốn của dự án. 

Nhìn thấy một danh sách khách hàng trong quá khứ và lịch sử thành công hay thất bại sẽ cung cấp cho dự án một cách đáng tin cậy để tìm sự giúp đỡ đáng tin cậy mà dự án cần. Tóm lại, những gọi vốn sử dụng Mạng lưới thị trường có thể chứng minh cho những người đầu tư rằng họ có các mối quan hệ đối tác để thực thi. 

`Knowledge Center (trung tâm dữ liệu)` là nền tảng lưu trữ thông tin của tất cả thành phần tham gia cộng đồng: doanh nghiệp, phát triển sản phẩm và gọi vốn cộng đồng, phuc vụ cho mục đích lưu trữ, tìm kiếm, hỏi đáp thông tin. Người dùng có thể tham gia nền tảng, xây dựng thương hiệu riêng và nhận thưởng khi có đóng góp tích cực và hiệu quả cho cộng đồng.


`Pledgecamp hiện tại chưa ra mắt nền tảng gọi vốn phiên bản chính thức.`

Phiên bản demo tại đây: https://demo.pledgecamp.com/


## Tài liệu tham khảo

https://blogtienao.com/blockchain-va-hinh-thuc-goi-von-cong-dong-lieu-pledgecamp-co-the-dinh-hinh-lai-nganh-cong-nghiep-gay-quy-nay-khong/

https://coin98.net/pledgecamp-plg/

https://medium.com/pledgecamp/jrr-genesis-invest-b58404f34393
https://bigcoinvietnam.com/pledgecamp-la-gi--tat-tan-tat-ve-du-an-goi-von-cong-dong-the-he-moi

https://pledgecamp.com/__pdf/pledgecamp_whitepaper_v2-2_vi.pdf







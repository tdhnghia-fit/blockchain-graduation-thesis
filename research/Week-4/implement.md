Cách thức triển khai một hệ thống Blockchain
=

## Nội dung

- Mô tả kiến trúc hệ thống Blockchain
  - Tham khảo IBM Blockchain Platform 
  - Tham khảo Azure Blockchain Workbench
  - Một số kiến trúc khác

- Mã nguồn nền tảng sổ cái phổ biến (hỗ trợ Java)


## IBM Blockchain Platform Architecture


![](./media/ibm-platform-architecture.png)

### Flow

```
Kiến trúc Blockchain này cho thấy các actors như users(Web Application hay Devices) và các thiết bị IoT tương tác với một ứng dụng blockchain điển hình như thế nào. Trước khi flow này bắt đầu, The Blockchain Network và Governance bao gồm peers, membership services và endorsement policies (chính sách chứng thực) đã được design, implement, deploy và đi vào hoạt động.
```

`Step 1`

Một user thường giữ an toàn private key của họ trong một private wallet.

`Step 2`

Một user hay IoT device yêu cầu truy cập đến một ứng dụng blockchain qua việc sử dụng mobile device hay web application. Credentials được giữ trong wallet.

`Step 3`

The edge services, ví dụ như một DNS và CDN, chịu trách nhiệm nhận các request

`Step 4`

Request được route đến ứng dụng Blockchain

`Step 5`

Ứng dụng Blockchain sử dụng một identity and access management framework (quản lý danh tính và truy cập) để authenticate (xác thực) và authorize (ủy quyền) user bằng các sử dụng the digital certificate (chứng chỉ điện tử) hoặc một số công nghệ được biến đến như single sign-on, multifactor, authentication.

`Step 6`

Ứng dụng định tuyến các user requests mà yêu cầu Blockchain Smart Contracts đến The Transaction Manager bằng cách sử dụng một message bus tùy chọn. Trong các giải pháp đơn giản hơn, có thể không cần messages bus.

`Step 7`

The Transaction Manager xử lý các messages từ message bus. Trong các giải pháp đơn giản hơn The Transaction Manager nhận request trực tiếp từ Application Logic.

`Step 8`

The Transaction Manager có thể truyền một message đến Blockchain Network bằng các sử dụng credentials. The Transaction Manager lấy the payload được dựng bởi ứng dụng (chúng thường chứa thông tin nhận diện của the end user), truy cập The Digital Credentials được lưu trữ trong wallet được maintain bởi người tổ chức và chữ ký điện tử. Ngoài ra, The Transaction Manager có thể truyền một message signed bởi trực tiếp end user đến Blockchain thay mặt cho user.


`Step 9`

Thông thường, một ứng dụng sẽ quản lý dữ liệu off-chain (dữ liệu như được lưu trên sổ cái) tại nơi lưu trữ dữ liệu local.

`Step 10`

The Transaction Manager khi yêu cầu Smart Contracts, với payload tương ứng, trên một hay nhiều nodes Blockchain. Khi có dữ liệu off-chain, The Transaction Manager thường write và read dữ liệu đến và từ kho lưu trữ dữ liệu off-chain phối hợp với việc gọi các hợp đồng thông minh blockchain.

`Step 11`

Mỗi mạng lưới Blockchain sử dụng giao thức của nó để xử lý The Smart Contract (Hợp đồng thông minh), lấy chứng thực từ yêu cầu của người tham gia và commit transactions (Nó được tạo ra bởi hợp đồng thông minh) đến sổ cái Blockchain. Trong Hyperledger Fabric, mỗi instantiation của một Hợp Đồng Thông Minh(chaincode) có một chính sách chứng thực phải được thỏa mãn.

`Step 12`

Hợp đồng thông minh thường yêu cầu quyền truy cập vào dữ liệu từ hệ thống external đáng tin cậy đến sổ cái blockchain. Để đảm bảo sự đồng thuận của việc thực hiện hợp đồng thông minh giữa distributed peers, hợp đồng thông minh phải có tính quyết định. Điều này đòi hỏi mọi quyền truy cập vào dữ liệu ngoài đều trả về cùng một kết quả. Những nguồn dữ liệu bên ngoài này được thiết kế để cung cấp nguồn sự thật cho việc thực hiện hợp đồng thông minh.

`Step 13`

Khi blockchain đặt hàng các transactions và hình thành các khối, nó phát ra các sự kiện. The Event Listener lắng nghe những sự kiện này. Sau đó, các sự kiện có thể kích hoạt 1) hành động bên ngoài (không hiển thị), 2) nhiều tương tác blockchain hơn với bất kỳ dữ liệu off-chain liên quan nào, 3) gửi lại cho người dùng cuối thông qua message bus và 4) sao chép an toàn dữ liệu blockchain có liên quan vào một hệ thống doanh nghiệp. Trong hệ thống doanh nghiệp, dữ liệu blockchain có thể kết hợp với dữ liệu độc quyền phi blockchain và sử dụng các công cụ phân tích business.

`Step 14`

Các sự kiện được gửi đến Message Bus

`Step 15`

Ứng dụng truy xuất và hành động đối với sự kiện hoặc thông báo cho người dùng hoặc kích hoạt các hành động trong hệ thống doanh nghiệp.

`Step 16`

Các giao thức ứng dụng với hệ thống doanh nghiệp bên ngoài thông qua transformation và connectivity gateway.

`Step 17`

Giao dịch blockchain được phản ánh trong hệ thống doanh nghiệp, ví dụ như sao chép an toàn dữ liệu blockchain có liên quan vào hệ thống doanh nghiệp.

`Step 18`

Như một tùy chọn, người dùng có thể yêu cầu các dịch vụ phân tích để hiểu rõ hơn về blockchain và các giao dịch khác.

`Step 19`

Tùy chọn, dịch vụ phân tích có thể sử dụng dữ liệu trong kho lưu trữ dữ liệu off-chain để rút ra những thông tin về business.

## Azure Blockchain Workbench

![](media/azure-architecture.png)

Các thành phần chính trong kiến trúc này:
- Identity và Authentication
- Client Applications
- Gateway Service API
- Message Broker
- Consumers

Chi tiết: https://docs.microsoft.com/vi-vn/azure/blockchain/workbench/architecture

## Other Architecturs

![](./media/others-architecture-1.png)

## Ledger Open-Source

- [HyperLedger Fabric](https://github.com/hyperledger/fabric)
  - Bộ SDK Java : https://github.com/hyperledger/fabric-sdk-java
- [Ethereum](https://github.com/ethereum/go-ethereum)
  - Java Client : https://github.com/web3j/web3j
- [Corda](https://github.com/corda/corda)
  - Written Kotlin (Support Java)
## BÁO CÁO TÀI CHÍNH LÀ GÌ?
Báo cáo tài chính (BCTC) cung cấp thông tin liên quan đến hoạt động tài chính của doanh nghiệp, như: tài sản, nợ, vốn chủ sở hữu, doanh thu, lợi nhuận, dòng tiền…

### Bộ báo cáo tài chính hoàn chỉnh bao gồm:

* Báo cáo của Ban giám đốc
* Báo cáo của công ty kiểm toán độc lập
* Bảng cân đối kế toán
* Báo cáo kết quả hoạt động kinh doanh
* Báo cáo lưu chuyển tiền tệ
* Thuyết minh báo cáo tài chính

### Các thành phần quan trọng

1. Bảng cân đối kế toán

    Các mục phải quan tâm trong bảng can đối kế toán

    `Tài sản = Nợ phải trả + Vốn chủ sở hữu`

    `Tài sản`

    Đây là những thứ thuộc sở hữu của doanh nghiệp, có khả năng tạo ra lợi ích kinh tế cho doanh nghiệp.

    Tài sản được phân thành 2 loại: 
    
    + Tài sản ngắn hạn: Tiền và tương đương tiền, Các khoản phải thu, hàng tồn kho

    + Tài sản dài hạn: Tài sản hữu hình (như máy móc thiết bị, nhà xưởng, máy tính…) và Tài sản vô hình (như bằng sáng chế, bản quyền phát minh…)

    `Nợ phải trả`

    Nợ phải trả thể hiện nghĩa vụ tài chính của doanh nghiệp đối với bên ngoài.

    Nợ phải trả cũng được chia làm 2 loại: 
    
    + Nợ ngắn hạn: là những khoản nợ và các nghĩa vụ tài chính phải thanh toán dưới 1 năm

    +  Nợ dài hạn: các khoản phải trả người bán, nộp cho nhà nước như: thuế GTGT, thuế TNDN, phải trả người lao động, khoản tiền vay nợ tín dụng, ...

    `Vốn chủ sở hữu`

    Vốn góp chủ sở hữu: hay vốn cổ phần, là số vốn thực tế được góp vào doanh nghiệp.

    Lợi nhuận chưa phân phối: phần lợi nhuận giữ lại để doanh nghiệp tái đầu tư.

    Quỹ đầu tư phát triển

    `Tỷ trọng các khoản mục trong Tài sản và Nguồn vốn, và sự thay đổi của các khoản mục tại thời điểm báo cáo`

    ![](https://govalue.b-cdn.net/wp-content/uploads/2018/12/Doc-va-phan-tich-bao-cao-tai-chinh-doanh-nghiep-2.jpg)


2. Báo cáo kết quả hoạt động kinh doanh

    Báo cáo KQKD là báo cáo tổng kết doanh thu, chi phí hoạt động của doanh nghiệp trong kỳ báo cáo (quý hoặc năm tài chính).

    Báo cáo KQKD chia hoạt động của doanh nghiệp thành 3 mảng: Hoạt động kinh doanh chính (hoạt động cốt lõi), Hoạt động tài chính và Hoạt động khác.

    `Lợi nhuận = Doanh thu – Chi phí`

    a) Hoạt động kinh doanh bao gồm các khoảng mục 

    * Doanh thu thuần về bán hàng và cung cấp dịch vụ: Đây là doanh thu từ hoạt động kinh doanh “nòng cốt” của doanh nghiệp (sau khi trừ các Khoản giảm trừ doanh thu). Thông thường, đây là hoạt động chiếm tỷ trọng lớn nhất trong cơ cấu doanh thu.

    * Giá vốn hàng bán: Thể hiện tất cả chi phí để làm ra hàng hóa, dịch vụ đã cung cấp.

    * Lợi nhuận gộp = Doanh thu thuần BH, CCDV – Giá vốn hàng bán

    * Chi phí bán hàng, chi phí quản lý doanh nghiệp (QLDN).

    b) Hoạt động tài chính bao gồm các khoảng mục

    * Doanh thu tài chính: có từ các nguồn như: lãi tiền gửi, lãi từ nhận đầu tư, lãi chênh lệch tỷ giá…

    * Chi phí tài chính: gồm có chi phí lãi vay, lỗ chênh lệch tỷ giá, dự phòng các khoản đầu tư tài chính,… phát sinh trong kỳ báo cáo của doanh nghiệp.

    c) Hoạt động khác bao gồm các khoảng mục

    * Thu nhập khác: có nguồn từ lãi thanh lý, nhượng bán tài sản hay được bồi thường hợp đồng…

    * Chi phí khác: Trái ngược với thu nhập khác, chi phí khác sẽ có nguồn từ lỗ thanh lý, nhượng bán tài sản, phải bồi thường vi phạm hợp đồng…

    `Tương tự, chi phí cũng sẽ bao gồm các mục như trên, do đó ở phần này ta cần tập trung quan sát tỷ trọng của từng doanh thu trong Tổng doanh thu, tỷ trọng từng chi phí trong Tổng chi phí, và sự thay đổi của chúng so với cùng kỳ`

    ![](https://govalue.b-cdn.net/wp-content/uploads/2018/12/Doc-va-phan-tich-bao-cao-tai-chinh-doanh-nghiep-4.jpg)


3. Báo cáo lưu chuyển tiền tệ

    Ở Báo cáo KQKD, doanh thu và lợi nhuận sẽ được doanh nghiệp ghi nhập ngay khi bán hàng, kể cả chưa nhận được tiền từ khách hàng. Thực tế, khách hàng sẽ thanh toán cho doanh nghiệp vào 1 thời điểm nào đó, có thể vài tháng, vài năm hoặc không bao giờ.

    Báo cáo lưu chuyển tiền tệ đã được trình bày thành 3 phần tương ứng với 3 dòng tiền: Dòng tiền từ hoạt động kinh doanh, Dòng tiền từ hoạt động đầu tư, và Dòng tiền từ hoạt động tài chính.

    * Dòng tiền từ hoạt động kinh doanh: là dòng tiền phát sinh trong quá trình thanh toán cho nhà cung cấp, khách hàng, cho người lao động, chi trả lãi vay, và nộp các khoản thuế cho nhà nước… Đây là lượng tiền mặt mà bản thân doanh nghiệp làm ra, chứ không phải từ việc huy động thêm vốn đầu tư hay vay nợ.

    * Dòng tiền từ hoạt động đầu tư: bao gồm dòng tiền vào và dòng tiền ra có liên quan đến hoạt động đầu tư, mua sắm, thanh lý… tài sản cố định và các tài sản dài hạn khác.
    
    * Dòng tiền từ hoạt động tài chính sẽ liên quan đến việc tăng/giảm vốn chủ sở hữu (nhận vốn góp mới, thu từ phát hành cổ phiếu, trả cổ tức cho cổ đông…) và vay nợ (chi trả nợ gốc vay, hay vay nợ mới nhận được…)

    ![](https://govalue.b-cdn.net/wp-content/uploads/2018/12/Doc-va-phan-tich-bao-cao-tai-chinh-doanh-nghiep-5.jpg)

    ![](https://govalue.b-cdn.net/wp-content/uploads/2018/12/Doc-va-phan-tich-bao-cao-tai-chinh-doanh-nghiep-6.jpg)


    ## TÀI LIỆU THAM KHẢO

    1. https://govalue.vn/bao-cao-tai-chinh/
 
    2. Resource
    https://www.google.com/search?q=income&tbm=isch&ved=2ahUKEwjz_vbpwK7oAhXWApQKHbnJBx4Q2-cCegQIABAA&oq=income&gs_l=img.12..0l10.2700.2700..4382...0.0..0.112.112.0j1......0....1..gws-wiz-img.H_eoOaHGhBU&ei=5ZJ3XrP4O9aF0AS5k5_wAQ&bih=678&biw=1440#imgrc=sFYVcNeH-XOK0M

    https://www.google.com/search?q=financial+%1F&tbm=isch&ved=2ahUKEwj3xPeHwK7oAhVTxIsBHdtaBXoQ2-cCegQIABAA&oq=financial+%1F&gs_l=img.3..0i67l4j0l6.135348.136699..137040...0.0..0.334.1013.3j1j1j1......0....1..gws-wiz-img.mzPZOxw3yVE&ei=GJJ3XvfYHNOIr7wP27WV0Ac&bih=678&biw=1440#imgrc=E89ATKV5J8jQMM&imgdii=kXwUPiEdVVnwBM

    https://www.google.com/search?q=cash+flow&tbm=isch&ved=2ahUKEwibrbfKwK7oAhWRAJQKHTG8CNoQ2-cCegQIABAA&oq=cash+f&gs_l=img.1.0.0i67l3j0l7.9949.11294..12380...0.0..0.449.1279.2j4j4-1......0....1..gws-wiz-img.aZBQKKgLWnE&ei=o5J3XtvPOJGB0ASx-KLQDQ&bih=678&biw=1440#imgrc=M8T5JH5691NCyM



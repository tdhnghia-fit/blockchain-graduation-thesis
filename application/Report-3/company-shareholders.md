# CÁC LOẠI CỔ ĐÔNG, QUYỀN HẠN VÀ NGHĨA VỤ CỦA TỪNG LOẠI TRONG CÔNG TY CỔ PHẦN

Căn cứ theo luật doanh nghiệp năm 2014, cổ đông trong công ty sẽ bao gồm cổ đông phổ thông cổ đông sáng lập và cổ đông nắm giữ cổ phần ưu đãi. Quyền hạn và nghĩa vụ của mỗi loại cổ đông khác nhau, tùy thuộc vào loại cổ phần mà họ nắm giữ.

`Cổ phần` là phần `vốn nhỏ nhất` của công ty cổ phần. Vốn điều lệ của công ty cổ phần được chia thành nhiều phần bằng nhau được gọi là cổ phần. Người sở hữu cổ phần gọi là `cổ đông`, có thể là cá nhân hoặc tổ chức.

`Căn cứ theo điều 113 Luật doanh nghiệp 2014, các loại cổ phần bao gồm`:

– Cổ phần phổ thông. Công ty cổ phần phải có cổ phần phổ thông. Người sở hữu cổ phần phổ thông là cổ đông phổ thông.

– Ngoài cổ phần phổ thông, công ty cổ phần có thể có cổ phần ưu đãi. Người sở hữu cổ phần ưu đãi gọi là cổ đông ưu đãi. Cổ phần ưu đãi gồm các loại sau đây:

### 1. Cổ phần ưu đãi biểu quyết (Điều 114 Luật doanh nghiệp 2014)

– Nhận cổ tức với mức theo quyết định của Đại hội đồng cổ đông.

– Ưu tiên mua cổ phần mới chào bán tương ứng với tỷ lệ cổ phần phổ thông của từng cổ đông trong công ty.

– Tự do chuyển nhượng cổ phần của mình cho người khác, trừ trường hợp quy định tại khoản 3 Điều 119 và khoản 1 Điều 126 của Luật này.

– Xem xét, tra cứu và trích lục các thông tin trong Danh sách cổ đông `có quyền biểu quyết` và yêu cầu sửa đổi các thông tin không chính xác.

– Xem xét, tra cứu, trích lục hoặc sao chụp Điều lệ công ty, biên bản họp Đại hội đồng cổ đông và các nghị quyết của Đại hội đồng cổ đông.

– Khi công ty giải thể hoặc phá sản, được nhận một phần tài sản còn lại tương ứng với tỷ lệ sở hữu cổ phần tại công ty.

### 2. Cổ phần ưu đãi cổ tức (Điều 116 Luật doanh nghiệp 2014)

Hỏi:

Cổ đông nào không có quyền biểu quyết trong đại hội đồng cổ đông của công ty cổ phần?

Trả lời:

Căn cứ theo luật doanh nghiệp năm 2014, cổ đông trong công ty sẽ bao gồm cổ đông phổ thông cổ đông sáng lập và cổ đông nắm giữ cổ phần ưu đãi. Quyền hạn và nghĩa vụ của mỗi loại cổ đông khác nhau, tùy thuộc vào loại cổ phần mà họ nắm giữ.

Cổ phần là phần vốn nhỏ nhất của công ty cổ phần. Vốn điều lệ của công ty cổ phần được chia thành nhiều phần bằng nhau được gọi là cổ phần. Người sở hữu cổ phần gọi là cổ đông, có thể là cá nhân hoặc tổ chức.

Căn cứ theo điều 113 Luật doanh nghiệp 2014, các loại cổ phần bao gồm:

– Cổ phần phổ thông. Công ty cổ phần phải có cổ phần phổ thông. Người sở hữu cổ phần phổ thông là cổ đông phổ thông.

– Ngoài cổ phần phổ thông, công ty cổ phần có thể có cổ phần ưu đãi. Người sở hữu cổ phần ưu đãi gọi là cổ đông ưu đãi. Cổ phần ưu đãi gồm các loại sau đây:

a) Cổ phần ưu đãi biểu quyết

b) Cổ phần ưu đãi cổ tức

c) Cổ phần ưu đãi hoàn lại

d) Cổ phần ưu đãi khác do Điều lệ công ty quy định

* Quyền của các loại cổ đông theo quy định của Luật doanh nghiệp 2014

a. Cổ đông phổ thông : Điều 114 Luật doanh nghiệp 2014

– Tham dự và phát biểu trong các Đại hội đồng cổ đông và thực hiện quyền biểu quyết trực tiếp hoặc thông qua đại diện theo ủy quyền hoặc theo hình thức khác do pháp luật, Điều lệ công ty quy định. Mỗi cổ phần phổ thông có một phiếu biểu quyết.

– Nhận cổ tức với mức theo quyết định của Đại hội đồng cổ đông.

– Ưu tiên mua cổ phần mới chào bán tương ứng với tỷ lệ cổ phần phổ thông của từng cổ đông trong công ty.

– Tự do chuyển nhượng cổ phần của mình cho người khác, trừ trường hợp quy định tại khoản 3 Điều 119 và khoản 1 Điều 126 của Luật này.

– Xem xét, tra cứu và trích lục các thông tin trong Danh sách cổ đông có quyền biểu quyết và yêu cầu sửa đổi các thông tin không chính xác.

– Xem xét, tra cứu, trích lục hoặc sao chụp Điều lệ công ty, biên bản họp Đại hội đồng cổ đông và các nghị quyết của Đại hội đồng cổ đông.

– Khi công ty giải thể hoặc phá sản, được nhận một phần tài sản còn lại tương ứng với tỷ lệ sở hữu cổ phần tại công ty.

b. Cổ phần ưu đãi biểu quyết (điều 116)

Cổ phần ưu đãi biểu quyết là cổ phần có số phiếu biểu quyết nhiều hơn so với cổ phần phổ thông, số phiếu biểu quyết của một cổ phần ưu đãi biểu quyết do Điều lệ công ty quy định.

Cổ đông sở hữu cổ phần ưu đãi biểu quyết có các quyền sau đây:

– `Biểu quyết` về các vấn đề thuộc thẩm quyền của Đại hội đồng cổ đông với số phiếu biểu quyết theo quy định tại khoản 1 Điều này.

– Các quyền khác như cổ đông phổ thông, trừ trường hợp quy định tại khoản 3 Điều này.

Cổ đông sở hữu cổ phần ưu đãi biểu quyết không được chuyển nhượng cổ phần đó cho người khác.

### 3. Cổ phần ưu đãi hoàn lại (Điều 117 Luật doanh nghiệp 2014)

ổ phần ưu đãi cổ tức là cổ phần được trả cổ tức với mức cao hơn so với mức cổ tức của cổ phần phổ thông hoặc mức ổn định hằng năm. Cổ tức được chia hằng năm gồm cổ tức cố định và cổ tức thưởng, cổ tức cố định không phụ thuộc vào kết quả kinh doanh của công ty. Mức cổ tức cố định cụ thể và phương thức xác định cổ tức thưởng được ghi trên cổ phiếu của cổ phần ưu đãi cổ tức.

Cổ đông sở hữu cổ phần ưu đãi cổ tức có các quyền sau đây:

– Nhận cổ tức theo quy định tại khoản 1 Điều này.

– Nhận phần tài sản còn lại tương ứng với tỷ lệ sở hữu cổ phần tại công ty, sau khi công ty đã thanh toán hết các khoản nợ, cổ phần ưu đãi hoàn lại khi công ty giải thể hoặc phá sản.

– Các quyền khác như cổ đông phổ thông, trừ trường hợp quy định tại khoản 3 Điều này.

Cổ đông sở hữu cổ phần ưu đãi cổ tức `không có quyền biểu quyết`, dự họp Đại hội đồng cổ đông, đề cử người vào Hội đồng quản trị và Ban kiểm soát.

### 4. Cổ phần ưu đãi khác do Điều lệ công ty quy định (Điều 118 Luật doanh nghiệp 2014)

Cổ phần ưu đãi hoàn lại là cổ phần được công ty hoàn lại vốn góp theo yêu cầu của người sở hữu hoặc theo các điều kiện được ghi tại cổ phiếu của cổ phần ưu đãi hoàn lại.

– Cổ đông sở hữu cổ phần ưu đãi hoàn lại có các quyền khác như cổ đông phổ thông, trừ trường hợp quy định tại khoản 3 Điều này.

– Cổ đông sở hữu cổ phần ưu đãi hoàn lại `không có quyền biểu quyết`, dự họp Đại hội đồng cổ đông, đề cử người vào Hội đồng quản trị và Ban kiểm soát.


### ==> TÓM LẠI:

Cổ đông không có quyền biểu quyết trong đại hội đồng cổ đông là : `cổ đông sở hữu cổ phần ưu đãi cổ tức và cổ đông sở hữu cổ phần ưu đãi hoàn lại`.


## TÀI LIỆU THAM KHẢO:

1. https://luatvietnam.vn/doanh-nghiep/co-dong-la-gi-cac-loai-co-dong-trong-cong-ty-co-phan-561-19308-article.html?fbclid=IwAR361tnYcPBQifvzJCXIS8fXYQxCtYunupIsXXamZKOeqj7sqHO1LY5CJnw
2. https://luatduonggia.vn/co-dong-nam-giu-bao-nhieu-co-phan-thi-duoc-bieu-quyet/
3. https://luatvietnam.vn/doanh-nghiep/co-dong-so-huu-bao-nhieu-co-phan-thi-duoc-bieu-quyet-561-20089-article.html
Fabric Model
===

 
## Privacy

Hyperledger Fabric sử dụng một sổ cái bất biến trên nền tảng mỗi kênh, cũng như chaincode có thể điều khiển và sửa đổi trạng thái hiện tại của tài sản (ví dụ: cập nhật các cặp giá trị khóa). `Một sổ cái tồn tại trong phạm vi của một kênh - nó có thể được chia sẻ trên toàn bộ mạng (giả sử mọi người tham gia đang hoạt động trên một kênh chung) - hoặc có thể được tư nhân hóa để chỉ bao gồm một nhóm người tham gia cụ thể.`

Trong kịch bản sau, những người tham gia này sẽ tạo một kênh riêng và từ đó cô lập / tách biệt các giao dịch và sổ cái của họ. Để giải quyết các kịch bản muốn thu hẹp khoảng cách giữa tổng độ sự minh bạch và sự riêng tư, mã chuỗi chỉ có thể được cài đặt trên các máy ngang hàng cần truy cập vào trạng thái nội dung để thực hiện đọc và ghi (nói cách khác, nếu mã chuỗi không được cài đặt trên máy ngang hàng , nó sẽ không thể tương tác đúng với sổ cái).

Khi một tập hợp con của các tổ chức trên kênh đó cần giữ bí mật dữ liệu giao dịch của họ, một bộ sưu tập dữ liệu riêng tư (bộ sưu tập) được sử dụng để phân tách dữ liệu này trong một cơ sở dữ liệu riêng, tách biệt khỏi sổ cái kênh, chỉ có thể truy cập bởi các tập hợp con các tổ chức được ủy quyền.

Do đó, các kênh giữ giao dịch riêng tư từ mạng rộng hơn trong khi các bộ sưu tập giữ dữ liệu riêng tư giữa các tập hợp con của các tổ chức trên kênh.

Để làm xáo trộn dữ liệu hơn nữa, các giá trị trong mã chuỗi có thể được mã hóa (một phần hoặc toàn bộ) bằng các thuật toán mã hóa phổ biến như AES trước khi gửi giao dịch đến dịch vụ đặt hàng (ordering service) và nối các khối vào sổ cái. Khi dữ liệu được mã hóa đã được ghi vào sổ cái, nó chỉ có thể được giải mã bởi người dùng sở hữu khóa tương ứng được sử dụng để tạo văn bản mã hóa.

## Consensus

Trong công nghệ sổ cái phân tán, sự đồng thuận gần đây đã trở nên đồng nghĩa với một thuật toán cụ thể, trong một chức năng duy nhất. Tuy nhiên, sự đồng thuận bao gồm nhiều hơn là chỉ đồng ý với thứ tự giao dịch và sự khác biệt này được nêu bật trong Hyperledger Fabric thông qua vai trò cơ bản của nó trong toàn bộ dòng giao dịch, từ đề xuất và chứng thực, đến đặt hàng, xác nhận và cam kết. Tóm lại, sự đồng thuận được định nghĩa là xác minh toàn vòng tròn về tính đúng đắn của một tập hợp các giao dịch bao gồm một khối.

Sự đồng thuận đạt được cuối cùng khi thứ tự và kết quả của một giao dịch khối khối đã đáp ứng các kiểm tra tiêu chí chính sách rõ ràng. Các kiểm tra và số dư này diễn ra trong vòng đời của một giao dịch, và bao gồm việc sử dụng các chính sách chứng thực để ra lệnh cho các thành viên cụ thể phải xác nhận một loại giao dịch nhất định, cũng như các chuỗi hệ thống để đảm bảo rằng các chính sách này được thi hành và duy trì.
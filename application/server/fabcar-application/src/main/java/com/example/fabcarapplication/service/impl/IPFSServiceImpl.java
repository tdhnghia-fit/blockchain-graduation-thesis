package com.example.fabcarapplication.service.impl;

import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import io.ipfs.multiaddr.MultiAddress;
import io.ipfs.multihash.Multihash;
import java.io.IOException;
import javax.annotation.PostConstruct;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

@Service
@Configuration
@ConfigurationProperties("ipfs")
@Data
public class IPFSServiceImpl {

  private IPFS ipfs;

  private String host;

  private int port;

  @PostConstruct
  public void IPFSServiceImpl() {
    ipfs = new IPFS(host, port);
  }

  public String addContent(String proof) {
    try {
      NamedStreamable.ByteArrayWrapper bytearray = new NamedStreamable.ByteArrayWrapper(
          proof.getBytes());
      MerkleNode response = ipfs.add(bytearray).get(0);
      return response.hash.toBase58();
    } catch (IOException ex) {
      throw new RuntimeException("Error whilst communicating with the IPFS node", ex);
    }
  }

  public String getContent(String hash) {
    try {
      Multihash multihash = Multihash.fromBase58(hash);
      byte[] content = ipfs.cat(multihash);
      return new String(content);
    } catch (IOException ex) {
      throw new RuntimeException("Error whilst communicating with the IPFS node", ex);
    }
  }

}

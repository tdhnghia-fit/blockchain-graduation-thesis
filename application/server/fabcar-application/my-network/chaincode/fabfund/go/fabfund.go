package main

import (
	"fmt"
	"github.com/hyperledger/fabric-chaincode-go/pkg/cid"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
	//"github.com/hyperledger/fabric-chaincode-go/shim"
	//
	//sc "github.com/hyperledger/fabric-protos-go/peer"
)

// Contract
var activitySmartContract = new(ActivitySmartContract)

var statementSmartContract = new(StatementSmartContract)
var cashFlowStatementSmartContract = new(CashFlowStatementSmartContract)
var financialStatementSmartContract = new(FinancialStatementSmartContract)
var incomesStatementSmartContract = new(IncomesStatementSmartContract)
var typeSmartContract = new(TypeSmartContract)

//
type FabFundSmartContract struct{}

func (s *FabFundSmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

func (s *FabFundSmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger appropriately

	// Get ID
	//id, _ := cid.GetID(APIstub)

	x509, _ := cid.GetX509Certificate(APIstub)
	id := x509.Subject.CommonName
	//
	mspid, _ := cid.GetMSPID(APIstub)

	if function == "initLedger" {
		return s.initLedger(APIstub)
	} else if function == "queryStatement" {
		return statementSmartContract.queryStatement(APIstub, args)
	} else if function == "queryAllStatements" {
		return statementSmartContract.queryAllStatements(APIstub)
	} else if function == "queryAllStatementsByTime" {
		return statementSmartContract.queryAllStatementsByTime(APIstub, args)

	} else if function == "queryAllTypeLv2" {
		return typeSmartContract.queryAllType2(APIstub)
	} else if function == "queryAllTypeLv3ByLv3" {
		return typeSmartContract.queryType3ByType2(APIstub, args)
	} else if function == "queryCashFlowStatement" {
		return cashFlowStatementSmartContract.queryCashFlowStatement(APIstub, args)
	} else if function == "queryAllCashFlowStatements" {
		return cashFlowStatementSmartContract.queryAllStatements(APIstub)
	} else if function == "queryAllCashFlowStatementsByTime" {
		return cashFlowStatementSmartContract.queryAllStatementsByTime(APIstub, args)
	} else if function == "queryFinStatement" {
		return financialStatementSmartContract.queryFinancialStatement(APIstub, args)
	} else if function == "queryFinStatementOverview" {
		return financialStatementSmartContract.queryFinancialStatementForOverview(APIstub, args)
	} else if function == "queryAllFinStatements" {
		return financialStatementSmartContract.queryAllStatements(APIstub)
	} else if function == "queryAllFinStatementsByTime" {
		return financialStatementSmartContract.queryAllStatementsByTime(APIstub, args)
	} else if function == "queryAllIncomesStatements" {
		return incomesStatementSmartContract.queryAllStatements(APIstub)
	} else if function == "queryAllIncomesStatementsByTime" {
		return incomesStatementSmartContract.queryAllStatementsByTime(APIstub, args)
	} else if function == "queryIncomesStatement" {
		return incomesStatementSmartContract.queryIncomesStatement(APIstub, args)
	} else if function == "queryIncomesOverview" {
		return incomesStatementSmartContract.queryIncomesStatementForOverview(APIstub, args)

	} else if function == "queryActivity" {
		if id == "accounting" || mspid == "shareholder" {
			return activitySmartContract.queryActivity(APIstub, args)
		}
	} else if function == "queryAllActivities" {
		if id == "accounting" || mspid == "shareholder" {
			return activitySmartContract.queryAllActivities(APIstub)
		}
	} else if function == "queryAllActivitiesByDate" {
		if id == "accounting" || mspid == "shareholder" {
			return activitySmartContract.queryAllActivitiesByDate(APIstub, args)
		}
	} else if function == "queryAllActivitiesByType" {
		if id == "accounting" || mspid == "shareholder" {
			return activitySmartContract.queryAllActivitiesByType(APIstub, args)
		}
	} else if function == "queryAllActivitiesByActorIdByApprovedId" {
		if id == "accounting" || mspid == "shareholder" {
			return activitySmartContract.queryAllActivitiesByActorIdByApprovedId(APIstub, args)
		}
	} else if function == "queryAllActivitiesByActorApprovedId" {
		if id == "accounting" || mspid == "shareholder" {
			return activitySmartContract.queryAllActivitiesByActorApprovedId(APIstub, args)
		}
	} else if function == "queryHistory" {
		return activitySmartContract.queryHistoryKey(APIstub, args)

	} else if function == "updateCashFlowStatement" {
		if id == "accounting" {
			return cashFlowStatementSmartContract.updateCashFlowStatement(APIstub, args)
		} else {
			return shim.Error("Id is not accounting")
		}
	} else if function == "deleteCashFlowStatement" {
		if id == "accounting" {
			return cashFlowStatementSmartContract.deleteCashFlowStatement(APIstub, args)
		}
	} else if function == "createStatement" {
		if id == "accounting" {
			return statementSmartContract.createStatement(APIstub, args)
		} else {
			return shim.Error("Invalid ID. " + id)
		}
	} else if function == "updateStatement" {
		if id == "accounting" {
			return statementSmartContract.updateStatement(APIstub, args)
		}
	} else if function == "deleteStatement" {
		if id == "accounting" {
			return statementSmartContract.deleteStatement(APIstub, args)
		}
	} else if function == "createTypeLv2" {
		if id == "accounting" {
			return typeSmartContract.createType2(APIstub, args)
		}
	} else if function == "createTypeLv3" {
		if id == "accounting" {
			return typeSmartContract.createType3(APIstub, args)
		}
	} else if function == "createCashFlowStatement" {
		if id == "accounting" {
			return cashFlowStatementSmartContract.createCashFlowStatement(APIstub, args)
		}
	} else if function == "createFinStatement" {
		if id == "accounting" {
			return financialStatementSmartContract.createFinancialStatement(APIstub, args)
		}
	} else if function == "updateFinStatement" {
		if id == "accounting" {
			return financialStatementSmartContract.updateFinancialStatement(APIstub, args)
		}
	} else if function == "deleteFinStatement" {
		if id == "accounting" {
			return financialStatementSmartContract.deleteFinancialStatement(APIstub, args)
		}
	} else if function == "createIncomesStatement" {
		if id == "accounting" {
			return incomesStatementSmartContract.createIncomesStatement(APIstub, args)
		}
	} else if function == "updateIncomesStatement" {
		if id == "accounting" {
			return incomesStatementSmartContract.updateIncomesStatement(APIstub, args)
		}
	} else if function == "deleteIncomesStatement" {
		if id == "accounting" {
			return incomesStatementSmartContract.deleteIncomesStatement(APIstub, args)
		}

		//	Activity

	} else if function == "createActivity" {
		if id == "accounting" {
			return activitySmartContract.createActivity(APIstub, args)
		}

	} else if function == "updateActivity" {
		if id == "accounting" {
			return activitySmartContract.updateActivity(APIstub, args)
		}
	} else if function == "deleteActivity" {
		if id == "accounting" {
			return activitySmartContract.deleteActivity(APIstub, args)
		}
	}  else if function == "verifyFinancialStatement" {
		if id == "auditor" {
			return financialStatementSmartContract.verifyFinancialStatement(APIstub, args)
		} else {
			return shim.Error("Invalid auditor")
		}
	}

	return shim.Error("Invalid Smart Contract function name.")
}

//func (s *FabFundSmartContract) checkAccountingId (APIstub shim.ChaincodeStubInterface) sc.Response {
//
//	// Get ID
//	id, err := cid.GetID(APIstub)
//
//	if id == "accounting@org1.example.com" {
//		return shim.Error("Invalid Smart Contract function name.")
//	} else {
//		return shim.Error("ID is not accounting")
//	}
//}
//
//func (s *FabFundSmartContract) checkShareHolderId (APIstub shim.ChaincodeStubInterface) sc.Response {
//
//	// Get ID
//	id, err := cid.GetID(APIstub)
//
//	if id == "shareholder" {
//		return shim.
//	} else {
//		return shim.Error("ID is not accounting")
//	}
//}

func (s *FabFundSmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
	activitySmartContract.initLedger(APIstub)
	cashFlowStatementSmartContract.initLedger(APIstub)
	financialStatementSmartContract.initLedger(APIstub)
	incomesStatementSmartContract.initLedger(APIstub)
	statementSmartContract.initLedger(APIstub)
	typeSmartContract.initLedger(APIstub)
	return shim.Success(nil)
}

// The main function is only relevant in unit test mode. Only included here for completeness.
func main() {

	// Create a new Smart Contract
	err := shim.Start(new(FabFundSmartContract))
	if err != nil {
		fmt.Printf("Error creating new FabFundSmartContract: %s", err)
	}
}

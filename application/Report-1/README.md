Report - 1 (17/2 - 2/3)
===

Tạ Đăng Hiếu Nghĩa

1. [Tìm hiểu Hyperledger Fabric](../notes-fabric.md)
2. [Cấu trúc một mạng lưới Hyperledger Fabric](./network-fabric.md)
3. [Demo một ứng dụng mẫu (Fabcar)](./fabric-samples-report.md)

Trương Hổ Phong

- Tìm hiểu cách thức triển khai ứng dụng phía client

## Report - 2 (3/3 - 10/3)

Tạ Đăng Hiếu Nghĩa

1. Bài viết về Smart Contract của Hyperledger
2. Bài viết về cách lưu trữ dữ liệu của Hyperledger
3. 
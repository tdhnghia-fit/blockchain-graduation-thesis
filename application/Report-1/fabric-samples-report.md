Báo cáo demo và tìm hiểu ứng dụng Fabcar
===

Hyperledger Fabric đi kèm với rất nhiều ví dụ ứng dụng và trong số đó Fabcar là một ứng dụng hoàn chỉnh. Một ứng dụng blockchain hoàn chỉnh chứa hai phần mã: Chaincode (Smart Contract) được triển khai và thực thi bên trong Fabric Network và client là nơi thế giới bên ngoài tương tác với Chaincode được triển khai trên Fabric Network.

## A. Tổng quan về ứng dụng Fabcar

- Fabcar thực chất là một cơ sở dữ liệu các thông tin xe ô tô được lưu trữ trong [sổ cái của một fabric network][1].
Chúng ta có thể coi đây là cơ sở dữ liệu truyền thống lưu trữ dữ liệu: nó giống như một table, được lập index với Car Identifier (CarID) và thông tin nhà sản xuất (Maker), mẫu mã (Model), màu sắc (Color) và Chủ sở hữu (Owner) được ghi lại cho chiếc xe này.    
- Dữ liệu được lưu trong world state database trong sổ cái. Tương tác với dữ liệu thông qua chaincode. Fabcar đi kèm với Chaincode chứa các functions có thể tương tác với dữ liệu được lưu trữ trong sổ cái. Thông qua nó chúng ta có thể khởi tạo, truy vấn và cập nhật database  (ledger). The world state được truy vấn hay cập nhật chỉ thông qua `the chaincode functions` và bất cứ cập nhật nào cũng được ghi lại (log) blockchain bên trong sổ cái dưới dạng bản ghi chống giả mạo.

[1]:https://hyperledger-fabric.readthedocs.io/en/release-1.4/ledger/ledger.html

## B. Fabric Network

- Fabric Chaincode có thể hoạt động trên bất cứ fabric network nào. Ở đây, Fabcar hoạt động trên một fabric network được gọi là First Network.
- Chi tiết về First Network : [Link](https://medium.com/@kctheservant/understanding-first-network-example-in-hyperledger-fabric-part-1-c03391af798) 
- Tổng quan cơ sở hạ tầng của First Network bao gồm:
    - 2 tổ chức, Org1 và Org2, mỗi tổ chức đi kèm với 2 peer nodes(peer0 và peer1). `Vì vậy First Network sẽ có tổng cộng 4 ndoes`
    - 1 tổ chức orderer và một node orderer sử dụng SOLO như một phương thức ordering
    - Tùy chọn, mỗi peer node chạy một couchdb như một `world state database`
    - Tùy chọn, mỗi Org1 và Org2 có thể đi kèm với một Certificate Authority - Cơ quan cấp chứng chỉ, sử dụng phần mềm fabric-CA với cấu hình phù hợp
    - Một command line interface (CLI) như một client tương tác với fabric network

- Toàn bộ thành phần được deployed như các `containers` ([Docker phần mềm chạy ảo hóa](https://vi.wikipedia.org/wiki/Docker_(ph%E1%BA%A7n_m%E1%BB%81m)))

![](https://miro.medium.com/max/1827/0*lBATbOmYOl9Kh0H9)

## C. Chaincode

- Chaincode khởi tạo và quản lý trạng thái sổ cái thông qua các giao dịch được gửi bởi các ứng dụng. 
- Chaincode thường xử lý logic nghiệp vụ được các thành viên của mạng đồng ý, do đó, nó được coi là một smart contract.

### Chaincode trong Fabcar

- Cấu trúc dữ liệu

![](./media/car-structure.png)

- Contract interface

Contract bao gồm các method quan trong createTransaction (tạo một giao dịch), evaluateTransaction (dùng để tạo một giao dịch để truy vấn), submitTransaction (dùng để tạo giao dịch insert, update)

![](./media/contract-interface.png)

- Với các 2 phương thức evaluate và submit sẽ được truyền vào các tham số để thực hiện một số hành động cụ thể như sau:

![](./media/function.png)

- [Source code Car](./fabcar/Car.java)
- [Source code FabCar](./fabcar/FabCar.java)

## D. Demo

- Trong ứng dụng FabCar có cung cấp sẵn một shell script để install mạng lưới và cài đặt chaincode là startFabric.sh

- Script startFabric.sh thực hiện các bước sau đây:
    - Launch network : tạo channel và cho cái peer node tham gia vào channel đó
    - Cài đặt các smart contract (fabcar chaincode) lên các node trong channel
    - Khởi tạo Fabcar chaincode trên kênh mychannel
    - Gọi hàm InitLedger

- Sau khi network và sổ cái được thiết lập ta có thể tương tác với dữ liệu trên sổ cái thông các transaction invoke trong chaincode (smart contract)
- Có thể gọi trực tiếp qua CLI nhưng Hyperledger Ledger có cung cấp bộ SDK Java nên em sẽ demo gọi các hàm qua ứng dụng Java:

  - Đăng ký Admin

![](./media/enrolladmin.png)

  - Dùng tài khoản Admin đăng ký user

![](./media/registeruser.png)

  - Dùng tài khoản User thực hiện các method queryAllCars, createCar, queryCar, changeCarOwner 

![](./media/clientapp.png)

## THAM KHẢO
- https://hyperledger-fabric.readthedocs.io/en/release-1.4/write_first_app.html
- https://hyperledger-fabric.readthedocs.io/en/release-1.4/build_network.html
- https://medium.com/@kctheservant/deep-dive-into-fabcar-revised-57cb54642572
- https://hyperledger-fabric.readthedocs.io/en/release-1.4/whatis.html



import React, { Component } from 'react';
import { Formik } from "formik";
import * as EmailValidator from "email-validator";
import * as Yup from "yup";
import axios from 'axios'
import API from './../API/API.js'
import { Redirect } from 'react-router-dom'
import CONSTANT from './../Constant'
import AppUtils from '../Utils/AppUtils.js';
import swal from '@sweetalert/with-react'

class ValidatedLoginForm extends Component {

    constructor(props) {
        super(props);
        const token = localStorage.getItem(CONSTANT.USER_LOGIN_TOKEN)
        let loggedIn = true
        if (token == null) {
            loggedIn = false
        }

        this.state = {
            userId: "",
            role: "",
            accessToken: "",
            tokenType: "",
            loggedIn
        }

        this.onChange = this.onChange.bind(this)
    }

    login() {
        let email = document.getElementById("txtEmail").value
        let password = document.getElementById("txtPwd").value
        console.log("url", API.LOGIN)
        axios({
            method: 'post',
            url: API.LOGIN,
            data: {
                usernameOrEmail: email,
                password: password
            }
        }).then(res => {
            console.log(res)
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else if (res.Status == 401) {
                swal("Bạn nhập sai mật khẩu!", "" , "warning");
            }
            else {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
            console.log(res)
                    localStorage.setItem(CONSTANT.USER_LOGIN_TOKEN, res.data.id)
                    localStorage.setItem(CONSTANT.USER_LOGIN_ROLE, res.data.role)
                    localStorage.setItem(CONSTANT.ACCESS_TOKEN, res.data.accessToken)
                    localStorage.setItem(CONSTANT.TOKEN_TYPE, res.data.tokenType)
                    let authorization = AppUtils.HEADER_AUTHORIZATION()
                    console.log("login", authorization)
                    this.setState({
                        userId: res.data.id,
                        role: res.data.role,
                        accessToken: res.data.accessToken,
                        tokenType: res.data.tokenType,
                        loggedIn: true
                    })
                }
            }
        }).catch((res) => {
            this.setState({
                loggedIn: false
            })
            console.log('request failed', res.response)
            let status = res.response.status
            if (status === 401) {
                swal(res.response.data.message, "" , "warning");
            }
        });
    }

    componentWillMount() {
        let loginRole = localStorage.getItem(CONSTANT.USER_LOGIN_ROLE)
        let isLogin = loginRole != undefined
        this.setState({
            loggedIn: isLogin,
            role: loginRole
        })
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        let str = "/trangchu"
        if (this.state.loggedIn) {
            let role = this.state.role
            if (role == CONSTANT.ROLE_ADMIN) str = "/quanly/taikhoan"
            console.log("role", role)
            return <Redirect to={str} />
        }
        return (
            <Formik
                initialValues={{ email: "", password: "" }}
                onSubmit={(values, { setSubmitting }) => {
                    this.login()
                }}

                //********Handling validation messages yourself*******/
                validate={values => {
                    let errors = {};
                    if (!values.email) {
                        errors.email = "Bắt buộc";
                    } else if (!EmailValidator.validate(values.email)) {
                        errors.email = "Email không hợp lệ";
                    }

                    const passwordRegex = /(?=.*[0-9])/;
                    if (!values.password) {
                        errors.password = "Bắt buộc";
                    } else if (values.password.length < 6) {
                        errors.password = "Mật khẩu phải có độ dài 6 ký tự.";
                    }

                    return errors;
                }}
            >

                {props => {
                    const {
                        values,
                        touched,
                        errors,
                        isSubmitting,
                        handleChange,
                        handleBlur,
                        handleSubmit
                    } = props;

                    return (
                        <div className="login-content" style={{ width: '80%' }}>
                            <h3>Đăng nhập</h3>
                            <div className="login-form">
                                <form onSubmit={handleSubmit} id="myForm" style={{ width: "100%" }}>
                                    <div className="form-group">
                                        <label htmlFor="email">Email</label>
                                        <input
                                            name="email"
                                            type="text"
                                            id="txtEmail"
                                            value={values.email}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            className={errors.email && touched.email && "error"}
                                        />
                                        {errors.email && touched.email && (
                                            <div className="input-feedback">{errors.email}</div>
                                        )}
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="email">Mật khẩu</label>
                                        <input
                                            name="password"
                                            type="password"
                                            id="txtPwd"
                                            value={values.password}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            className={errors.password && touched.password && "error"}
                                        />
                                        {errors.password && touched.password && (
                                            <div className="input-feedback">{errors.password}</div>
                                        )}
                                    </div>
                                    <div className="text-right m-b-20">
                                        Quên mật khẩu?
                                                    </div>
                                    <div className="text-center">
                                        <button className="au-btn au-btn--block au-btn--green m-b-20" type="submit">Đăng nhập</button>
                                        {/* <button type="submit" disabled={isSubmitting}>Đăng nhập</button> */}
                                    </div>
                                </form>
                            </div>
                        </div>
                    );
                }}
            </Formik>
        );
    }
}

export default ValidatedLoginForm;

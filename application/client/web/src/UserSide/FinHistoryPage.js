import React, { Component } from 'react';
import FinancialHistory from '../admin/FinancialHistory';
import { Route } from "react-router-dom";

class FinHistoryPage extends Component {
    render() {
        return (
            <section id="portfolio" className="section-bg">
                <div className="container">
                    <header className="section-header">
                        <h3 className="section-title">Báo cáo tài chính</h3>
                    </header>
                    <Route exact path="/baocao/taichinh/lichsu/:id" component={FinancialHistory} />
                </div>
            </section>
        );
    }
}

export default FinHistoryPage;
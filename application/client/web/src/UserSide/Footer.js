import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer id="footer">
                <div className="footer-top">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-3 col-md-6 footer-info text-center">
                                <h3>FabFund</h3>
                                <p>Sản phẩm Khóa luận tốt nghiệp của Tạ Đăng Hiếu Nghĩa, Trương Hổ Phong do Ths. Lê Viết Long hướng dẫn</p>
                                <img className="m-t-10" style={{ width: '180px', height: '150px' }} src='/img/logo-khtn.png' />
                            </div>
                            <div className="col-lg-3 col-md-6 footer-links" >
                                <h4>Chuyển hướng</h4>
                                <nav id="nav-menu-container" >
                                    <ul className="nav-menu" >
                                        <li style={{ width: '100%' }}><a href="#intro">Trang chủ</a></li>
                                        <li style={{ width: '100%' }}><a href="#about">Chúng tôi</a></li>
                                        <li style={{ width: '100%' }}><a href="#services">Dịch vụ</a></li>
                                        <li style={{ width: '100%' }}><a href="#portfolio">Hoạt động</a></li>
                                        <li style={{ width: '100%' }}><a href="#testimonials">Ban quản trị</a></li>
                                        <li style={{ width: '100%' }}><a href="#contact">Liên hệ</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div className="col-lg-3 col-md-6 footer-contact">
                                <h4>Liên Hệ</h4>
                                <p>
                                    227, Nguyễn Văn Cừ <br />
                                    P4, Q5, Tp. Hồ Chí Minh<br />
                                    Việt Nam <br />
                                    <strong>Email:</strong> hophongit98@gmail.com | tdhb.612@gmail.com<br />
                                </p>
                            </div>
                            <div className="col-lg-3 col-md-6 footer-newsletter">
                                <h4>Đội ngũ phát triển</h4>
                                <p>
                                    Tạ Đăng Hiếu Nghĩa<br />
                                    Trương Hổ Phong <br />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="copyright">
                        © Copyright <strong>2020</strong>. Trường Đại học Khoa học tự nhiên
          </div>
                    <div className="credits">
                        {/*
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=BizPage
        */}
            Teamplate Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;
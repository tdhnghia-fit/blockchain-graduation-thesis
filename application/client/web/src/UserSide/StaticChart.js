import React, { Component } from 'react';
import {
    ChartComponent, SeriesCollectionDirective, SeriesDirective, Inject,
    Legend, Category, Tooltip, ILoadedEventArgs, ChartTheme, ColumnSeries, DataLabel
} from '@syncfusion/ej2-react-charts';
import { groupStyles, groupBadgeStyles, chartYearOption, quarterOptions1, chartToYearOption } from '../Utils/DataGeneral'
import Select from 'react-select';
import CONSTANT from '../Constant';
import { Browser } from '@syncfusion/ej2-base';
import ActivityChart from '../component/ActivityChart';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import API from './../API/API.js'
import AppUtils from '../Utils/AppUtils';
import axios from 'axios'
import NumberFormat from 'react-number-format';
import swal from '@sweetalert/with-react'

export let data5 = [{ x: '2015', y: 34 }, { x: '2016', y: 46 }, { x: '2017', y: 46 }, { x: '2018', y: 27 }, { x: '2019', y: 26 }];
export let data6 = [{ x: '2015', y: 24 }, { x: '2016', y: 26 }, { x: '2017', y: 37 }, { x: '2018', y: 23 }, { x: '2019', y: 18 }];
export let data7 = [{ x: '2015', y: 44 }, { x: '2016', y: 36 }, { x: '2017', y: 38 }, { x: '2018', y: 17 }, { x: '2019', y: 26 }];

const formatGroupLabel = data => (
    <div style={groupStyles}>
        <span>{data.label}</span>
        <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
);

class StaticChart extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fromYear: CONSTANT.TIMESTAMP_2017,
            toYear: CONSTANT.TIMESTAMP_2020 - 1,
            totalAsset: [],
            totalDebt: [],
            balance: [],
            isLoading: false,
            isLoading1: false,
            fromYear1: CONSTANT.TIMESTAMP_2017,
            toYear1: CONSTANT.TIMESTAMP_2020 - 1,
            totalInput: [],
            totalOutput: [],
            balance1: [],
            quarter: 1,
            isS1Empty: false,
            isS2Empty: false
        }
    }

    onChartLoad1() {
        let chart = document.getElementById('charts1');
        chart.setAttribute('title', '');
    };

    onChartLoad2() {
        let chart = document.getElementById('charts2');
        chart.setAttribute('title', '');
    };

    onFromYearSelect(value) {
        let year = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.timeStamp
        this.setState({
            fromYear: year
        }, this.getData)
    }

    onFromYearSelect1(value) {
        let year = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.timeStamp
        this.setState({
            fromYear1: year
        })
    }

    onToYearSelect(value) {
        let quarter = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        this.setState({
            quarter: quarter
        }, this.getData)
    }

    onToYearSelect1(value) {
        let year = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.timeStamp
        year = year
        this.setState({
            toYear1: year
        })
    }

    getData() {
        this.setState({
            core: [],
            financial: [],
            other: [],
            total: [],
            isLoading: false
        }, function () {
            this.getFinancialChart()
        })
    }

    getFinancialChart() {
        let { fromYear, quarter } = this.state
        let param = ""
        if (fromYear !== CONSTANT.NO_FILTER)
            param = "start=" + fromYear

        let q = ""
        if (quarter !== CONSTANT.NO_FILTER && quarter != undefined)
            q = "&quarter=" + quarter

        let url = API.GET_FINANCIAL_CHART + "?" + param + q
        console.log("getFinancialChart", url)

        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.parseFinancialDataChart(res.data)
                    } else {
                        this.setState({
                            isS2Empty: true,
                            isLoading: true
                        })
                    }
                }
            })
            .catch(res => {
                let status = res.response.status
                if (status === 401) {
                    swal(res.response.data.message, "", "warning");
                }
                if (res.response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    parseFinancialDataChart(data) {
        let totalAsset = []
        let totalDebt = []
        let balance = []

        let dataSort = AppUtils.sortFinancialData(data)
        dataSort.forEach(item => {
            var t = new Date(1970, 0, 1); // Epoch
            t.setMilliseconds(item.createAt);
            let label = t.getFullYear()
            let debt = {
                x: label,
                y: item.totalDebt,
            }
            let asset = {
                x: label,
                y: item.totalAsset,
            }
            let bal = {
                year: label,
                total: item.totalAsset - item.totalDebt,
            }
            totalAsset.push(asset)
            totalDebt.push(debt)
            balance.push(bal)
        });

        this.setState({
            totalAsset: totalAsset,
            totalDebt: totalDebt,
            balance: balance,
            isLoading: true,
            isS2Empty: false
        })
    }

    getCashflowData() {
        this.setState({
            totalInput: [],
            totalOutput: [],
            balance1: [],
            isLoading1: false
        }, this.getCashflowChartByYear())
    }

    getCashflowChartByYear() {
        let url = API.CASH_FLOW_CHART_BY_YEAR + "?start=" + this.state.fromYear1 + "&end=" + this.state.toYear1 + "&quarter=5"
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("getCashflowChartByYear", url)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.parseCashflowDataChartByYear(res.data)
                    } else {
                        this.setState({
                            isS1Empty: true,
                            isLoading1: true
                        })
                    }
                }
            })
            .catch(res => {
                let status = res.response.status
                if (status === 401) {
                    swal(res.response.data.message, "", "warning");
                }
                if (res.response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    parseCashflowDataChartByYear(data) {
        let totalInput = []
        let totalOutput = []
        let balance1 = []

        let dataSort = AppUtils.sortFinancialData(data)
        dataSort.forEach(item => {
            var t = new Date(1970, 0, 1); // Epoch
            t.setMilliseconds(item.createAt);
            let label = t.getFullYear()
            let sumInput = item.inputActivityBusiness + item.inputActivityInvestment + item.inputActivityFinancial
            let sumOutput = item.outputActivityBusiness + item.outputActivityInvestment + item.outputActivityFinancial

            let input = {
                x: label,
                y: sumInput,
            }
            let output = {
                x: label,
                y: sumOutput,
            }
            let bal = {
                year: label,
                total: sumInput - sumOutput,
            }
            totalInput.push(input)
            totalOutput.push(output)
            balance1.push(bal)
        });

        this.setState({
            totalInput: totalInput,
            totalOutput: totalOutput,
            balance1: balance1,
            isLoading1: true,
            isS1Empty: false
        })
    }

    componentWillMount() {
        this.getFinancialChart()
        this.getCashflowChartByYear()
    }

    render() {
        const { totalAsset, totalDebt, balance, isLoading, totalInput, totalOutput, isLoading1, balance1, toYear, fromYear, toYear1, fromYear1, isS1Empty, isS2Empty } = this.state
        let show = toYear != CONSTANT.NO_FILTER && fromYear != CONSTANT.NO_FILTER
        let show1 = toYear1 != CONSTANT.NO_FILTER && fromYear1 != CONSTANT.NO_FILTER
        console.log("isS1Empty", isS1Empty)
        console.log("isLoading1", isLoading1)

        return (
            <section className="statistic-chart col-lg-12 m-t-0">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="au-card m-b-30">
                                <div className="au-card-inner">
                                    <div className="m-l-20 m-b-20">
                                        <h3 className="title-2">Biểu đồ tổng tài sản và tổng nợ</h3>
                                    </div>
                                    <div className="row m-b-20 m-t-10">
                                        <div className="col-lg-3">
                                            <Select
                                                onChange={this.onFromYearSelect.bind(this)}
                                                options={chartYearOption}
                                                formatGroupLabel={formatGroupLabel}
                                                placeholder="2017"
                                                maxMenuHeight="200px"
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <Select
                                                onChange={this.onToYearSelect.bind(this)}
                                                options={quarterOptions1}
                                                formatGroupLabel={formatGroupLabel}
                                                placeholder="Q1"
                                                maxMenuHeight="200px"
                                            />
                                        </div>
                                        {/* {
                                            show && <div className="col-lg-3 text-left">
                                                <button className="au-btn au-btn-icon au-btn--blue au-btn--small" onClick={this.getData.bind(this)}>
                                                    <FontAwesomeIcon className="custom-icon" icon={faSearch} /></button>
                                            </div>
                                        } */}
                                    </div>
                                    {
                                        !isS2Empty && isLoading && <div className="row">
                                            <ChartComponent id='charts2' style={{ textAlign: "center" }}
                                                primaryXAxis={{ valueType: 'Category', interval: 1, majorGridLines: { width: 0 } }}
                                                primaryYAxis={{
                                                    lineStyle: { width: 0 },
                                                    minimum: 0,
                                                    majorTickLines: { width: 0 },
                                                    majorGridLines: { width: 1 },
                                                    minorGridLines: { width: 1 },
                                                    minorTickLines: { width: 0 },
                                                    labelFormat: '{value}',
                                                }}
                                                chartArea={{ border: { width: 0 } }}
                                                tooltip={{ enable: true }}
                                                width={Browser.isDevice ? '100%' : '60%'}
                                                loaded={this.onChartLoad2.bind(this)}>
                                                <Inject services={[ColumnSeries, Legend, Tooltip, Category, DataLabel]} />
                                                <SeriesCollectionDirective>
                                                    <SeriesDirective dataSource={totalAsset} xName='x' yName='y' name='Tổng tài sản' type='Column'>
                                                    </SeriesDirective>
                                                    <SeriesDirective dataSource={totalDebt} xName='x' yName='y' name='Tổng nợ' type='Column'>
                                                    </SeriesDirective>
                                                </SeriesCollectionDirective>
                                            </ChartComponent>
                                            <div className="col-lg-4">
                                                {/* TOP CAMPAIGN*/}
                                                <div className="top-campaign">
                                                    <h3 className="title-3 m-b-30">Độ chênh lệch giữa tổng tài sản và tổng nợ</h3>
                                                    <div className="table-responsive">
                                                        <table className="table table-top-campaign mystyle">
                                                            <tbody>
                                                                {
                                                                    balance.map((item, key) => {
                                                                        return (
                                                                            <tr>
                                                                                <td>{item.year}</td>
                                                                                <td>
                                                                                    <NumberFormat value={item.total} displayType={'input'} thousandSeparator={true} prefix={''} />
                                                                                </td>
                                                                                <td>{CONSTANT.CURRENCY_UNIT}</td>
                                                                            </tr>
                                                                        );
                                                                    })
                                                                }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                {/* END TOP CAMPAIGN*/}
                                            </div>
                                        </div>
                                    }
                                    {
                                        isLoading && isS2Empty && <img style={{ width: "100%" }} src="/images/nodata.png" className="img-fluid" alt="" />
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="au-card m-b-30">
                                <div className="au-card-inner">
                                    <div className="m-l-20 m-b-20">
                                        <h3 className="title-2">Biểu đồ tổng thu và tổng chi</h3>
                                    </div>
                                    <div className="row m-b-20 m-t-10">
                                        <div className="col-lg-3">
                                            <Select
                                                onChange={this.onFromYearSelect1.bind(this)}
                                                options={chartYearOption}
                                                formatGroupLabel={formatGroupLabel}
                                                placeholder="2017"
                                                maxMenuHeight="200px"
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <Select
                                                onChange={this.onToYearSelect1.bind(this)}
                                                options={chartToYearOption}
                                                formatGroupLabel={formatGroupLabel}
                                                placeholder="2019"
                                                maxMenuHeight="200px"
                                            />
                                        </div>
                                        {
                                            show1 && <div className="col-lg-3 text-left">
                                                <button className="au-btn au-btn-icon au-btn--blue au-btn--small" onClick={this.getCashflowData.bind(this)}>
                                                    <FontAwesomeIcon className="custom-icon" icon={faSearch} /></button>
                                            </div>
                                        }
                                    </div>
                                    {
                                        isLoading1 && <div className="row">
                                            <ChartComponent id='charts1' style={{ textAlign: "center" }}
                                                primaryXAxis={{ valueType: 'Category', interval: 1, majorGridLines: { width: 0 } }}
                                                primaryYAxis={{
                                                    lineStyle: { width: 0 },
                                                    minimum: 0,
                                                    majorTickLines: { width: 0 },
                                                    majorGridLines: { width: 1 },
                                                    minorGridLines: { width: 1 },
                                                    minorTickLines: { width: 0 },
                                                    labelFormat: '{value}',
                                                }}
                                                chartArea={{ border: { width: 0 } }}
                                                tooltip={{ enable: true }}
                                                width={Browser.isDevice ? '100%' : '60%'}
                                                loaded={this.onChartLoad1.bind(this)}>
                                                <Inject services={[ColumnSeries, Legend, Tooltip, Category, DataLabel]} />
                                                <SeriesCollectionDirective>
                                                    <SeriesDirective dataSource={totalInput} xName='x' yName='y' name='Tổng chi' type='Column'>
                                                    </SeriesDirective>
                                                    <SeriesDirective dataSource={totalOutput} xName='x' yName='y' name='Tổng thu' type='Column'>
                                                    </SeriesDirective>
                                                </SeriesCollectionDirective>
                                            </ChartComponent>
                                            <div className="col-lg-4">
                                                {/* TOP CAMPAIGN*/}
                                                <div className="top-campaign">
                                                    <h3 className="title-3 m-b-30">Độ chênh lệch giữa tổng thu và tổng chi</h3>
                                                    <div className="table-responsive">
                                                        <table className="table table-top-campaign mystyle">
                                                            <tbody>
                                                                {
                                                                    balance1.map((item, key) => {
                                                                        return (
                                                                            <tr>
                                                                                <td>{item.year}</td>
                                                                                <td>
                                                                                    <NumberFormat value={item.total} displayType={'input'} thousandSeparator={true} prefix={''} />
                                                                                </td>
                                                                                <td>{CONSTANT.CURRENCY_UNIT}</td>
                                                                            </tr>
                                                                        );
                                                                    })
                                                                }
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                {/* END TOP CAMPAIGN*/}
                                            </div>
                                        </div>
                                    }
                                    {
                                        isLoading1 && isS1Empty && <img style={{ width: "100%" }} src="/images/nodata.png" className="img-fluid" alt="" />
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <ActivityChart />
                </div>
            </section >
        );
    }
}

export default StaticChart;
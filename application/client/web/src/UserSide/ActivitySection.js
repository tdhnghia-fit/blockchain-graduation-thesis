import React, { Component } from 'react';

class ActivitySection extends Component {
    render() {
        return (
            <section id="portfolio" className="section-bg">
                <div className="container">
                    <header className="section-header">
                        <h3 className="section-title">Các hoạt động ở công ty</h3>
                    </header>
                    <div className="row">
                        <div className="col-lg-12">
                            <ul id="portfolio-flters">
                                <li data-filter="*" className="filter-active">Tất cả</li>
                                <li data-filter=".filter-app">Thể thao</li>
                                <li data-filter=".filter-card">Từ tiện</li>
                                <li data-filter=".filter-web">Câu lạc bộ</li>
                            </ul>
                        </div>
                    </div>
                    <div className="row portfolio-container">
                        <div className="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp">
                            <div className="portfolio-wrap">
                                <figure>
                                    <img src="images/thethao1.jpg" className="img-fluid" alt="" />
                                    <a href="images/thethao1.jpg" data-lightbox="portfolio" data-title="The thao 1" className="link-preview" title="Preview"><i className="ion ion-eye" /></a>
                                    <a className="link-details" title="More Details"><i className="ion ion-android-open" /></a>
                                </figure>
                                <div className="portfolio-info">
                                    <h4><a >Thể thao</a></h4>
                                    <p>Team Building<i class="fas fa-building    "></i></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 portfolio-item filter-card wow fadeInUp" data-wow-delay="0.1s">
                            <div className="portfolio-wrap">
                                <figure>
                                    <img src="images/tuthien1.jpg" className="img-fluid" alt="" />
                                    <a href="images/tuthien1.jpg" className="link-preview" data-lightbox="portfolio" data-title="Web 3" title="Preview"><i className="ion ion-eye" /></a>
                                    <a className="link-details" title="More Details"><i className="ion ion-android-open" /></a>
                                </figure>
                                <div className="portfolio-info">
                                    <h4><a >Từ thiện</a></h4>
                                    <p>Trao tặng máy tính</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
                            <div className="portfolio-wrap">
                                <figure>
                                    <img src="images/clb1.jpg" className="img-fluid" alt="" />
                                    <a href="images/clb1.jpg" className="link-preview" data-lightbox="portfolio" data-title="App 2" title="Preview"><i className="ion ion-eye" /></a>
                                    <a className="link-details" title="More Details"><i className="ion ion-android-open" /></a>
                                </figure>
                                <div className="portfolio-info">
                                    <h4><a >Câu lạc bộ</a></h4>
                                    <p>Đạp xe đạp</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp">
                            <div className="portfolio-wrap">
                                <figure>
                                    <img src="images/thethao2.jpg" className="img-fluid" alt="" />
                                    <a href="images/thethao2.jpg" className="link-preview" data-lightbox="portfolio" data-title="Card 2" title="Preview"><i className="ion ion-eye" /></a>
                                    <a className="link-details" title="More Details"><i className="ion ion-android-open" /></a>
                                </figure>
                                <div className="portfolio-info">
                                    <h4><a >Thể thao</a></h4>
                                    <p>Leo núi</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 portfolio-item filter-card wow fadeInUp" data-wow-delay="0.1s">
                            <div className="portfolio-wrap">
                                <figure>
                                    <img src="images/tuthien2.jpg" className="img-fluid" alt="" />
                                    <a href="images/tuthien2.jpg" className="link-preview" data-lightbox="portfolio" data-title="Web 2" title="Preview"><i className="ion ion-eye" /></a>
                                    <a className="link-details" title="More Details"><i className="ion ion-android-open" /></a>
                                </figure>
                                <div className="portfolio-info">
                                    <h4><a >Từ thiện</a></h4>
                                    <p>Giúp đỡ đồng bào miền núi</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
                            <div className="portfolio-wrap">
                                <figure>
                                    <img src="images/clb2.jpg" className="img-fluid" alt="" />
                                    <a href="images/clb2.jpg" className="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i className="ion ion-eye" /></a>
                                    <a  className="link-details" title="More Details"><i className="ion ion-android-open" /></a>
                                </figure>
                                <div className="portfolio-info">
                                    <h4><a >Câu lạc bộ</a></h4>
                                    <p>Bóng đá</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default ActivitySection;
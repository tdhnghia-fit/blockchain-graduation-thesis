import React, { Component } from 'react';
import CONSTANT from '../Constant';
import { NavLink } from "react-router-dom";
import AppUtils from '../Utils/AppUtils';

class NavHeader extends Component {

  constructor(props) {
    super(props);
    const token = localStorage.getItem(CONSTANT.USER_LOGIN_TOKEN)
    let loggedIn = true
    if (token == null) {
      loggedIn = false
    }

    this.state = {
      loggedIn
    }
  }

  logout() {
    AppUtils.logout()
  }

  render() {
    let isLogin = localStorage.getItem(CONSTANT.USER_LOGIN_TOKEN) != null
    let userRole = localStorage.getItem(CONSTANT.USER_LOGIN_ROLE)
    let isAdmin = userRole == CONSTANT.ROLE_ADMIN
    let isAccountant = userRole == CONSTANT.ROLE_ACCOUNTANT
    let isAuditor = userRole == CONSTANT.ROLE_AUDITOR
    let isUser = (userRole == CONSTANT.ROLE_USER) || (userRole == undefined)
    console.log("isAccountant", isAccountant)
    return (
      <header id="header">
        <div className="container-fluid">
          <div id="logo" className="pull-left">
            <h1><a href="#intro" className="scrollto">FabFund</a></h1>
          </div>
          <nav id="nav-menu-container">
            <ul className="nav-menu">
              <li className="menu-active"><a href="#intro">Trang chủ</a></li>
              <li><a href="#about">Chúng tôi</a></li>
              <li><a href="#services">Dịch vụ</a></li>
              <li><a href="#portfolio">Hoạt động</a></li>
              <li><a href="#testimonials">Ban quản trị</a></li>
              <li><a href="#contact">Liên hệ</a></li>
              {
                isLogin && !isUser && <li class="menu-has-children"><a>Khác</a>
                  <ul>
                    <li><a href="/baocaotaichinh">Báo cáo tài chính</a></li>
                    {
                      !isUser && <li><a href="/thongke">Thống kê</a></li>
                    }
                    {
                      !isUser && <li><a href="/hoatdongthuchi">Hoạt động thu chi</a></li>
                    }
                    {
                      !isUser && <li><a href="/codong">Cổ đông</a></li>
                    }
                    {
                      (isAdmin || isAccountant || isAuditor) && <li><a href="/quanly/thongke">Trang Quản lý</a></li>
                    }
                  </ul>
                </li>
              }
              {
                isLogin && isUser && <li><a href="/baocaotaichinh">Báo cáo</a></li>
              }

              {
                !isLogin && <li>
                  <a href="/dangnhap">Đăng nhập</a>
                </li>
              }
              {
                isLogin && <a onClick={() => this.logout()}>
                  <NavLink to="/dangnhap">Đăng xuất</NavLink>
                </a>
              }
            </ul>
          </nav>{/* #nav-menu-container */}
        </div>
      </header>

    );
  }
}

export default NavHeader;
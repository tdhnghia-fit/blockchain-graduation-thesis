import React, { Component } from 'react';

class OtherStatement extends Component {
    render() {
        return (
            <section id="portfolio" className="section-bg m-t-100 text-center">
                <div className="container">
                    <header className="section-header">
                        <h3 className="section-title">Báo cáo, tài liệu khác</h3>
                    </header>
                    <div className="row portfolio-container">
                        <div className="col-lg-4 col-md-6 portfolio-item ">
                            <div className="portfolio-wrap">
                                <figure>
                                    <img src="img/portfolio/app1.jpg" className="img-fluid" alt="" />
                                    <a href="img/portfolio/app1.jpg" data-lightbox="portfolio" data-title="App 1" className="link-preview" title="Preview"><i className="ion ion-eye" /></a>
                                    <a href="#" className="link-details" title="More Details"><i className="ion ion-android-open" /></a>
                                </figure>
                                <div className="portfolio-info">
                                    <h4><a href="#">App 1</a></h4>
                                    <p>App</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 portfolio-item ">
                            <div className="portfolio-wrap">
                                <figure>
                                    <img src="img/portfolio/web3.jpg" className="img-fluid" alt="" />
                                    <a href="img/portfolio/web3.jpg" className="link-preview" data-lightbox="portfolio" data-title="Web 3" title="Preview"><i className="ion ion-eye" /></a>
                                    <a href="#" className="link-details" title="More Details"><i className="ion ion-android-open" /></a>
                                </figure>
                                <div className="portfolio-info">
                                    <h4><a href="#">Web 3</a></h4>
                                    <p>Web</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 portfolio-item ">
                            <div className="portfolio-wrap">
                                <figure>
                                    <img src="img/portfolio/app2.jpg" className="img-fluid" alt="" />
                                    <a href="img/portfolio/app2.jpg" className="link-preview" data-lightbox="portfolio" data-title="App 2" title="Preview"><i className="ion ion-eye" /></a>
                                    <a href="#" className="link-details" title="More Details"><i className="ion ion-android-open" /></a>
                                </figure>
                                <div className="portfolio-info">
                                    <h4><a href="#">App 2</a></h4>
                                    <p>App</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 portfolio-item ">
                            <div className="portfolio-wrap">
                                <figure>
                                    <img src="img/portfolio/card2.jpg" className="img-fluid" alt="" />
                                    <a href="img/portfolio/card2.jpg" className="link-preview" data-lightbox="portfolio" data-title="Card 2" title="Preview"><i className="ion ion-eye" /></a>
                                    <a href="#" className="link-details" title="More Details"><i className="ion ion-android-open" /></a>
                                </figure>
                                <div className="portfolio-info">
                                    <h4><a href="#">Card 2</a></h4>
                                    <p>Card</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 portfolio-item " >
                            <div className="portfolio-wrap">
                                <figure>
                                    <img src="img/portfolio/web2.jpg" className="img-fluid" alt="" />
                                    <a href="img/portfolio/web2.jpg" className="link-preview" data-lightbox="portfolio" data-title="Web 2" title="Preview"><i className="ion ion-eye" /></a>
                                    <a href="#" className="link-details" title="More Details"><i className="ion ion-android-open" /></a>
                                </figure>
                                <div className="portfolio-info">
                                    <h4><a href="#">Web 2</a></h4>
                                    <p>Web</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 portfolio-item ">
                            <div className="portfolio-wrap">
                                <figure>
                                    <img src="img/portfolio/app3.jpg" className="img-fluid" alt="" />
                                    <a href="img/portfolio/app3.jpg" className="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i className="ion ion-eye" /></a>
                                    <a href="#" className="link-details" title="More Details"><i className="ion ion-android-open" /></a>
                                </figure>
                                <div className="portfolio-info">
                                    <h4><a href="#">App 3</a></h4>
                                    <p>App</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button className="au-btn au-btn-icon au-btn--green au-btn--small">Xem thêm</button>
            </section>
        );
    }
}

export default OtherStatement;
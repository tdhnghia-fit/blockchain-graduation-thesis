import React, { Component } from 'react';
import axios from 'axios'
import API from './../API/API.js'
import CONSTANT from './../Constant'
import AppUtils from '../Utils/AppUtils.js';
import swal from '@sweetalert/with-react'
import { NavLink } from "react-router-dom";
import Pagination from "react-js-pagination";

class Statement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            activePage: 1,
            totalData: 0,
            limit: 5,
            chosenItem: {},
            showFormAdd: false,
            showFormEdit: false,
            yearArray: [],
            year: CONSTANT.NO_FILTER,
            month: CONSTANT.NO_FILTER,
            quarter: CONSTANT.NO_FILTER,
            isLoading: false
        }

        this.onDeleteItem = this.onDeleteItem.bind(this)
        this.onDownloadItem = this.onDownloadItem.bind(this)
        this.onAddItem = this.onAddItem.bind(this)
    }

    handleClose() {
        this.setState({
            showFormAdd: false
        })
    }

    handleShow() {
        this.setState({
            showFormAdd: true
        })
    }

    handleCloseEdit() {
        this.setState({
            showFormEdit: false
        })
    }

    handleShowEdit(value) {
        this.setState({
            chosenItem: value,
            showFormEdit: true
        })
    }

    onDownloadItem(item) {
        let url = API.GET_STATEMENT_BY_ID + item.id
        console.log(url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            console.log("sucess", res.data)
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data.length !== 0) {
                    let proof = res.data.proof
                    let quarter = item.quarter
                    let linkView = "/baocaochitiet/" + item.year
                    if (proof != undefined && proof != "") {
                        if (quarter !== 0 && quarter !== "" && quarter !== undefined)
                            linkView += "." + quarter
                    }
                    window.open(linkView, "_blank")
                } else {
                    localStorage.removeItem(CONSTANT.BASE64_REPORT)
                    console.log("lỗi")
                }
            }
        }).catch(response => {
            console.log("error", response)

            if (response.error === 'Unauthorized')
                AppUtils.logout()
        })
            .finally(() => { });
    }

    onAddItem(value) {
        console.log("newItem", value)
        const tempArr = [...this.state.data]
        tempArr.splice(tempArr.length - 1, 1);
        tempArr.unshift(value)
        this.setState({
            data: tempArr
        })
        this.handleClose()
    }

    onEditCallback(value) {
        localStorage.setItem(CONSTANT.BASE64_REPORT, value.proof)
        this.setState({
            showFormEdit: false
        }, this.queryDataByField)
    }

    delete(value) {
        console.log(value)
        let itemId = value.id
        console.log(API.DEL_STATEMENT_BY_ID + itemId)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("authorization", authorization)
        axios({
            method: 'DELETE',
            url: API.DEL_STATEMENT_BY_ID + itemId,
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                this.queryDataByField()
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
            }
        })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            });
    }

    onDeleteItem(value) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.delete(value)
                }
            });
    }

    handlePageChange(pageNumber) {
        this.setState({
            activePage: pageNumber
        }, this.queryDataByField);
    }

    componentWillMount() {
        this.queryDataByField()
        // this.getYear()
    };

    getYear() {
        let url = API.GET_YEAR
        console.log("getYear", url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("authorization", authorization)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    let yearOps = AppUtils.convertYearArrayToOptions(res.data)
                    console.log(yearOps)
                    this.setState({
                        yearArray: yearOps
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    onYearSelect(value) {
        let year = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        this.setState({
            year: year
        }, this.queryDataByField)
    }

    onQuarterMonthSelect(value) {
        console.log(value)
        if (value != null) {
            this.setState({
                quarter: value.value
            }, this.queryDataByField)
        } else {
            this.setState({
                quarter: CONSTANT.NO_FILTER,
                month: CONSTANT.NO_FILTER
            }, this.queryDataByField)
        }
    }

    queryDataByField() {
        let param = ""
        const { year, month, quarter } = this.state

        if (year != CONSTANT.NO_FILTER)
            param += "?year=" + year
        if (quarter != CONSTANT.NO_FILTER && year != CONSTANT.NO_FILTER) {
            param += "&quarter=" + quarter
        } else if (quarter != CONSTANT.NO_FILTER) param += "?quarter=" + quarter
        let connector = "&"
        if (year == CONSTANT.NO_FILTER && quarter == CONSTANT.NO_FILTER && month == CONSTANT.NO_FILTER)
            connector = "?"
        let url = API.GET_ALL_STATEMENT_BY_PAGE + param + connector + `limit=${this.state.limit}&page=${this.state.activePage}`
        console.log("url", url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.setState({
                            data: res.data.records,
                            totalData: res.data.totalPages * this.state.limit,
                            isLoading: true
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    onLoadMore() {
        let newLimit = this.state.activePage + 1
        this.setState({
            data: [],
            activePage: newLimit
        }, function () {
            this.queryDataByField()
        })
    }

    render() {
        var { data, isLoading } = this.state;
        return (
            isLoading && <div id="portfolio" className="m-t-100 text-center">
                <div className="container">
                    <header className="section-header">
                        <h3 className="section-title">Báo cáo tài chính</h3>
                    </header>
                    <div className="row portfolio-container">
                        {
                            data.map((item, key) => {
                                return (
                                    <div className="col-lg-4 col-md-6 portfolio-item ">
                                        <div className="portfolio-wrap">
                                            <figure>
                                                <img src="img/financial_statement.jpg" className="img-fluid" alt="" />
                                                <NavLink to={"/baocaotaichinh/chitiet/" + item.id} >
                                                    <a data-title="App 1" className="link-preview" title="Preview" ><i className="ion ion-eye" /></a>
                                                </NavLink>
                                                <a className="link-details" title="More Details" onClick={() => this.onDownloadItem(item)}><i className="ion ion-android-open" /></a>
                                            </figure>
                                            <div className="portfolio-info">
                                                <h4><a href="#">{item.title}</a></h4>
                                                <p>{item.description}</p>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })
                        }
                    </div>
                </div>
                <div lg="12" style={{ display: 'flex', justifyContent: 'center', marginTop: "1%" }}>
                    <Pagination
                        activePage={this.state.activePage}
                        itemsCountPerPage={this.state.limit}
                        totalItemsCount={this.state.totalData}
                        pageRangeDisplayed={5}
                        onChange={this.handlePageChange.bind(this)}
                        itemClass="page-item"
                        linkClass="page-link"
                    />
                </div>
            </div>
        );
    }
}

export default Statement;
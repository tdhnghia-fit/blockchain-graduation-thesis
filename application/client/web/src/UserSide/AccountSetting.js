import React, { Component } from 'react';

class AccountSetting extends Component {
  render() {
    return (
      <div className="sub-header-mobile-2 d-block d-lg-none">
        <div className="header__tool">
          <div className="header-button-item js-item-menu">
            <i className="zmdi zmdi-settings" />
            <div className="setting-dropdown js-dropdown">
              <div className="account-dropdown__body">
                <div className="account-dropdown__item">
                  <a href="#">
                    <i className="zmdi zmdi-account" />Account</a>
                </div>
                <div className="account-dropdown__item">
                  <a href="#">
                    <i className="zmdi zmdi-settings" />Setting</a>
                </div>
                <div className="account-dropdown__item">
                  <a href="#">
                    <i className="zmdi zmdi-money-box" />Billing</a>
                </div>
              </div>
              <div className="account-dropdown__body">
                <div className="account-dropdown__item">
                  <a href="#">
                    <i className="zmdi zmdi-globe" />Language</a>
                </div>
                <div className="account-dropdown__item">
                  <a href="#">
                    <i className="zmdi zmdi-pin" />Location</a>
                </div>
                <div className="account-dropdown__item">
                  <a href="#">
                    <i className="zmdi zmdi-email" />Email</a>
                </div>
                <div className="account-dropdown__item">
                  <a href="#">
                    <i className="zmdi zmdi-notifications" />Notifications</a>
                </div>
              </div>
            </div>
          </div>
          <div className="account-wrap">
            <div className="account-item account-item--style2 clearfix js-item-menu">
              <div className="image">
                <img src="images/icon/avatar-01.jpg" alt="John Doe" />
              </div>
              <div className="content">
                <a className="js-acc-btn" href="#">john doe</a>
              </div>
              <div className="account-dropdown js-dropdown">
                <div className="info clearfix">
                  <div className="image">
                    <a href="#">
                      <img src="images/icon/avatar-01.jpg" alt="John Doe" />
                    </a>
                  </div>
                  <div className="content">
                    <h5 className="name">
                      <a href="#">john doe</a>
                    </h5>
                    <span className="email">johndoe@example.com</span>
                  </div>
                </div>
                <div className="account-dropdown__body">
                  <div className="account-dropdown__item">
                    <a href="#">
                      <i className="zmdi zmdi-account" />Account</a>
                  </div>
                  <div className="account-dropdown__item">
                    <a href="#">
                      <i className="zmdi zmdi-settings" />Setting</a>
                  </div>
                  <div className="account-dropdown__item">
                    <a href="#">
                      <i className="zmdi zmdi-money-box" />Billing</a>
                  </div>
                </div>
                <div className="account-dropdown__footer">
                  <a href="#">
                    <i className="zmdi zmdi-power" />Logout</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AccountSetting;
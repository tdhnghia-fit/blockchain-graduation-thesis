import React, { Component } from 'react';
import IncomeHistory from '../admin/IncomeHistory';
import { Route } from "react-router-dom";

class IncomeHistoryPage extends Component {
    render() {
        return (
            <section id="portfolio" className="section-bg">
                <div className="container">
                    <header className="section-header">
                        <h3 className="section-title">Báo cáo thu nhập</h3>
                    </header>
                    <Route exact path="/baocao/thunhap/lichsu/:id" component={IncomeHistory} />
                </div>
            </section>
        );
    }
}

export default IncomeHistoryPage;
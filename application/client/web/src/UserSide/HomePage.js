import React, { Component } from 'react';
import StaticInfo from './StaticInfo';
import StaticChart from './StaticChart';
import DataTable from './DataTable';
import ActivityLineChart from '../component/ActivityLineChart';

class HomePage extends Component {
    render() {
        return (
            <div className="container">
                <header className="section-header">
                    <h3>Tổng quan</h3>
                </header>
                <StaticInfo />
                <DataTable />
                <ActivityLineChart />
                <StaticChart />
            </div>
        );
    }
}

export default HomePage;
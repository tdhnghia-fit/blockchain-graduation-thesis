import React, { Component } from 'react';
import detailIcon from '@iconify/icons-zmdi/more';
import { Icon } from '@iconify/react';
import NumberFormat from 'react-number-format';
import axios from 'axios'
import API from '../API/API.js'
import CONSTANT from '../Constant'
import AppUtils from '../Utils/AppUtils.js';
import { typeParent } from './../Utils/DataGeneral'
import { NavLink } from "react-router-dom";

class DataTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            typeChildLv2: [],
            isLoading: false,
            isLoadingType2: false,
        }
    }

    formatAuthor(list) {
        if (list.length < 2) return list
        return list.join()
    }

    componentWillMount() {
        this.queryData()
        this.getTypeChildLv2()
    };

    getTypeChildLv2() {
        let url = API.GET_TYPE_LV2
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    this.setState({
                        typeChildLv2: AppUtils.convertTypeToSelectModel(res.data, true),
                        isLoadingType2: true
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }
    queryData() {
        let url = API.GET_ACTIVITY_BY_PAGE + "?limit=10&page=1"
        console.log("queryDataByField", url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    this.setState({
                        data: res.data.records,
                        isLoading: true
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    render() {
        var { isLoading, isLoadingType2, data, typeChildLv2 } = this.state;
        return (
            isLoading && isLoadingType2 && <section className="statistic-chart col-lg-12">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="au-card m-b-30">
                                <div className="au-card-inner">
                                    <h2 className="title-1 m-b-25">Các hoạt động gần đây</h2>
                                    <div className="table-responsive table-responsive-data2">
                                        <table className="table table-data2 ">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Ngày</th>
                                                    <th className="text-center medium">Thực hiện</th>
                                                    <th className="text-center">Số tiền ({CONSTANT.CURRENCY_UNIT})</th>
                                                    <th className="text-center">Loại</th>
                                                    <th className="text-center biglarge">Chi tiết</th>
                                                    {/* <th className="text-center">Mô tả loại hoạt động</th> */}
                                                    {/* <th className="text-center medium">Mục đích</th> */}
                                                    <th className="text-center">Thao tác</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    data.map((item, key) => {
                                                        let state = "role user"
                                                        let type = item.type.split(":")
                                                        let parent = typeParent.find((item) => {
                                                            let label = CONSTANT.TYPE_INPUT
                                                            if (item.label == CONSTANT.TYPE_CHI) label = CONSTANT.TYPE_OUTPUT
                                                            return label == type[0]
                                                        }).label

                                                        if (parent == CONSTANT.TYPE_CHI) state = "role admin"

                                                        let type2 = typeChildLv2[0].options.find((item) => {
                                                            if (item.value != 0 && item.value + "" == type[1]) {
                                                                return item
                                                            }
                                                        })

                                                        let child = type2.label
                                                        let child2 = type2.description

                                                        return (
                                                            <tr className="tr-shadow">
                                                                <td className="text-center">{item.id}</td>
                                                                <td>{AppUtils.toDateTime(item.createAt)}</td>
                                                                <td className="text-center">{item.actorId}</td>
                                                                <td className="text-center">
                                                                    <NumberFormat value={item.amount} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                </td>
                                                                <td><span className={state}>{parent}</span></td>
                                                                <td className="biglarge"><span >{child}</span></td>
                                                                {/* <td><span >{child2}</span></td> */}
                                                                {/* <td className="text-center">{item.purpose}</td> */}
                                                                <td className="text-center">
                                                                    <NavLink to={"chitiethoatdong/" + item.id} >
                                                                        <button className="item" data-toggle="tooltip" data-placement="top" title="Chi tiết">
                                                                            <Icon icon={detailIcon} />
                                                                        </button>
                                                                    </NavLink>
                                                                </td>
                                                            </tr>
                                                        )
                                                    }
                                                    )
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default DataTable;
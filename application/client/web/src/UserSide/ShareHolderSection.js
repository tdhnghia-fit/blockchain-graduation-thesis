import React, { Component } from 'react';
import ShareHolderSlider from '../ShareHolderSide/ShareHolderSlider';
import CONSTANT from '../Constant';

class ShareHolderSection extends Component {
    render() {
        return (
            <section id="team">
                <div className="container">
                    <header className="section-header">
                        <h3>Cổ đông sáng lập</h3>
                    </header>
                    <ShareHolderSlider id={CONSTANT.ROLE_SHAREHOLDER_FOUNDER_ID} />
                </div>
            </section >
        );
    }
}

export default ShareHolderSection;
import React, { Component } from 'react';
import API from '../API/API';
import AppUtils from '../Utils/AppUtils';
import axios from 'axios'
import NumberFormat from 'react-number-format';
import CONSTANT from '../Constant';
import ReactTooltip from "react-tooltip";

class StaticInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: {}
        }
    }

    componentWillMount() {
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(API.GET_OVERVIEW, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.setState({
                            data: res.data
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    render() {
        const { data } = this.state
        let initialCapital = Math.round(data.initialCapital / 1000000000)
        let totalAsset = Math.round(data.totalAsset / 1000000000)
        let totalRevenue = Math.round(data.totalRevenue / 1000000000)
        return (
            <section className="statistic statistic2">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6 col-lg-3">
                            <div className="statistic__item statistic__item--green">
                                <h2 className="number">{data.numberOfShareholder}</h2>
                                <span className="desc">Số cổ đông</span>
                                <div className="icon">
                                    <i className="zmdi zmdi-account-o" />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-3">
                            <div className="statistic__item statistic__item--orange" data-tip data-for='initialCapital'>
                                <h2 className="number">
                                    <NumberFormat value={initialCapital} displayType={'text'} thousandSeparator={true} suffix={" tỷ " + CONSTANT.CURRENCY_UNIT} />
                                </h2>
                                <span className="desc">Vốn chủ sở hữu</span>
                                <div className="icon">
                                    <i className="zmdi zmdi-shopping-cart" />
                                </div>
                                <ReactTooltip id="initialCapital">
                                    <span>
                                        <NumberFormat value={data.initialCapital} displayType={'text'} thousandSeparator={true}/>

                                    </span>
                                </ReactTooltip>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-3">
                            <div className="statistic__item statistic__item--blue" data-tip data-for='totalAsset'>
                                <h2 className="number">
                                    <NumberFormat value={totalAsset} displayType={'text'} thousandSeparator={true} suffix={" tỷ " + CONSTANT.CURRENCY_UNIT} />
                                </h2>
                                <span className="desc">Doanh thu năm 2019</span>
                                <div className="icon">
                                    <i className="zmdi zmdi-calendar-note" />
                                </div>
                                <ReactTooltip id="totalAsset">
                                    <span>
                                        <NumberFormat value={data.totalAsset} displayType={'text'} thousandSeparator={true}/>

                                    </span>
                                </ReactTooltip>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-3">
                            <div className="statistic__item statistic__item--red" data-tip data-for='totalRevenue'>
                                <h2 className="number">
                                    <NumberFormat value={totalRevenue} displayType={'text'} thousandSeparator={true} suffix={" tỷ " + CONSTANT.CURRENCY_UNIT} />
                                </h2>
                                <span className="desc">Tổng tài sản</span>
                                <div className="icon">
                                    <i className="zmdi zmdi-money" />
                                </div>
                                <ReactTooltip id="totalRevenue">
                                    <span>
                                        <NumberFormat value={data.totalRevenue} displayType={'text'} thousandSeparator={true}/>

                                    </span>
                                </ReactTooltip>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default StaticInfo;
import React, { Component } from 'react';

class ExecutiveSection extends Component {
    render() {
        return (
            <section id="testimonials">
                <div className="container">
                    <header className="section-header">
                        <h3>Ban điều hành công ty</h3>
                    </header>
                    <div className="owl-carousel testimonials-carousel">
                        <div className="testimonial-item">
                            <img src="img/testimonial-1.jpg" className="testimonial-img" alt="" />
                            <h3>Ông Nguyễn Tấn Sơn</h3>
                            <h4>Chủ tịch &amp; Tổng giám đốc</h4>
                            <p>
                                <img src="img/quote-sign-left.png" className="quote-sign-left" alt="" />
                                Chúng tôi coi việc phát triển con người là nền tảng cho mọi hoạt động tổ chức. Trong tương lai, FabFund có thể thay đổi chiến lược kinh doanh và vận hành những sản phẩm mới,
                                ... Chính những thành viên trong FabFund sẽ tạo ra những thay đổi này.
                                <img src="img/quote-sign-right.png" className="quote-sign-right" alt="" />
                            </p>
                        </div>
                        <div className="testimonial-item">
                            <img src="img/testimonial-2.jpg" className="testimonial-img" alt="" />
                            <h3>Bà Lê Thị Mỹ Loan</h3>
                            <h4>Phó tổng giám đốc</h4>
                            <p>
                                <img src="img/quote-sign-left.png" className="quote-sign-left" alt="" />
                                Chúng tôi tin rằng việc nuôi dưỡng niềm đam mê công việc của mỗi thành viên phải ưu tiên hàng đầu khi xây dựng một doanh nghiệp.
                                FabFund không chỉ chú trọng việc kinh doanh mà còn nổ lực tạo niềm vui, sự yêu thích trong công việc cho nhân viên.
                                <img src="img/quote-sign-right.png" className="quote-sign-right" alt="" />
                            </p>
                        </div>
                        <div className="testimonial-item">
                            <img src="img/testimonial-5.jpg" className="testimonial-img" alt="" />
                            <h3>Ông Huỳnh Quang Tài</h3>
                            <h4>Phó tổng giám đốc</h4>
                            <p>
                                <img src="img/quote-sign-left.png" className="quote-sign-left" alt="" />
                                Trong những lúc khó khăn nhất, tôi luôn nghĩ tới những gì mình đã cố gắng và tiếp tục công việc, phải luôn có niềm tin vào tương lai, niềm tin rằng
                                khó khăn chỉ là tạm thời, chúng ta phải cùng nhau vượt qua khó khăn.
                                <img src="img/quote-sign-right.png" className="quote-sign-right" alt="" />
                            </p>
                        </div>

                    </div>
                </div>
            </section>
        );
    }
}

export default ExecutiveSection;
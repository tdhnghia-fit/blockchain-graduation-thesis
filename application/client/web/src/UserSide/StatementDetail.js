import React, { Component } from 'react';
import ReportDetail from '../admin/ReportDetail';
import { Route } from "react-router-dom";

class StatementDetail extends Component {
    render() {
        return (
            <div className="m-t-100">
                <Route exact path="/baocaotaichinh/chitiet/:id" component={ReportDetail} />
            </div>
        );
    }
}

export default StatementDetail;
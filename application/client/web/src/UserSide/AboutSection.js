import React, { Component } from 'react';

class AboutSection extends Component {
    render() {
        return (
            <section id="about">
                <div className="container">
                    <header className="section-header text-center">
                        <h3>Chúng tôi</h3>
                        <h5>Thành lập ngày 10/10/2020, FabFund hiện là công ty Internet và công nghệ hàng đầu, kỳ lân công nghệ của Việt Nam.</h5>
                    </header>
                    <div className="row about-cols">
                        <div className="col-md-4 wow fadeInUp">
                            <div className="about-col">
                                <div className="img">
                                    <img src="img/about-mission.jpg" alt="" className="img-fluid" />
                                    <div className="icon"><i className="ion-ios-speedometer-outline" /></div>
                                </div>
                                <h2 className="title"><a href="#">Sứ mệnh</a></h2>
                                <p>
                                    Đối với khách hàng: Tôn trọng và cam kết mang đến cho khách hàng những sản phẩm và dịch vụ tốt nhất.
                                </p>
                                <p>
                                    Đối với đối tác: Xây dựng mối quan hệ tin cậy, uy tín, hợp tác cùng phát triển bền vững.
                                </p>
                                <p>
                                    Đối với nhân viên: Xây dựng môi trường làm việc thân thiện, chuyên nghiệp, năng động, sáng tạo để mọi người có thể phát huy hết khả năng của mình.
                                </p>
                                <p>
                                    Đối với xã hội: Luôn ý thức lợi ích của FabFund gắn liền với lợi ích xã hội và những hoạt động đóng góp cho xã hội là một phần trách nhiệm của FabFund đối với cộng đồng.
                                </p>
                            </div>
                        </div>
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
                            <div className="about-col">
                                <div className="img">
                                    <img src="img/about-plan.jpg" alt="" className="img-fluid" />
                                    <div className="icon"><i className="ion-ios-list-outline" /></div>
                                </div>
                                <h2 className="title"><a href="#">Giá trị cốt lõi</a></h2>
                                <p>
                                    Luôn năng động, sáng tạo nhằm thích nghi với sự thay đổi của thị trường; tạo sự khác biệt, đa dạng hóa sản phẩm và dịch vụ để đáp ứng tối ưu nhu cầu của khách hàng.
                                 </p>
                                <p>
                                    Đổi mới trong tư duy và linh hoạt trong hành động, liên tục cải tiến hệ thống quản lý và đi đầu về công nghệ để tạo giá trị gia tăng cho các cổ đông, khách hàng và đối tác.                                 </p>
                                <p>
                                    Chinh phục mọi thử thách bằng ngọn lửa nhiệt huyết và niềm đam mê. Đó cũng chính là sự tận tâm và tinh thần trách nhiệm trong việc hiện thực hóa các mục tiêu chung của tập đoàn.                                 </p>
                            </div>
                        </div>
                        <div className="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
                            <div className="about-col">
                                <div className="img">
                                    <img src="img/about-vision.jpg" alt="" className="img-fluid" />
                                    <div className="icon"><i className="ion-ios-eye-outline" /></div>
                                </div>
                                <h2 className="title"><a href="#">Tầm nhìn chiến lược</a></h2>
                                <p>
                                    Xây dựng FabFund trở thành doanh nghiệp mạnh và chuyên nghiệp trong lĩnh vực công nghê, Thương mại điện tử và Dịch vụ, khẳng định vị thế trên thị trường trong nước và quốc tế.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default AboutSection;
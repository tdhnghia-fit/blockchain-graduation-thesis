import React, { Component } from 'react';

class IntroSection extends Component {
    render() {
        return (
            <section id="intro">
                <div className="intro-container">
                    <div id="introCarousel" className="carousel  slide carousel-fade" data-ride="carousel">
                        <ol className="carousel-indicators" />
                        <div className="carousel-inner" role="listbox">
                            <div className="carousel-item active">
                                <div className="carousel-background"><img src="img/intro-carousel/1.jpg" alt="" /></div>
                                <div className="carousel-container">
                                    <div className="carousel-content">
                                        <h2>Chúng tôi luôn phát triển</h2>
                                        <p>Sáng tạo công nghệ, phát triển nguồn nhân lực, vì cuộc sống ngày càng hiện đại hơn.</p>
                                        <a href="#about" className="btn-get-started scrollto">Bắt đầu</a>
                                    </div>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="carousel-background"><img src="img/intro-carousel/2.jpg" alt="" /></div>
                                <div className="carousel-container">
                                    <div className="carousel-content">
                                        <h2>Sứ mệnh</h2>
                                        <p>"Luôn sáng tạo để tạo ra những sản phẩm tốt nhất để phục vụ cộng đồng"</p>
                                        <a href="#about" className="btn-get-started scrollto">Bắt đầu</a>
                                    </div>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="carousel-background"><img src="img/intro-carousel/3.jpg" alt="" /></div>
                                <div className="carousel-container">
                                    <div className="carousel-content">
                                        <h2>Cơ hội nghề nghiệp</h2>
                                        <p>Chúng tôi luôn chào đón các ứng viên có ước mơ hoài bão, mong muốn thử thách vào lĩnh vực mới, yêu thích làm việc trong một môi trường năng động, trẻ trung và đầy thử thách</p>
                                        <a href="#about" className="btn-get-started scrollto">Bắt đầu</a>
                                    </div>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="carousel-background"><img src="img/intro-carousel/4.jpg" alt="" /></div>
                                <div className="carousel-container">
                                    <div className="carousel-content">
                                        <h2>Giá trị cốt lõi</h2>
                                        <p>Điểm mạnh của chúng tôi chính là con người và văn hóa doanh nghiệp. Các thành viên luôn mang trong mình một tinh thần cống hiến vì sự phát triển và mục tiêu chung của toàn công ty</p>
                                        <a href="#about" className="btn-get-started scrollto">Bắt đầu</a>
                                    </div>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="carousel-background"><img src="img/intro-carousel/5.jpg" alt="" /></div>
                                <div className="carousel-container">
                                    <div className="carousel-content">
                                    <h2>Phương châm</h2>
                                        <p>"Sáng tạo bắt nguồn từ giới hạn"</p>
                                        <p>"Dustin Moskovitz, Facebook"</p>
                                        <a href="#about" className="btn-get-started scrollto">Bắt đầu</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a className="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
                            <span className="carousel-control-prev-icon ion-chevron-left" aria-hidden="true" />
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
                            <span className="carousel-control-next-icon ion-chevron-right" aria-hidden="true" />
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </section>
        );
    }
}

export default IntroSection;
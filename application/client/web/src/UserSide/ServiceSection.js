import React, { Component } from 'react';

class ServiceSection extends Component {
    render() {
        return (
            <section id="services">
                <div className="container">
                    <header className="section-header wow fadeInUp">
                        <h3>Sản phẩm</h3>
                        <p>Chúng tôi tập trung chính vào các loại hình dịch vụ hướng đến những trải nghiệm người dùng, mang đến cho người dùng những giải pháp tối ưu, nổi bật nhất. Bên cạnh đó, chúng tôi còn tập trung nghiên cứu và phát triển nhiều dự án liên quan đến công nghệ mới, nhằm bắt kịp xu thế hỗ trợ hiệu quả cho hoạt động của cơ quan quản lý và doanh nghiệp.</p>
                    </header>
                    <div className="row">
                        <div className="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                            <div className="icon"><i className="ion-ios-people-outline" /></div>
                            <h4 className="title"><a href>Nền tảng kết nối</a></h4>
                            <p className="description">Các nền tảng kết nối đa dạng, đa dịch vụ, phục vụ toàn diện nhu cầu kết nối, giải trí, tìm kiếm của cá nhân và tổ chức.</p>
                        </div>
                        <div className="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                            <div className="icon"><i className="ion-ios-bookmarks-outline" /></div>
                            <h4 className="title"><a href>Tài chính và thanh toán</a></h4>
                            <p className="description">Các nền tảng và dịch vụ trung gian thanh toán, tài chính cá nhân theo xu hướng mới.</p>
                        </div>
                        <div className="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                            <div className="icon"><i className="ion-ios-paper-outline" /></div>
                            <h4 className="title"><a href>Dịch Vụ Đám Mây</a></h4>
                            <p className="description">Cung cấp các dịch vụ đám mây toàn diện cho tổ chức, doanh nghiệp, địa phương. Các hệ thống, giải pháp công nghệ thông minh dựa trên kết nối Internet và công nghệ đám mây</p>
                        </div>
                        <div className="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                            <div className="icon"><i className="ion-ios-speedometer-outline" /></div>
                            <h4 className="title"><a href>Dịch vụ quy trình kinh doanh</a></h4>
                            <p className="description">Dịch vụ quy trình kinh doanh của chúng tôi tự động hóa và hợp lý hóa các quy trình của bạn để giảm chi phí và tối ưu hóa trải nghiệm khách hàng của bạn.</p>
                        </div>
                        <div className="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                            <div className="icon"><i className="ion-ios-barcode-outline" /></div>
                            <h4 className="title"><a href>Trò chơi trực tuyến</a></h4>
                            <p className="description">Phát triển và sản xuất trò chơi trực tuyến, phát hành ra thị trường quốc tế. Nhập khẩu và Phát hành các trò chơi nổi tiếng thế giới.</p>
                        </div>
                        <div className="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                            <div className="icon"><i className="ion-ios-analytics-outline" /></div>
                            <h4 className="title"><a href>Các giao dịch tư vấn</a></h4>
                            <p className="description">Dịch vụ tư vấn của chúng tôi cung cấp chuyên môn đẳng cấp thế giới và sở hữu trí tuệ độc đáo để tự tin hướng dẫn bạn và đẩy nhanh hành trình chuyển đổi kỹ thuật số của bạn.</p>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default ServiceSection;
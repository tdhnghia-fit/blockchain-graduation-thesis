import React, { Component } from 'react';

class ContactSection extends Component {
    render() {
        return (
            <section id="contact" className="section-bg wow fadeInUp">
                <div className="container">
                    <div className="section-header">
                        <h3>Liên hệ</h3>
                        <p></p>
                    </div>
                    <div className="row contact-info">
                        <div className="col-md-6">
                            <div className="contact-address">
                                <i className="ion-ios-location-outline" />
                                <h3>Địa chỉ</h3>
                                <address>227, Nguyễn Văn Cừ, P4, Q5, Tp. Hồ Chí Minh</address>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="contact-email">
                                <i className="ion-ios-email-outline" />
                                <h3>Email</h3>
                                <p><a href="mailto:tdhb.612@gmail.com">tdhb.612@gmail.com</a></p>
                                <p><a href="mailto:hophongit98@gmail.com">hophongit98@gmail.com</a></p>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default ContactSection;
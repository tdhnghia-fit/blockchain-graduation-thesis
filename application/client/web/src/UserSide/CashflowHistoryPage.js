import React, { Component } from 'react';
import CashflowHistory from '../admin/CashflowHistory';
import { Route } from "react-router-dom";

class CashflowHistoryPage extends Component {
    render() {
        return (
            <section id="portfolio" className="section-bg">
                <div className="container">
                    <header className="section-header">
                        <h3 className="section-title">Báo cáo chuyển lưu tiền tệ</h3>
                    </header>
                    <Route exact path="/baocao/chuyenluutiente/lichsu/:id" component={CashflowHistory} />

                </div>
            </section>
        );
    }
}

export default CashflowHistoryPage;
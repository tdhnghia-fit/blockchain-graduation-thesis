import React, { Component } from 'react';
import '../css/style.css'
import NavHeader from './NavHeader';
import IntroSection from './IntroSection';
import AboutSection from './AboutSection';
import ServiceSection from './ServiceSection';
import ActivitySection from './ActivitySection';
import ExecutiveSection from './ExecutiveSection';
import ShareHolderSection from './ShareHolderSection';
import ContactSection from './ContactSection';
import Footer from './Footer';

class index extends Component {
    render() {
        return (
            <div>
                <NavHeader/>
                <IntroSection/>
                <main id="main">
                    <AboutSection/>
                    <ServiceSection/>
                    <ActivitySection/>
                    <ExecutiveSection/>
                    <ContactSection/>
                </main>
                <Footer/>
            </div>
        );
    }
}

export default index;
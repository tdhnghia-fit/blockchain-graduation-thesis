import React, { Component } from 'react';
import { Route } from "react-router-dom";
import ShareHolderInfo from '../component/ShareHolderInfo';

class ShareHolderDetail extends Component {
    render() {
        return (
            <div className="m-t-100">
                <Route exact path="/codong/chitiet/:id" component={ShareHolderInfo} />
            </div>
        );
    }
}

export default ShareHolderDetail;
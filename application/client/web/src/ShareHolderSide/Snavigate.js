import React, { Component } from 'react';
import { Route } from "react-router-dom";
import Statement from '../UserSide/Statement';
import ActivityList from './ActivityList';
import ShareHolderList from './ShareHolderList';
import FinancialStatement from '../UserSide/FinancialStatement';
import OtherStatement from '../UserSide/OtherStatement';
import Summary from './Summary';
import StatementDetail from '../UserSide/StatementDetail';
import ShareHolderDetail from './ShareHolderDetail';
import StatementHistory from '../admin/StatementHistory';
import FinancialHistory from '../admin/FinancialHistory';
import IncomeHistory from '../admin/IncomeHistory';
import CashflowHistory from '../admin/CashflowHistory';
import FinHistoryPage from '../UserSide/FinHistoryPage';
import IncomeHistoryPage from '../UserSide/IncomeHistoryPage';
import CashflowHistoryPage from '../UserSide/CashflowHistoryPage';
import ActivityDetail from '../admin/ActivityDetail';
import DetailActivity from './DetailActivity';
import HistoryActivity from './HistoryActivity';

class Snavigate extends Component {
    render() {
        return (
            <section id="about" className="section-bg text-center">
                <div className="container" style={{ maxWidth: "1200px" }}>
                    <Route exact path="/baocaotaichinh" component={Statement} />
                    <Route exact path="/baocaotaichinh/chitiet/:id" component={StatementDetail} />
                    <Route exact path="/hoatdongthuchi" component={ActivityList} />
                    <Route exact path="/hoatdongthuchi/lichsu/:id" component={HistoryActivity} />
                    <Route exact path="/chitiethoatdong/:id" component={DetailActivity} />
                    <Route exact path="/codong" component={ShareHolderList} />
                    <Route exact path="/codong/chitiet/:id" component={ShareHolderDetail} />
                    <Route exact path="/baocao/baocaotaichinh" component={FinancialStatement} />
                    <Route exact path="/baocao/khac" component={OtherStatement} />
                    <Route exact path="/thongke" component={Summary} />

                    <Route exact path="/baocao/lichsu/:id" component={StatementHistory} />
                    <Route exact path="/baocao/taichinh/lichsu/:id" component={FinHistoryPage} />
                    <Route exact path="/baocao/thunhap/lichsu/:id" component={IncomeHistoryPage} />
                    <Route exact path="/baocao/chuyenluutiente/lichsu/:id" component={CashflowHistoryPage} />
                </div>
            </section>
        );
    }
}

export default Snavigate;
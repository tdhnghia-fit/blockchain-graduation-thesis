import React, { Component } from 'react';
import ActivityHistory from '../admin/ActivityHistory';
import { Route } from "react-router-dom";

class HistoryActivity extends Component {
    render() {
        return (
            <section id="portfolio" className="section-bg">
                <div className="container">
                    <header className="section-header">
                        <h3 className="section-title">Hoạt động thu chi</h3>
                    </header>
                    <Route exact path="/hoatdongthuchi/lichsu/:id" component={ActivityHistory} />
                </div>
            </section>
        );
    }
}

export default HistoryActivity;
import React, { Component } from 'react';
import CONSTANT from '../Constant';
import { NavLink } from "react-router-dom";
import AppUtils from '../Utils/AppUtils';

class TopHeader extends Component {

  logout() {
    AppUtils.logout()
  }

  render() {
    let isLogin = localStorage.getItem(CONSTANT.USER_LOGIN_TOKEN) != null
    let userRole = localStorage.getItem(CONSTANT.USER_LOGIN_ROLE)
    let isAdmin = userRole == CONSTANT.ROLE_ADMIN
    let isUser = userRole == CONSTANT.ROLE_USER    
    let isAccountant = userRole == CONSTANT.ROLE_ACCOUNTANT
    let isAuditor = userRole == CONSTANT.ROLE_AUDITOR

    return (
      <header id="header" style={{ background: '#000000' }}>
        <div className="container-fluid">
          <div id="logo" className="pull-left">
            <h1><a href="#intro" className="scrollto">FabFund</a></h1>
          </div>
          <nav id="nav-menu-container">
            <ul className="nav-menu">
              <li ><a href="/trangchu">Trang chủ</a></li>
              <li class="menu-has-children"><a href="/baocaotaichinh">Báo cáo tài chính</a></li>
              {
                isLogin && !isUser && <li>
                  <NavLink to="/thongke">
                    <li>
                      <a href="#porforlio">
                        Thống kê
                  </a>
                    </li>
                  </NavLink>
                </li>
              }
              {
                isLogin && !isUser && <li>
                  <NavLink to="/hoatdongthuchi">
                    <li>
                      <a href="#porforlio">
                        Hoạt động thu chi
                </a>
                    </li>
                  </NavLink>
                </li>
              }

              {
                isLogin && !isUser && <li>
                  <NavLink to="/codong">
                    <li>
                      <a href="#porforlio">
                        Cổ đông
                      </a>
                    </li>
                  </NavLink>
                </li>
              }
              {
                (isAdmin || isAccountant || isAuditor) && <li>
                  <li>
                    <a href="/quanly/thongke">
                      Trang Quản lý
                      </a>
                  </li>
                </li>
              }
              {
                !isLogin && <li>
                  <a href="/dangnhap">Đăng nhập</a>
                </li>
              }
              {
                isLogin && <a onClick={() => this.logout()}>
                  <NavLink to="/dangnhap">Đăng xuất</NavLink>
                </a>
              }
            </ul>
          </nav>{/* #nav-menu-container */}
        </div>
      </header>

    );
  }
}

export default TopHeader;
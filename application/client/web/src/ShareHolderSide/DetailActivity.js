import React, { Component } from 'react';
import { Route } from "react-router-dom";
import ActivityDetail from '../admin/ActivityDetail';

class DetailActivity extends Component {
    render() {
        return (
            <section id="portfolio" className="section-bg">
                <div className="container">
                    <Route exact path="/chitiethoatdong/:id" component={ActivityDetail} />
                </div>
            </section>
        );
    }
}

export default DetailActivity;
import React, { Component } from 'react';
import TopHeader from './TopHeader';
import Footer from '../UserSide/Footer';
import Snavigate from './Snavigate';

class ShareHolderIndex extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <TopHeader />
                </div>
                <div className="col-lg-12">
                    <Snavigate />
                </div>
                <div className="col-lg-12">
                    <Footer />
                </div>
            </div>
        );
    }
}

export default ShareHolderIndex;
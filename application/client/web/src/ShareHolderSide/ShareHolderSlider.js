import React, { Component } from 'react';
import Slider from "react-slick";
import API from '../API/API';
import AppUtils from '../Utils/AppUtils';
import axios from 'axios'
import CONSTANT from '../Constant';
import { NavLink } from 'react-router-dom';

class ShareHolderSlider extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
        this.click = this.click.bind(this)
    }

    componentWillMount() {
        this.querryUserByRole()
    }

    querryUserByRole() {
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        let { id } = this.props
        let param = "?role=" + id

        let url = API.GET_USER.slice(0, -1) + param
        console.log(url)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                console.log(res.data)
                if (res.data.length !== 0) {
                    this.setState({
                        data: res.data,
                        isLoading: true
                    })
                } else {
                    console.log("lỗi")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    click(image) {
        localStorage.setItem("image", image)
    }

    render() {
        var settings = {
            dots: true,
            infinite: false,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 2
        };

        let src = []
        let img1 = ["/images/icon/c1.png", "/images/icon/c2.jpg"]
        let img2 = ["/images/icon/c3.jpg", "/images/icon/c4.jpg"]
        let img3 =  ["/images/icon/c5.jpg", "/images/icon/c6.jpg", "/images/icon/c8.jpg"]

        const { data } = this.state
        let { id } = this.props
        if (id == CONSTANT.ROLE_SHAREHOLDER_FOUNDER_ID) src = img1
        else if (id == CONSTANT.ROLE_SHAREHOLDER_PREFERRED_ID) src = img2
        else src = img3
        let userRole = localStorage.getItem(CONSTANT.USER_LOGIN_ROLE)
        let isUser = (userRole == CONSTANT.ROLE_USER) || (userRole == undefined)
        let isLogin = localStorage.getItem(CONSTANT.USER_LOGIN_TOKEN) != null

        return (
            <>
                <Slider {...settings}>
                    {
                        data.map((item, key) => {
                            let role = AppUtils.getStrRole(item.roles[0].name)
                            let defaultSrc = src[key]
                            return (
                                <div className=" wow fadeInUp" >
                                    {
                                        isLogin && !isUser && <NavLink to={"/codong/chitiet/" + item.id}>
                                            <div className="member" onClick={() => this.click(defaultSrc)} >
                                                <img src={defaultSrc} className="img-fluid" alt="" />
                                                <div className="member-info">
                                                    <div className="member-info-content">
                                                        <h4>{item.name}</h4>
                                                        <span>{role}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </NavLink>
                                    }

                                    {
                                        isUser && <div className="member">
                                            <img src={defaultSrc} className="img-fluid" alt="" />
                                            <div className="member-info">
                                                <div className="member-info-content">
                                                    <h4>{item.name}</h4>
                                                    <span>{role}</span>
                                                </div>
                                            </div>
                                        </div>
                                    }

                                </div>
                            );
                        })
                    }
                </Slider>
            </>
        );
    }
}

export default ShareHolderSlider;
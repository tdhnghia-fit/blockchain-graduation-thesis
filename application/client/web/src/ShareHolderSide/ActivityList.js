import React, { Component } from 'react';
import ListActivity from '../component/ListActivity';
import ActivityChartByType from '../component/ActivityChartByType';
import ActivityChart from '../component/ActivityChart';

class ActivityList extends Component {
    render() {
        return (
            <section id="about" className="section-bg m-t-100 text-center">
                <div className="container">
                    <header className="section-header m-b-50">
                        <h3 className="section-title">Hoạt động thu chi</h3>
                    </header>
                    <ListActivity isAdminPageNavigate={false}/>
                    {/* <ActivityChart/> */}
                    {/* <ActivityChartByType/> */}
                </div>
            </section>
        );
    }
}

export default ActivityList;
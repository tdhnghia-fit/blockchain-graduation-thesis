import React, { Component } from 'react';
import ShareHolderSlider from './ShareHolderSlider';
import CONSTANT from '../Constant';

class ShareHolderList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isShowCommon: false,
            isShowPrefered: false,
            textButtonCommon: "Hiển thị",
            textButtonPreferred: "Hiển thị"
        }
    }

    showCommon() {
        let state = this.state.isShowCommon
        let text = ""
        if (!state) {
            text = "Ẩn bớt"
        } else text = "Hiển thị"

        this.setState({
            isShowCommon: !state,
            textButtonCommon: text
        })
    }

    showPreferred() {
        let state = this.state.isShowPrefered
        let text = ""
        if (!state) {
            text = "Ẩn bớt"
        } else text = "Hiển thị"
        this.setState({
            isShowPrefered: !state,
            textButtonPreferred: text
        })
    }

    render() {
        const { isShowCommon, isShowPrefered, textButtonPreferred, textButtonCommon } = this.state
        return (
            <>
                <section id="team" className="section-bg m-t-100 text-center">
                    <div className="section-header">
                        <h3>Cổ đông sáng lập</h3>
                    </div>
                    <ShareHolderSlider id={CONSTANT.ROLE_SHAREHOLDER_FOUNDER_ID} />
                </section>

                <section id="team" className="section-bg text-center">
                    <div className="section-header">
                        <h3>Cổ đông ưu đãi</h3>
                    </div>
                    <button className="au-btn au-btn-icon au-btn--green au-btn--small m-b-30" onClick={() => this.showPreferred()}>{textButtonPreferred}</button>
                    {
                        isShowPrefered &&
                        <ShareHolderSlider id={CONSTANT.ROLE_SHAREHOLDER_PREFERRED_ID} />
                    }
                </section>

                <section id="team" className="section-bg text-center">
                    <div className="section-header">
                        <h3>Cổ đông phổ thông</h3>
                    </div>
                    <button className="au-btn au-btn-icon au-btn--green au-btn--small m-b-30" onClick={() => this.showCommon()}>{textButtonCommon}</button>
                    {
                        isShowCommon &&
                        <ShareHolderSlider id={CONSTANT.ROLE_SHAREHOLDER_COMMON_ID} />
                    }
                </section>

                
            </>
        );
    }
}

export default ShareHolderList;
import React, { Component } from 'react';
import ActivityChartByType from './ActivityChartByType';
import CONSTANT from '../Constant';
import { Container, Row, Col } from 'react-bootstrap'
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import TabContainer from 'react-bootstrap/TabContainer'
import TabContent from 'react-bootstrap/TabContent'
import TabPane from 'react-bootstrap/TabPane'
import ActivityByMonth from './ActivityBymonth';

class ActivityLineChart extends Component {
    render() {
        return (
            <section className="statistic-chart col-lg-12 col-md-12">
                <div className="container">
                    <div className="au-card m-b-30">
                        {/* <ActivityChartByType idChart={"input"} /> */}
                        <Tabs variant="tabs" defaultActiveKey="home" id="uncontrolled-tab-example">
                            <Tab eventKey="home" title="Tất cả">
                                <ActivityByMonth idChart={"all"} isShowFromTo={true}/>
                            </Tab>
                            <Tab eventKey="thu" title="Thu">
                                <ActivityChartByType type={CONSTANT.TYPE_INPUT} idChart={"input"} />
                            </Tab>
                            <Tab eventKey="chi" title="Chi">
                                <ActivityChartByType type={CONSTANT.TYPE_OUTPUT} idChart={"output"} />
                            </Tab>
                            <Tab eventKey="begin" title="T1-T6">
                                <ActivityByMonth start={1580490000000} end={1593536400000} idChart={"begin"} isShowFromTo={false}/>
                            </Tab>
                            <Tab eventKey="end" title="T7-T12">
                                <ActivityByMonth start={1593622800000} end={1609434000000} idChart={"end"} isShowFromTo={false}/>
                            </Tab>

                        </Tabs>
                    </div>
                </div>
            </section>
        );
    }
}

export default ActivityLineChart;
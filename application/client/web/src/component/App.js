import React, { Component } from 'react';
import '../css/App.css';
import { BrowserRouter as Router } from "react-router-dom";
import { Route } from "react-router-dom";
import ViewReport from '../Report/ViewReport';
import Home from './AdminNavigate';
import Login from './Login';
import Error404Page from './Error404Page';
import AdminNavigate from './AdminNavigate';
import index from '../UserSide';
import ShareHolderIndex from '../ShareHolderSide/ShareHolderIndex';
import RedirectHome from './RedirectHome';
import Logout from './Logout';

class App extends Component {
  render() {
    return (
      <Router>

        <Route exact path="/" component={index} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/activity/:id" component={ViewReport} />
        <Route exact path="/quanly/user" component={AdminNavigate} />
        <Route exact path="/quanly/activity" component={AdminNavigate} />
        <Route exact path="/quanly/statement" component={AdminNavigate} />
        <Route exact path="/quanly/dashboard" component={AdminNavigate} />
        <Route exact path="/quanly/userinfo" component={AdminNavigate} />
        <Route exact path="/quanly/activity/history/:id" component={AdminNavigate} />
        <Route exact path="/quanly/statement/detail/:id" component={AdminNavigate} />
        <Route exact path="/quanly/statement/history/:id" component={AdminNavigate} />
        <Route exact path="/quanly/statement/financial/history/:id" component={AdminNavigate} />
        <Route exact path="/quanly/statement/income/history/:id" component={AdminNavigate} />
        <Route exact path="/quanly/statement/cashflow/history/:id" component={AdminNavigate} />
        <Route exact path="/quanly/cashflowchart" component={AdminNavigate} />
        <Route exact path="/quanly/incomechart" component={AdminNavigate} />
        <Route exact path="/quanly/financialchart" component={AdminNavigate} />
        <Route exact path="/quanly/home" component={AdminNavigate} />

        <Route exact path="/quanly/taikhoan/chitiet/:id" component={AdminNavigate} />
        <Route exact path="/quanly/themhoatdongmoi" component={AdminNavigate} />
        <Route exact path="/quanly/chitiethoatdong/:id" component={AdminNavigate} />
        <Route exact path="/quanly/danhmuchoatdong" component={AdminNavigate} />
        <Route exact path="/quanly/hoatdongthuchi" component={AdminNavigate} />
        <Route exact path="/quanly/taikhoan" component={AdminNavigate} />
        <Route exact path="/quanly/thongke" component={AdminNavigate} />
        <Route exact path="/quanly/baocao" component={AdminNavigate} />

        <Route exact path="/error" component={Error404Page} />
        <Route exact path="/dangxuat" component={Logout} />


        <Route exact path="/" component={RedirectHome} />
        <Route exact path="/trangchu" component={index} />
        <Route exact path="/dangnhap" component={Login} />
        <Route exact path="/baocaochitiet/:year.:quarter" component={ViewReport} />
        <Route exact path="/baocaochitiet/:year" component={ViewReport} />
        <Route exact path="/baocaotaichinh" component={ShareHolderIndex} />
        <Route exact path="/baocaotaichinh/chitiet/:id" component={ShareHolderIndex} />
        <Route exact path="/baocao/baocaotaichinh" component={ShareHolderIndex} />
        <Route exact path="/baocao/khac" component={ShareHolderIndex} />
        <Route exact path="/hoatdongthuchi" component={ShareHolderIndex} />
        <Route exact path="/hoatdongthuchi/lichsu/:id" component={ShareHolderIndex} />
        <Route exact path="/chitiethoatdong/:id" component={ShareHolderIndex} />
        <Route exact path="/codong" component={ShareHolderIndex} />
        <Route exact path="/codong/chitiet/:id" component={ShareHolderIndex} />
        <Route exact path="/thongke" component={ShareHolderIndex} />
        <Route exact path="/baocao/lichsu/:id" component={ShareHolderIndex} />
        <Route exact path="/baocao/taichinh/lichsu/:id" component={ShareHolderIndex} />
        <Route exact path="/baocao/thunhap/lichsu/:id" component={ShareHolderIndex} />
        <Route exact path="/baocao/chuyenluutiente/lichsu/:id" component={ShareHolderIndex} />


      </Router>
    );
  }
}

export default App;

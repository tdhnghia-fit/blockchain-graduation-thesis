import React, { Component } from 'react';
import Carousel from 'react-bootstrap/Carousel'
import { NavLink } from "react-router-dom";
import API from '../API/API';
import AppUtils from '../Utils/AppUtils';
import axios from 'axios'
import CONSTANT from '../Constant';

class ListShareHolders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            index: 0
        }
    }

    componentDidMount() {
        this.querryUserByRole();
    }

    querryUserByRole() {
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("authorization", authorization)
        let role = this.state.role
        let param = "?role=" + CONSTANT.ROLE_SHAREHOLDER_ID

        let url = API.GET_USER + param
        console.log(url)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data.length !== 0) {
                    this.setState({
                        data: res.data,
                        isLoading: true
                    })
                } else {
                    console.log("lỗi")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    handleSelect = (selectedIndex, e) => {
        this.setState({
            index: selectedIndex
        })
    };
    render() {
        const { data } = this.state

        return (
            <div className="row h-100 align-items-center justify-content-center text-center">
                <div className="col-lg-10 align-self-end">
                    <h1 className="text-uppercase font-weight-bold">Cổ đông</h1>
                    <div className="m-t-30">
                        <Carousel style={{ width: '40%', height: '80%', margin: '0 auto' }}>
                            {
                                data.map((item, key) => {
                                    return (
                                        <Carousel.Item>
                                            <NavLink to={"/quanly/shareholderinfo/" + item.id}>
                                                <img
                                                    className="d-block w-100"
                                                    src="/images/icon/companylogo.png"
                                                    alt="First slide"
                                                />
                                            </NavLink>
                                            {/* <Carousel.Caption>
                                            <h4 style={{ color: 'black' }}>{item.name}</h4>
                                        </Carousel.Caption> */}
                                        </Carousel.Item>
                                    )
                                }
                                )
                            }
                        </Carousel>
                    </div>
                </div>
            </div>
        );
    }
}

export default ListShareHolders;
import React, { Component } from 'react';
import axios from 'axios'
import API from './../API/API.js'
import CONSTANT from './../Constant'
import AppUtils from '../Utils/AppUtils.js';
import { Modal } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusSquare, faSearch } from '@fortawesome/free-solid-svg-icons'
import { Icon } from '@iconify/react';
import deleteIcon from '@iconify/icons-zmdi/delete';
import editIcon from '@iconify/icons-zmdi/edit';
import detailIcon from '@iconify/icons-zmdi/more';
import historyIcon from '@iconify/icons-fa-solid/history';
import Pagination from "react-js-pagination";
import Select from 'react-select';
import { groupStyles, groupBadgeStyles, typeParent } from './../Utils/DataGeneral'
import DatePicker from "react-datepicker";
import { NavLink } from "react-router-dom";
import "react-datepicker/dist/react-datepicker.css";
import NumberFormat from 'react-number-format';
import swal from '@sweetalert/with-react'
import EditActivity from './EditActivity.js';

const formatGroupLabel = data => (
    <div style={groupStyles}>
        <span>{data.label}</span>
        <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
);

class ListActivity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            activePage: 1,
            totalData: 0,
            limit: 5,
            isLoading: false,
            isLoadingType2: false,
            showFormAdd: false,
            showFormEdit: false,
            showFormDetail: false,
            chosenItem: {},
            titleModalEdit: "Chỉnh sửa hoạt động",
            yearArray: [],
            typeParent: CONSTANT.NO_FILTER,
            typeLv2: CONSTANT.NO_FILTER,
            typeLv3: CONSTANT.NO_FILTER,
            start: CONSTANT.NO_FILTER,
            end: CONSTANT.NO_FILTER,
            startDate: "",
            endDate: "",
            actor: CONSTANT.NO_FILTER,
            actorApprove: CONSTANT.NO_FILTER,
            enableFilter: false,
            isDateDesc: true,
            isAmountDesc: true,
            isActorIdDesc: true,
            isTypeDesc: true,
            isPurposeDesc: true,
            isDescriptionDesc: true,
            typeChildLv2: [],
            typeInputLv2: [],
            typeOutputLv2: [],
            typeAll: [],
            actorId: [],
            maxId: CONSTANT.INIT_VALUE,
            defaultLv2: ""
        }
        this.onDeleteItem = this.onDeleteItem.bind(this)
        this.onAddItem = this.onAddItem.bind(this)
        this.queryDataByField = this.queryDataByField.bind(this);
        this.onEditItemCallback = this.onEditItemCallback.bind(this)
        this.dateClick = this.dateClick.bind(this)
        this.amountClick = this.amountClick.bind(this)
        this.actorIdClick = this.actorIdClick.bind(this)
        this.typeClick = this.typeClick.bind(this)
        this.purposeClick = this.purposeClick.bind(this)
        this.descriptionClick = this.descriptionClick.bind(this)
        this.onDownloadItem = this.onDownloadItem.bind(this)
    }

    formatAuthor(list) {
        if (list.length < 2) return list
        return list.join()
    }

    onEditItemCallback(value) {
        this.setState({
            showFormEdit: false
        }, this.queryDataByField)
    }

    handleClose() {
        this.setState({
            showFormAdd: false
        })
    }

    handleShow() {
        this.setState({
            showFormAdd: true
        })
    }

    handleCloseEdit() {
        this.setState({
            showFormEdit: false
        })
    }

    handleShowEdit(value) {
        this.setState({
            chosenItem: value,
            showFormEdit: true,
            isEdit: true,
            titleModalEdit: "Chỉnh sửa hoạt động"
        })
    }

    handleShowDetail(value) {
        this.setState({
            chosenItem: value,
            showFormEdit: true,
            isEdit: false,
            titleModalEdit: "Chi tiết hoạt động"
        })
    }

    queryDataByField() {
        let param = ""
        const { start, end, typeParent, typeLv2, typeLv3, actor, actorApprove } = this.state

        let typeStr = ""
        if (typeParent != CONSTANT.NO_FILTER) typeStr = typeParent + ":"
        if (typeLv2 != CONSTANT.NO_FILTER && typeParent != CONSTANT.NO_FILTER) typeStr += typeLv2
        else if (typeLv2 != CONSTANT.NO_FILTER && typeParent == CONSTANT.NO_FILTER) typeStr += ":" + typeLv2
        if (typeStr != "")
            param += "?type=" + typeStr

        if (actor != CONSTANT.NO_FILTER && typeLv3 == CONSTANT.NO_FILTER && typeLv2 == CONSTANT.NO_FILTER
            && typeParent == CONSTANT.NO_FILTER)
            param += "?actorId=" + actor
        else if (actor != CONSTANT.NO_FILTER) param += "&actorId=" + actor

        if (actorApprove != CONSTANT.NO_FILTER && actor == CONSTANT.NO_FILTER && typeLv3 == CONSTANT.NO_FILTER && typeLv2 == CONSTANT.NO_FILTER
            && typeParent == CONSTANT.NO_FILTER)
            param += "?actorApprovedId=" + actorApprove
        else if (actorApprove != CONSTANT.NO_FILTER)
            param += "&actorApprovedId=" + actorApprove

        if (start != CONSTANT.NO_FILTER && actorApprove == CONSTANT.NO_FILTER && actor == CONSTANT.NO_FILTER && typeLv3 == CONSTANT.NO_FILTER && typeLv2 == CONSTANT.NO_FILTER
            && typeParent == CONSTANT.NO_FILTER)
            param += "?start=" + start
        else if (start != CONSTANT.NO_FILTER)
            param += "&start=" + start

        if (end != CONSTANT.NO_FILTER)
            param += "&end=" + end
        let connector = "&"
        if (start == CONSTANT.NO_FILTER && end == CONSTANT.NO_FILTER && actorApprove == CONSTANT.NO_FILTER && actor == CONSTANT.NO_FILTER && typeLv3 == CONSTANT.NO_FILTER && typeLv2 == CONSTANT.NO_FILTER
            && typeParent == CONSTANT.NO_FILTER)
            connector = "?"
        let url = API.GET_ACTIVITY_BY_PAGE + param + connector + `limit=${this.state.limit}&page=${this.state.activePage}`
        console.log("queryDataByField", url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    console.log("queryDataByField - sucess", res.data.records)
                    let data = res.data.records
                    let sortedArr = AppUtils.sortListById(data, true)
                    let maxid = sortedArr[0].id
                    localStorage.setItem(CONSTANT.MAX_ID, maxid)
                    this.setState({
                        data: sortedArr,
                        totalData: res.data.totalPages * this.state.limit,
                        isLoading: true,
                        maxId: maxid
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });

    }

    getYear() {
        let url = API.GET_YEAR
        console.log("componentWillMount", url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("authorization", authorization)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    let yearOps = AppUtils.convertYearArrayToOptions(res.data)
                    this.setState({
                        yearArray: yearOps
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    onAddItem(value) {
        this.setState({
            showFormAdd: false
        }, this.queryDataByField)
    }

    delete(value) {
        let itemId = value.id
        console.log(API.DEL_ACTIVITY + itemId)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("authorization", authorization)
        axios({
            method: 'DELETE',
            url: API.DEL_ACTIVITY + itemId,
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                this.queryDataByField()
                swal("Đã xoá!", "Thông tin đã được xoá khỏi hệ thống", "success");
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        });
    }

    onDeleteItem(value) {
        swal({
            title: "Bạn muốn thực hiện hành động?",
            text: "Khi xoá, bạn sẽ không thể khôi phục lại dữ liệu!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.delete(value)
                }
            });
    }

    handlePageChange(pageNumber) {
        this.setState({
            activePage: pageNumber
        }, this.queryDataByField);
    }

    componentWillMount() {
        this.getActorId()
        this.getTypeChildLv2()
        this.queryDataByField()
    };

    getActorId() {
        let url = API.GET_ACTORID
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    let actorIdOption = AppUtils.convertActorIdToSelectModel(res.data, true)
                    this.setState({
                        actorId: actorIdOption
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    getTypeChildLv2() {
        let url = API.GET_TYPE_LV2
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    let sortedArr = AppUtils.sortListById(res.data, false)

                    this.setState({
                        typeAll: sortedArr
                    })
                    let arr = AppUtils.convertTypeToSelectModel(sortedArr, true)
                    this.setState({
                        typeChildLv2: arr,
                        isLoadingType2: true
                    })
                    this.parseType(sortedArr)
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    parseType(data) {
        let input = data.slice(0, 5)
        let output = data.slice(5, 11)
        this.setState({
            typeInputLv2: input,
            typeOutputLv2: output
        })
    }

    onActorIdSelect(value) {
        let type = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        console.log("onActorIdSelect", type)
        this.setState({
            actor: type
        }, this.queryDataByField)
    }

    onActorApprovedIdSelect(value) {
        let type = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value
        console.log("onActorApprovedIdSelect", type)
        let res = type
        if (type != 0) {
            res = []
            type.forEach((item, key) => {
                res.push(item.value)
            })
        }
        this.setState({
            actorApprove: res
        }, this.queryDataByField)
    }

    onTypeParentSelect(value) {
        const { typeAll, typeInputLv2, typeOutputLv2 } = this.state

        let type = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.label
        let str = ""
        let typeLv2 = typeAll
        if (type == CONSTANT.TYPE_CHI) {
            str = CONSTANT.TYPE_OUTPUT
            typeLv2 = typeOutputLv2
        }
        else if (type == CONSTANT.TYPE_THU) {
            str = CONSTANT.TYPE_INPUT
            typeLv2 = typeInputLv2
        }
        let last = typeLv2.length
        let convertList = AppUtils.convertTypeToSelectModel(typeLv2, true)
        this.setState({
            typeParent: str,
            typeLv2: CONSTANT.NO_FILTER,
            typeChildLv2: convertList,
            defaultLv2: convertList[0].options[last]
        }, this.queryDataByField)
    }

    getTypeLv3ByTypeLv2(idTypelv2) {
        if (idTypelv2 != CONSTANT.NO_FILTER) {
            let url = API.GET_TYPE_LV3 + "?idLv2=" + idTypelv2
            let authorization = AppUtils.HEADER_AUTHORIZATION()
            axios.get(url, {
                headers: {
                    "Authorization": authorization,
                    "Content-Type": "application/json"
                }
            }).then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data && res.status == 200) {
                        this.setState({
                            type3Filter: AppUtils.convertType3ToSelectModel(res.data, true)
                        })
                    } else {
                        console.log("Không tìm lấy dữ liệu :))")
                    }
                }
            }).catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            }).finally(() => { });
        }
    }

    getDataByType2(idTypelv2) {
        this.queryDataByField()
        // this.getTypeLv3ByTypeLv2(idTypelv2)
    }

    onTypeLv2Select(value) {
        let type2 = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        let type3 = this.state.typeLv3
        if (type2 == CONSTANT.NO_FILTER) type3 = CONSTANT.NO_FILTER
        this.setState({
            typeLv2: type2,
            typeLv3: type3,
            defaultLv2: value
        }, function () {
            this.getDataByType2(type2);
        })
    }

    onTypeLv3Select(value) {
        let type = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        this.setState({
            typeLv3: type
        }, this.queryDataByField)
    }

    onStartSelect = date => {
        let time = date.getTime()
        console.log("start", time)

        this.setState({
            startDate: date,
            start: time,
            enableFilter: this.state.end != CONSTANT.NO_FILTER
        })
    };

    onEndSelect = date => {
        let time = date.getTime()
        console.log("end", time)
        this.setState({
            endDate: date,
            end: time,
            enableFilter: this.state.start != CONSTANT.NO_FILTER
        })
    };

    dateClick() {
        let { data } = this.state;
        let state = !this.state.isDateDesc
        let array = AppUtils.sortListByDate(data, state)

        this.setState({
            isDateDesc: state,
            data: array
        })
    }

    amountClick() {
        let { data } = this.state;
        let state = !this.state.isAmountDesc
        let array = AppUtils.sortListByAmount(data, state)

        this.setState({
            isAmountDesc: state,
            data: array
        })
    }

    actorIdClick() {
        let { data } = this.state;
        let state = !this.state.isActorIdDesc
        let array = AppUtils.sortListByActorId(data, state)

        this.setState({
            isActorIdDesc: state,
            data: array
        })
    }

    typeClick() {
        let { data } = this.state;
        let state = !this.state.isPurposeDesc
        let array = AppUtils.sortListByType(data, state)

        this.setState({
            isPurposeDesc: state,
            data: array
        })
    }

    purposeClick() {
        let { data } = this.state;
        let state = !this.state.isDescriptionDesc
        let array = AppUtils.sortListByPurpose(data, state)

        this.setState({
            isDescriptionDesc: state,
            data: array
        })
    }

    descriptionClick() {
        let { data } = this.state;
        let state = !this.state.isTypeDesc
        let array = AppUtils.sortListByDescription(data, state)

        this.setState({
            isTypeDesc: state,
            data: array
        })
    }

    onDownloadItem(item) {
        let url = API.GET_ACTIVITY_BY_ID + item.id
        console.log(url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            console.log(res.data)
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data.length !== 0) {
                    let proof = res.data.proofImage
                    let id = item.id
                    let linkView = "/activity/" + id
                    if (proof != undefined && proof != "") {
                    }
                    window.open(linkView, "_blank")
                } else {
                    localStorage.removeItem(CONSTANT.BASE64_REPORT)
                    console.log("lỗi")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        })
            .finally(() => { });
    }

    render() {
        const { data, enableFilter, typeChildLv2, isLoading, isLoadingType2, typeLv2, defaultLv2 } = this.state;
        let { isAdminPageNavigate } = this.props
        let userRole = localStorage.getItem(CONSTANT.USER_LOGIN_ROLE)
        let isAdmin = userRole == CONSTANT.ROLE_ACCOUNTANT
        let isFilterTypeLv2 = typeLv2 != CONSTANT.NO_FILTER

        let his = "activity/history/"
        if (isAdminPageNavigate == false) {
            his = "hoatdongthuchi/lichsu/"
        }

        return (
            isLoading && isLoadingType2 && <div className="row">
                <div className="col-md-12 ">
                    <div className="table-data__tool">
                        <div className="table-data__tool-left">
                            <div className="rs-select2--light rs-select2--sm">
                                <Select
                                    onChange={this.onTypeParentSelect.bind(this)}
                                    options={typeParent}
                                    formatGroupLabel={formatGroupLabel}
                                    placeholder="Loại hoạt động"
                                />
                            </div>
                            <div className="rs-select2--light rs-select2--sm">
                                <Select
                                    onChange={this.onTypeLv2Select.bind(this)}
                                    options={typeChildLv2}
                                    formatGroupLabel={formatGroupLabel}
                                    placeholder="Chi tiết"
                                    value={defaultLv2}
                                />
                            </div>
                            <div class="rs-select2--light rs-select2--sm">
                                <DatePicker
                                    placeholderText="Từ ngày"
                                    selected={this.state.startDate}
                                    onChange={this.onStartSelect}
                                />

                            </div>
                            <div class="rs-select2--light rs-select2--sm">
                                <DatePicker
                                    placeholderText="Đến ngày"
                                    selected={this.state.endDate}
                                    onChange={this.onEndSelect}
                                />
                            </div>
                            {
                                enableFilter && <button className="au-btn au-btn-icon au-btn--blue au-btn--small" onClick={this.queryDataByField.bind(this)}>
                                    <FontAwesomeIcon className="custom-icon" icon={faSearch} /></button>
                            }
                        </div>
                        {
                            isAdmin && isAdminPageNavigate &&
                            <div className="table-data__tool-right">
                                <NavLink to={"/quanly/themhoatdongmoi"} >
                                    <button className="au-btn au-btn-icon au-btn--green au-btn--small">
                                        <FontAwesomeIcon className="custom-icon" icon={faPlusSquare} />Thêm hoạt động</button>
                                </NavLink>
                            </div>
                        }
                        {
                            isAdmin && !isAdminPageNavigate &&
                            <div className="table-data__tool-right">
                                <a href="/quanly/themhoatdongmoi">
                                    <button className="au-btn au-btn-icon au-btn--green au-btn--small" >
                                        <FontAwesomeIcon className="custom-icon" icon={faPlusSquare} />Thêm hoạt động</button>
                                </a>
                            </div>

                        }
                    </div>

                    <div className="table-responsive table-responsive-data2 m-t-20">
                        <table className="table table-data2 ">
                            <thead >
                                <th>id</th>
                                <th >
                                    <nav className="test" onClick={this.dateClick}>
                                        <ul className="list-unstyled navbar__list">
                                            <li className="active has-sub">
                                                <a className="js-arrow">
                                                    Ngày diễn ra hoạt động
                                                                <span className="arrow">
                                                        <i className="fas fa-angle-down"></i>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </th>
                                <th className="small">
                                    <nav className="test" onClick={this.actorIdClick}>
                                        <ul className="list-unstyled navbar__list">
                                            <li className="active has-sub">
                                                <a className="js-arrow">
                                                    Thực hiện
                                                    <span className="arrow">
                                                        <i className="fas fa-angle-down"></i>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>

                                </th>
                                <th>
                                    <nav className="test" onClick={this.amountClick}>
                                        <ul className="list-unstyled navbar__list">
                                            <li className="active has-sub">
                                                <a className="js-arrow">
                                                    Số tiền
                                                            <span className="arrow">
                                                        <i className="fas fa-angle-down"></i>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </th>
                                <th>
                                    <nav className="test" onClick={this.typeClick}>
                                        <ul className="list-unstyled navbar__list">
                                            <li className="active has-sub">
                                                <a className="js-arrow">
                                                    Loại
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </th>
                                <th >
                                    <nav className="test" onClick={this.typeClick}>
                                        <ul className="list-unstyled navbar__list">
                                            Chi tiết
                                        </ul>
                                    </nav>
                                </th>
                                {/* <th>
                                    <nav className="test" onClick={this.typeClick}>
                                        <ul className="list-unstyled navbar__list">
                                            Mô tả hoạt động
                                        </ul>
                                    </nav>
                                </th> */}
                                {/* <th className="small">
                                    <nav className="test" onClick={this.purposeClick}>
                                        <ul className="list-unstyled navbar__list">
                                            Mục đích
                                        </ul>
                                    </nav>
                                </th> */}
                                <th >
                                    <nav className="test" onClick={this.dateClick}>
                                        <ul className="list-unstyled navbar__list">
                                            <li className="active has-sub">
                                                <a className="js-arrow">
                                                    Ngày cập nhật hoạt động
                                                                <span className="arrow">
                                                        <i className="fas fa-angle-down"></i>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </th>
                                <th >
                                    <nav className="test" onClick={this.dateClick}>
                                        <ul className="list-unstyled navbar__list">
                                            <li className="active has-sub">
                                                <a className="js-arrow">
                                                    Lịch sử cập nhật hoạt động
                                                                <span className="arrow">
                                                        <i className="fas fa-angle-down"></i>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </th>
                                <th className="text-center"> Thao tác </th>
                            </thead>
                            <tbody>
                                {
                                    data.map((item, key) => {
                                        let state = "role user"

                                        let type = item.type.split(":")
                                        let parent = typeParent.find((item) => {
                                            let label = CONSTANT.TYPE_INPUT
                                            if (item.label == CONSTANT.TYPE_CHI) label = CONSTANT.TYPE_OUTPUT
                                            return label == type[0]
                                        }).label

                                        if (parent == CONSTANT.TYPE_CHI) state = "role admin"
                                        let type2 = typeChildLv2[0].options.find((item) => {
                                            if (item.value != 0 && item.value + "" == type[1]) {
                                                return item
                                            }
                                        })

                                        let child = item.type
                                        if (type2 != undefined)
                                            child = type2.label
                                        // let note = type2.description

                                        return (
                                            <tr className="tr-shadow">
                                                <td className="height">{item.id}</td>
                                                <td className="medium">
                                                    {AppUtils.toDateTime(item.date)}
                                                </td>
                                                <td className="desc">{item.actorId}</td>
                                                <td>
                                                    <NumberFormat value={item.amount} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                </td>
                                                {/* <td>{item.percentage}</td> */}
                                                <td><span className={state}>{parent}</span></td>
                                                <td className="large">{child}</td>
                                                {/* <td><span >{note}</span></td> */}
                                                {/* <td className="small">{item.purpose}</td> */}
                                                {/* <td>{item.description}</td> */}
                                                {/* <td>{this.formatAuthor(item.actorApprovedId)}</td> */}
                                                <td className="medium">
                                                    {AppUtils.toDateTime(item.createAt)}
                                                </td>
                                                <td className="medium">
                                                    {AppUtils.toDateTime(item.lastCreateAt)}
                                                </td>
                                                <td>
                                                    <div className="table-data-feature">
                                                        {
                                                            isAdmin && <button className="item" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" onClick={() => this.handleShowEdit(item)}>
                                                                <Icon icon={editIcon} />
                                                            </button>
                                                        }
                                                        {
                                                            isAdmin && <button className="item" data-toggle="tooltip" data-placement="top" title="Xoá" onClick={() => this.onDeleteItem(item)}>
                                                                <Icon icon={deleteIcon} />
                                                            </button>
                                                        }
                                                        <NavLink to={his + item.id} >
                                                            <button className="item" data-toggle="tooltip" data-placement="top" title="Lịch sử">
                                                                <Icon icon={historyIcon} />
                                                            </button>
                                                        </NavLink>
                                                        <NavLink to={"chitiethoatdong/" + item.id} >
                                                            <button className="item" data-toggle="tooltip" data-placement="top" title="Chi tiết">
                                                                <Icon icon={detailIcon} />
                                                            </button>
                                                        </NavLink>
                                                    </div>
                                                </td>
                                            </tr>
                                        )
                                    }
                                    )
                                }
                                <Modal show={this.state.showFormEdit} onHide={this.handleCloseEdit.bind(this)}
                                    size="lg"
                                    aria-labelledby="contained-modal-title-vcenter"
                                    centered>
                                    <Modal.Header closeButton>
                                        <Modal.Title>{this.state.titleModalEdit}</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <EditActivity itemDetail={this.state.chosenItem} itemId={this.state.chosenItem.id} onEditItemCallback={d => this.onEditItemCallback(d)} isEdit={this.state.isEdit} />
                                    </Modal.Body>
                                </Modal>
                            </tbody>
                        </table>
                    </div>
                    {/* END DATA TABLE */}
                </div>
                <div className="col-lg-12" style={{ justifyContent: 'center', display: 'flex', marginTop: "1%" }}>
                    <Pagination
                        activePage={this.state.activePage}
                        itemsCountPerPage={this.state.limit}
                        totalItemsCount={this.state.totalData}
                        pageRangeDisplayed={5}
                        onChange={this.handlePageChange.bind(this)}
                        itemClass="page-item"
                        linkClass="page-link"
                    />
                </div>
            </div>
        );
    }
}

export default ListActivity;
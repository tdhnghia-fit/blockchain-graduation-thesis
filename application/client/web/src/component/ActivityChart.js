import React, { Component } from 'react';
import {
    ChartComponent, SeriesCollectionDirective, SeriesDirective, Inject,
    Legend, Category, StackingColumnSeries, Tooltip, ILoadedEventArgs, ChartTheme, ColumnSeries, DataLabel
} from '@syncfusion/ej2-react-charts';
import { groupStyles, groupBadgeStyles, chartYearOption, quarterOptions } from '../Utils/DataGeneral'
import CONSTANT from '../Constant';
import Select from 'react-select';
import API from './../API/API.js'
import AppUtils from '../Utils/AppUtils';
import axios from 'axios'
import { Browser } from '@syncfusion/ej2-base';
import NumberFormat from 'react-number-format';
import swal from '@sweetalert/with-react'

const formatGroupLabel = data => (
    <div style={groupStyles}>
        <span>{data.label}</span>
        <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
);

class ActivityChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            year: CONSTANT.YEAR_2019,
            core: [],
            financial: [],
            other: [],
            total: [],
            isLoading: false,
            max: 0,
            isS2Empty: false,
        }
    }
    onYearSelect(value) {
        let year = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        this.setState({
            year: year
        }, this.getData)
    }

    getData() {
        this.setState({
            core: [],
            financial: [],
            other: [],
            total: [],
            max: 0,
            isLoading: false
        }, function () {
            this.getIncomeChart()
        })
    }

    componentWillMount() {
        this.getIncomeChart()
    }

    getIncomeChart() {
        let url = API.GET_INCOME_CHART + "?year=" + this.state.year
        console.log("getIncomeChart", url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.parseIncomeDataChart(res.data)
                    } else {
                        this.setState({
                            isS2Empty: true,
                            isLoading: true
                        })
                    }
                }
            })
            .catch(res => {
                let status = res.response.status
                if (status === 401) {
                    swal(res.response.data.message, "", "warning");
                }
                if (res.response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    parseIncomeDataChart(data) {
        let core = []
        let financial = []
        let other = []
        let total = []
        let max = 0

        let sortArr = AppUtils.sortListByQuater(data)

        sortArr.forEach(item => {
            let quarter = item.quarter
            let label = 'Q' + quarter
            if (quarter == 5) label = 'Cả năm'
            let coreObj = {
                x: label,
                y: item.coreRevenue,
            }
            let financialObj = {
                x: label,
                y: item.financialRevenue,
            }
            let otherObj = {
                x: label,
                y: item.otherRevenue,
            }

            if (max < item.totalRevenue) max = item.totalRevenue

            let totalObj = {
                year: label,
                total: item.totalRevenue
            }
            core.push(coreObj)
            financial.push(financialObj)
            other.push(otherObj)
            total.push(totalObj)
        })

        this.setState({
            core: core,
            financial: financial,
            other: other,
            total: total,
            max: max,
            isLoading: true
        })
    }

    onChartLoad() {
        let chart = document.getElementById('charts');
        chart.setAttribute('title', '');
    };
    onChartLoad1() {
        let chart = document.getElementById('charts1');
        chart.setAttribute('title', '');
    };
    render() {
        const { core, financial, other, total, isLoading, max, isS2Empty } = this.state
        let maximum = Math.round(max / 1000000000)

        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="au-card m-b-30">
                        <div className="au-card-inner">
                            <div className="m-b-20">
                                <h3 className="title-2 text-center">Tổng quan Doanh thu năm </h3>
                                <div className="col-lg-3">
                                    <Select
                                        onChange={this.onYearSelect.bind(this)}
                                        options={chartYearOption}
                                        formatGroupLabel={formatGroupLabel}
                                        placeholder="2019"
                                    />
                                </div>
                            </div>
                            <div className="row m-t-30 m-b-20 col-lg-12">
                                <div class="chart-info">
                                    <div class="chart-note">
                                        <span class="dot dot--blue-green"></span>
                                        <span>Doanh thu chính</span>
                                    </div>
                                    <div class="chart-note">
                                        <span class="dot dot--black"></span>
                                        <span>Doanh thu từ hoạt động tài chính</span>
                                    </div>
                                    <div class="chart-note">
                                        <span class="dot dot--blue-resful"></span>
                                        <span>Doanh thu khác</span>
                                    </div>
                                </div>
                            </div>
                            {
                                !isS2Empty && isLoading && <div className="row">
                                    <ChartComponent id='charts' style={{ textAlign: "center" }}
                                        primaryXAxis={{ valueType: 'Category', interval: 1, majorGridLines: { width: 0 } }}
                                        primaryYAxis={{
                                            lineStyle: { width: 0 },
                                            minimum: 0,
                                            majorTickLines: { width: 0 },
                                            majorGridLines: { width: 1 },
                                            minorGridLines: { width: 1 },
                                            minorTickLines: { width: 0 },
                                            labelFormat: '{value}',
                                        }}
                                        chartArea={{ border: { width: 0 } }}
                                        tooltip={{ enable: true }}
                                        width={Browser.isDevice ? '100%' : '60%'}
                                        loaded={this.onChartLoad.bind(this)}>
                                        <Inject services={[StackingColumnSeries, Category, Legend, Tooltip]} />
                                        <SeriesCollectionDirective>
                                            <SeriesDirective dataSource={core} xName='x' yName='y' name='Doanh thu chính' type='StackingColumn'>
                                            </SeriesDirective>
                                            <SeriesDirective dataSource={financial} xName='x' yName='y' name='Doanh thu từ HĐTC' type='StackingColumn'>
                                            </SeriesDirective>
                                            <SeriesDirective dataSource={other} xName='x' yName='y' name='Doanh thu khác' type='StackingColumn'>
                                            </SeriesDirective>
                                        </SeriesCollectionDirective>
                                    </ChartComponent>

                                    <div className="col-lg-4">
                                        {/* TOP CAMPAIGN*/}
                                        <div className="top-campaign">
                                            <h3 className="title-3 m-b-30">Tổng Chi phí theo từng giai đoạn</h3>
                                            <div className="table-responsive">
                                                <table className="table table-top-campaign">
                                                    <tbody>
                                                        {
                                                            total.map((item, key) => {
                                                                return (
                                                                    <tr>
                                                                        <td>{item.year}</td>
                                                                        <td>
                                                                            <NumberFormat value={item.total} displayType={'input'} thousandSeparator={true} prefix={''} />
                                                                        </td>
                                                                        <td>{CONSTANT.CURRENCY_UNIT}</td>
                                                                    </tr>
                                                                );
                                                            })
                                                        }
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        {/* END TOP CAMPAIGN*/}
                                    </div>
                                </div>
                            }
                            {
                                isLoading && isS2Empty && <img style={{ width: "100%" }} src="/images/nodata.png" className="img-fluid" alt="" />
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ActivityChart;
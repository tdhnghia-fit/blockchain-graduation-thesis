import React, { Component } from 'react';
import ValidatedLoginForm from '../Login/ValidatedLoginForm.js';
import '../css/login.css';
import axios from 'axios'
import API from './../API/API.js'
import { Redirect } from 'react-router-dom'
import CONSTANT from './../Constant'
import AppUtils from '../Utils/AppUtils.js';
import LoginPage from './LoginPage.js';

class Login extends Component {

    constructor(props) {
        super(props);
        const token = localStorage.getItem(CONSTANT.USER_LOGIN_TOKEN)
        let loggedIn = true
        if (token == null) {
            loggedIn = false
        }

        this.state = {
            userId: "",
            role: "",
            accessToken: "",
            tokenType: "",
            loggedIn
        }

        this.login = this.login.bind(this)
    }

    componentWillMount() {
        let loginRole = localStorage.getItem(CONSTANT.USER_LOGIN_ROLE)
        let isLogin = loginRole != undefined
        this.setState({
            loggedIn: isLogin,
            role: loginRole
        })
    }

    login() {
        let email = document.getElementById("txtEmail").value
        let password = document.getElementById("txtPwd").value
        console.log("email", email)
        console.log("password", password)

        axios({
            method: 'GET',
            url: API.LOGIN,
            data: {
                usernameOrEmail: email,
                password: password
            }
        }).then(res => {
            console.log(res)
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                localStorage.setItem(CONSTANT.USER_LOGIN_TOKEN, res.data.id)
                localStorage.setItem(CONSTANT.USER_LOGIN_ROLE, res.data.role)
                localStorage.setItem(CONSTANT.ACCESS_TOKEN, res.data.accessToken)
                localStorage.setItem(CONSTANT.TOKEN_TYPE, res.data.tokenType)
                let authorization = AppUtils.HEADER_AUTHORIZATION()
                console.log("login", authorization)
                this.setState({
                    userId: res.data.id,
                    role: res.data.role,
                    accessToken: res.data.accessToken,
                    tokenType: res.data.tokenType,
                    loggedIn: true
                })
            }
        }).catch((res) => {
            this.setState({
                loggedIn: false
            })
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            console.log('request failed', res.error)
        });
    }

    render() {
        if (this.state.loggedIn) {
            let role = this.state.role
            console.log("role", role)
            return <Redirect to="/quanly/activity" />
        }
        return (
            <LoginPage />
        );
    }
}

export default Login;

    // <div>
            //     <div className="d-flex align-items-center justify-content-center h-100vh">
            //         <form className>
            //         <h1 id="titleLogin">Sign In</h1>
            //         <ValidatedLoginForm />
            //         </form>
            //     </div>
            // </div>
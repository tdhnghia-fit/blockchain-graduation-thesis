import React, { Component } from 'react';
import {
    AccumulationChartComponent, AccumulationSeriesCollectionDirective, AccumulationSeriesDirective,
    Inject, AccumulationLegend, PieSeries, AccumulationTooltip, IAccLoadedEventArgs, AccumulationTheme,
    AccumulationDataLabel
} from '@syncfusion/ej2-react-charts';
import CONSTANT from '../Constant';
import { Browser, Property } from '@syncfusion/ej2-base';
import AppUtils from '../Utils/AppUtils';

class UserActivityRatioChart extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataInput: [],
            dataOutput: [],
            isLoading: false,
            actorId: ""
        }
    }

    componentWillMount() {
        const { actorId, sumInput, anotherActorSumInput, sumOutput, anotherActorSumOutput } = this.props
        let actorIdRatio = Math.ceil(AppUtils.getRatio(sumInput, sumInput + anotherActorSumInput))
        let totalRatio = 100 - actorIdRatio
        let dataInput = [
            { 'x': actorId, y: sumInput, text: actorIdRatio + "%" },
            { 'x': "Tổng", y: anotherActorSumInput, text: totalRatio + "%" }
        ]

        let actorIdOutputRatio = Math.ceil(AppUtils.getRatio(sumOutput, sumOutput + anotherActorSumOutput))
        let totalOutputRatio = 100 - actorIdOutputRatio
        let dataOutput = [
            { 'x': actorId, y: sumOutput, text: actorIdOutputRatio + "%" },
            { 'x': "Tổng", y: anotherActorSumOutput, text: totalOutputRatio + "%" }
        ]

        this.setState({
            dataInput: dataInput,
            dataOutput: dataOutput,
            actorId: actorId,
            isLoading: true
        })
    }


    pieangle(e) {
        let angle = (document.getElementById('pieangle')).value;
        this.pie.series[0].startAngle = parseFloat(angle);
        this.pie.series[0].endAngle = parseFloat(angle);
        this.pie.series[0].animation.enable = false;
        document.getElementById('anglevalue').innerHTML = angle;
        this.pie.removeSvg();
        this.pie.refreshSeries();
        this.pie.refreshChart();
    }

    pieradius(e) {
        let radius = (document.getElementById('pieradius')).value;
        this.pie.series[0].radius = radius + '%';
        document.getElementById('radius').innerHTML = (parseInt(radius, 10) / 100).toFixed(2);
        this.pie.series[0].animation.enable = false;
        this.pie.removeSvg();
        this.pie.refreshSeries();
        this.pie.refreshChart();
    };
    pieexploderadius(e) {
        let radius = (document.getElementById('pieexploderadius')).value;
        this.pie.visibleSeries[0].explodeOffset = radius + '%';
        document.getElementById('exploderadius').innerHTML = (parseInt(radius, 10) / 100).toFixed(2);
        this.pie.series[0].animation.enable = false;
        this.pie.removeSvg();
        this.pie.refreshSeries();
        this.pie.refreshChart();
    };
    pieexplodeindex(e) {
        let index = (document.getElementById('pieexplodeindex')).value;
        this.pie.visibleSeries[0].explodeIndex = index;
        document.getElementById('explodeindex').innerHTML = index.toString();
        this.pie.series[0].animation.enable = false;
        this.pie.removeSvg();
        this.pie.refreshSeries();
        this.pie.refreshChart();
    };
    piecenterx(e) {
        let x = (document.getElementById('x')).value;
        this.pie.center.x = x + '%';
        document.getElementById('xvalue').innerHTML = x + '%';
        this.pie.series[0].animation.enable = false;
        this.pie.removeSvg();
        this.pie.refreshSeries();
        this.pie.refreshChart();
    };
    piecentery(e) {
        let y = (document.getElementById('y')).value;
        this.pie.center.y = y + '%';
        document.getElementById('yvalue').innerHTML = y + '%';
        this.pie.series[0].animation.enable = false;
        this.pie.removeSvg();
        this.pie.refreshSeries();
        this.pie.refreshChart();
    };

    onChartLoad(args) {
        document.getElementById('pie-chart').setAttribute('title', '');
    };

    onChartLoad1(args) {
        document.getElementById('pie-chart1').setAttribute('title', '');
    };

    render() {
        const { isLoading, dataInput, dataOutput } = this.state
        let s1 = "col-lg-6"
        let s2 = "col-md-6"

        // if (dataInput.length == 0 || dataOutput.length == 0) {
        //     s1 = "col-lg-12"
        //     s2 = "col-md-12"
        // }
        return (
            isLoading && <div className="card">
                <div className="card-body">
                    <div className="row col-lg-12 ">
                        {/* <div className='col-lg-12 col-md-12 text-center' style={{fontWeight: '200px'}}>
                            <h3>Tỷ lệ đóng góp</h3>
                        </div> */}
                        <div className='col-lg-12 col-md-12 text-center' style={{ padding: '0px' }}>
                            <AccumulationChartComponent id='pie-chart' ref={pie => this.pie = pie}
                                title="Tỉ lệ đóng góp"
                                titleStyle={{
                                    size: '30px'
                                }}
                                legendSettings={{ 
                                    visible: true,
                                    textStyle: {
                                        size: '15px'
                                    }
                                }}
                                enableSmartLabels={true}
                                enableAnimation={true}
                                center={{ x: '50%', y: '50%' }}
                                tooltip={{ enable: true, format: '${point.x} : <b>${point.y}</b>' }}
                                loaded={this.onChartLoad.bind(this)}
                            >
                                <Inject services={[AccumulationLegend, PieSeries, AccumulationTooltip, AccumulationDataLabel]} />
                                <AccumulationSeriesCollectionDirective>
                                    <AccumulationSeriesDirective dataSource={dataInput} name={"Thu"} xName='x' yName='y'
                                        explode={true} explodeOffset='0%' explodeIndex={0}
                                        dataLabel={{
                                            visible: true,
                                            position: 'Inside', name: 'text',
                                            font: {
                                                fontWeight: '600'
                                            }
                                        }}
                                        radius='70%'
                                    >
                                    </AccumulationSeriesDirective>
                                </AccumulationSeriesCollectionDirective>
                            </AccumulationChartComponent>
                        </div>
                        {/* <div className='col-lg-6 col-md-6 text-center' style={{ padding: '0px' }}>
                            <AccumulationChartComponent id='pie-chart1' ref={pie => this.pie = pie}
                                title="Chi"
                                titleStyle={{
                                    size: '30px'
                                }}
                                legendSettings={{ 
                                    visible: true,
                                    textStyle: {
                                        size: '15px'
                                    }
                                }}
                                enableSmartLabels={true}
                                enableAnimation={true}
                                center={{ x: '50%', y: '50%' }}
                                tooltip={{ enable: true, format: '${point.x} : <b>${point.y}</b>' }}
                                loaded={this.onChartLoad1.bind(this)}
                            >
                                <Inject services={[AccumulationLegend, PieSeries, AccumulationTooltip, AccumulationDataLabel]} />
                                <AccumulationSeriesCollectionDirective>
                                    <AccumulationSeriesDirective dataSource={dataOutput} name={"Chi"} xName='x' yName='y'
                                        explode={true} explodeOffset='0%' explodeIndex={0}
                                        dataLabel={{
                                            visible: true,
                                            position: 'Inside', name: 'text',
                                            font: {
                                                fontWeight: '600'
                                            }
                                        }}
                                        radius='70%'
                                    >
                                    </AccumulationSeriesDirective>
                                </AccumulationSeriesCollectionDirective>
                            </AccumulationChartComponent>
                        </div> */}
                        <div className='col-lg-12 col-md-12 text-center'>
                            <h5>Tỷ lệ phần trăm đóng góp của {this.state.actorId} so với tổng thu từ các cổ đông khác</h5>
                        </div>
                    </div >
                </div >
            </div >
        );
    }
}

export default UserActivityRatioChart;
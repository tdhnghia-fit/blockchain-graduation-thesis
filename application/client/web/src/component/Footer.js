import React, { Component } from 'react';

class footer extends Component {
    render() {
        return (
            <div style={{marginTop:"20px", position: "relative", left: "0", bottom: "0", right: "0"}}>
                {/* Footer */}
                <footer className="bg-light py-4">
                    <div className="container">
                    <div className="small text-center text-muted">Copyright © 2019 - Start Bootstrap</div>
                    </div>
                </footer>
            </div>
        );
    }
}

export default footer;
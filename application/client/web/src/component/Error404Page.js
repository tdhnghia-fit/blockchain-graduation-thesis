import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';


class Error404Page extends Component {
    render() {
        return (
            <section className="page-section" id="report" >
                <Container style={{ height: '3.5rem' }}>
                    <Row>
                        <Col lg="12">
                            <h1 class="kern-this">404 Error.</h1>
                            <p>
                                Oooooops! Không tìm thấy trang bạn yêu cầu. Vui lòng quay lại trang chủ 
                                để tiếp tục.
			  			</p>
                        </Col>  
                    </Row>
                </Container>
            </section >
        );
    }
}

export default Error404Page;
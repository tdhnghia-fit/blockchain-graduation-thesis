import React, { Component } from 'react';
import axios from 'axios'
import FileBase64 from '../component/FileBase64';
import API from './../API/API.js'
import CONSTANT from './../Constant'
import AppUtils from '../Utils/AppUtils';
import Select from 'react-select';
import { groupTypeOptions1, groupStyles, groupBadgeStyles, typeParent, typeParentExcludeAll } from '../Utils/DataGeneral'
import swal from '@sweetalert/with-react'
import DatePicker from "react-datepicker";

const formatGroupLabel = data => (
    <div style={groupStyles}>
        <span>{data.label}</span>
        <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
);


class EditActivity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            files: [],
            isEdit: true,
            isHistory: false,
            isLoading: false,
            isType2Loading: false,
            actorApprovedId: [],
            typeParent: "",
            typeLv2: "",
            endDate: "",
            typeChildLv2: [],
            typeInputLv2: [],
            typeOutputLv2: [],
            typeAll: [],
            defaultLv2: "",
            defaultParent: ""
        }
    }

    onTypeSelect(value) {
        const { typeAll, typeInputLv2, typeOutputLv2, defaultParent } = this.state

        let type = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        let typeLv2 = typeAll
        let str = "input"
        if (type == 2) str = "output"

        if (type == 1) {
            typeLv2 = typeInputLv2
        }
        else if (type == 2) {
            typeLv2 = typeOutputLv2
        }
        let convertList = AppUtils.convertTypeToSelectModel(typeLv2, true)
        this.setState({
            typeParent: str,
            typeLv2: CONSTANT.NO_FILTER,
            typeChildLv2: convertList,
            defaultLv2: "",
            defaultParent: value
        })
    }

    onTypeLv2Select(value) {
        let type = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        this.setState({
            typeLv2: type,
            defaultLv2: value
        })
    }

    componentWillMount() {
        const { itemDetail, itemId, isEdit, isHistory } = this.props

        this.getTypeChildLv2()
        this.getDetailActivity()

        let type = itemDetail.type.split(":")

        this.setState({
            isEdit: isEdit,
            isHistory: isHistory,
            actorApprovedId: itemDetail.actorApprovedId,
            typeParent: type[0],
            typeLv2: type[1],
            typeLv3: type[2]
        })
    }

    getDetailActivity() {
        const { itemId, itemDetail } = this.props
        let url = API.GET_ACTIVITY_BY_ID + itemId
        console.log(url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        let proofImage = res.data.proofImage
                        let arr = proofImage
                        if (!Array.isArray(proofImage)) {
                            arr = [proofImage]
                        }
                        this.setState({
                            data: [res.data],
                            files: arr,
                            endDate: itemDetail.date,
                            end: itemDetail.date,
                            isLoading: true
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    getTypeChildLv2() {
        const { itemDetail } = this.props
        let url = API.GET_TYPE_LV2
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {

                    let sortedArr = AppUtils.sortListById(res.data, false)
                    let type = itemDetail.type.split(":")
                    this.setState({
                        typeAll: sortedArr
                    })

                    let arr = AppUtils.convertTypeToSelectModel(sortedArr, false)


                    let parent = typeParentExcludeAll.find((item) => {
                        let str = AppUtils.getStrType(item.value)
                        if (item.value != 0 && str + "" == type[0]) {
                            return item
                        }
                    })

                    let input = sortedArr.slice(0, 5)
                    let output = sortedArr.slice(5, 11)

                    if (parent.label == CONSTANT.TYPE_CHI) {
                        arr = AppUtils.convertTypeToSelectModel(output, true)
                    } else {
                        arr = AppUtils.convertTypeToSelectModel(input, true)
                    }

                    let child = arr[0].options.find((item) => {
                        if (item.value != 0 && item.value + "" == type[1]) {
                            return item
                        }
                    })

                    let arr1 = AppUtils.convertTypeToSelectModel(sortedArr, true)
                    console.log(arr1[0].options[child.value - 1])

                    this.setState({
                        typeChildLv2: arr,
                        defaultLv2: arr1[0].options[child.value - 1],
                        defaultParent: parent,
                        typeInputLv2: input,
                        typeOutputLv2: output,
                        isType2Loading: true,
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    getFiles(files) {
        let file = files.map((file, i) => file.base64)
        this.setState({ files: file })
    }

    onEndSelect = date => {
        let time = date.getTime()
        console.log("date", date)
        this.setState({
            endDate: date,
            end: time,
            enableFilter: this.state.start != CONSTANT.NO_FILTER
        })
    };

    onCompareData(newData, oldData) {

        if (newData.type != oldData.type
            || newData.amount != oldData.amount || newData.purpose != oldData.purpose
            || newData.percentage != oldData.percentage
            || newData.description != oldData.description
            || newData.actorId != oldData.actorId || newData.proofImage != oldData.proofImage) {
            return true
        }
        return false
    }

    async postActivityData() {
        const { itemDetail, onEditItemCallback } = this.props
        let currentDate = new Date()
        let currentMiliSecs = currentDate.getTime()
        let proof = this.state.files
        // let note = document.getElementById("inputNote").value + ""
        let type = this.state.typeParent + ":" + this.state.typeLv2
        let newData = {
            id: itemDetail.id,
            date: this.state.end + "",
            type: type,
            purpose: document.getElementById("inputPurpose").value + "",
            amount: document.getElementById("inputAmount").value + "",
            percentage: "",
            description: document.getElementById("inputDes").value + "",
            actorId: document.getElementById("inputActorId").value + "",
            actorApprovedId: this.state.actorApprovedId,
            proofImage: proof,
            createAt: currentMiliSecs + "",
            lastCreateAt: itemDetail.createAt + ""
        }

        console.log("newData", newData)

        let isEqual = this.onCompareData(newData, itemDetail)

        if (isEqual) {
            let authorization = AppUtils.HEADER_AUTHORIZATION()
            console.log("authorization", authorization)
            console.log("put - url", API.PUT_ACTIVITY + itemDetail.id)
            axios({
                method: 'put',
                url: API.PUT_ACTIVITY + itemDetail.id,
                data: newData,
                headers: {
                    "Authorization": authorization,
                    "Content-Type": "application/json"
                }
            }).then(res => {
                console.log(res);
                if (res.data.status == 200 || res.data.return_code == undefined) {
                    if (res.error === 'Unauthorized')
                        AppUtils.logout()
                    else {
                        swal("Hoàn tất!", "Chỉnh sửa hoạt động thành công.", "success");
                        onEditItemCallback(newData)
                    }
                } else {
                    swal("Có lỗi xảy ra!", "", "warning");
                }
            })
                .catch(response => {
                    swal("Không thành công!", "Có lỗi xảy ra trong quá trình chỉnh sửa.", "warning");
                    if (response.error === 'Unauthorized')
                        AppUtils.logout()
                });
        } else {
            alert("Không có thay đổi")
        }
    }

    render() {
        var { isEdit, isHistory, files, data, isLoading, typeChildLv2, isType2Loading, endDate, defaultLv2, defaultParent } = this.state
        return (
            isLoading && isType2Loading && <div className="container" style={{ height: "100%" }}>
                {
                    isEdit && !isHistory && <div className="row form-group">
                        <div class="col col-md-3">
                            <label htmlFor="input1">ID</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text" className="form-control" id="inputId" value={this.props.itemDetail.id} disabled />
                        </div>
                    </div>
                }
                {
                    data.map((item, key) => {
                        let parent = ""
                        let child = ""
                        let note = ""
                        let state = "role user"
                        if (item.type != undefined) {
                            let type = item.type.split(":")
                            parent = typeParentExcludeAll.find((item) => {
                                let label = CONSTANT.TYPE_INPUT
                                if (item.label == CONSTANT.TYPE_CHI) label = CONSTANT.TYPE_OUTPUT
                                return label == type[0]
                            })

                            child = typeChildLv2[0].options.find((item) => {
                                if (item.value != 0 && item.value + "" == type[1]) {
                                    return item
                                }
                            })
                            note = item.type
                            if (child != undefined) note = child.description
                        }
                        return (
                            <>
                                <div className="row form-group">
                                    <div class="col col-md-3">
                                        <label htmlFor="input1">Số tiền</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" className="form-control" id="inputAmount" defaultValue={this.props.itemDetail.amount} disabled={!this.state.isEdit} />
                                    </div>
                                </div>
                                <div className="row form-group">
                                    <div class="col col-md-3">
                                        <label htmlFor="input1">Mục đích</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" className="form-control" id="inputPurpose" defaultValue={this.props.itemDetail.purpose} disabled={!this.state.isEdit} />
                                    </div>
                                </div>
                                <div className="row form-group">
                                    <div class="col col-md-3">
                                        <label htmlFor="input2">Người thực hiện</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" className="form-control" id="inputActorId" defaultValue={this.props.itemDetail.actorId} disabled />
                                    </div>
                                </div>
                                <div className="row form-group">
                                    <div class="col col-md-3">
                                        <label htmlFor="input2">Người xác thực</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" className="form-control" id="inputActorApprovedId" defaultValue={this.props.itemDetail.actorApprovedId} disabled />
                                    </div>
                                </div>
                                <div className="row form-group">
                                    <div class="col col-md-3">
                                        <label htmlFor="input2">Loại hoạt động</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <Select
                                            onChange={this.onTypeSelect.bind(this)}
                                            options={typeParentExcludeAll}
                                            formatGroupLabel={formatGroupLabel}
                                            isDisabled={!this.state.isEdit}
                                            value={defaultParent}
                                        />
                                    </div>
                                </div>
                                <div className="row form-group">
                                    <div class="col col-md-3">
                                        <label htmlFor="input2">Chi tiết</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <Select
                                            onChange={this.onTypeLv2Select.bind(this)}
                                            options={typeChildLv2}
                                            formatGroupLabel={formatGroupLabel}
                                            isDisabled={!this.state.isEdit}
                                            value={defaultLv2}
                                        />
                                    </div>
                                </div>
                                <div className="row form-group">
                                    <div class="col col-md-3">
                                        <label htmlFor="input2">Mô tả thay đổi</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" className="form-control" id="inputDes" disabled={!this.state.isEdit} />
                                    </div>
                                </div>
                                <div className="row form-group">
                                    <div class="col col-md-3">
                                        <label htmlFor="input2">Ngày diễn ra hoạt động</label>
                                    </div>
                                    <div className="col-sm-9">
                                        <DatePicker
                                            selected={endDate}
                                            onChange={this.onEndSelect}
                                        />
                                    </div>
                                </div>
                            </>
                        );
                    })
                }
                {
                    isEdit && <div className="text-center mt-25">
                        <FileBase64
                            multiple={true}
                            onDone={this.getFiles.bind(this)} />
                    </div>
                }
                <div className="container m-t-20">
                    <div className="row col-lg-12" style={{ display: 'flex', justifyContent: 'center' }}>
                        {
                            files.map((item, i) => {
                                return (
                                    <div className="col-lg-6 col-md-6 fadeInUp" >
                                        <figure>
                                            <a data-lightbox="portfolio" href={item} className="link-preview" title="Preview">
                                                <img style={{ width: "100%" }} src={item} className="img-fluid" alt="" /></a>
                                        </figure>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
                {
                    isEdit && <div className="text-center">
                        <button className="btn btn-primary" onClick={() => this.postActivityData()}>Xác nhận</button>
                    </div>
                }
            </div>
        );
    }
}

export default EditActivity;
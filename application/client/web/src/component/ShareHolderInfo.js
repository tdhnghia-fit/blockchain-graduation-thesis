import React, { Component } from 'react';
import axios from 'axios'
import API from './../API/API.js'
import CONSTANT from './../Constant'
import AppUtils from '../Utils/AppUtils.js';
import { Modal } from 'react-bootstrap'
import { NavLink } from "react-router-dom";
import UserRecentActivity from './UserRecentActivity.js';
import UserActivityRatioChart from './UserActivityRatioChart.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faIdCard, faEnvelope, faUserFriends, faCalendarDay } from '@fortawesome/free-solid-svg-icons'

class ShareHolderInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            username: "",
            isLoading: false,
            isShowRecentActivity: false
        }

        this.recentButtonClick = this.recentButtonClick.bind(this)
    }

    componentWillMount() {
        let idUser = this.props.match.params.id
        // let idUser = localStorage.getItem(CONSTANT.USER_LOGIN_TOKEN)
        let url = API.GET_USER + idUser
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.setState({
                            data: res.data,
                            isLoading: true,
                            username: res.data.name
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    querryActivityByUser() {

    }

    recentButtonClick() {
        let oldState = this.state.isShowRecentActivity
        this.setState({
            isShowRecentActivity: !oldState
        })
    }

    render() {
        var { data, isLoading, isShowRecentActivity } = this.state
        let role = ""
        if (data.roles != undefined) {
            role = AppUtils.getStrRole(data.roles[0].name)
        }
        let src = localStorage.getItem("image")
        let defaultSrc = src
        return (
            isLoading && <section id="about">
                <div className="container">
                    <header className="section-header">
                        <h3>Thông tin người dùng </h3>
                    </header>
                    <div className="row">
                        <div className="col-lg-4 col-md-4">
                            <div className="card card-user">
                                <div className="image">
                                    <img src={defaultSrc} alt="..." />
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-8 col-md-8">
                            <div className="card">
                                <div className="card-body text-left">
                                    <div className="form-group">
                                        <label htmlFor="cc-payment" className="control-label mb-1">
                                            <FontAwesomeIcon className="custom-icon" icon={faEnvelope} /> Email</label>
                                        <input id="cc-pament" name="cc-payment" type="text" className="form-control" aria-required="true" aria-invalid="false" defaultValue={data.email} disabled />
                                    </div>
                                    <div className="form-group has-success">
                                        <label htmlFor="username" className="control-label mb-1">
                                            <FontAwesomeIcon className="custom-icon" icon={faUser} />Tên</label>
                                        <input id="username" name="username" type="text" className="form-control cc-name valid" defaultValue={data.name} disabled />
                                        <span className="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="cc-number" className="control-label mb-1">
                                            <FontAwesomeIcon className="custom-icon" icon={faIdCard} />Định danh</label>
                                        <input id="name" name="name" type="tel" className="form-control" defaultValue={data.id} disabled />
                                        <span className="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true" />
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="form-group">
                                                <label htmlFor="cc-exp" className="control-label mb-1">
                                                    <FontAwesomeIcon className="custom-icon" icon={faUserFriends} />Vai trò</label>
                                                <div className="input-group">
                                                    <input id="x_card_code" className="form-control" defaultValue={role} disabled />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <button type="submit" className="btn btn-primary btn-sm m-r-10" onClick={this.recentButtonClick}>
                                        <i className="fa fa-dot-circle-o"></i> Hoạt động tham gia
                                        </button> */}
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-12 m-t-30">
                            {
                                <UserRecentActivity actorId={data.name} />
                            }
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default ShareHolderInfo;
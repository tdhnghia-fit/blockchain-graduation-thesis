import React, { Component } from 'react';
import AdminSidebar from '../admin/AdminSidebar'
import MainContent from '../admin/MainContent'
import { Redirect } from 'react-router-dom'
import CONSTANT from '../Constant'

class AdminNavigate extends Component {

    constructor(props) {
        super(props);
        const token = localStorage.getItem(CONSTANT.USER_LOGIN_TOKEN)
        const role = localStorage.getItem(CONSTANT.USER_LOGIN_ROLE)
        let loggedIn = true
        if (token == null) {
            loggedIn = false
        }

        this.state = {
            loggedIn: loggedIn,
            role: role
        }
    }

    render() {
        if (this.state.loggedIn == false) {
            return <Redirect to="/" />
        }
        if (this.state.role != CONSTANT.ROLE_ADMIN && this.state.role != CONSTANT.ROLE_ACCOUNTANT && this.state.role != CONSTANT.ROLE_AUDITOR) {
            return <Redirect to="/" />
        }
        return (
            <>
                <AdminSidebar />
                <MainContent />
            </>
        );
    }
}

export default AdminNavigate;
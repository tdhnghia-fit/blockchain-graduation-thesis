import React, { Component } from 'react';
import { Bar, Line, Pie } from 'react-chartjs-2';
import API from './../API/API.js'
import AppUtils from '../Utils/AppUtils';
import axios from 'axios'
import Select from 'react-select';
import CONSTANT from '../Constant.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import {
    ChartComponent, SeriesCollectionDirective, SeriesDirective, Inject,
    LineSeries, DateTime, Legend, Tooltip, ILoadedEventArgs, ChartTheme, Category
} from '@syncfusion/ej2-react-charts';
import { Browser } from '@syncfusion/ej2-base';
import { groupTypeOptions1, groupStyles, groupBadgeStyles, typeParent } from '../Utils/DataGeneral'
import DatePicker from "react-datepicker";
import { NavLink } from "react-router-dom";
import "react-datepicker/dist/react-datepicker.css";

const formatGroupLabel = data => (
    <div style={groupStyles}>
        <span>{data.label}</span>
        <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
);

class ActivityChartByType extends Component {
    constructor(props) {
        super(props);
        this.state = {
            start: CONSTANT.NO_FILTER,
            end: CONSTANT.NO_FILTER,
            startDate: "",
            endDate: "",
            titleChart: '',
            typeChildLv2: [],
            typeChildLv3: [],
            type3Filter: [],
            actorIds: [],
            typeParent: CONSTANT.NO_FILTER,
            typeLv2: CONSTANT.NO_FILTER,
            typeLv3: CONSTANT.NO_FILTER,
            data: [],
            isLoading: false,
            actor: CONSTANT.NO_FILTER,
            actorApprove: CONSTANT.NO_FILTER,
        }
    }

    componentWillMount() {
        this.getDataFromServer()
        this.getActorId()
        this.getTypeChildLv2()
        const { type, start, end } = this.props
        let t = CONSTANT.NO_FILTER
        if (type != undefined) t = type
        let s = CONSTANT.NO_FILTER
        if (start != undefined) s = start
        let e = CONSTANT.NO_FILTER
        if (end != undefined) e = end
        this.setState({
            typeParent: t,
            start: s,
            end: e
        })
    }

    getData() {
        let param = ""
        const { idChart } = this.props
        const { start, end, typeParent, typeLv2, typeLv3, actor, actorApprove } = this.state

        let typeStr = ""
        if (typeParent != CONSTANT.NO_FILTER) typeStr = typeParent + ":"
        if (typeLv2 != CONSTANT.NO_FILTER && typeParent != CONSTANT.NO_FILTER) typeStr += typeLv2
        else if (typeLv2 != CONSTANT.NO_FILTER && typeParent == CONSTANT.NO_FILTER) typeStr += ":" + typeLv2
        if (typeStr != "")
            param += "?type=" + typeStr

        if (actor != CONSTANT.NO_FILTER && typeLv2 == CONSTANT.NO_FILTER
            && typeParent == CONSTANT.NO_FILTER)
            param += "?actorId=" + actor
        else if (actor != CONSTANT.NO_FILTER) param += "&actorId=" + actor

        if (actorApprove != CONSTANT.NO_FILTER && actor == CONSTANT.NO_FILTER && typeLv2 == CONSTANT.NO_FILTER
            && typeParent == CONSTANT.NO_FILTER)
            param += "?actorApprovedId=" + actorApprove
        else if (actorApprove != CONSTANT.NO_FILTER)
            param += "&actorApprovedId=" + actorApprove

        if (start != CONSTANT.NO_FILTER && actorApprove == CONSTANT.NO_FILTER && actor == CONSTANT.NO_FILTER && typeLv2 == CONSTANT.NO_FILTER
            && typeParent == CONSTANT.NO_FILTER)
            param += "?start=" + start
        else if (start != CONSTANT.NO_FILTER)
            param += "&start=" + start

        if (end != CONSTANT.NO_FILTER)
            param += "&end=" + end

        let url = API.API_USER_CHART + param
        console.log("idChart= ", idChart, url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    console.log("getDataFromServer - sucess", res.data)
                    let sortedArr = AppUtils.sortListChart(res.data, false)
                    this.setState({
                        data: this.convertDataToChartModel(sortedArr),
                        isLoading: true
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    getDataFromServer() {
        this.setState({
            data: [],
            isLoading: false
        }, function () {
            this.getData()
        })
    }

    convertDataToChartModel(data) {
        let res = []
        data.forEach(element => {
            let obj = {
                x: element.label, y: element.amount
            }
            res.push(obj)
        })
        return res
    }

    getActorId() {
        let url = API.GET_ACTORID
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    let actorIdOption = AppUtils.convertActorIdToSelectModel(res.data, true)
                    this.setState({
                        actorIds: actorIdOption
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    onTypeParentSelect(value) {
        let type = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.label
        let str = ""
        if (type == CONSTANT.TYPE_CHI) str = CONSTANT.TYPE_OUTPUT
        else if (type == CONSTANT.TYPE_THU) str = CONSTANT.TYPE_INPUT
        console.log("onTypeParentSelect", str)
        this.setState({
            typeParent: str
        }, this.getDataFromServer)
    }


    getTypeChildLv2() {
        let url = API.GET_TYPE_LV2
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        const { type } = this.props
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    let typeLv2 = res.data
                    let start = 0
                    let end = 5
                    if (type == CONSTANT.TYPE_OUTPUT) {
                        start = 5
                        end = 11
                    }

                    let sortedArr = AppUtils.sortListById(res.data, false)
                    let arr = sortedArr.slice(start, end)
                    this.setState({
                        typeChildLv2: AppUtils.convertTypeToSelectModel(arr, true),
                        isLoadingType2: true
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    onActorIdSelect(value) {
        let type = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        console.log("onActorIdSelect", type)
        this.setState({
            actor: type
        }, this.getDataFromServer)
    }

    onActorApprovedIdSelect(value) {
        let type = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        console.log("onActorApprovedIdSelect", type)
        this.setState({
            actorApprove: type
        }, this.getDataFromServer)
    }

    onTypeLv2Select(value) {
        let type2 = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        console.log("onTypeLv2Select", type2)
        this.setState({
            typeLv2: type2
        }, function () {
            this.getDataFromServer();
        })
    }

    onStartSelect = date => {
        let time = date.getTime()
        console.log("start", time)

        this.setState({
            startDate: date,
            start: time,
            enableFilter: this.state.end != CONSTANT.NO_FILTER
        }, function () {
            this.getDataFromServer();
        })
    };

    onEndSelect = date => {
        let time = date.getTime()
        console.log("end", time)
        this.setState({
            endDate: date,
            end: time,
            enableFilter: this.state.start != CONSTANT.NO_FILTER
        }, function () {
            this.getDataFromServer();
        })
    };

    onChartLoad() {
        const { idChart, type, start } = this.props
        let chart = document.getElementById(idChart);
        chart.setAttribute('title', '');
    };

    render() {
        const { titleChart, actorIds, data, typeChildLv2, typeChildLv3, isLoading, isLoadingType2, isLoadingType3, typeLv2, type3Filter } = this.state;
        const { type, start, idChart } = this.props
        let userRole = localStorage.getItem(CONSTANT.USER_LOGIN_ROLE)
        let isFilterTypeLv2 = typeLv2 != CONSTANT.NO_FILTER
        return (
            <>
                <h3 className="title-2 m-b-40 m-t-30">Biểu đồ thể hiện tăng giảm theo các loại hoạt động</h3>
                <div className="row m-b-20 m-t-50">
                    <div className="col-lg-4 col-md-4">
                        <div className="table-data__tool">
                            <div className="table-data__tool-left">
                                <div className="rs-select2--light rs-select2--sm m-t-20">
                                    <Select
                                        onChange={this.onTypeLv2Select.bind(this)}
                                        options={typeChildLv2}
                                        formatGroupLabel={formatGroupLabel}
                                        placeholder="Chi tiết"
                                    />
                                </div>
                                <div className="rs-select2--light rs-select2--sm m-t-20">
                                    <Select
                                        onChange={this.onActorIdSelect.bind(this)}
                                        options={actorIds}
                                        formatGroupLabel={formatGroupLabel}
                                        placeholder="Người thực hiện"
                                    />
                                </div>
                                <div class="rs-select2--light rs-select2--sm m-t-20">
                                    <DatePicker
                                        placeholderText="Từ ngày"
                                        selected={this.state.startDate}
                                        onChange={this.onStartSelect}
                                    />

                                </div>

                                <div class="rs-select2--light rs-select2--sm m-t-20">
                                    <DatePicker
                                        placeholderText="Đến ngày"
                                        selected={this.state.endDate}
                                        onChange={this.onEndSelect}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    {
                        isLoading && <div className="text-center">
                            <ChartComponent id={idChart} style={{ width: "100%" }}
                                primaryXAxis={{
                                    majorGridLines: { width: 0 },
                                    minorGridLines: { width: 0 },
                                    majorTickLines: { width: 0 },
                                    minorTickLines: { width: 0 },
                                    interval: 1,
                                    lineStyle: { width: 0 },
                                    valueType: 'Category'
                                }}
                                primaryYAxis={{
                                    title: '', lineStyle: { width: 0 },
                                    // minimum: 0, maximum: 400,
                                    majorTickLines: { width: 0 },
                                    majorGridLines: { width: 1 },
                                    minorGridLines: { width: 1 },
                                    minorTickLines: { width: 0 },
                                    labelFormat: '{value}',
                                }}
                                chartArea={{ border: { width: 0 } }}
                                tooltip={{ enable: true }}
                                width={Browser.isDevice ? '100%' : '60%'}
                                title={titleChart}
                                loaded={this.onChartLoad.bind(this)}>
                                <Inject services={[LineSeries, Category, Legend, Tooltip]} />
                                <SeriesCollectionDirective>
                                    <SeriesDirective dataSource={data} xName='x' yName='y'
                                        width={2} marker={{ visible: true, width: 10, height: 10 }} type='Line'>
                                    </SeriesDirective>
                                </SeriesCollectionDirective>
                            </ChartComponent>
                        </div>
                    }
                </div>
            </>
        );
    }
}

export default ActivityChartByType;
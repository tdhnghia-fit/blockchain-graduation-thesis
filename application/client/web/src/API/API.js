const API = {

    // API TEST

    AUDITOR_VERIFY_FINANCIAL_STATEMENT_BY_ID: `http://localhost:8080/api/fabfund/financialstatements/verify/`,
    // ------------------------------------------------------------------

    // GET_YEAR: `http://localhost:3333/year`,

    // GET_INPUT_ACTIVITY: "http://localhost:3333/inputactivity",

    // GET_OUTPUT_ACTIVITY: "http://localhost:3333/outputactivity",

    // GET_STATEMENT_BY_YEAR: "http://localhost:3333/statementbyYear",

    // GET_STATEMENT_BY_QUARTER: "http://localhost:3333/statementbyMonth",

    // GET_USER_ROLE_ADMIN: "http://localhost:3333/getuseradmin",

    // GET_USER_ROLE_USER: "http://localhost:3333/getuser",

    // CHANGE_PASSWORD: `http://localhost:3333/updateinfo/`,

    CASH_FLOW_CHART_BY_YEAR: `http://localhost:8080/api/fabfund/cashflowstatements/charts`,

    CASH_FLOW_CHART_BY_QUARTER: `http://localhost:8080/api/fabfund/cashflowstatements/charts`,

    // GET_OUTPUT_CHART: `http://localhost:3333/outputchart`,

    // GET_INPUT_OUTPUT_CHART: `http://localhost:3333/inputoutputchart`,

    GET_INCOME_CHART: `http://localhost:8080/api/fabfund/incomesstatements/charts`,

    GET_FINANCIAL_CHART: `http://localhost:8080/api/fabfund/financialstatements/charts`,

    // DOWNLOAD_STATEMENT_pdf: `http://localhost:3333/downloadstatement/`,

    GET_TYPE_LV2: `http://localhost:8080/api/fabfund/types/level2`,

    GET_TYPE_LV3: `http://localhost:8080/api/fabfund/types/level3`,

    GET_ACTORID: `http://localhost:8080/api/admin/users/actorId`,

    GET_ACTIVITY_BY_USER: `http://localhost:8080/api/fabfund/activities/actorId`,

    GET_OVERVIEW: `http://localhost:8080/api/fabfund/overview`,

    API_USER_CHART: `http://localhost:8080/api/fabfund/activities/chart`,

    ///// test
    API_USER_LOGIN: `http://localhost:3334/userlogin`,

    API_ADMIN_LOGIN: `http://localhost:3334/adminlogin`,

    API_SHAREHOLDER_LOGIN: `http://localhost:3334/shareholderlogin`,

    // API_STATEMENT_TEST: `http://localhost:3333/report1`,

    INCOME_CHART_BY_YEAR: `http://localhost:3334/incomechartbyyear`,

    // API TEST
    // GET_ALL_ACTIVITIES: `http://www.mocky.io/v2/5e915c833300005d00e9ce62?fbclid=IwAR0SU6E8_ppvjHzap1n5_FNketooOWj00Pnm5JaUH7a6cCs92ZzNORqd0YQ`,
    // GET_ACTIVIVTY_BY_PAGE: `http://localhost:3333/activities`,
    // GET_ACTIVITY_BY_ID: `http://localhost:3333/activity/`,
    // GET_HISTORY_ACTIVITY: `http://localhost:3333/activityhistory`,
    // PUT_ACTIVITY: `http://localhost:3333/activity/`,
    // POST_ACTIVITY: `http://localhost:3333/activity`,
    // DEL_ACTIVITY: `http://localhost:3333/activity/`,

    // GET_ALL_STATEMENT_BY_PAGE: `http://localhost:3333/report`,
    // GET_STATEMENT_BY_ID: `http://localhost:3333/statement/`,
    // PUT_STATEMENT_BY_ID: `http://localhost:3333/statement/`,
    // DEL_STATEMENT_BY_ID: `http://localhost:3333/statement/`,
    // POST_STATEMENT: `http://localhost:3333/statement`,
    // GET_HISTORY_REPORT: `http://localhost:3333/statementhistory`,

    GET_ALL_STATEMENT_BY_PAGE: `http://localhost:8080/api/fabfund/statements`,
    GET_STATEMENT_BY_ID: `http://localhost:8080/api/fabfund/statements/`,
    PUT_STATEMENT_BY_ID: `http://localhost:8080/api/fabfund/statements/`,
    DEL_STATEMENT_BY_ID: `http://localhost:8080/api/fabfund/statements/`,
    POST_STATEMENT: `http://localhost:8080/api/fabfund/statements`,
    GET_HISTORY_REPORT: `http://localhost:8080/api/fabfund/statements/history/`,

    GET_FINANCIAL_STATEMENT_BY_ID: `http://localhost:8080/api/fabfund/financialstatements/`,
    PUT_FINANCIAL_STATEMENT_BY_ID: `http://localhost:8080/api/fabfund/financialstatements/`,
    POST_FINANCIAL_STATEMENT: `http://localhost:8080/api/fabfund/financialstatements/`,
    GET_HISTORY_FINANCIAL_STATEMENT: `http://localhost:8080/api/fabfund/financialstatements/history/`,

    GET_INCOME_STATEMENT_BY_ID: `http://localhost:8080/api/fabfund/incomesstatements/`,
    PUT_INCOME_STATEMENT_BY_ID: `http://localhost:8080/api/fabfund/incomesstatements/`,
    POST_INCOME_STATEMENT: `http://localhost:8080/api/fabfund/incomesstatements/`,
    GET_HISTORY_INCOME_STATEMENT: `http://localhost:8080/api/fabfund/incomesstatements/history/`,

    GET_CASHFLOW_STATEMENT_BY_ID: `http://localhost:8080/api/fabfund/cashflowstatements/`,
    PUT_CASHFLOW_STATEMENT_BY_ID: `http://localhost:8080/api/fabfund/cashflowstatements/`,
    POST_CASHFLOW_STATEMENT: `http://localhost:8080/api/fabfund/cashflowstatements/`,
    GET_HISTORY_CASHFLOW_STATEMENT: `http://localhost:8080/api/fabfund/cashflowstatements/history/`,

    GET_ME: `http://localhost:8080/api/user/me/`,
    GET_USER: `http://localhost:8080/api/admin/users/`,
    POST_ACCOUNT: `http://localhost:8080/api/admin/users`,
    PUT_ACCOUNT: `http://localhost:8080/api/admin/users/`,
    DEL_USER: "http://localhost:8080/api/admin/users/", 

    // LOGIN: `http://localhost:3333/userloggedin`,

    GET_ROLES: `http://localhost:8080/api/admin/users/roles`,

    UPDATE_ROLE: `http://localhost:8080/api/admin/users/roles/`,

    UPDATE_INFO: `http://localhost:8080/api/admin/users/`,

    
    // API AT BE SIDE
    GET_ALL_ACTIVITIES: `http://localhost:8080/api/fabfund/activities`,
    GET_ACTIVITY_BY_PAGE: `http://localhost:8080/api/fabfund/activities`,
    GET_ACTIVITY_BY_ID: `http://localhost:8080/api/fabfund/activities/`,
    GET_HISTORY_ACTIVITY: `http://localhost:8080/api/fabfund/activities/history/`,
    PUT_ACTIVITY: `http://localhost:8080/api/fabfund/activities/`,
    POST_ACTIVITY: `http://localhost:8080/api/fabfund/activities`,
    DEL_ACTIVITY: `http://localhost:8080/api/fabfund/activities/`,
    
    // GET_ALL_STATEMENT_BY_PAGE: `http://localhost:8080/api/fabfund/statements`,
    // GET_STATEMENT_BY_ID: `http://localhost:8080/api/fabfund/statements/`,
    // PUT_STATEMENT_BY_ID: `http://localhost:8080/api/fabfund/statements/`,
    // DEL_STATEMENT_BY_ID: `http://localhost:8080/api/fabfund/statements/`,
    // POST_STATEMENT: `http://localhost:8080/api/fabfund/statements`,

    // GET_FINANCIAL_STATEMENT_BY_ID: `http://localhost:3333/financial/`,
    // PUT_FINANCIAL_STATEMENT_BY_ID: `http://localhost:3333/financial/`,
    // POST_FINANCIAL_STATEMENT: `http://localhost:3333/financial`,
    // GET_HISTORY_FINANCIAL_STATEMENT: `http://localhost:3333/financialhistory/`,

    // GET_INCOME_STATEMENT_BY_ID: `http://localhost:3333/income/`,
    // PUT_INCOME_STATEMENT_BY_ID: `http://localhost:3333/income/`,
    // POST_INCOME_STATEMENT: `http://localhost:3333/income`,
    // GET_HISTORY_INCOME_STATEMENT: `http://localhost:3333/incomehistory/`,

    // GET_CASHFLOW_STATEMENT_BY_ID: `http://localhost:3333/cashflow/`,
    // PUT_CASHFLOW_STATEMENT_BY_ID: `http://localhost:3333/cashflow/`,
    // POST_CASHFLOW_STATEMENT: `http://localhost:3333/cashflow`,
    // GET_HISTORY_CASHFLOW_STATEMENT: `http://localhost:3333/cashflowhistory/`,

    // GET_USER: `http://localhost:8080/api/user/me/`,
    // POST_ACCOUNT: `http://localhost:3333/user`,
    // PUT_ACCOUNT: `http://localhost:3333/user/`,
    // DEL_USER: "http://localhost:3333/user/", 

    LOGIN: `http://localhost:8080/api/auth/signin`,
}

export default API

import React, { Component } from 'react';
import NumberFormat from 'react-number-format';

class IncomeHisDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            files: [],
            isLoading: false
        }
    }

    componentWillMount() {
        const { itemDetail } = this.props
        console.log("itemDetail", itemDetail)
        let proof = ""
        if (itemDetail.proofImage != undefined) proof = itemDetail.proofImage
        this.setState({
            files: [{ "base64": proof }],
            isLoading: true
        })
    }
    render() {
        const { isLoading } = this.state

        return (
            isLoading && <div className="container">
                <header className="section-header m-b-50">
                    <h3 className="section-title">Chi tiết hoạt động</h3>
                </header>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card" >
                            <div className="card-body">
                                <form style={{ width: "100%" }}>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Tổng doanh thu</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control" >
                                                        <NumberFormat value={this.props.itemDetail.totalRevenue} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Doanh thu chính</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control">
                                                        <NumberFormat value={this.props.itemDetail.coreRevenue} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Doanh thu tài chính</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control"  >
                                                        <NumberFormat value={this.props.itemDetail.financialRevenue} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Doanh thu khác</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control" id="inputLongTermDebt">
                                                        <NumberFormat value={this.props.itemDetail.otherRevenue} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Lợi nhuận trước thuế</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control">
                                                        <NumberFormat value={this.props.itemDetail.profitBeforeTax} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Lợi nhuận sau thuế</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control" >
                                                        <NumberFormat value={this.props.itemDetail.profitAfterTax} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div className="text-center" style={{ paddingTop: "10px" }}>
                                    {
                                        this.state.files.length > 0 ? this.state.files.map((file, i) => {
                                            return <img style={{ width: "100%", marginBottom: "20px" }} key={i} defaultValue={this.props.image} src={file.base64} />
                                        }) : <img style={{ width: "100%", marginBottom: "20px" }} src="????" />
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default IncomeHisDetail;
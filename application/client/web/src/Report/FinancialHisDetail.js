import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import AppUtils from '../Utils/AppUtils';

class FinancialHisDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            files: [],
            isLoading: false
        }
    }

    componentWillMount() {
        const { itemDetail } = this.props
        console.log("itemDetail", itemDetail)
        let proof = ""
        console.log(this.props)
        if (itemDetail.proofImage != undefined) proof = itemDetail.proofImage
        this.setState({
            files: [{ "base64": proof }],
            isLoading: true
        })
    }

    render() {
        const { isLoading } = this.state
        const { year, quarter, isAuditor } = this.props
        let str = " - Q"
        let quarterStr = str + quarter + ""
        if (quarter == 5) quarterStr = ""
        let isShow = true
        if (year == undefined) isShow = false
        return (
            isLoading && <div className="container">
                <header className="section-header m-b-50">
                    <h3 className="section-title">Chi tiết hoạt động</h3>
                </header>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card" >
                            <div className="card-body">
                                <form style={{ width: "100%" }}>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Ý kiến Kiểm toán viên</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control" >
                                                        {this.props.itemDetail.auditorOption}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Nợ ngắn hạn</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control"  >
                                                        <NumberFormat value={this.props.itemDetail.shortTermDebt} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Nợ dài hạn</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control" id="inputLongTermDebt">
                                                        <NumberFormat value={this.props.itemDetail.longTermDebt} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Tổng nợ</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control">
                                                        <NumberFormat value={this.props.itemDetail.totalDebt} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Vốn chủ sở hữu</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control">
                                                        <NumberFormat value={this.props.itemDetail.initialCapital} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Tổng tài sản</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control">
                                                        <NumberFormat value={this.props.itemDetail.totalAsset} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {
                                            isShow && <div className="col-md-6">
                                                <div className="form-group row">
                                                    <label className="col-sm-12 col-form-label">Năm</label>
                                                    <div className="col-sm-12">
                                                        <input type="text" className="form-control" defaultValue={year + quarterStr} disabled />
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                        {
                                            !isShow && <div className="col-md-6">
                                                <div className="form-group row">
                                                    <label className="col-sm-12 col-form-label">Ngày cập nhật</label>
                                                    <div className="col-sm-12">
                                                        <input type="text" className="form-control" defaultValue={AppUtils.toDateTime(this.props.itemDetail.lastCreateAt)} disabled />
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                    </div>
                                </form>
                                <div className="text-center" style={{ paddingTop: "10px" }}>
                                    {
                                        this.state.files.length > 0 ? this.state.files.map((file, i) => {
                                            return <img style={{ width: "100%", marginBottom: "20px" }} key={i} defaultValue={this.props.image} src={file.base64} />
                                        }) : <img style={{ width: "100%", marginBottom: "20px" }} src="????" />
                                    }
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default FinancialHisDetail;
import React, { Component } from 'react';
import NumberFormat from 'react-number-format';

class CashflowHisDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            files: [],
            isLoading: false
        }
    }

    componentWillMount() {
        const { itemDetail } = this.props
        console.log("itemDetail", itemDetail)
        let proof = ""
        console.log(this.props)
        if (itemDetail.proofImage != undefined) proof = itemDetail.proofImage
        this.setState({
            files: [{ "base64": proof }],
            isLoading: true
        })
    }

    render() {
        const { isLoading } = this.state

        return (
            isLoading && <div className="container">
                <header className="section-header m-b-50">
                    <h3 className="section-title">Chi tiết hoạt động</h3>
                </header>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card" >
                            <div className="card-body">
                                <form style={{ width: "100%" }}>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Chi phí hoạt động Doanh nghiệp</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control" >
                                                        <NumberFormat value={this.props.itemDetail.outputActivityBusiness} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Thu nhập hoạt động Doanh nghiệp</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control">
                                                        <NumberFormat value={this.props.itemDetail.inputActivityBusiness} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Chi phí hoạt động Đầu tư</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control"  >
                                                        <NumberFormat value={this.props.itemDetail.outputActivityInvestment} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Thu nhập hoạt động Đầu tư</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control" id="inputLongTermDebt">
                                                        <NumberFormat value={this.props.itemDetail.inputActivityInvestment} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Chi phí hoạt động Tài chính</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control">
                                                        <NumberFormat value={this.props.itemDetail.outputActivityFinancial} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Thu nhập hoạt động Tài chính</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control" >
                                                        <NumberFormat value={this.props.itemDetail.inputActivityFinancial} displayType={'text'} thousandSeparator={true} prefix={''} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div className="text-center" style={{ paddingTop: "10px" }}>
                                    {
                                        this.state.files.length > 0 ? this.state.files.map((file, i) => {
                                            return <img style={{ width: "100%", marginBottom: "20px" }} key={i} defaultValue={this.props.image} src={file.base64} />
                                        }) : <img style={{ width: "100%", marginBottom: "20px" }} src="????" />
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CashflowHisDetail;
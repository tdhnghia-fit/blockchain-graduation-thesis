import React, { Component } from 'react';
import API from './../API/API.js'
import axios from 'axios'
import AppUtils from '../Utils/AppUtils';
import FileBase64 from '../component/FileBase64';
import NumberFormat from 'react-number-format';
import swal from '@sweetalert/with-react'

class EditIncome extends Component {

    constructor(props) {
        super(props);
        this.state = {
            files: [],
            item: [],
            isLoading: false
        }
    }

    getFiles(files) {
        if (Array.isArray(files))
            this.setState({ files: files })
        else {
            let file = [files]
            this.setState({ files: file })
        }
    }

    componentWillMount() {
        const { itemDetail, itemId } = this.props
        itemDetail.id = itemId
        let proof = ""
        console.log("itemDetail", itemDetail)
        if (itemDetail.proofImage != undefined) proof = itemDetail.proofImage
        this.setState({
            item: itemDetail,
            files: [{ "base64": itemDetail.proofImage }],
            isLoading: true
        })
    }

    onCompareData(newData, oldData) {
        if (newData.totalRevenue != oldData.totalRevenue || newData.coreRevenue != oldData.coreRevenue
            || newData.financialRevenue != oldData.financialRevenue || newData.otherRevenue != oldData.otherRevenue
            || newData.profitBeforeTax != oldData.profitBeforeTax || newData.profitAfterTax != oldData.profitAfterTax
            || newData.proofImage != oldData.proofImage) {
            return true
        }
        return false
    }

    async postActivityData() {
        const { itemDetail, itemId, onEditCallback, isEdit, year, quarter } = this.props
        itemDetail.id = itemId
        let currentDate = new Date()
        let currentMiliSecs = currentDate.getTime()
        let proofImage = this.state.files.map((file, i) => file.base64)
        let newData = {
            id: itemId + "",
            totalRevenue: document.getElementById("inputTotalRevenue").value + "",
            coreRevenue: document.getElementById("inputCoreRevenue").value + "",
            financialRevenue: document.getElementById("inputFinancialRevenue").value + "",
            otherRevenue: document.getElementById("inputOtherRevenue").value + "",
            profitBeforeTax: document.getElementById("inputProfitBeforeTax").value + "",
            profitAfterTax: document.getElementById("inputProfitAfterTax").value + "",
            proofImage: proofImage[0],
            year: year,
            quarter: quarter,
            createAt: currentMiliSecs + "",
            lastCreateAt: itemDetail.createAt
        }
        console.log("newData: ", newData)

        let isEqual = this.onCompareData(newData, itemDetail)
        let method = 'put'
        let url = API.PUT_INCOME_STATEMENT_BY_ID + itemId
        console.log("put - url", url)

        if (isEdit === 'Thêm mới') {
            method = 'post'
            url = API.POST_INCOME_STATEMENT
        }

        if (isEqual) {
            let authorization = AppUtils.HEADER_AUTHORIZATION()
            console.log("authorization", authorization)
            console.log("url", method, API.PUT_INCOME_STATEMENT_BY_ID + itemId)
            axios({
                method: method,
                url: url,
                data: newData,
                headers: {
                    "Authorization": authorization,
                    "Content-Type": "application/json"
                }
            }).then(res => {
                console.log(res);
                if (res.data.status == 200 || res.data.return_code == undefined) {
                    if (res.error === 'Unauthorized')
                        AppUtils.logout()
                    else {
                        swal("Hoàn tất!", "Chỉnh sửa hoạt động thành công.", "success");
                        onEditCallback(newData)
                    }
                } else {
                    swal("Có lỗi xảy ra!", "", "warning");
                }
            })
                .catch(response => {
                    swal("Không thành công!", "Có lỗi xảy ra trong quá trình chỉnh sửa.", "warning");
                    if (response.error === 'Unauthorized')
                        AppUtils.logout()
                });
        } else {
            alert("Không có thay đổi")
        }
    }

    render() {
        const { year, quarter } = this.props
        const { isLoading } = this.state
        let quarterStr = quarter + ""
        if (quarter == 5) quarterStr = "Cả năm"

        return (
            isLoading && <div className="container">
                <header className="section-header m-b-50">
                    <h3 className="section-title">Chi tiết hoạt động</h3>
                </header>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card" >
                            <div className="card-body">
                                <form style={{ width: "100%" }}>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Tổng doanh thu</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="inputTotalRevenue" defaultValue={this.props.itemDetail.totalRevenue} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Doanh thu chính</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="inputCoreRevenue" defaultValue={this.props.itemDetail.coreRevenue} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Doanh thu tài chính</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="inputFinancialRevenue" defaultValue={this.props.itemDetail.financialRevenue} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Doanh thu khác</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="inputOtherRevenue" defaultValue={this.props.itemDetail.otherRevenue} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Lợi nhuận trước thuế</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="inputProfitBeforeTax" defaultValue={this.props.itemDetail.profitBeforeTax} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Lợi nhuận sau thuế</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="inputProfitAfterTax" defaultValue={this.props.itemDetail.profitAfterTax} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Năm</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control">
                                                        {year}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Quý</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control" >
                                                        {quarterStr}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <p>Thêm hình ảnh</p>
                                <div className="form-group text-center mt-25">
                                    <FileBase64
                                        multiple={false}
                                        onDone={this.getFiles.bind(this)} />
                                </div>
                                <div className="text-center" style={{ paddingTop: "10px" }}>
                                    {
                                        this.state.files.length > 0 ? this.state.files.map((file, i) => {
                                            return <img style={{ width: "100%", marginBottom: "20px" }} key={i} defaultValue={this.props.image} src={file.base64} />
                                        }) : <img style={{ width: "100%", marginBottom: "20px" }} src="????" />
                                    }
                                </div>
                                {
                                    <div className="text-center m-t-20">
                                        <button className="btn btn-primary" onClick={() => this.postActivityData()}>Xác nhận</button>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default EditIncome;
import React, { Component } from 'react';
import API from './../API/API.js'
import axios from 'axios'
import AppUtils from '../Utils/AppUtils';
import FileBase64 from '../component/FileBase64';
import NumberFormat from 'react-number-format';
import swal from '@sweetalert/with-react'

class EditFinancial extends Component {

    constructor(props) {
        super(props);
        this.state = {
            files: [],
            item: [],
            isPut: true
        }
    }

    getFiles(files) {
        if (Array.isArray(files))
            this.setState({ files: files })
        else {
            let file = [files]
            this.setState({ files: file })
        }
    }

    componentWillMount() {
        const { itemDetail, isPut } = this.props
        let proof = ""
        console.log(this.props)
        if (itemDetail.proofImage != undefined) proof = itemDetail.proofImage
        this.setState({
            files: [{ "base64": proof }],
            isPut: isPut
        })
    }

    onCompareData(newData, oldData) {
        if (newData.auditorOption != oldData.auditorOption || newData.totalDebt != oldData.totalDebt
            || newData.shortTermDebt != oldData.shortTermDebt || newData.longTermDebt != oldData.longTermDebt
            || newData.initialCapital != oldData.initialCapital || newData.totalAsset != oldData.totalAsset
            || newData.proofImage != oldData.proofImage) {
            return true
        }
        return false
    }

    async postActivityData() {
        const { itemDetail, itemId, onEditCallback, isEdit, year, quarter, isAuditor } = this.props
        itemDetail.id = itemId
        let currentDate = new Date()
        let currentMiliSecs = currentDate.getTime()
        let proofImage = this.state.files.map((file, i) => file.base64)
        let dataresponse = {
            id: itemId + "",
            auditorOption: document.getElementById("inputAuditorOption").value + "",
            totalDebt: document.getElementById("inputTotalDebt").value + "",
            totalAsset: document.getElementById("inputTotalAsset").value + "",
            initialCapital: document.getElementById("inputInitialCapital").value + "",
            longTermDebt: document.getElementById("inputLongTermDebt").value + "",
            shortTermDebt: document.getElementById("inputShortTermDebt").value + "",
            proofImage: proofImage[0],
            year: year,
            quarter: quarter,
            createAt: currentMiliSecs + "",
            lastCreateAt: itemDetail.createAt
        }


        let method = 'put'
        let url = API.PUT_FINANCIAL_STATEMENT_BY_ID + itemId
        let newData = dataresponse
        if (isAuditor) {
            url = API.AUDITOR_VERIFY_FINANCIAL_STATEMENT_BY_ID + itemId
            newData = {
                auditorOption: document.getElementById("inputAuditorOption").value + "",
                createAt: currentMiliSecs + "",
                lastCreateAt: itemDetail.createAt
            }
        }

        let isEqual = this.onCompareData(newData, itemDetail)


        if (isEdit === 'Thêm mới') {
            method = 'post'
            url = API.POST_FINANCIAL_STATEMENT
        }

        console.log("put - url", url)
        console.log("newData: ", newData)

        if (isEqual) {
            let authorization = AppUtils.HEADER_AUTHORIZATION()
            console.log("authorization", authorization)
            axios({
                method: method,
                url: url,
                data: newData,
                headers: {
                    "Authorization": authorization,
                    "Content-Type": "application/json"
                }
            }).then(res => {
                console.log(res);
                if (res.data.status == 200 || res.data.return_code == undefined) {
                    if (res.error === 'Unauthorized')
                        AppUtils.logout()
                    else {
                        swal("Hoàn tất!", "Chỉnh sửa hoạt động thành công.", "success");
                        onEditCallback(dataresponse)
                    }
                } else {
                    swal("Có lỗi xảy ra!", "", "warning");
                }
            })
                .catch(response => {
                    swal("Không thành công!", "Có lỗi xảy ra trong quá trình chỉnh sửa.", "warning");
                    if (response.error === 'Unauthorized')
                        AppUtils.logout()
                });
        } else {
            alert("Không có thay đổi")
        }
    }

    render() {
        const { year, quarter, isAuditor } = this.props
        let str = " - Q"
        let quarterStr = str + quarter + ""
        if (quarter == 5) quarterStr = ""
        return (
            <div className="container">
                <header className="section-header m-b-50">
                    <h3 className="section-title">Chỉnh sửa báo cáo</h3>
                </header>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card" >
                            <div className="card-body">
                                <form style={{ width: "100%" }}>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Ý kiến Kiểm toán viên</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="inputAuditorOption" disabled={!isAuditor} id="inputAuditorOption" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Nợ ngắn hạn</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="inputShortTermDebt" defaultValue={this.props.itemDetail.shortTermDebt} disabled={isAuditor} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Nợ dài hạn</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="inputLongTermDebt" defaultValue={this.props.itemDetail.longTermDebt} disabled={isAuditor} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Tổng nợ</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="inputTotalDebt" defaultValue={this.props.itemDetail.totalDebt} disabled={isAuditor} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Vốn chủ sở hữu</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="inputInitialCapital" defaultValue={this.props.itemDetail.initialCapital} disabled={isAuditor} />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Tổng tài sản</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="inputTotalAsset" defaultValue={this.props.itemDetail.totalAsset} disabled={isAuditor} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Năm</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" defaultValue={year + quarterStr} disabled />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                {
                                    !isAuditor && <>
                                        <p>Thêm hình ảnh</p>
                                        <div className="form-group text-center mt-25">
                                            <FileBase64
                                                multiple={false}
                                                onDone={this.getFiles.bind(this)} />
                                        </div>
                                    </>
                                }
                                <div className="text-center" style={{ paddingTop: "10px" }}>
                                    {
                                        this.state.files.length > 0 ? this.state.files.map((file, i) => {
                                            return <img style={{ width: "100%", marginBottom: "20px" }} key={i} defaultValue={this.props.image} src={file.base64} />
                                        }) : <img style={{ width: "100%", marginBottom: "20px" }} src="????" />
                                    }
                                </div>
                                {
                                    <div className="text-center m-t-20">
                                        <button className="btn btn-primary" onClick={() => this.postActivityData()}>Xác nhận</button>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default EditFinancial;
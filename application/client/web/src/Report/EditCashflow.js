import React, { Component } from 'react';
import API from './../API/API.js'
import axios from 'axios'
import AppUtils from '../Utils/AppUtils';
import FileBase64 from '../component/FileBase64';
import NumberFormat from 'react-number-format';
import swal from '@sweetalert/with-react'

class EditCashflow extends Component {

    constructor(props) {
        super(props);
        this.state = {
            files: [],
            item: [],
            isLoading: false
        }
    }

    getFiles(files) {
        if (Array.isArray(files))
            this.setState({ files: files })
        else {
            let file = [files]
            this.setState({ files: file })
        }
    }

    componentWillMount() {
        const { itemDetail } = this.props
        let proof = ""
        console.log(this.props)
        if (itemDetail.proofImage != undefined) proof = itemDetail.proofImage
        this.setState({
            item: itemDetail,
            files: [{ "base64": proof }],
            isLoading: true
        })
    }

    onCompareData(newData, oldData) {
        if (newData.outputActivityBusiness != oldData.outputActivityBusiness || newData.inputActivityBusiness != oldData.inputActivityBusiness
            || newData.outputActivityInvestment != oldData.outputActivityInvestment || newData.inputActivityInvestment != oldData.inputActivityInvestment
            || newData.outputActivityFinancial != oldData.outputActivityFinancial || newData.inputActivityFinancial != oldData.inputActivityFinancial
            || newData.proofImage != oldData.proofImage) {
            return true
        }
        return false
    }

    async postActivityData() {
        const { itemDetail, itemId, onEditCallback, isEdit, year, quarter } = this.props
        itemDetail.id = itemId
        let currentDate = new Date()
        let currentMiliSecs = currentDate.getTime()
        let proofImage = this.state.files.map((file, i) => file.base64)
        let newData = {
            id: itemId + "",
            outputActivityBusiness: document.getElementById("outputActivityBusiness").value + "",
            inputActivityBusiness: document.getElementById("inputActivityBusiness").value + "",
            outputActivityInvestment: document.getElementById("outputActivityInvestment").value + "",
            inputActivityInvestment: document.getElementById("inputActivityInvestment").value + "",
            outputActivityFinancial: document.getElementById("outputActivityFinancial").value + "",
            inputActivityFinancial: document.getElementById("inputActivityFinancial").value + "",
            proofImage: proofImage[0],
            year: year,
            quarter: quarter,
            createAt: currentMiliSecs + "",
            lastCreateAt: itemDetail.createAt
        }
        console.log("newData: ", newData)

        let isEqual = this.onCompareData(newData, itemDetail)
        let method = 'put'
        let url = API.PUT_CASHFLOW_STATEMENT_BY_ID + itemId
        console.log("put - url", url)

        if (isEdit === 'Thêm mới') {
            method = 'post'
            url = API.POST_CASHFLOW_STATEMENT
        }

        if (isEqual) {
            let authorization = AppUtils.HEADER_AUTHORIZATION()
            console.log("authorization", authorization)
            console.log("url", method, API.PUT_CASHFLOW_STATEMENT_BY_ID + itemId)
            axios({
                method: method,
                url: url,
                data: newData,
                headers: {
                    "Authorization": authorization,
                    "Content-Type": "application/json"
                }
            }).then(res => {
                console.log(res);
                if (res.data.status == 200 || res.data.return_code == undefined) {
                    if (res.error === 'Unauthorized')
                        AppUtils.logout()
                    else {
                        swal("Hoàn tất!", "Chỉnh sửa hoạt động thành công.", "success");
                        onEditCallback(newData)
                    }
                } else {
                    swal("Có lỗi xảy ra!", "" , "warning");
                }
            })
                .catch(response => {
                    swal("Không thành công!", "Có lỗi xảy ra trong quá trình chỉnh sửa.", "warning");
                    if (response.error === 'Unauthorized')
                        AppUtils.logout()
                });
        } else {
            alert("Không có thay đổi")
        }
    }

    render() {
        const { year, quarter } = this.props
        const { isLoading } = this.state
        let quarterStr = quarter + ""
        if (quarter == 5) quarterStr = "Cả năm"

        return (
            isLoading && <div className="container">
                <header className="section-header m-b-50">
                    <h3 className="section-title">Chỉnh sửa báo cáo</h3>
                </header>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card" >
                            <div className="card-body">
                                <form style={{ width: "100%" }}>
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Chi phí hoạt động Doanh nghiệp</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="outputActivityBusiness" defaultValue={this.props.itemDetail.outputActivityBusiness} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Thu nhập hoạt động Doanh nghiệp</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="inputActivityBusiness" defaultValue={this.props.itemDetail.inputActivityBusiness} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-sm-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Chi phí hoạt động Đầu tư</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="outputActivityInvestment" defaultValue={this.props.itemDetail.outputActivityInvestment} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Thu nhập hoạt động Đầu tư</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="inputActivityInvestment" defaultValue={this.props.itemDetail.inputActivityInvestment} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-sm-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Chi phí hoạt động Tài chính</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="outputActivityFinancial" defaultValue={this.props.itemDetail.outputActivityFinancial} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Thu nhập hoạt động Tài chính</label>
                                                <div className="col-sm-12">
                                                    <input type="text" className="form-control" id="inputActivityFinancial" defaultValue={this.props.itemDetail.inputActivityFinancial} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Năm</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control">
                                                        {year}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <div className="form-group row">
                                                <label className="col-sm-12 col-form-label">Quý</label>
                                                <div className="col-sm-12">
                                                    <div className="form-control" >
                                                        {quarterStr}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <p>Thêm hình ảnh</p>
                                <div className="form-group text-center mt-25">
                                    <FileBase64
                                        multiple={false}
                                        onDone={this.getFiles.bind(this)} />
                                </div>
                                <div className="text-center" style={{ paddingTop: "10px" }}>
                                    {
                                        this.state.files.length > 0 ? this.state.files.map((file, i) => {
                                            return <img style={{ width: "100%", marginBottom: "20px" }} key={i} defaultValue={this.props.image} src={file.base64} />
                                        }) : <img style={{ width: "100%", marginBottom: "20px" }} src="????" />
                                    }
                                </div>
                                {
                                    <div className="text-center m-t-20">
                                        <button className="btn btn-primary" onClick={() => this.postActivityData()}>Xác nhận</button>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default EditCashflow;
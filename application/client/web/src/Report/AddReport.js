import React, { Component } from 'react';
import FileBase64 from '../component/FileBase64';
import axios from 'axios'
import AppUtils from '../Utils/AppUtils';
import API from '../API/API';
import CONSTANT from '../Constant';
import swal from '@sweetalert/with-react'

class AddReport extends Component {

    constructor(props) {
        super(props);
        this.state = {
            proof: [],
            fileName: "",
            maxId: CONSTANT.INIT_VALUE
        }
    }

    async saveStatement() {
        const { onAddItem } = this.props
        let statementId = document.getElementById("inputId").value + ""
        let proof = this.state.proof.map((file, i) => file.base64)
        let selectQuarter = document.getElementById("selectQuarter")
        let dt = {
            id: statementId,
            title: document.getElementById("inputTitle").value + "",
            description: document.getElementById("inputDescription").value + "",
            quarter: selectQuarter.options[selectQuarter.selectedIndex].value + "",
            year: document.getElementById("inputYear").value + "",
            proof: proof[0] + ""
        }
        console.log("new data", dt)

        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios({
            method: 'post',
            url: API.POST_STATEMENT,
            data: dt,
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.data.status == 200 || res.data.return_code == undefined) {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    swal("Hoàn tất!", "Chỉnh sửa hoạt động thành công.", "success");
                    onAddItem(dt)
                }
            } else {
                swal("Có lỗi xảy ra!", "" , "warning");
            }
        }).catch(response => {
            console.log('sdasdsada', response)

            if (response.error === 'Unauthorized')
                AppUtils.logout()
        });
    }

    getFiles(files) {
        console.log(files)
        this.setState({
            proof: files
        })
    }

    componentWillMount() {
        let maxId = localStorage.getItem(CONSTANT.MAX_ID_REPORT)
        this.setState({
            maxId: maxId
        })
    }

    render() {
        const { maxId } = this.state
        let genId = parseInt(maxId) + 1

        return (
            <div className="container">
                <header className="section-header m-b-50">
                    <h3 className="section-title">Thêm báo cáo</h3>
                </header>
                <div className="row form-group">
                    <div class="col-md-2">
                        <label htmlFor="input1">ID</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" className="form-control" id="inputId" value={genId} disabled />
                    </div>
                </div>
                <div className="row form-group">
                    <div class="col-md-2">
                        <label htmlFor="input2">Tiêu đề</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" className="form-control" id="inputTitle" />
                    </div>
                </div>
                <div className="row form-group">
                    <div class="col-md-2">
                        <label htmlFor="input1">Mô tả</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" className="form-control" id="inputDescription" />
                    </div>
                </div>
                <div className="row form-group">
                    <div class="col-md-2">
                        <label htmlFor="input2">Năm</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" className="form-control" id="inputYear" />
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-2">
                        <label for="select" class=" form-control-label">Quý</label>
                    </div>
                    <div class="col-md-10">
                        <select name="select" id="selectQuarter" class="form-control">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">All year</option>
                        </select>
                    </div>
                </div>

                <p>Thêm báo cáo</p>
                <div className="form-group text-center mt-25">
                    <FileBase64
                        multiple={true}
                        onDone={this.getFiles.bind(this)} />
                </div>
                <div className="text-center">
                    <button className="btn btn-primary" onClick={() => this.saveStatement()}>Xác nhận</button>
                </div>
            </div>
        );
    }
}

export default AddReport;
import React, { Component } from 'react';
import axios from 'axios'
import API from './../API/API.js'
import CONSTANT from './../Constant'
import AppUtils from '../Utils/AppUtils.js';
import { Modal } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusSquare, faSearch } from '@fortawesome/free-solid-svg-icons'
import { Icon } from '@iconify/react';
import deleteIcon from '@iconify/icons-zmdi/delete';
import editIcon from '@iconify/icons-zmdi/edit';
import detailIcon from '@iconify/icons-zmdi/more';
import historyIcon from '@iconify/icons-fa-solid/history';
import downloadIcon from '@iconify/icons-zmdi/download';
import Pagination from "react-js-pagination";
import AddReport from './AddReport.js';
import EditReport from './EditReport.js';
import { NavLink } from "react-router-dom";
import Select from 'react-select';
import { groupTypeOptions, groupStyles, groupBadgeStyles, quarterOptions, yearOptions } from '../Utils/DataGeneral'
import swal from '@sweetalert/with-react'

const groupedOptions = [
    {
        label: 'Quarter',
        options: quarterOptions,
    }
];

const formatGroupLabel = data => (
    <div style={groupStyles}>
        <span>{data.label}</span>
        <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
);

class Statement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            activePage: 1,
            totalData: 0,
            limit: 5,
            chosenItem: {},
            showFormAdd: false,
            showFormEdit: false,
            yearArray: [],
            year: CONSTANT.NO_FILTER,
            month: CONSTANT.NO_FILTER,
            quarter: CONSTANT.NO_FILTER,
            maxId: CONSTANT.INIT_VALUE
        }

        this.onDeleteItem = this.onDeleteItem.bind(this)
        this.onDownloadItem = this.onDownloadItem.bind(this)
        this.onAddItem = this.onAddItem.bind(this)
    }

    handleClose() {
        this.setState({
            showFormAdd: false
        })
    }

    handleShow() {
        this.setState({
            showFormAdd: true
        })
    }

    handleCloseEdit() {
        this.setState({
            showFormEdit: false
        })
    }

    handleShowEdit(value) {
        this.setState({
            chosenItem: value,
            showFormEdit: true
        })
    }

    onDownloadItem(item) {
        let url = API.GET_STATEMENT_BY_ID + item.id
        console.log(url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            console.log(res.data)
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data.length !== 0) {
                    let proof = res.data.proof
                    let quarter = item.quarter
                    console.log("xxx", proof)
                    let linkView = "/baocaochitiet/" + item.year
                    if (proof != undefined && proof != "") {
                        localStorage.setItem(CONSTANT.BASE64_REPORT, proof)
                        if (quarter !== 0 && quarter !== "" && quarter !== undefined)
                            linkView += "." + quarter
                    }
                    window.open(linkView, "_blank")
                } else {
                    localStorage.removeItem(CONSTANT.BASE64_REPORT)
                    console.log("lỗi")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        })
            .finally(() => { });
    }

    onAddItem(value) {
        console.log("newItem", value)
        this.handleClose()
        this.queryDataByField()
    }

    onEditCallback(value) {
        localStorage.setItem(CONSTANT.BASE64_REPORT, value.proof)
        this.setState({
            showFormEdit: false
        }, this.queryDataByField)
    }

    delete(value) {
        console.log(value)
        let itemId = value.id
        console.log(API.DEL_STATEMENT_BY_ID + itemId)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("authorization", authorization)
        axios({
            method: 'DELETE',
            url: API.DEL_STATEMENT_BY_ID + itemId,
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                this.queryDataByField()
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
            }
        })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            });
    }

    onDeleteItem(value) {
        swal({
            title: "Bạn muốn thực hiện hành động?",
            text: "Khi xoá, bạn sẽ không thể khôi phục lại dữ liệu!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.delete(value)
                }
            });
    }

    handlePageChange(pageNumber) {
        this.setState({
            activePage: pageNumber
        }, this.queryDataByField);
    }

    componentWillMount() {
        this.queryDataByField()
        // this.getYear()
    };

    getYear() {
        let url = API.GET_YEAR
        console.log("getYear", url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("authorization", authorization)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    let yearOps = AppUtils.convertYearArrayToOptions(res.data)
                    console.log(yearOps)
                    this.setState({
                        yearArray: yearOps
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    onYearSelect(value) {
        let year = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        this.setState({
            year: year
        }, this.queryDataByField)
    }

    onQuarterMonthSelect(value) {
        console.log(value)
        if (value != null) {
            this.setState({
                quarter: value.value
            }, this.queryDataByField)
        } else {
            this.setState({
                quarter: CONSTANT.NO_FILTER,
                month: CONSTANT.NO_FILTER
            }, this.queryDataByField)
        }
    }

    queryDataByField() {
        let param = ""
        const { year, month, quarter, maxId } = this.state

        if (year != CONSTANT.NO_FILTER)
            param += "?year=" + year
        if (quarter != CONSTANT.NO_FILTER && year != CONSTANT.NO_FILTER) {
            param += "&quarter=" + quarter
        } else if (quarter != CONSTANT.NO_FILTER) param += "?quarter=" + quarter
        let connector = "&"
        if (year == CONSTANT.NO_FILTER && quarter == CONSTANT.NO_FILTER && month == CONSTANT.NO_FILTER)
            connector = "?"
        let url = API.GET_ALL_STATEMENT_BY_PAGE + param + connector + `limit=${this.state.limit}&page=${this.state.activePage}`
        console.log("url", url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        let data = res.data.records
                        let maxid = data[0].id
                        localStorage.setItem(CONSTANT.MAX_ID_REPORT, maxid)
                        this.setState({
                            data: data,
                            totalData: res.data.totalPages * this.state.limit,
                            maxid: maxid
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    render() {
        var { data, yearArray } = this.state;
        let userRole = localStorage.getItem(CONSTANT.USER_LOGIN_ROLE)
        let isAdmin = userRole == CONSTANT.ROLE_ACCOUNTANT
        return (
            <section id="portfolio" className="text-center">
                <div className="container">
                    <header className="section-header">
                        <h3 className="section-title">Báo cáo tài chính</h3>
                    </header>
                    <div className="row">
                        <div className="col-md-12">
                            {/* DATA TABLE */}
                            <div className="table-data__tool">
                                <div className="table-data__tool-left">
                                    <div class="rs-select2--light rs-select2--sm">
                                        <Select
                                            onChange={this.onYearSelect.bind(this)}
                                            options={yearOptions}
                                            formatGroupLabel={formatGroupLabel}
                                            placeholder="Theo năm"
                                        />
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                    <div class="rs-select2--light rs-select2--sm">
                                        <Select
                                            onChange={this.onQuarterMonthSelect.bind(this)}
                                            options={groupedOptions}
                                            formatGroupLabel={formatGroupLabel}
                                            placeholder="Theo Quý"
                                        />
                                    </div>
                                </div>
                                {
                                    isAdmin && <div className="table-data__tool-right">
                                        <button className="au-btn au-btn-icon au-btn--green au-btn--small" onClick={this.handleShow.bind(this)}>
                                            <FontAwesomeIcon className="custom-icon" icon={faPlusSquare} />Thêm mới</button>
                                    </div>
                                }
                            </div>
                            <div className="table-responsive table-responsive-data2 m-t-20">
                                <table className="table table-data2 ">
                                    <thead>
                                        <tr>
                                            <th className="medium">id</th>
                                            <th className="large">Tiêu đề</th>
                                            <th className="large">Mô tả</th>
                                            <th className="medium">Quý</th>
                                            <th className="medium">Năm</th>
                                            <th className="large">Thao tác</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            data.map((item, key) => {
                                                let quarter = item.quarter
                                                if (quarter == 5) quarter = ""
                                                return (
                                                    <tr className="tr-shadow">
                                                        <td>{item.id}</td>
                                                        <td className="desc">{item.title}</td>
                                                        <td>
                                                            {item.description}
                                                        </td>
                                                        <td>{
                                                            quarter
                                                        }</td>
                                                        <td>{item.year}</td>
                                                        <td>
                                                            <div className="table-data-feature">
                                                                {
                                                                    isAdmin && <button className="item" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" onClick={() => this.handleShowEdit(item)}>
                                                                        <Icon icon={editIcon} />
                                                                    </button>
                                                                }
                                                                {
                                                                    isAdmin && <button className="item" data-toggle="tooltip" data-placement="top" title="Xoá" onClick={() => this.onDeleteItem(item)}>
                                                                        <Icon icon={deleteIcon} />
                                                                    </button>
                                                                }
                                                                <button className="item" data-toggle="tooltip" data-placement="top" title="Tải về" onClick={() => this.onDownloadItem(item)}>
                                                                    <Icon icon={downloadIcon} />
                                                                </button>
                                                                <NavLink to={"/quanly/statement/detail/" + item.id} >
                                                                    <button className="item" data-toggle="tooltip" data-placement="top" title="Chi tiết">
                                                                        <Icon icon={detailIcon} />
                                                                    </button>
                                                                </NavLink>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                )
                                            }
                                            )
                                        }
                                        <Modal show={this.state.showFormAdd} onHide={this.handleClose.bind(this)}
                                            size="lg"
                                            aria-labelledby="contained-modal-title-vcenter"
                                            centered>
                                            <Modal.Header closeButton>
                                            </Modal.Header>
                                            <Modal.Body>
                                                <AddReport onAddItem={d => this.onAddItem(d)} />
                                            </Modal.Body>
                                        </Modal>

                                        <Modal show={this.state.showFormEdit} onHide={this.handleCloseEdit.bind(this)}
                                            size="lg"
                                            aria-labelledby="contained-modal-title-vcenter"
                                            centered>
                                            <Modal.Header closeButton>
                                            </Modal.Header>
                                            <Modal.Body>
                                                <EditReport itemId={this.state.chosenItem.id} itemDetail={this.state.chosenItem} onEditCallback={d => this.onEditCallback(d)} />
                                            </Modal.Body>
                                        </Modal>
                                    </tbody>
                                </table>
                            </div>
                            {/* END DATA TABLE */}
                        </div>
                        <div className="col-lg-12" style={{ justifyContent: 'center', display: 'flex', marginTop: "1%" }}>
                            <Pagination
                                activePage={this.state.activePage}
                                itemsCountPerPage={this.state.limit}
                                totalItemsCount={this.state.totalData}
                                pageRangeDisplayed={5}
                                onChange={this.handlePageChange.bind(this)}
                                itemClass="page-item"
                                linkClass="page-link"
                            />
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Statement;
import React, { Component } from 'react';
import CONSTANT from '../Constant';
import { Redirect } from 'react-router-dom'

class ViewReport extends Component {

    constructor(props) {
        super(props);
        const token = localStorage.getItem(CONSTANT.USER_LOGIN_TOKEN)
        let loggedIn = true
        if (token == null) {
            loggedIn = false
        }

        this.state = {
            loggedIn
        }
    }

    componentWillMount() {
        const token = localStorage.getItem(CONSTANT.USER_LOGIN_TOKEN)
        let loggedIn = true
        if (token == null) {
            this.setState({
                loggedIn: false
            })
        }
    }

    render() {
        let data = localStorage.getItem(CONSTANT.BASE64_REPORT)
        if (data == null) {
            return <Redirect to="/error" />
        }
        var obj = document.createElement('object');
        obj.style.width = '100%';
        obj.style.height = '842pt';
        obj.type = 'application/pdf';
        obj.data = 'data:application/pdf;base64,' + data.split(",")[1];
        document.body.appendChild(obj);
        return (
            <div>

            </div>
        );
    }
}

export default ViewReport;
import React, { Component } from 'react';
import API from './../API/API.js'
import axios from 'axios'
import AppUtils from '../Utils/AppUtils';
import FileBase64 from '../component/FileBase64';
import Select from 'react-select';
import { groupStyles, groupBadgeStyles, quarterOptions } from '../Utils/DataGeneral'
import swal from '@sweetalert/with-react'
import CONSTANT from '../Constant.js';

const formatGroupLabel = data => (
    <div style={groupStyles}>
        <span>{data.label}</span>
        <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
);

class EditReport extends Component {

    constructor(props) {

        super(props);
        this.state = {
            files: [],
            item: [],
            valueDropDown: ""
        }
    }

    getFiles(files) {
        this.setState({ files: files })
        console.log(files)
    }

    componentWillMount() {
        const { itemDetail } = this.props
        console.log("itemDetail", itemDetail)
        this.getReportDetail()
    }

    getReportDetail() {
        const { itemId, itemDetail } = this.props

        let url = API.GET_STATEMENT_BY_ID + itemId
        console.log("getReportDetail", url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            console.log(res.data)
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data.length !== 0) {
                    console.log("res.data", res.data)
                    let proof = res.data.proof
                    this.setState({
                        item: itemDetail,
                        valueDropDown: itemDetail.quarter,
                        files: [{ "base64": proof }]
                    })
                } else {
                    localStorage.removeItem(CONSTANT.BASE64_REPORT)
                    console.log("lỗi")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        })
            .finally(() => { });
    }

    onCompareData(newData, oldData) {
        if (newData.title != oldData.title || newData.quarter != oldData.quarter
            || newData.description != oldData.description
            || newData.year != oldData.year || newData.proof != oldData.proof) {
            return true
        }
        return false
    }

    postActivityData() {
        const { itemDetail, onEditCallback, itemId } = this.props
        let proofpdf = this.state.files.map((file, i) => file.base64)
        let currentDate = new Date()
        let currentMiliSecs = currentDate.getTime()
        let newData = {
            id: itemId,
            title: document.getElementById("inputTitle").value + "",
            description: document.getElementById("inputDes").value + "",
            quarter: this.state.valueDropDown + "",
            year: document.getElementById("inputYear").value + "",
            proof: proofpdf[0] + "",
            createAt: currentMiliSecs + "",
            lastCreateAt: itemDetail.createAt
        }

        console.log("newData: ", newData)
        let isEqual = this.onCompareData(newData, itemDetail)

        if (isEqual) {
            let authorization = AppUtils.HEADER_AUTHORIZATION()
            console.log("put - url", API.PUT_STATEMENT_BY_ID + itemId)
            axios({
                method: 'put',
                url: API.PUT_STATEMENT_BY_ID + itemId,
                data: newData,
                headers: {
                    "Authorization": authorization,
                    "Content-Type": "application/json"
                }
            }).then(res => {
                if (res.data.status == 200 || res.data.return_code == undefined) {
                    if (res.error === 'Unauthorized')
                        AppUtils.logout()
                    else {
                        console.log("newData", newData)
                        swal("Thành công", "Thông tin đã được chỉnh sửa", "success");
                        onEditCallback(newData)
                    }

                } else {
                    swal("Có lỗi xảy ra!", "" , "warning");
                }
            })
                .catch(response => {
                    console.log(response)
                    swal("Không thành công!", "Có lỗi xảy ra trong quá trình chỉnh sửa.", "warning");
                    if (response.error === 'Unauthorized')
                        AppUtils.logout()
                });
        } else {
            alert("Không có thay đổi")
        }
    }

    onQuarterSelect(value) {
        this.setState({
            valueDropDown: value.value
        })
    }

    render() {
        let proofpdf = this.state.files.map((file, i) => file.base64)
        let isEmpty = proofpdf == ""
        return (
            <div className="container">
                <header className="section-header m-b-50">
                    <h3 className="section-title">Chỉnh sửa báo cáo</h3>
                </header>
                <div className="row form-group">
                    <div class="col-md-2">
                        <label htmlFor="input1">ID</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" className="form-control" id="inputId" value={this.props.itemId} disabled />
                    </div>
                </div>
                <div className="row form-group">
                    <div class="col-md-2">
                        <label htmlFor="input2">Tiêu đề</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" className="form-control" id="inputTitle" defaultValue={this.props.itemDetail.title} />
                    </div>
                </div>
                <div className="row form-group">
                    <div class="col-md-2">
                        <label htmlFor="input1">Mô tả</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" className="form-control" id="inputDes" defaultValue={this.props.itemDetail.description} />
                    </div>
                </div>
                <div className="row form-group">
                    <div class="col-md-2">
                        <label htmlFor="input2">Năm</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" className="form-control" id="inputYear" defaultValue={this.props.itemDetail.year} />
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-2">
                        <label for="select" class=" form-control-label">Quý</label>
                    </div>
                    <div class="col-md-10">
                        <Select
                            onChange={this.onQuarterSelect.bind(this)}
                            options={quarterOptions}
                            formatGroupLabel={formatGroupLabel}
                            defaultValue={quarterOptions.find((item) => {
                                return item.value == this.state.valueDropDown
                            })}
                            placeholder="All"
                        />
                    </div>
                </div>
                {
                    !isEmpty && <div>Đã có file báo cáo</div> 
                }
                <span className="m-t-20">Thêm báo cáo khác</span>
                <div className="form-group text-center m-t-10">
                    <FileBase64
                        multiple={true}
                        onDone={this.getFiles.bind(this)} />
                </div>

                <div className="text-center">
                    <button className="btn btn-primary" onClick={() => this.postActivityData()}>Xác nhận</button>
                </div>
            </div >
        );
    }
}

export default EditReport;
import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap'
import Button from 'react-bootstrap/Button'
import Table from 'react-bootstrap/Table'
import '../css/detail.css';
import API from '../API/API';
import AppUtils from '../Utils/AppUtils';
import axios from 'axios'
import CONSTANT from '../Constant';
import detailIcon from '@iconify/icons-zmdi/more';
import AddAccount from './AddAccount';
import EditAccount from './EditAccount';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserPlus } from '@fortawesome/free-solid-svg-icons'
import { Icon } from '@iconify/react';
import deleteIcon from '@iconify/icons-zmdi/delete';
import editIcon from '@iconify/icons-zmdi/edit';
import { Modal } from 'react-bootstrap'
import swal from '@sweetalert/with-react'
import Select from 'react-select';
import { groupStyles, groupBadgeStyles } from '../Utils/DataGeneral'
import { NavLink } from 'react-router-dom';

const formatGroupLabel = data => (
    <div style={groupStyles}>
        <span>{data.label}</span>
        <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
);

class Account extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            roleArray: [],
            chosenItem: {},
            showFormAdd: false,
            showFormEdit: false,
            showFormDetail: false,
            role: "",
            isDateDesc: true,
            isLoading: false
        }

        this.onDeleteItem = this.onDeleteItem.bind(this)
        this.onAddAccount = this.onAddAccount.bind(this)
        this.dateClick = this.dateClick.bind(this)
        this.onEditInfo = this.onEditInfo.bind(this)
    }

    dateClick() {
        let { data } = this.state;
        let state = !this.state.isDateDesc
        let array = AppUtils.sortListUserByDate(data, state)

        this.setState({
            isDateDesc: state,
            data: array
        })
    }

    onEditInfo(value) {
        swal("Thành công!", "Thông tin đã được chỉnh sửa", "success");
        this.setState({
            showFormEdit: false,
            isLoading: false
        }, function () {
            this.querryUserByRole();
        })
    }

    handleClose() {
        this.setState({
            showFormAdd: false
        })
    }

    handleShow() {
        this.setState({
            showFormAdd: true
        })
    }

    handleCloseEdit() {
        this.setState({
            showFormEdit: false
        })
    }

    handleShowEdit(value) {
        this.setState({
            chosenItem: value,
            showFormEdit: true
        })
    }

    handleCloseDetail() {
        this.setState({
            showFormDetail: false
        })
    }

    handleShowDetail() {
        this.setState({
            showFormDetail: true
        })
    }

    onAddAccount(value) {
        this.setState({
            showFormAdd: false
        }, this.querryUserByRole)
    }

    delete(value) {
        let itemId = value.id
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("authorization", authorization)
        axios({
            method: 'DELETE',
            url: API.DEL_USER + itemId,
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                this.querryUserByRole()
                swal("Deleted!", "Thông tin đã được xoá.", "success");
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        });
    }

    onDeleteItem(value) {
        swal({
            title: "Tiếp tục thực hiện hành động?",
            text: "Nếu xoá, Bạn sẽ không thể khôi phục lại data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.delete(value)
                }
            });
    }

    onGetRoles() {
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(API.GET_ROLES, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        console.log("yyyy", res.data)
                        let roles = AppUtils.convertRoleArrayToOptions(res.data, true)
                        console.log("yyyy", roles)
                        this.setState({
                            roleArray: roles
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    componentDidMount() {
        this.querryUserByRole();
        this.onGetRoles();
    }

    querryUserByRole() {
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        let role = this.state.role
        let param = "?role="
        let id = AppUtils.getRoleId(role)
        if (id != CONSTANT.NO_FILTER) param += id
        else param = ""

        let url = API.GET_USER.slice(0, -1) + param
        console.log(url)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data.length !== 0) {
                    this.setState({
                        data: res.data,
                        isLoading: true
                    })
                } else {
                    console.log("lỗi")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    onRoleSelect(value) {
        let role = value == null || value.length == 0 ? "" : value.value
        console.log("role", value)
        this.setState({
            role: role
        }, this.querryUserByRole)
    }

    render() {
        var { data, roleArray, isLoading } = this.state;
        let userRole = localStorage.getItem(CONSTANT.USER_LOGIN_ROLE)
        let isAdmin = userRole == CONSTANT.ROLE_ADMIN
        return (
            isLoading && <section id="about">
                <div className="container">
                    <header className="section-header">
                        <h3>Tài khoản người dùng</h3>
                    </header>
                    <div className="row">
                        <div className="col-md-12">
                            {/* DATA TABLE */}
                            <div className="table-data__tool">
                                <div className="rs-select2--light rs-select2--sm">
                                    <Select
                                        onChange={this.onRoleSelect.bind(this)}
                                        options={roleArray}
                                        formatGroupLabel={formatGroupLabel}
                                        placeholder="Tất cả"
                                    />
                                </div>
                                <div className="table-data__tool-right">
                                    <button className="au-btn au-btn-icon au-btn--green au-btn--small" onClick={this.handleShow.bind(this)}>
                                        <FontAwesomeIcon className="custom-icon" icon={faUserPlus} />Thêm tài khoản</button>
                                </div>
                            </div>
                            <div className="table-responsive table-responsive-data2 m-t-20">
                                <table className="table table-data2">
                                    <thead>
                                        <tr>
                                            {/* <th>id</th> */}
                                            <th>Tài khoản</th>
                                            <th>email</th>
                                            <th>Tên</th>
                                            <th>Vai trò</th>
                                            <th>Thao tác</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            data.map((item, key) => {
                                                let state = ""
                                                let role = ""

                                                if (item.roles != undefined) {
                                                    role = AppUtils.getStrRole(item.roles[0].name)
                                                    if (role == CONSTANT.QUAN_TRI_VIEN) {
                                                        state = "role admin"
                                                    } else if (role == CONSTANT.NGUOI_DUNG) {
                                                        state = "role user"
                                                    } else if (role == CONSTANT.KE_TOAN) {
                                                        state = "role accountant"
                                                    } else if (role == CONSTANT.KIEM_TOAN_VIEN) {
                                                        state = "role auditor"
                                                    }
                                                    else {
                                                        state = "role member"
                                                    }
                                                }
                                                return (
                                                    <tr className="tr-shadow">
                                                        {/* <td>{item.id}</td> */}
                                                        <td className="desc">{item.username}</td>
                                                        <td>
                                                            <span className="block-email">{item.email}</span>
                                                        </td>
                                                        <td>{item.name}</td>
                                                        <td>
                                                            <span className={state}>{role}</span>
                                                        </td>
                                                        <td>
                                                            <div className="table-data-feature">
                                                                <button className="item" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" onClick={() => this.handleShowEdit(item)}>
                                                                    <Icon icon={editIcon} />
                                                                </button>
                                                                <button className="item" data-toggle="tooltip" data-placement="top" title="Xoá" onClick={() => this.onDeleteItem(item)}>
                                                                    <Icon icon={deleteIcon} />
                                                                </button>
                                                                <NavLink to={"/quanly/taikhoan/chitiet/" + item.id}  >
                                                                    <button className="item" data-toggle="tooltip" data-placement="top" title="Detail">
                                                                        <Icon icon={detailIcon} />
                                                                    </button>
                                                                </NavLink>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                )
                                            }
                                            )
                                        }
                                        <Modal show={this.state.showFormAdd} onHide={this.handleClose.bind(this)}>
                                            <Modal.Header closeButton>
                                            </Modal.Header>
                                            <Modal.Body>
                                                <AddAccount onAddAccount={d => this.onAddAccount(d)} />
                                            </Modal.Body>
                                        </Modal>

                                        <Modal show={this.state.showFormEdit} onHide={this.handleCloseEdit.bind(this)}>
                                            <Modal.Header closeButton>
                                            </Modal.Header>
                                            <Modal.Body>
                                                <EditAccount account={this.state.chosenItem} isAdmin={isAdmin} onEditInfo={d => this.onEditInfo(d)} roles={this.state.roleArray} />
                                            </Modal.Body>
                                        </Modal>
                                    </tbody>
                                </table>
                            </div>
                            {/* END DATA TABLE */}
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Account;
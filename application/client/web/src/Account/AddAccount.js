import React, { Component } from 'react';
import Dropdown from 'react-bootstrap/Dropdown'
import axios from 'axios'
import API from './../API/API.js'
import CONSTANT from './../Constant'
import AppUtils from '../Utils/AppUtils.js';
import swal from '@sweetalert/with-react'

class AddAccount extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            roleWriteToDB: CONSTANT.ROLE_USER,
            roles: []
        }
        this.select = this.select.bind(this);
    }


    select(event) {
        this.setState({
            roleWriteToDB: event.target.innerText
        });
    }

    componentWillMount() {
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("authorization", authorization)
        axios.get(API.GET_ROLES, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.setState({
                            roles: res.data
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    async postData() {
        const { onAddAccount } = this.props
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("authorization", authorization)
        let newAccount = {
            email: document.getElementById("email").value + "",
            username: document.getElementById("username").value + "",
            name: document.getElementById("name").value + "",
            password: document.getElementById("password").value + "",
        }
        axios({
            method: 'post',
            url: API.POST_ACCOUNT,
            data: newAccount,
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            console.log(res.data);
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                swal("Hoàn tất!", "Thêm tài khoản thành công.", "success");
                onAddAccount(newAccount)
            }
        })
            .catch(response => {
                swal("Không thành công!", "Có lỗi xảy ra trong quá trình chỉnh sửa.", "warning");
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            });
    }

    render() {
        return (
            <div className="container">
                <header className="section-header m-b-50">
                    <h3 className="section-title">Thêm tài khoản</h3>
                </header>
                <div className="form-group">
                    <div className="input-group">
                        <div className="input-group-addon">
                            <i className="fa fa-user" />
                        </div>
                        <input type="text" id="username" name="username" placeholder="Username" className="form-control" />
                    </div>
                </div>
                <div className="form-group">
                    <div className="input-group">
                        <div className="input-group-addon">
                            <i className="fa fa-envelope" />
                        </div>
                        <input type="email" id="email" name="email" placeholder="Email" className="form-control" />
                    </div>
                </div>
                <div className="form-group">
                    <div className="input-group">
                        <div className="input-group-addon">
                            <i className="fa fa-address-book" />
                        </div>
                        <input type="text" id="name" name="name" placeholder="Name" className="form-control" />
                    </div>
                </div>
                <div className="form-group">
                    <div className="input-group">
                        <div className="input-group-addon">
                            <i className="fa fa-asterisk" />
                        </div>
                        <input type="password" id="password" name="password" placeholder="Password" className="form-control" />
                    </div>
                </div>
                <div className="form-actions form-group text-center">
                    <button type="submit" className="btn btn-success btn-sm" onClick={() => this.postData()}>Submit</button>
                </div>
            </div>
        );
    }
}

export default AddAccount;
import React, { Component } from 'react';
import Dropdown from 'react-bootstrap/Dropdown'
import axios from 'axios'
import API from './../API/API.js'
import CONSTANT from './../Constant'
import AppUtils from '../Utils/AppUtils.js';
import Select from 'react-select';
import { groupStyles, groupBadgeStyles, roleOptions } from '../Utils/DataGeneral'
import swal from '@sweetalert/with-react'

const formatGroupLabel = data => (
    <div style={groupStyles}>
        <span>{data.label}</span>
        <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
);

class EditAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: [],
            valueDropDown: CONSTANT.ROLE_USER,
            roles: [],
            account: {}
        }
        this.select = this.select.bind(this);
    }

    select(event) {
        this.setState({
            valueDropDown: event.target.innerText
        });
    }

    componentWillMount() {
        this.onGetRoles()
        const { account, roles } = this.props
        let role = account.roles
        let str = AppUtils.getStrRole(role[0].name)

        this.setState({
            valueDropDown: str,
            account: account,
        })
    }

    onGetRoles() {
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(API.GET_ROLES, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        let roleArray = AppUtils.convertRoleArrayToOptions(res.data, false)
                        this.setState({
                            roles: roleArray
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    async postData() {
        const { isAdmin, onEditInfo } = this.props
        const { account } = this.state
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("authorization", authorization)
        console.log("put - url", API.UPDATE_INFO + account.id)
        account.name = document.getElementById("inputName").value + ""

        axios({
            method: 'put',
            url: API.UPDATE_INFO + account.id,
            data: {
                password: document.getElementById("inputPassword").value + "",
                name: document.getElementById("inputName").value + ""
            },
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            console.log(res);
            if (res.data.status == 200 || res.data.return_code == undefined) {
                this.updateRole()
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
            } else {
                swal("Có lỗi xảy ra!", "", "warning");
            }
        })
            .catch(response => {
                swal("Có lỗi xảy ra!", "", "warning");
                if (response.error === 'Unauthorized')
                    if (response.error === 'Unauthorized')
                        if (response.error === 'Unauthorized')
                            AppUtils.logout()
            });


    }

    updateRole() {
        const { isAdmin, onEditInfo } = this.props
        const { account } = this.state
        let role = this.state.valueDropDown
        let id = AppUtils.getRoleId(role)
        let label = AppUtils.getRoleLabel(role)
        let roles = [
            {
                id: id,
                name: label
            }
        ]
        account.roles = roles
        console.log("account ", account)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        let url = API.UPDATE_ROLE + account.id
        console.log("update role", url)
        axios({
            method: 'put',
            url: url,
            data: {
                role: label
            },
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            console.log(res);
            if (res.data.status == 200 || res.data.return_code == undefined) {
                swal("Hoàn tất!", "Chỉnh sửa thành công.", "success");
                onEditInfo(account)
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
            } else {
                swal("Có lỗi xảy ra!", "", "warning");
            }
        })
            .catch(response => {
                swal("Có lỗi xảy ra!", "", "warning");
                if (response.error === 'Unauthorized')
                    if (response.error === 'Unauthorized')
                        if (response.error === 'Unauthorized')
                            AppUtils.logout()
            });
    }

    onRoleSelect(value) {
        let role = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.label
        console.log("role select", role)
        this.setState({
            valueDropDown: role
        })
    }

    render() {
        var { account, isAdmin } = this.props
        let list = this.state.roles

        const { valueDropDown } = this.state
        return (
            <div className="container">
                <header className="section-header m-b-50">
                    <h3 className="section-title">Chỉnh sửa tài khoản</h3>
                </header>
                <div className="row form-group">
                    <div class="col col-md-3">
                        <label htmlFor="input2">Name</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" className="form-control" id="inputName" defaultValue={account.name} />
                    </div>
                </div>

                <div className="row form-group">
                    <div class="col col-md-3">
                        <label htmlFor="input2">Password</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" className="form-control" id="inputPassword" defaultValue={account.password} />
                    </div>
                </div>

                {
                    isAdmin && <div className="row form-group">
                        <div class="col col-md-3">
                            <label htmlFor="input2">Role</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <Select
                                onChange={this.onRoleSelect.bind(this)}
                                options={list}
                                formatGroupLabel={formatGroupLabel}
                                defaultValue={roleOptions.find((item) => {
                                    return item.label == this.state.valueDropDown
                                })}
                                placeholder="All"
                            />
                        </div>
                    </div>
                }
                <div className="text-center">
                    <button className="btn btn-primary" onClick={() => this.postData()}>Save</button>
                </div>
            </div>
        );
    }
}

export default EditAccount;
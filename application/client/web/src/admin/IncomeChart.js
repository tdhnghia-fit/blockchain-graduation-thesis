import React, { Component } from 'react';
import { Bar, Line, Pie } from 'react-chartjs-2';
import API from './../API/API.js'
import AppUtils from '../Utils/AppUtils';
import axios from 'axios'
import { groupStyles, groupBadgeStyles, chartYearOption } from '../Utils/DataGeneral'
import Select from 'react-select';
import CONSTANT from '../Constant.js';

const formatGroupLabel = data => (
    <div style={groupStyles}>
        <span>{data.label}</span>
        <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
);

const color1 = "rgb(51, 153, 51)"
const color2 = "rgb(255, 128, 0)"
const color3 = "rgb(0, 0, 255)"

// Recent Report 2
const pieData = {
    datasets: [{
        data: [],
        backgroundColor: [
            color1,
            color2,
            color3
        ],
        hoverBackgroundColor: [
            color1,
            color2,
            color3
        ]
    }],
    labels: [
        "Core",
        "Financial",
        "Other"
    ]
}

const pieData1 = {
    datasets: [{
        data: [],
        backgroundColor: [
            color1,
            color2,
            color3
        ],
        hoverBackgroundColor: [
            color1,
            color2,
            color3
        ]
    }],
    labels: [
        "Doanh thu chính",
        "Doanh thu tài chính",
        "Doanh thu khác"
    ]
}

const pieData2 = {
    datasets: [{
        data: [],
        backgroundColor: [
            color1,
            color2,
            color3
        ],
        hoverBackgroundColor: [
            color1,
            color2,
            color3
        ]
    }],
    labels: [
        "Doanh thu chính",
        "Doanh thu tài chính",
        "Doanh thu khác"
    ]
}

const pieData3 = {
    datasets: [{
        data: [],
        backgroundColor: [
            color1,
            color2,
            color3
        ],
        hoverBackgroundColor: [
            color1,
            color2,
            color3
        ]
    }],
    labels: [
        "Doanh thu chính",
        "Doanh thu tài chính",
        "Doanh thu khác"
    ]
}

const pieData4 = {
    datasets: [{
        data: [],
        backgroundColor: [
            color1,
            color2,
            color3
        ],
        hoverBackgroundColor: [
            color1,
            color2,
            color3
        ]
    }],
    labels: [
        "Doanh thu chính",
        "Doanh thu tài chính",
        "Doanh thu khác"
    ]
}

const pieOption = {
    legend: {
        position: 'top',
        labels: {
            fontFamily: 'Poppins'
        }

    },
    responsive: true
}

class IncomeChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isPieLoading: false,
            year: CONSTANT.YEAR_2019,
            isYearEmpty: false,
            isQ1Empty: false,
            isQ2Empty: false,
            isQ3Empty: false,
            isQ4Empty: false,
        }
    }

    componentWillMount() {
        this.getIncomeChart()
    }

    onYearSelect(value) {
        let year = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        this.setState({
            year: year
        }, this.getIncomeChart)
    }

    getIncomeChart() {
        let url = API.GET_INCOME_CHART + "?year=" + this.state.year
        console.log("getIncomeChart", url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized') 
                AppUtils.logout()
            else {if (res.data.length !== 0) {
                    this.parseIncomeDataChart(res.data)
                } else {
                    console.log("lỗi")
                }
            }
            })
            .catch(response => {
                if (response.error === 'Unauthorized') 
                AppUtils.logout()
            })
            .finally(() => { });
    }

    parseIncomeDataChart(data) {
        let dtY = []
        let dtQ1 = []
        let dtQ2 = []
        let dtQ3 = []
        let dtQ4 = []

        data.forEach(element => {
            if (element.quarter == 5) {
                dtY = this.parseData(element)
                pieData.datasets[0].data = dtY
            }
            else if (element.quarter == 1) {
                dtQ1 = this.parseData(element)
                pieData1.datasets[0].data = dtQ1
            }
            else if (element.quarter == 2) {
                dtQ2 = this.parseData(element)
                pieData2.datasets[0].data = dtQ2
            }
            else if (element.quarter == 3) {
                dtQ3 = this.parseData(element)
                pieData3.datasets[0].data = dtQ3
            } else {
                dtQ4 = this.parseData(element)
                pieData4.datasets[0].data = dtQ4
            }
        })

        console.log("dtY", dtY)
        console.log("dtQ1", dtQ1)
        console.log("dtQ2", dtQ2)
        console.log("dtQ3", dtQ3)
        console.log("dtQ4", dtQ4)
        
        this.setState({
            isPieLoading: true,
            isYearEmpty: dtY.length == 0,
            isQ1Empty: dtQ1.length == 0,
            isQ2Empty: dtQ2.length == 0,
            isQ3Empty: dtQ3.length == 0,
            isQ4Empty: dtQ4.length == 0,
        })
    }

    parseData(data) {
        if (data.length == 0) return []
        let dt = []
        dt.push(data.coreRevenue)
        dt.push(data.financialRevenue)
        dt.push(data.otherRevenue)
        return dt
    }

    render() {
        const { isYearEmpty, isQ1Empty, isQ2Empty, isQ3Empty, isQ4Empty } = this.state
        return (
            <div className="section__content section__content--p30">
                <div className="container-fluid">
                    <div className="row">
                        {
                            !isYearEmpty && <div className="col-lg-12">
                                <div className="au-card m-b-30">
                                    <div className="au-card-inner">
                                        <div className="row m-b-40">
                                        <h3 className="title-2 justify-content-center text-center">Tổng doanh thu cả năm</h3>
                                        {
                                            isYearEmpty && <div className="col-lg-3 m-b-30">
                                                <Select
                                                    onChange={this.onYearSelect.bind(this)}
                                                    options={chartYearOption}
                                                    formatGroupLabel={formatGroupLabel}
                                                    placeholder="2019"
                                                    maxMenuHeight="200px"
                                                />
                                            </div>
                                        }
                                            <Pie data={pieData} options={pieOption} height='100' />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        }
                        <div className="col-lg-12">
                            <div className="au-card m-b-30">
                                <div className="au-card-inner">
                                    <div className="row m-b-40">
                                        <h3 className="title-2 justify-content-center text-center">Doanh thu Quý 1</h3>
                                        {
                                            isYearEmpty && <div className="col-lg-3 m-b-30">
                                                <Select
                                                    onChange={this.onYearSelect.bind(this)}
                                                    options={chartYearOption}
                                                    formatGroupLabel={formatGroupLabel}
                                                    placeholder="2019"
                                                    maxMenuHeight="200px"
                                                />
                                            </div>
                                        }
                                        {
                                            !isQ1Empty && <Pie data={pieData1} options={pieOption} height='100' />
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-12">
                            <div className="au-card m-b-30">
                                <div className="au-card-inner">
                                    <h3 className="title-2 m-b-10">Doanh thu Quý 2 </h3>
                                    <div className="row m-b-40">
                                        {
                                            !isQ2Empty && <Pie data={pieData2} options={pieOption} height='100' />
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-12">
                            <div className="au-card m-b-30">
                                <div className="au-card-inner">
                                    <div className="row m-b-40">
                                        <h3 className="title-2 m-b-10">Doanh thu Quý 3</h3>
                                        {
                                            !isQ3Empty && <Pie data={pieData3} options={pieOption} height='100' />
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-12">
                            <div className="au-card m-b-30">
                                <div className="au-card-inner">
                                    <div className="row m-b-40">
                                        <h3 className="title-2 m-b-10">Doanh thu Quý 4</h3>
                                        {
                                            !isQ4Empty && <Pie data={pieData4} options={pieOption} height='100' />
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default IncomeChart;
import React, { Component } from 'react';
import API from './../API/API.js'
import CONSTANT from './../Constant'
import AppUtils from '../Utils/AppUtils';
import { Container, Row, Col } from 'react-bootstrap'
import Button from 'react-bootstrap/Button'
import Table from 'react-bootstrap/Table'
import axios from 'axios'
import { Pie } from 'react-chartjs-2';
import NumberFormat from 'react-number-format';
import editIcon from '@iconify/icons-zmdi/edit';
import chartIcon from '@iconify/icons-zmdi/chart';
import downloadIcon from '@iconify/icons-zmdi/download';
import historyIcon from '@iconify/icons-fa-solid/history';
import { Icon } from '@iconify/react';
import { Modal } from 'react-bootstrap'
import EditReport from '../Report/EditReport.js';
import EditFinancial from '../Report/EditFinancial.js';
import EditIncome from '../Report/EditIncome.js';
import EditCashflow from '../Report/EditCashflow.js';
import { NavLink } from "react-router-dom";
import FinancialHisDetail from '../Report/FinancialHisDetail.js';
import CashflowHisDetail from '../Report/CashflowHisDetail.js';
import IncomeHisDetail from '../Report/IncomeHisDetail.js';

const incomeChartData = {
    datasets: [
        {
            label: "My dataset",
            data: [1, 1],
            backgroundColor: [
                '#00b5e9',
                '#fa4251'
            ],
            hoverBackgroundColor: [
                '#00b5e9',
                '#fa4251'
            ],
            borderWidth: [
                0, 0
            ],
            hoverBorderColor: [
                'transparent',
                'transparent'
            ]
        }]
    ,
    labels: [
        'Profit Before Tax',
        'Profit After Tax'
    ]
}

const cashflowChartData = {
    datasets: [
        {
            label: "My dataset",
            data: [1, 1],
            backgroundColor: [
                '#00b5e9',
                '#fa4251'
            ],
            hoverBackgroundColor: [
                '#00b5e9',
                '#fa4251'
            ],
            borderWidth: [
                0, 0
            ],
            hoverBorderColor: [
                'transparent',
                'transparent'
            ]
        }]
    ,
    labels: [
        'Total Input',
        'Total Output'
    ]
}

const chartOption = {
    maintainAspectRatio: false,
    responsive: true,
    cutoutPercentage: 87,
    animation: {
        animateScale: true,
        animateRotate: true
    },
    legend: {
        display: false,
        position: 'bottom',
        labels: {
            fontSize: 14,
            fontFamily: "Poppins,sans-serif"
        }

    },
    tooltips: {
        titleFontFamily: "Poppins",
        xPadding: 15,
        yPadding: 10,
        caretPadding: 0,
        bodyFontSize: 16,
    },
    responsive: true
}

class ReportDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            statement: {},
            financial: {},
            income: {},
            cashflow: {},
            isLoading: false,
            isDownload: false,
            showFormStatement: false,
            showFormFinancail: false,
            showFormIncome: false,
            showFormCashflow: false,
            chosenItem: {},
            financialHis: [],
            incomeHis: [],
            cashflowHis: [],
            showFormFinancialHisDetail: false,
            showFormIncomeHisDetail: false,
            showFormCashflowHisDetail: false,
            isPutFinan: true,
            isPutIncome: true,
            isPutCash: true
        }

        this.onDownloadItem = this.onDownloadItem.bind(this)
        this.onEditCallback = this.onEditCallback.bind(this);
        this.onFinancialCallback = this.onFinancialCallback.bind(this);
        this.onIncomeCallback = this.onIncomeCallback.bind(this);
        this.onCashflowCallback = this.onCashflowCallback.bind(this);
    }

    onDownloadItem() {
        let item = this.state.statement
        let url = API.GET_STATEMENT_BY_ID + this.props.match.params.id
        console.log("onDownloadItem", url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            console.log(res.data)
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data.length !== 0) {
                    let proof = res.data.proof
                    console.log("res.data", res.data)
                    let quarter = item.quarter
                    let linkView = "/baocaochitiet/" + item.year
                    if (proof != undefined && proof != "") {
                        localStorage.setItem(CONSTANT.BASE64_REPORT, proof)
                        if (quarter !== 0 && quarter !== "" && quarter !== undefined)
                            linkView += "." + quarter
                    }
                    window.open(linkView, "_blank")
                } else {
                    console.log("lỗi")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        })
            .finally(() => { });
    }

    handleCloseStatement() {
        this.setState({
            showFormStatement: false
        })
    }

    handleShowStatement(value) {
        this.setState({
            showFormStatement: true,
            chosenItem: value
        })
    }

    onEditCallback(value) {
        if (value.proof != undefined && value.proof != "") {
            this.setState({
                isDownload: true
            })
        }
        this.setState({
            statement: value,
            showFormStatement: false
        })
        this.onAddbucket()
        localStorage.setItem(CONSTANT.BASE64_REPORT, value.proof)
    }

    handleCloseFinancial() {
        this.setState({
            showFormFinancail: false
        })
    }

    handleShowFinancial(value) {
        this.setState({
            showFormFinancail: true,
            chosenItem: value
        })
    }

    handleCloseFinancialHisDetail() {
        this.setState({
            showFormFinancialHisDetail: false
        })
    }

    handleShowFinancialHisDetail(value) {
        this.setState({
            showFormFinancialHisDetail: true,
            chosenItem: value
        })
    }

    handleCloseIncomeHisDetail() {
        this.setState({
            showFormIncomeHisDetail: false
        })
    }

    handleShowIncomeHisDetail(value) {
        this.setState({
            showFormIncomeHisDetail: true,
            chosenItem: value
        })
    }

    handleCloseCashflowHisDetail() {
        this.setState({
            showFormCashflowHisDetail: false
        })
    }

    handleShowCashflowHisDetail(value) {
        this.setState({
            showFormCashflowHisDetail: true,
            chosenItem: value
        })
    }

    onFinancialCallback(value) {
        if (value != undefined) {
            this.setState({
                financial: value,
                showFormFinancail: false
            }, function () {
                this.loadFinancialHistory();
            })
        }
    }

    handleCloseIncome() {
        this.setState({
            showFormIncome: false
        })
    }

    handleShowIncome(value) {
        this.setState({
            showFormIncome: true,
            chosenItem: value
        })
    }

    handleCloseIncomeHisDetail() {
        this.setState({
            showFormIncomeHisDetail: false
        })
    }

    handleShowIncomeHisDetail(value) {
        this.setState({
            showFormIncomeHisDetail: true,
            chosenItem: value
        })
    }

    onIncomeCallback(value) {
        if (value != undefined) {
            this.setState({
                income: value,
                showFormIncome: false
            })
        }
    }

    handleShowCashflow(value) {
        this.setState({
            showFormCashflow: true,
            chosenItem: value
        })
    }

    handleCloseCashflow() {
        this.setState({
            showFormCashflow: false
        })
    }

    handleCloseCashflowHisDetail() {
        this.setState({
            showFormCashflowHisDetail: false
        })
    }

    handleShowCashflowHisDetail(value) {
        this.setState({
            showFormCashflowHisDetail: true,
            chosenItem: value
        })
    }

    onCashflowCallback(value) {
        if (value != undefined) {
            this.setState({
                cashflow: value,
                showFormCashflow: false
            })
        }
    }

    onAddbucket() {
        this.loadFinancialHistory()
        this.loadCashflowHistory()
        this.loadIncomeHistory()
        this.loadFinancialDetail()
        this.loadIncomeDetail()
        this.loadCashflowDetail()
        this.loadStatementData()
    }

    componentWillMount() {
        this.onAddbucket()
    }

    loadStatementData() {
        let url = API.GET_STATEMENT_BY_ID + this.props.match.params.id
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("loadStatementData", url)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        if (res.data.proof != undefined && res.data.proof != "") {
                            this.setState({
                                isDownload: true,
                            })
                        }
                        this.setState({
                            statement: res.data,
                            isLoading: true
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    loadFinancialDetail() {
        let url = API.GET_FINANCIAL_STATEMENT_BY_ID + this.props.match.params.id
        console.log("url", url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("loadFinancialDetail", url)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.setState({
                            financial: res.data,
                            isAddnew: false
                        })
                    } else {
                        console.log("lỗi")
                        this.setState({
                            isAddnew: true
                        })
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    loadFinancialHistory() {
        let itemId = this.props.match.params.id
        let url = API.GET_HISTORY_FINANCIAL_STATEMENT + itemId
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("loadFinancialHistory", url)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.setState({
                            financialHis: res.data
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    loadCashflowDetail() {
        let url = API.GET_CASHFLOW_STATEMENT_BY_ID + this.props.match.params.id
        console.log("url", url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("loadCashflowDetail", url)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.setState({
                            cashflow: res.data,
                            isAddnew: false
                        })

                        let totalInput = parseInt(res.data.inputActivityBusiness) + parseInt(res.data.inputActivityInvestment) + parseInt(res.data.inputActivityFinancial);
                        let totalOutput = parseInt(res.data.outputActivityBusiness) + parseInt(res.data.outputActivityInvestment) + parseInt(res.data.outputActivityFinancial);

                        if (isNaN(totalInput)) totalInput = 1
                        if (isNaN(totalOutput)) totalOutput = 1

                        cashflowChartData.datasets[0].data = []
                        cashflowChartData.datasets[0].data.push(totalInput)
                        cashflowChartData.datasets[0].data.push(totalOutput)

                    } else {
                        console.log("lỗi")
                        this.setState({
                            isAddnew: true
                        })
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    loadCashflowHistory() {
        let itemId = this.props.match.params.id
        let url = API.GET_HISTORY_CASHFLOW_STATEMENT + itemId
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("loadCashflowHistory", url)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.setState({
                            cashflowHis: res.data
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    loadIncomeDetail() {
        let url = API.GET_INCOME_STATEMENT_BY_ID + this.props.match.params.id
        console.log("url", url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("loadIncomeDetail", url)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.setState({
                            income: res.data,
                            isAddnew: false,
                        })

                        // console.log(chartData.datasets[0].data)
                        incomeChartData.datasets[0].data = []
                        let before = parseInt(res.data.profitBeforeTax)
                        if (isNaN(before)) before = 1
                        let after = parseInt(res.data.profitAfterTax)
                        if (isNaN(after)) after = 1
                        incomeChartData.datasets[0].data.push(before)
                        incomeChartData.datasets[0].data.push(after)
                    } else {
                        console.log("lỗi")
                        this.setState({
                            isAddnew: true
                        })
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    loadIncomeHistory() {
        let itemId = this.props.match.params.id
        let url = API.GET_HISTORY_INCOME_STATEMENT + itemId
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("url", url)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.setState({
                            incomeHis: res.data
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    render() {
        var { statement, financial, income, cashflow, financialHis, incomeHis, cashflowHis, isLoading } = this.state
        let statementId = this.props.match.params.id
        let userRole = localStorage.getItem(CONSTANT.USER_LOGIN_ROLE)
        let isAccountant = userRole == CONSTANT.ROLE_ACCOUNTANT
        let isAuditor = userRole == CONSTANT.ROLE_AUDITOR
        let finanCount = financialHis.length
        let incomeCount = incomeHis.length
        let cashCount = cashflowHis.length
        let showFinHisBtn = finanCount != 0
        let showIncomeHisBtn = incomeCount != 0
        let showCashHisBtn = cashCount != 0

        let editFinancial = "Chỉnh sửa"
        if (Object.keys(financial).length == 0) {
            editFinancial = "Thêm mới"
        }
        if (isAuditor) editFinancial = "Xác minh"
        let editIncome = "Chỉnh sửa"
        if (Object.keys(income).length == 0) {
            editIncome = "Thêm mới"
        }
        let editCashflow = "Chỉnh sửa"
        if (Object.keys(cashflow).length == 0) {
            editCashflow = "Thêm mới"
        }

        let quarter = statement.quarter + ""
        if (statement.quarter == 5) quarter = "Cả năm"

        let pathname = this.props.location.pathname

        let statementHis = "/quanly/statement/history/"
        let FinancialHis = "/quanly/statement/financial/history/"
        let IncomeHis = "/quanly/statement/income/history/"
        let CashflowHis = "/quanly/statement/cashflow/history/"

        if (pathname.indexOf("admin") == - 1) {
            statementHis = "/baocao/lichsu/:id/"
            FinancialHis = "/baocao/taichinh/lichsu/"
            IncomeHis = "/baocao/thunhap/lichsu/"
            CashflowHis = "/baocao/chuyenluutiente/lichsu/"
        }

        return (
            isLoading && <div className="section__content section__content--p30">
                <div className="container-fluid">
                    <div class="row">
                        <div className="col-md-12">
                            <div className="au-card chart-percent-card">
                                <div className="overview-wrap">
                                    <h2 className="title-1">Báo cáo {statement.year}</h2>
                                    <div>
                                        {
                                            isAccountant && <Button variant='success' onClick={() => this.handleShowStatement(statement)}>
                                                <Icon icon={editIcon} /> {' '}Chỉnh sửa
                                             </Button>
                                        }

                                        {' '}
                                        <Button variant='info' onClick={this.onDownloadItem}>
                                            <Icon icon={downloadIcon} /> {' '}Tải về
                                        </Button>
                                    </div>

                                </div>

                                <div className="col-lg-12">
                                    <div className="top-campaign">
                                        <div className="table-responsive">
                                            <table className="table table-top-campaign">
                                                <tbody>
                                                    <tr>
                                                        <td>1. Tiêu đề</td>
                                                        <td>{statement.title}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2. Mô tả</td>
                                                        <td>{statement.description}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3. Quý</td>
                                                        <td>{quarter}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>4. Năm</td>
                                                        <td>{statement.year}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="au-card chart-percent-card">
                                <div className="overview-wrap">
                                    <h2 className="title-1">Báo cáo Tài chính</h2>
                                    <div>
                                        {
                                            (isAccountant || isAuditor) && <Button variant='success' onClick={() => this.handleShowFinancial(financial)}>
                                                <Icon icon={editIcon} /> {' '}{editFinancial}
                                            </Button>
                                        }
                                        {' '}
                                        {
                                            showFinHisBtn && <div className="mydropdown">
                                                <NavLink to={FinancialHis + statementId} >
                                                    <Button variant='warning'>
                                                        <Icon icon={historyIcon} /> {' '}Lịch sử
                                                    </Button>
                                                </NavLink>
                                                <div className="mydropdown-content">
                                                    {
                                                        financialHis.map((item, key) => {
                                                            return (
                                                                <p onClick={() => this.handleShowFinancialHisDetail(item)}>{AppUtils.toDateTime(item.createAt)}</p>
                                                            );
                                                        })
                                                    }
                                                </div>
                                            </div>
                                        }
                                    </div>
                                </div>
                                <div className="col-lg-12 text-right" style={{ fontStyle: 'italic' }}>
                                    <span>Số lần thay đổi: {finanCount}</span>
                                </div>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="top-campaign">
                                            <div className="table-responsive">
                                                <table className="table table-top-campaign">
                                                    <tbody>
                                                        <tr>
                                                            <td>1. Ý kiến kiểm toán viên</td>
                                                            <td>{financial.auditorOption}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>2. Tổng nợ</td>
                                                            <td>
                                                                <NumberFormat value={financial.totalDebt} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                <a>{CONSTANT.CURRENCY_UNIT}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>3. Tổng tài sản</td>
                                                            <td>
                                                                <NumberFormat value={financial.totalAsset} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                <a>{CONSTANT.CURRENCY_UNIT}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>4. Vốn chủ sở hữu</td>
                                                            <td>
                                                                <NumberFormat value={financial.initialCapital} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                <a>{CONSTANT.CURRENCY_UNIT}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>5. Nợ dài hạn</td>
                                                            <td>
                                                                <NumberFormat value={financial.longTermDebt} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                <a>{CONSTANT.CURRENCY_UNIT}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>6. Nợ ngắn hạn</td>
                                                            <td>
                                                                <NumberFormat value={financial.shortTermDebt} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                <a>{CONSTANT.CURRENCY_UNIT}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>5. Quý</td>
                                                            <td>
                                                                {quarter}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>6. Năm</td>
                                                            <td>
                                                                {statement.year}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>7. Ngày tạo</td>
                                                            <td>{AppUtils.toDateTime(financial.createAt)}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-lg-6">
                                        {/* <div className="top-campaign" style={{
                                            boxShadow: "0px 10px 20px 0px rgba(0, 0, 0, 0.03)",
                                            marginTop: "5%",
                                            height: "85%"
                                        }}>
                                            <figure>
                                                <a data-lightbox="portfolio" href={this.state.financial.proofImage} className="link-preview" title="Preview">
                                                    <img style={{ width: "100%" }} src={this.state.financial.proofImage} className="img-fluid" alt="" /></a>
                                            </figure>
                                        </div> */}
                                        <figure>
                                            <a data-lightbox="portfolio" href={this.state.financial.proofImage} className="link-preview" title="Preview">
                                                <img style={{ width: "100%" }} src={this.state.financial.proofImage} className="img-fluid" alt="" /></a>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-12">
                            <div className="au-card chart-percent-card">
                                <div className="overview-wrap">
                                    <h2 className="title-1">Báo cáo thu nhập</h2>
                                    <div>
                                        {
                                            isAccountant && <Button variant='success' onClick={() => this.handleShowIncome(income)}>
                                                <Icon icon={editIcon} /> {' '}{editIncome}
                                            </Button>
                                        }
                                        {' '}
                                        {
                                            showIncomeHisBtn && <div className="mydropdown">
                                                <NavLink to={IncomeHis + statementId} >
                                                    <Button variant='warning'>
                                                        <Icon icon={historyIcon} /> {' '}Lịch sử
                                                    </Button>
                                                </NavLink>
                                                <div className="mydropdown-content">
                                                    {
                                                        incomeHis.map((item, key) => {
                                                            return (
                                                                <p onClick={() => this.handleShowIncomeHisDetail(item)}>{AppUtils.toDateTime(item.createAt)}</p>
                                                            );
                                                        })
                                                    }
                                                </div>
                                            </div>
                                        }
                                    </div>
                                </div>
                                <div className="col-lg-12 text-right" style={{ fontStyle: 'italic' }}>
                                    <span>Số lần thay đổi: {incomeCount}</span>
                                </div>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="top-campaign">
                                            <div className="table-responsive">
                                                <table className="table table-top-campaign">
                                                    <tbody>
                                                        <tr>
                                                            <td>1. Tổng doanh thu</td>
                                                            <td>
                                                                <NumberFormat value={income.totalRevenue} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                <a>{CONSTANT.CURRENCY_UNIT}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2. Doanh thu chính</td>
                                                            <td>
                                                                <NumberFormat value={income.coreRevenue} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                <a>{CONSTANT.CURRENCY_UNIT}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>3. Doanh thu tài chính</td>
                                                            <td>
                                                                <NumberFormat value={income.financialRevenue} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                <a>{CONSTANT.CURRENCY_UNIT}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>4. Doanh thu khác</td>
                                                            <td>
                                                                <NumberFormat value={income.otherRevenue} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                <a>{CONSTANT.CURRENCY_UNIT}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>5. Quý</td>
                                                            <td>
                                                                {quarter}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>6. Năm</td>
                                                            <td>
                                                                {statement.year}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>7. Ngày tạo</td>
                                                            <td>{AppUtils.toDateTime(income.createAt)}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <figure>
                                            <a data-lightbox="portfolio" href={this.state.income.proofImage} className="link-preview" title="Preview">
                                                <img style={{ width: "100%" }} src={this.state.income.proofImage} className="img-fluid" alt="" /></a>
                                        </figure>
                                    </div>
                                </div>
                                {/* <div className="row">
                                    <div className="col-lg-6" >
                                        <div class="chart-percent-2" >
                                            <div class="chart-wrap">
                                                <Pie data={incomeChartData} options={chartOption} />
                                            </div>
                                            <div class="chart-info">
                                                <div class="chart-note">
                                                    <span class="dot dot--blue"></span>
                                                    <span>Lợi nhuận trước thuế</span>
                                                </div>
                                                <div class="chart-note">
                                                    <span class="dot dot--red"></span>
                                                    <span>Lợi nhuận sau thuế</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> */}
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-12">
                            <div className="au-card chart-percent-card">
                                <div className="overview-wrap">
                                    <h2 className="title-1">Báo cáo chuyển lưu tiền tệ</h2>
                                    <div>
                                        {
                                            isAccountant && <Button variant='success' onClick={() => this.handleShowCashflow(cashflow)}>
                                                <Icon icon={editIcon} /> {' '}{editCashflow}
                                            </Button>
                                        }
                                        {' '}
                                        {
                                            showCashHisBtn && <div className="mydropdown">
                                                <NavLink to={CashflowHis + statementId} >
                                                    <Button variant='warning'>
                                                        <Icon icon={historyIcon} /> {' '}Lịch sử
                                                    </Button>
                                                </NavLink>
                                                <div className="mydropdown-content">
                                                    {
                                                        cashflowHis.map((item, key) => {
                                                            return (
                                                                <p onClick={() => this.handleShowCashflowHisDetail(item)}>{AppUtils.toDateTime(item.createAt)}</p>
                                                            );
                                                        })
                                                    }
                                                </div>
                                            </div>
                                        }

                                    </div>
                                </div>
                                <div className="col-lg-12 text-right" style={{ fontStyle: 'italic' }}>
                                    <span>Số lần thay đổi: {cashCount}</span>
                                </div>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="top-campaign">
                                            <div className="table-responsive">
                                                <table className="table table-top-campaign">
                                                    <tbody>
                                                        <tr>
                                                            <td>1. Chi phí hoạt động doanh nghiệp</td>
                                                            <td>
                                                                <NumberFormat value={cashflow.outputActivityBusiness} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                <a>{CONSTANT.CURRENCY_UNIT}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2. Thu nhập hoạt động doanh nghiệp</td>
                                                            <td>
                                                                <NumberFormat value={cashflow.inputActivityBusiness} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                <a>{CONSTANT.CURRENCY_UNIT}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>3. Chi phí hoạt động đầu tư</td>
                                                            <td>
                                                                <NumberFormat value={cashflow.outputActivityInvestment} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                <a>{CONSTANT.CURRENCY_UNIT}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>4. Thu nhập hoạt động đầu tư</td>
                                                            <td>
                                                                <NumberFormat value={cashflow.inputActivityInvestment} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                <a>{CONSTANT.CURRENCY_UNIT}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>5. Chi phí hoạt động tài chính</td>
                                                            <td>
                                                                <NumberFormat value={cashflow.outputActivityFinancial} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                <a>{CONSTANT.CURRENCY_UNIT}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>6. Thu nhập hoạt động tài chính</td>
                                                            <td>
                                                                <NumberFormat value={cashflow.inputActivityFinancial} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                                <a>{CONSTANT.CURRENCY_UNIT}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>7. Quý</td>
                                                            <td>
                                                                {quarter}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>8. Năm</td>
                                                            <td>
                                                                {statement.year}
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>9. Ngày tạo</td>
                                                            <td>{AppUtils.toDateTime(cashflow.createAt)}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        {/* <div className="top-campaign" style={{ height: "100%" }}>
                                            <div className="au-card-inner">
                                                <div className="table-responsive">
                                                    <img style={{ width: "100%" }} src={this.state.cashflow.proofImage} />
                                                </div>
                                            </div>
                                        </div> */}
                                        <figure>
                                            <a data-lightbox="portfolio" href={this.state.cashflow.proofImage} className="link-preview" title="Preview">
                                                <img style={{ width: "100%" }} src={this.state.cashflow.proofImage} className="img-fluid" alt="" /></a>
                                        </figure>
                                    </div>
                                </div>
                                {/* <div className="row">
                                    <div className="col-lg-6" >
                                        <div class="chart-percent-2" >
                                            <div class="chart-wrap">
                                                <Pie data={cashflowChartData} options={chartOption} />
                                            </div>
                                            <div class="chart-info">
                                                <div class="chart-note">
                                                    <span class="dot dot--blue"></span>
                                                    <span>Tổng thu nhập</span>
                                                </div>
                                                <div class="chart-note">
                                                    <span class="dot dot--red"></span>
                                                    <span>Tổng chi phí</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>
                <Modal show={this.state.showFormStatement} onHide={this.handleCloseStatement.bind(this)}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Chỉnh sửa</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <EditReport itemId={this.props.match.params.id} itemDetail={this.state.chosenItem} onEditCallback={d => this.onEditCallback(d)} />
                    </Modal.Body>
                </Modal>

                <Modal show={this.state.showFormFinancail} onHide={this.handleCloseFinancial.bind(this)}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Báo cáo tài chính</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <EditFinancial isAuditor={isAuditor}  itemId={this.props.match.params.id} itemDetail={this.state.chosenItem} onEditCallback={d => this.onFinancialCallback(d)} isEdit={editFinancial} year={statement.year} quarter={statement.quarter} />
                    </Modal.Body>
                </Modal>

                <Modal show={this.state.showFormFinancialHisDetail} onHide={this.handleCloseFinancialHisDetail.bind(this)}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Lịch sử thay đổi</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FinancialHisDetail itemDetail={this.state.chosenItem} year={statement.year} quarter={statement.quarter}/>
                    </Modal.Body>
                </Modal>

                <Modal show={this.state.showFormIncome} onHide={this.handleCloseIncome.bind(this)}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Báo cáo thu nhập</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <EditIncome itemId={this.props.match.params.id} itemDetail={this.state.chosenItem} onEditCallback={d => this.onIncomeCallback(d)} isEdit={editIncome} year={statement.year} quarter={statement.quarter} />
                    </Modal.Body>
                </Modal>

                <Modal show={this.state.showFormIncomeHisDetail} onHide={this.handleCloseIncomeHisDetail.bind(this)}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Lịch sử thay đổi</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <IncomeHisDetail itemDetail={this.state.chosenItem} />
                    </Modal.Body>
                </Modal>

                <Modal show={this.state.showFormCashflow} onHide={this.handleCloseCashflow.bind(this)}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Báo cáo chuyển lưu tiền tệ</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <EditCashflow  itemId={this.props.match.params.id} itemDetail={this.state.chosenItem} onEditCallback={d => this.onCashflowCallback(d)} isEdit={editCashflow} year={statement.year} quarter={statement.quarter} />
                    </Modal.Body>
                </Modal>

                <Modal show={this.state.showFormCashflowHisDetail} onHide={this.handleCloseCashflowHisDetail.bind(this)}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Lịch sử thay đổi</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <CashflowHisDetail itemDetail={this.state.chosenItem} />
                    </Modal.Body>
                </Modal>

                <Modal show={this.state.showFormCashflowHisDetail} onHide={this.handleCloseCashflowHisDetail.bind(this)}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Lịch sử thay đổi</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <CashflowHisDetail itemDetail={this.state.chosenItem} />
                    </Modal.Body>
                </Modal>
            </div >
        );
    }
}

export default ReportDetail;
import React, { Component } from 'react';
import axios from 'axios'
import API from './../API/API.js'
import AppUtils from '../Utils/AppUtils';
import { Icon } from '@iconify/react';
import detailIcon from '@iconify/icons-zmdi/more';
import ViewImage from './ViewImage.js';
import { Modal } from 'react-bootstrap'
import NumberFormat from 'react-number-format';
import CONSTANT from '../Constant.js';
import FinancialHisDetail from '../Report/FinancialHisDetail.js';

class FinancialHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            chosenItem: {},
            showFormDetail: false,
            isLoading: false
        }
    }

    handleClose() {
        this.setState({
            showFormDetail: false
        })
    }

    handleShow(image) {
        this.setState({
            chosenItem: image,
            showFormDetail: true
        })
    }

    componentWillMount() {
        let itemId = this.props.match.params.id
        let url = API.GET_HISTORY_FINANCIAL_STATEMENT + itemId
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("authorization", authorization)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        let sortedData = AppUtils.sortListByLastCreateDate(res.data, true)
                        this.setState({
                            data: sortedData,
                            isLoading: true
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    };

    render() {
        var { data, isLoading } = this.state;
        return (
            isLoading && <div className="section__content section__content--p30">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            {/* DATA TABLE */}
                            <h3 className="title-5 m-b-35">Lịch sử thay đổi</h3>
                            <a >* Đơn vị tiền tệ: {CONSTANT.CURRENCY_UNIT}</a>
                            <div className="table-responsive table-responsive-data2">
                                <table className="table table-data2">
                                    <thead>
                                        <tr>
                                            <th>Ngày thêm báo cáo</th>
                                            <th>Tổng nợ</th>
                                            <th>Nợ ngắn hạn</th>
                                            <th>Nợ dài hạn</th>
                                            <th>Tổng tài sản</th>
                                            <th>Vốn chủ sở hữu</th>
                                            <th>Ngày cập nhật báo cáo</th>
                                            <th />
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            data.map((item, key) => {
                                                return (
                                                    <tr className="tr-shadow">
                                                        <td>{AppUtils.toDateTime(item.createAt)}</td>
                                                        <td>
                                                            <NumberFormat value={item.totalDebt} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                        </td>
                                                        <td>
                                                            <NumberFormat value={item.shortTermDebt} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                        </td>
                                                        <td>
                                                            <NumberFormat value={item.longTermDebt} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                        </td>
                                                        <td>
                                                            <NumberFormat value={item.totalAsset} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                        </td>
                                                        <td>
                                                            <NumberFormat value={item.initialCapital} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                        </td>
                                                        <td>{AppUtils.toDateTime(item.lastCreateAt)}</td>
                                                        <td>
                                                            <button className="item" data-toggle="tooltip" data-placement="top" title="Detail" onClick={() => this.handleShow(item)}>
                                                                <Icon icon={detailIcon} />
                                                            </button>
                                                        </td>
                                                    </tr>
                                                )
                                            }
                                            )
                                        }
                                    </tbody>
                                </table>
                                <Modal show={this.state.showFormDetail} onHide={this.handleClose.bind(this)}
                                    size="lg"
                                    aria-labelledby="contained-modal-title-vcenter"
                                    centered>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Lịch sử thay đổi</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <FinancialHisDetail itemDetail={this.state.chosenItem} />
                                    </Modal.Body>
                                </Modal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default FinancialHistory;
import React, { Component } from 'react';
import Account from '../Account/Account';
import Footer from '../admin/Footer';
import Activity from './Activity';
import { Route } from "react-router-dom";
import Statement from '../Report/Statement';
import ActivityHistory from './ActivityHistory';
import ReportDetail from './ReportDetail';
import StatementHistory from './StatementHistory';
import UserInfo from './UserInfo';
import FinancialHistory from './FinancialHistory';
import IncomeHistory from './IncomeHistory';
import CashflowHistory from './CashflowHistory';
import CashflowChart from './CashflowChart';
import IncomeChart from './IncomeChart';
import FinancialChart from './FinancialChart';
import ShareHolderInfo from '../component/ShareHolderInfo';
import AddNewActivity from './AddNewActivity';
import ActivityDetail from './ActivityDetail';
import Category from './Category';
import HomePage from '../UserSide/HomePage';

class MainContent extends Component {
    render() {
        return (
            <div className="page-container2">
                {/* <Header/> */}
                <section id="about" className="section-bg text-center">
                    <div className="container" style={{ maxWidth: "1200px" }}>
                        <Route exact path="/quanly/user" component={Account} />
                        <Route exact path="/quanly/activity" component={Activity} />
                        <Route exact path="/quanly/statement" component={Statement} />
                        <Route exact path="/quanly/userinfo" component={UserInfo} />
                        <Route exact path="/quanly/activity/history/:id" component={ActivityHistory} />
                        <Route exact path="/quanly/statement/detail/:id" component={ReportDetail} />
                        <Route exact path="/quanly/statement/history/:id" component={StatementHistory} />
                        <Route exact path="/quanly/statement/financial/history/:id" component={FinancialHistory} />
                        <Route exact path="/quanly/statement/income/history/:id" component={IncomeHistory} />
                        <Route exact path="/quanly/statement/cashflow/history/:id" component={CashflowHistory} />
                        <Route exact path="/quanly/cashflowchart" component={CashflowChart} />
                        <Route exact path="/quanly/incomechart" component={IncomeChart} />
                        <Route exact path="/quanly/financialchart" component={FinancialChart} />
                        <Route exact path="/quanly/home" component={HomePage} />
                        <Route exact path="/quanly/taikhoan/chitiet/:id" component={ShareHolderInfo} />

                        <Route exact path="/quanly/themhoatdongmoi" component={AddNewActivity} />
                        <Route exact path="/chitiethoatdong/:id" component={ActivityDetail} />
                        <Route exact path="/quanly/hoatdongthuchi" component={Activity} />
                        <Route exact path="/quanly/chitiethoatdong/:id" component={ActivityDetail} />
                        <Route exact path="/quanly/danhmuchoatdong" component={Category} />
                        <Route exact path="/quanly/taikhoan" component={Account} />
                        <Route exact path="/quanly/thongke" component={HomePage} />
                        <Route exact path="/quanly/codong/chitiet/:id" component={ShareHolderInfo} />
                        <Route exact path="/codong/chitiet/:id" component={ShareHolderInfo} />
                        <Route exact path="/quanly/baocao" component={Statement} />
                    </div>
                </section>
                {/* <Footer /> */}
            </div>
        );
    }
}

export default MainContent;
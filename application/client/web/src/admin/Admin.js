import React, { Component } from 'react';
import AdminSidebar from './AdminSidebar';
import Header from './Header';
import AdminHome from '../router/AdminHome';
import { Redirect } from 'react-router-dom'
import CONSTANT from '../Constant';
import MainContent from './MainContent';

class Admin extends Component {
    constructor(props) {
        super(props);
        const token = localStorage.getItem(CONSTANT.USER_LOGIN_TOKEN)
        let loggedIn = true

        if (token == null) {
            loggedIn = false
        }

        this.state = {
            isUser: false,
            loggedIn
        }

    }

    componentWillMount() {
        let userRole = localStorage.getItem(CONSTANT.USER_LOGIN_ROLE)
        let isUser = userRole == CONSTANT.ROLE_USER
        this.setState({
            isUser: isUser
        })
    }

    render() {
        if (this.state.isUser) {
            return <Redirect to="/error"></Redirect>
        }
        if (this.state.loggedIn == false) {
            return <Redirect to="/admin"></Redirect>
        }
        return (
            <div className="row col-lg-12">
                <div className="col-lg-4 col-md-4">
                    <AdminSidebar />
                </div>
                <div className="col-lg-8 col-md-8">
                    <MainContent />
                </div>
                {/* <Header/>
                <div className="wrapper d-flex">
                    <AdminSidebar/>
                    <AdminHome/>
                </div> */}
            </div>
        );
    }
}

export default Admin;
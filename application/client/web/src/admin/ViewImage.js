import React, { Component } from 'react';

class ViewImage extends Component {
    render() {
        return (
            <div className="section__content section__content--p30">
                <div className="container-fluid">
                    <div class="row">
                        <div className="col-md-12">
                            <div className="top-campaign">
                                <div className="au-card-inner">
                                    <div className="table-responsive">
                                        <img style={{ width: "100%" }} src={this.props.image} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ViewImage;
import React, { Component } from 'react';
import axios from 'axios'
import { Container, Row, Col } from 'react-bootstrap'
import Table from 'react-bootstrap/Table'
import API from './../API/API.js'
import AppUtils from '../Utils/AppUtils';
import { NavLink } from "react-router-dom";
import { Icon } from '@iconify/react';
import downloadIcon from '@iconify/icons-zmdi/download';
import CONSTANT from './../Constant'

class StatementHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            chosenItem: {},
            showFormDetail: false
        }
    }

    handleClose() {
        this.setState({
            showFormDetail: false
        })
    }

    handleShow() {
        this.setState({
            showFormDetail: true
        })
    }

    componentWillMount() {
        let itemId = this.props.match.params.id
        let url = API.GET_HISTORY_REPORT  + itemId
        console.log("url", url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("authorization", authorization)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        let sortedData = AppUtils.sortListByLastCreateDate(res.data, true)
                        this.setState({
                            data: sortedData,
                            isLoading: true
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    };

    onDownloadItem(item) {
        let proof = item.proof
        if (proof != undefined && proof != "") {
            localStorage.setItem(CONSTANT.BASE64_REPORT, item.proof)
            let linkView = "/statement/" + item.title
            window.open(linkView, "_blank")
        } else {
            alert("No File Pdf")
        }
    }

    render() {
        var { data } = this.state;
        return (
            <section id="portfolio" className="text-center">
                <div className="container">
                    <header className="section-header">
                        <h3 className="section-title">Lịch sử thay đổi</h3>
                    </header>
                    <div className="row">
                        <div className="col-md-12">
                            {/* DATA TABLE */}
                            <div className="table-responsive table-responsive-data2">
                                <table className="table table-data2">
                                    <thead>
                                        <tr>
                                            <th>Ngày thêm báo cáo</th>
                                            <th>Tiêu đề</th>
                                            <th>Mô tả</th>
                                            <th>Quý</th>
                                            <th>Năm</th>
                                            <th>Ngày cập nhật báo cáo</th>
                                            <th />
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            data.map((item, key) => {
                                                return (
                                                    <tr className="tr-shadow">
                                                        <td>{AppUtils.toDateTime(item.createAt)}</td>
                                                        <td className="desc">{item.title}</td>
                                                        <td>
                                                            {item.description}
                                                        </td>
                                                        <td>{item.quarter}</td>
                                                        <td>{item.year}</td>
                                                        <td>{AppUtils.toDateTime(item.lastCreateAt)}</td>
                                                        <td>
                                                            <div >
                                                                <button className="item" data-toggle="tooltip" data-placement="top" title="Tải về" onClick={() => this.onDownloadItem(item)}>
                                                                    <Icon icon={downloadIcon} />
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                )
                                            }
                                            )
                                        }
                                    </tbody>
                                </table>
                            </div>
                            {/* END DATA TABLE */}
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default StatementHistory;
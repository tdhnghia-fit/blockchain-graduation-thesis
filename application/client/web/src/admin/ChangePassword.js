import React, { Component } from 'react';
import AppUtils from '../Utils/AppUtils';
import CONSTANT from '../Constant';
import API from '../API/API';
import axios from 'axios'
import { Formik } from "formik";
import * as EmailValidator from "email-validator";

class ChangePassword extends Component {

    changePassword() {
        let oldPass = document.getElementById("oldPassword").value
        let newPass = document.getElementById("newPassword").value 
        let confirmPass = document.getElementById("password").value
        let idUser = localStorage.getItem(CONSTANT.USER_LOGIN_TOKEN)
        let authorization = AppUtils.HEADER_AUTHORIZATION()

        let url = API.CHANGE_PASSWORD + idUser
        console.log(url)

        var {onChangePassword} = this.props
        axios({
            method: 'put',
            url: url,
            data: {
                oldPass: oldPass,
                newPass: newPass,
                confirmPass: confirmPass
            },
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            console.log(res);
            if (res.error === 'Unauthorized') 
            AppUtils.logout()
        else {
            onChangePassword()
        }
        })
        .catch(response => {
            if (response.error === 'Unauthorized') 
            AppUtils.logout()
        });
    }

    render() {
        return (
            <Formik
                initialValues={{ oldPassword: "", password: "", newPassword: "" }}
                onSubmit={(values, { setSubmitting }) => {
                    this.changePassword()
                }}

                //********Handling validation messages yourself*******/
                validate={values => {
                    let errors = {};

                    const passwordRegex = /(?=.*[0-9])/;

                    if (!values.password) {
                        errors.password = "Bắt buộc";
                    } else if (values.password.length < 8) {
                        errors.password = "Mật khẩu phải có 8 ký tự";
                    } else if (!passwordRegex.test(values.password)) {
                        errors.password = "Mật khẩu phải chứa ít nhất 1 ký tự";
                    }

                    if (!values.oldPassword) {
                        errors.oldPassword = "Bắt buộc";
                    } else if (values.oldPassword.length < 8) {
                        errors.password = "Mật khẩu phải có 8 ký tự";
                    } else if (!passwordRegex.test(values.oldPassword)) {
                        errors.password = "Mật khẩu phải chứa ít nhất 1 ký tự";
                    }

                    if (!values.newPassword) {
                        errors.oldPassword = "Bắt buộc";
                    } else if (values.newPassword.length < 8) {
                        errors.password = "Mật khẩu phải có 8 ký tự";
                    } else if (!passwordRegex.test(values.newPassword)) {
                        errors.password = "Mật khẩu phải chứa ít nhất 1 ký tự";
                    }

                    return errors;
                }}
            >

                {props => {
                    const {
                        values,
                        touched,
                        errors,
                        isSubmitting,
                        handleChange,
                        handleBlur,
                        handleSubmit
                    } = props;


                    return (
                        <form onSubmit={handleSubmit} id="myForm" style={{ width: "100%" }}>
                            <label htmlFor="email">Mật khẩu cũ</label>
                            <input
                                name="oldPassword"
                                type="password"
                                id="oldPassword"
                                placeholder="Enter your password"
                                value={values.oldPassword}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={errors.oldPassword && touched.oldPassword && "error"}
                            />
                            {errors.oldPassword && touched.oldPassword && (
                                <div className="input-feedback">{errors.oldPassword}</div>
                            )}
                            <label htmlFor="email">Mật khẩu mới</label>
                            <input
                                name="newPassword"
                                type="password"
                                id="newPassword"
                                placeholder="Enter your password"
                                value={values.newPassword}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={errors.newPassword && touched.newPassword && "error"}
                            />
                            {errors.newPassword && touched.newPassword && (
                                <div className="input-feedback">{errors.newPassword}</div>
                            )}

                            <label htmlFor="email">Xác nhận mật khẩu</label>
                            <input
                                name="password"
                                type="password"
                                id="password"
                                placeholder="Enter your password"
                                value={values.password}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={errors.password && touched.password && "error"}
                            />
                            {errors.password && touched.password && (
                                <div className="input-feedback">{errors.password}</div>
                            )}
                            <div className="text-center">
                                <button type="submit" >Xác nhận</button>
                            </div>
                        </form>
                    );
                }}
            </Formik>
        );
    }

    // render() {
    //     return (
    //         <Formik
    //             initialValues={{ email: "", password: "" }}
    //             onSubmit={(values, { setSubmitting }) => {
    //                 this.login()
    //             }}

    //             //********Handling validation messages yourself*******/
    //             validate={values => {
    //                 let errors = {};
    //                 if (!values.email) {
    //                     errors.email = "Required";
    //                 } else if (!EmailValidator.validate(values.email)) {
    //                     errors.email = "Invalid email address";
    //                 }

    //                 const passwordRegex = /(?=.*[0-9])/;
    //                 if (!values.password) {
    //                     errors.password = "Required";
    //                 } else if (values.password.length < 8) {
    //                     errors.password = "Password must be 8 characters long.";
    //                 } else if (!passwordRegex.test(values.password)) {
    //                     errors.password = "Invalida password. Must contain one number";
    //                 }

    //                 return errors;
    //             }}
    //         >

    //             {props => {
    //                 const {
    //                     values,
    //                     touched,
    //                     errors,
    //                     isSubmitting,
    //                     handleChange,
    //                     handleBlur,
    //                     handleSubmit
    //                 } = props;

    //                 return (
    //                     <form className="card" onSubmit={handleSubmit} id="myForm" style={{width: }}>
    //                         <div className="card-body card-block" >
    //                             <div className="row form-group">
    //                                 <div className="col col-md-3">
    //                                     <label for="hf-email" className=" form-control-label">Old Password</label>
    //                                 </div>
    //                                 <div className="col-12 col-md-9">
    //                                     <input
    //                                         name="oldpassword"
    //                                         type="oldpassword"
    //                                         id="txtOldPwd"
    //                                         placeholder="Enter your old password"
    //                                         value={values.password}
    //                                         onChange={handleChange}
    //                                         onBlur={handleBlur}
    //                                         className={errors.password && touched.password && "error"}
    //                                     />
    //                                     {errors.email && touched.email && (
    //                                         <div className="input-feedback">{errors.email}</div>
    //                                     )}
    //                                 </div>
    //                             </div>
    //                             <div className="row form-group">
    //                                 <div className="col col-md-3">
    //                                     <label for="hf-email" className=" form-control-label">New Password</label>
    //                                 </div>
    //                                 <div className="col-12 col-md-9">
    //                                     <input
    //                                         name="newpassword"
    //                                         type="newpassword"
    //                                         id="txtNewPwd"
    //                                         placeholder="Enter your new password"
    //                                         value={values.password}
    //                                         onChange={handleChange}
    //                                         onBlur={handleBlur}
    //                                         className={errors.password && touched.password && "error"}
    //                                     />
    //                                     {errors.password && touched.password && (
    //                                         <div className="input-feedback">{errors.password}</div>
    //                                     )}
    //                                 </div>
    //                             </div>
    //                             <div className="row form-group">
    //                                 <div className="col col-md-3">
    //                                     <label for="hf-password" className=" form-control-label">Confirm Password</label>
    //                                 </div>
    //                                 <div className="col-12 col-md-9">
    //                                     <input
    //                                         name="confirmpassword"
    //                                         type="confirmpassword"
    //                                         id="txtConfimPwd"
    //                                         placeholder="Confirm your new password"
    //                                         value={values.password}
    //                                         onChange={handleChange}
    //                                         onBlur={handleBlur}
    //                                         className={errors.password && touched.password && "error"}
    //                                     />
    //                                 </div>
    //                             </div>
    //                         </div>
    //                         <div className="card-footer">
    //                             <button type="submit" className="btn btn-primary btn-sm text-right" disabled={isSubmitting}>
    //                                 Submit
    //                             </button>
    //                         </div>
    //                     </form>
    //                 );
    //             }}
    //         </Formik>
    //     );
    // }
}

export default ChangePassword;
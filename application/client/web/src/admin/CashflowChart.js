import React, { Component } from 'react';
import { Bar, Line, Pie } from 'react-chartjs-2';
import API from './../API/API.js'
import AppUtils from '../Utils/AppUtils';
import axios from 'axios'
import { groupStyles, groupBadgeStyles, chartYearOption, quarterOptions } from '../Utils/DataGeneral'
import Select from 'react-select';
import CONSTANT from '../Constant.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

const formatGroupLabel = data => (
    <div style={groupStyles}>
        <span>{data.label}</span>
        <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
);

// Recent Report 2
const bd_brandProduct2 = 'rgba(0,181,233,0.9)'

const inputLineChart = {
    labels: [],
    type: 'line',
    defaultFontFamily: 'Poppins',
    datasets: [
        {
            label: "HĐDN",
            data: [],
            backgroundColor: 'transparent',
            borderColor: 'rgba(220,53,69,0.75)',
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'rgba(220,53,69,0.75)',
        },
        {
            label: "HĐĐT",
            data: [],
            backgroundColor: 'transparent',
            borderColor: bd_brandProduct2,
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: bd_brandProduct2,
        },
        {
            label: "HĐTC",
            data: [],
            backgroundColor: 'transparent',
            borderColor: 'rgba(40,167,69,0.75)',
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'rgba(40,167,69,0.75)',
        }
    ]
}

const outputLineChart = {
    labels: [],
    type: 'line',
    defaultFontFamily: 'Poppins',
    datasets: [
        {
            label: "HĐDN",
            data: [],
            backgroundColor: 'transparent',
            borderColor: 'rgba(220,53,69,0.75)',
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'rgba(220,53,69,0.75)',
        },
        {
            label: "HĐĐT",
            data: [],
            backgroundColor: 'transparent',
            borderColor: bd_brandProduct2,
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: bd_brandProduct2,
        },
        {
            label: "HĐTC",
            data: [],
            backgroundColor: 'transparent',
            borderColor: 'rgba(40,167,69,0.75)',
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'rgba(40,167,69,0.75)',
        }
    ]
}

const lineoptions = {
    responsive: true,
    tooltips: {
        mode: 'index',
        titleFontSize: 12,
        titleFontColor: '#000',
        bodyFontColor: '#000',
        backgroundColor: '#fff',
        titleFontFamily: 'Poppins',
        bodyFontFamily: 'Poppins',
        cornerRadius: 3,
        intersect: false,
    },
    legend: {
        display: false,
        labels: {
            usePointStyle: true,
            fontFamily: 'Poppins',
        },
    },
    scales: {
        xAxes: [{
            display: true,
            gridLines: {
                display: false,
                drawBorder: false
            },
            scaleLabel: {
                display: false,
                labelString: 'Month'
            },
            ticks: {
                fontFamily: "Poppins"
            }
        }],
        yAxes: [{
            display: true,
            gridLines: {
                display: false,
                drawBorder: false
            },
            scaleLabel: {
                display: true,
                labelString: 'Value',
                fontFamily: "Poppins"

            },
            ticks: {
                fontFamily: "Poppins"
            }
        }]
    },
    title: {
        display: false,
        text: 'Normal Legend'
    }
}

const barData = {
    labels: [],
    datasets: [
        {
            label: "Thu nhập",
            data: [],
            borderColor: "rgba(0, 123, 255, 0.9)",
            borderWidth: "0",
            backgroundColor: "rgba(0, 123, 255, 0.5)",
            fontFamily: "Poppins"
        },
        {
            label: "Chi phí",
            data: [],
            borderColor: "rgba(0,0,0,0.09)",
            borderWidth: "0",
            backgroundColor: "rgba(0,0,0,0.07)",
            fontFamily: "Poppins"
        }
    ]
}

const barOptions = {
    legend: {
        position: 'top',
        labels: {
            fontFamily: 'Poppins'
        }

    },
    scales: {
        xAxes: [{
            ticks: {
                fontFamily: "Poppins"

            }
        }],
        yAxes: [{
            ticks: {
                beginAtZero: true,
                fontFamily: "Poppins"
            }
        }]
    }
}

class CashflowChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isInputLoading: false,
            isOutLoading: false,
            year: CONSTANT.YEAR_2019,
            fromYear: CONSTANT.TIMESTAMP_2017,
            toYear: CONSTANT.TIMESTAMP_2020 - 1,
            quarter: CONSTANT.NO_FILTER
        }
    }

    onFromYearSelect(value) {
        let year = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.timeStamp
        this.setState({
            fromYear: year
        })
    }

    onToYearSelect(value) {
        let year = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.timeStamp
        year = year + CONSTANT.ONE_DAY_IN_MILLISECONDS - 1 
        this.setState({
            toYear: year
        })
    }

    onYearSelect(value) {
        let year = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        this.setState({
            year: year
        }, this.getCashflowChartByQuarter)
    }

    onQuarterSelect(value) {
        let quarter = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        this.setState({
            quarter: quarter
        })
    }

    componentWillMount() {
        this.getCashflowChartByYear()
        this.getCashflowChartByQuarter()
    }

    getCashflowChartByYear() {
        let url = API.CASH_FLOW_CHART_BY_YEAR + "?start=" + this.state.fromYear + "&end=" + this.state.toYear
        if (this.state.quarter !== CONSTANT.NO_FILTER)
            url += "&quarter=" + this.state.quarter
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("getCashflowChartByYear", url)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                console.log(res.data)
                if (res.error === 'Unauthorized') 
                AppUtils.logout()
            else {
                if (res.data.length !== 0) {
                    this.parseCashflowDataChartByYear(res.data)
                } else {
                    console.log("lỗi")
                }
            }
            })
            .catch(response => {
                if (response.error === 'Unauthorized') 
                AppUtils.logout()
            })
            .finally(() => { });
    }

    parseCashflowDataChartByYear(data) {
        let totalInputBussiness = []
        let totalOutputBussiness = []
        let totalInputInvestment = []
        let totalOutputInvestment = []
        let totalInputFinancial = []
        let totalOutputFinancial = []
        let labels = []

        let dataSort = AppUtils.sortFinancialData(data)
        dataSort.forEach(element => {
            var t = new Date(1970, 0, 1); // Epoch
            t.setMilliseconds(element.createAt);
            let label = ""
            if (this.state.quarter !== CONSTANT.NO_FILTER)
                label = "Q" + this.state.quarter + "-" + t.getFullYear()
            else label = t.getFullYear()
            labels.push(label)
            totalInputBussiness.push(element.inputActivityBusiness)
            totalOutputBussiness.push(element.outputActivityBusiness)
            totalInputInvestment.push(element.inputActivityInvestment)
            totalOutputInvestment.push(element.outputActivityInvestment)
            totalInputFinancial.push(element.inputActivityFinancial)
            totalOutputFinancial.push(element.outputActivityFinancial)
        });

        inputLineChart.labels = labels
        inputLineChart.datasets[0].data = totalInputBussiness
        inputLineChart.datasets[1].data = totalInputInvestment
        inputLineChart.datasets[2].data = totalInputFinancial

        outputLineChart.labels = labels
        outputLineChart.datasets[0].data = totalOutputBussiness
        outputLineChart.datasets[1].data = totalOutputInvestment
        outputLineChart.datasets[2].data = totalOutputFinancial

        this.setState({
            isOutLoading: true,
            isInputLoading: true
        })

    }

    getCashflowChartByQuarter() {
        let url = API.CASH_FLOW_CHART_BY_QUARTER + "?year=" + this.state.year
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("getCashflowChartByQuarter", url)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            console.log(res.data)
            if (res.error === 'Unauthorized') 
            AppUtils.logout()
        else {if (res.data.length !== 0) {
                this.parseCashflowDataChartByQuarter(res.data)
            } else {
                console.log("lỗi")
            }
        }
        }).catch(response => {
            if (response.error === 'Unauthorized') 
            AppUtils.logout()
        }).finally(() => { });
    }

    parseCashflowDataChartByQuarter(data) {
        let inputTotal = []
        let outputTotal = []
        let labels = []

        let dataSort = AppUtils.sortListByQuater(data)

        dataSort.forEach(element => {
            let eleLabel = 'Q' + element.quarter
            if (element.quarter == 5) eleLabel = "All Year"
            labels.push(eleLabel)
            let totalInput = element.inputActivityBusiness + element.inputActivityInvestment + element.inputActivityFinancial
            let totalOutput = element.outputActivityBusiness + element.outputActivityInvestment + element.outputActivityFinancial

            //let inputFormat = AppUtils.formatNumber(totalInput)
            //let outputFormat = AppUtils.formatNumber(totalOutput)

            inputTotal.push(totalInput)
            outputTotal.push(totalOutput)

        });

        barData.labels = labels
        barData.datasets[0].data = inputTotal
        barData.datasets[1].data = outputTotal
        this.setState({
            isBarLoading: true,
        })
    }

    render() {
        const { isInputLoading, isOutLoading, isBarLoading, currtime } = this.state
        return (
            <div className="section__content section__content--p30">
                <div className="container-fluid">
                    <div className="au-card m-b-30">
                        <h3 className="title-2 m-b-40">Biểu đồ thể hiện Tổng thu nhập doanh nghiệp</h3>
                        <div className="row m-b-20">
                            <div className="col-lg-3">
                                <Select
                                    onChange={this.onFromYearSelect.bind(this)}
                                    options={chartYearOption}
                                    formatGroupLabel={formatGroupLabel}
                                    placeholder="2017"
                                    maxMenuHeight="200px"
                                />
                            </div>
                            <div className="col-lg-3">
                                <Select
                                    onChange={this.onToYearSelect.bind(this)}
                                    options={chartYearOption}
                                    formatGroupLabel={formatGroupLabel}
                                    placeholder="2019"
                                    maxMenuHeight="200px"
                                />
                            </div>
                            <div className="col-lg-3">
                                <Select
                                    onChange={this.onQuarterSelect.bind(this)}
                                    options={quarterOptions}
                                    formatGroupLabel={formatGroupLabel}
                                    maxMenuHeight="200px"
                                    placeholder="Chọn Quý"

                                />
                            </div>
                            <div className="col-lg-3">
                                <button className="au-btn au-btn-icon au-btn--blue au-btn--small" onClick={this.getCashflowChartByYear.bind(this)}>
                                    <FontAwesomeIcon className="custom-icon" icon={faSearch} /></button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-6" onClick={this.inputLineChartClick}>
                                <div className="au-card m-b-30">
                                    <div className="au-card-inner">
                                        <h3 className="title-2 m-b-40">Thu</h3>
                                        {
                                            <Line data={inputLineChart} options={lineoptions} />
                                        }
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="au-card m-b-30">
                                    <div className="au-card-inner">
                                        <h3 className="title-2 m-b-40">Chi</h3>
                                        {
                                            <Line data={outputLineChart} options={lineoptions} />
                                        }
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="au-card m-b-30">
                                <div className="au-card-inner">
                                    <h3 className="title-2 m-b-40">Tổng thu nhập doanh nghiệp trong năm {this.state.year}</h3>
                                    <div >
                                        <div className="col-lg-3">
                                            <Select
                                                onChange={this.onYearSelect.bind(this)}
                                                options={chartYearOption}
                                                formatGroupLabel={formatGroupLabel}
                                                placeholder="2019"
                                            />
                                        </div>
                                        {
                                            <Bar data={barData} options={barOptions} height='100' />
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CashflowChart;
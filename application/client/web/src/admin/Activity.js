import React, { Component } from 'react';
import ActivityChart from '../component/ActivityChart.js';
import ActivityChartByType from '../component/ActivityChartByType.js';
import ListActivity from '../component/ListActivity.js';


class Activity extends Component {
    render() {
        return (
                <div className="container">
                    <header className="section-header m-b-50">
                        <h3 className="section-title">Danh sách các hoạt động</h3>
                    </header>
                    <ListActivity isAdminPageNavigate={true}/>
                    {/* <ActivityChart/> */}
                    {/* <ActivityChartByType/> */}
                </div>
        );
    }
}

export default Activity;
import React, { Component } from 'react';
import { Bar, Line, Pie } from 'react-chartjs-2';
import API from './../API/API.js'
import AppUtils from '../Utils/AppUtils';
import axios from 'axios'
import { groupStyles, groupBadgeStyles, chartYearOption, quarterOptions } from '../Utils/DataGeneral'
import Select from 'react-select';
import CONSTANT from '../Constant.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

const formatGroupLabel = data => (
    <div style={groupStyles}>
        <span>{data.label}</span>
        <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
);

// Recent Report 2
const bd_brandProduct2 = 'rgba(0,181,233,0.9)'

const lineChartOption = {
    legend: {
        position: 'top',
        labels: {
            fontFamily: 'Poppins'
        }

    },
    responsive: true,
    tooltips: {
        mode: 'index',
        intersect: false
    },
    hover: {
        mode: 'nearest',
        intersect: true
    },
    scales: {
        xAxes: [{
            ticks: {
                fontFamily: "Poppins"

            }
        }],
        yAxes: [{
            ticks: {
                beginAtZero: true,
                fontFamily: "Poppins"
            }
        }]
    }
}

const lineChartData = {
    labels: [],
    defaultFontFamily: "Poppins",
    datasets: [
        {
            label: "Tổng nợ",
            borderColor: "rgba(0,0,0,.09)",
            borderWidth: "1",
            backgroundColor: "rgba(0,0,0,.07)",
            data: []
        },
        {
            label: "Tổng tài sản",
            borderColor: "rgba(0, 123, 255, 0.9)",
            borderWidth: "1",
            backgroundColor: "rgba(0, 123, 255, 0.5)",
            pointHighlightStroke: "rgba(26,179,148,1)",
            data: []
        }
    ]
}

class FinancialChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLineChartLoading: false,
            fromYear: CONSTANT.TIMESTAMP_2017, // 2017
            toYear: CONSTANT.TIMESTAMP_2020 - 1, // 2019
            quarter: CONSTANT.NO_FILTER,
            fromYearInYear: 2017
        }
    }

    onFromYearSelect(value) {
        let yearInMs = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.timeStamp
        var t = new Date(1970, 0, 1); // Epoch
        t.setMilliseconds(yearInMs);
        let year = t.getFullYear()
        this.setState({
            fromYear: yearInMs,
            fromYearInYear: year
        })
    }

    onQuarterSelect(value) {
        let quarter = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        this.setState({
            quarter: quarter
        })
    }

    componentWillMount() {
        this.getFinancialChart()
    }

    getFinancialChart() {
        let { fromYear, quarter } = this.state
        let param = ""
        if (fromYear !== CONSTANT.NO_FILTER)
            param = "start=" + fromYear
        if (quarter !== CONSTANT.NO_FILTER) {
            param += "&quarter=" + quarter
        }
        let url = API.GET_FINANCIAL_CHART + "?" + param
        console.log("getFinancialChart", url)

        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                console.log(res.data)
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.parseFinancialDataChart(res.data)
                        this.setState({
                            isLineChartLoading: true,
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    parseFinancialDataChart(data) {
        let inputAsset = []
        let outputDebt = []
        let labels = []
        let dataSort = AppUtils.sortFinancialData(data)
        dataSort.forEach(element => {
            var t = new Date(1970, 0, 1); // Epoch
            t.setMilliseconds(element.createAt);
            let label = ""
            if (this.state.quarter !== CONSTANT.NO_FILTER)
                label = "Q" + this.state.quarter + "-" + t.getFullYear()
            else label = t.getFullYear()
            labels.push(label)
            inputAsset.push(element.totalAsset)
            outputDebt.push(element.totalDebt)
        });

        console.log("labels", labels)
        console.log("inputAsset", inputAsset)
        console.log("outputDebt", outputDebt)

        lineChartData.labels = labels
        lineChartData.datasets[0].data = inputAsset
        lineChartData.datasets[1].data = outputDebt
        this.setState({
            isLineChartLoading: true,
        })
    }

    render() {
        const { fromYearInYear, isLineChartLoading, currtime } = this.state
        return (
            <div className="section__content section__content--p30">
                <div className="container-fluid">
                    <div className="row" style={{ height: "100%" }}>
                        <div className="col-lg-12">
                            <div className="au-card m-b-30">
                                <div className="au-card-inner">
                                    <h3 className="title-2 m-b-10">Tổng tài sản, tổng nợ của Doanh nghiệp từ năm {fromYearInYear} đến năm 2019</h3>
                                    <div className="row m-b-30 m-t-30">
                                        <div className="col-lg-3">
                                            <Select
                                                onChange={this.onFromYearSelect.bind(this)}
                                                options={chartYearOption}
                                                formatGroupLabel={formatGroupLabel}
                                                placeholder="2017"
                                                maxMenuHeight="200px"
                                            />
                                        </div>
                                        <div className="col-lg-3">
                                            <Select
                                                onChange={this.onQuarterSelect.bind(this)}
                                                options={quarterOptions}
                                                formatGroupLabel={formatGroupLabel}
                                                maxMenuHeight="200px"
                                                placeholder="Chọn Quý"
                                            />
                                        </div>
                                        <button className="au-btn au-btn-icon au-btn--blue au-btn--small" onClick={this.getFinancialChart.bind(this)}>
                                            <FontAwesomeIcon className="custom-icon" icon={faSearch} /></button>
                                    </div>
                                    {
                                        <Line data={lineChartData} options={lineChartOption} height='100' />
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default FinancialChart;
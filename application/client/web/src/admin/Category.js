import React, { Component } from 'react';
import axios from 'axios'
import API from './../API/API.js'
import CONSTANT from './../Constant'
import AppUtils from '../Utils/AppUtils.js';
import swal from '@sweetalert/with-react'

class Category extends Component {

    constructor(props) {
        super(props);
        this.state = {
            typeChildLv2: [],
            enableEditLv2: false,
            oldValueLv2: "",
            typeChildLv3: [],
            enableEditLv3: false,
            oldValueLv3: "",
        }

        this.deleteTypeLv2 = this.deleteTypeLv2.bind(this)
        this.editTypeLv2 = this.editTypeLv2.bind(this)
        this.deleteTypeLv3 = this.deleteTypeLv3.bind(this)
        this.editTypeLv3 = this.editTypeLv3.bind(this)
        this.onDeleteTypeLv2 = this.onDeleteTypeLv2.bind(this)
        this.onDeleteTypeLv3 = this.onDeleteTypeLv3.bind(this)

    }

    componentWillMount() {
        this.getNewData()
    };

    getNewData() {
        this.getTypeChildLv2()
        this.getTypeChildLv3()
    }

    onDeleteTypeLv2(value) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.deleteTypeLv2(value)
                }
            });
    }

    addNewTypeLv2() {
        let label = document.getElementById("newTypeLv2").value + ""
        let url = API.GET_TYPE_LV2
        let size = this.state.typeChildLv2.length
        let data = {
            "id": size + 1,
            "label": label
        }
        this.callApi(url, "post", data)
    }

    deleteTypeLv2(itemId) {
        let url = API.GET_TYPE_LV2 + itemId
        this.callApi(url, 'delete', "")
    }

    editTypeLv2(itemId) {
        let isEnable = this.state.enableEditLv2
        let eleId = "typeLv2" + itemId
        let label = document.getElementById(eleId).value + ""

        if (!isEnable) {
            this.setState({
                oldValueLv2: label,
                enableEditLv2: true
            })
        } else {
            let oldValueLv2 = this.state.oldValueLv2
            if (label !== oldValueLv2) {
                let url = API.GET_TYPE_LV2 + itemId
                let data = {
                    "id": itemId,
                    "label": label
                }
                this.callApi(url, 'put', data)
            }
            this.setState({
                enableEditLv2: false
            })
        }
    }

    onDeleteTypeLv3(value) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.deleteTypeLv3(value)
                }
            });
    }

    addNewTypeLv3() {
        let label = document.getElementById("newTypeLv3").value + ""
        let url = API.GET_TYPE_LV3
        let size = this.state.typeChildLv3.length
        let data = {
            "id": size + 1,
            "label": label
        }
        this.callApi(url, "post", data)
    }

    deleteTypeLv3(itemId) {
        let url = API.GET_TYPE_LV3 + itemId
        this.callApi(url, 'delete', "")
    }

    editTypeLv3(itemId) {
        let isEnable = this.state.enableEditLv3
        let eleId = "typeLv3" + itemId
        let label = document.getElementById(eleId).value + ""

        if (!isEnable) {
            this.setState({
                oldValueLv3: label,
                enableEditLv3: true
            })
        } else {
            let oldValueLv3 = this.state.oldValueLv3
            if (label !== oldValueLv3) {
                let url = API.GET_TYPE_LV3 + itemId
                let data = {
                    "id": itemId,
                    "label": label
                }
                this.callApi(url, 'put', data)
            }
            this.setState({
                enableEditLv3: false
            })
        }
    }

    callApi(url, method, data) {
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios({
            method: method,
            url: url,
            data: data,
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            this.getNewData()
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        });
    }

    getTypeChildLv2() {
        let url = API.GET_TYPE_LV2
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    this.setState({
                        typeChildLv2: res.data
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    getTypeChildLv3() {
        let url = API.GET_TYPE_LV3
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    this.setState({
                        typeChildLv3: res.data
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    render() {
        const { typeChildLv2, typeChildLv3 } = this.state
        return (
            <section id="about">
                <div className="container">
                    <header className="section-header">
                        <h3 className="section-title">Các danh mục hoạt động</h3>
                    </header>
                    <div className="row">
                        <div className="col">
                            <div className="card card-small">
                                <div className="card-header">
                                    <div className="row pb-3 border-bottom">
                                        <div className="col my-auto">
                                        </div>
                                        <div className="col">
                                            <div className="input-group border-bottom w-50 ml-auto">
                                                <input type="text" className="form-control" placeholder="Thêm loại hoạt động mới" aria-label="Add new category" aria-describedby="basic-addon2" />
                                                <div className="input-group-append">
                                                    <button className="btn btn-white js-add-category px-2" type="button" onClick="">
                                                        <i className="material-icons">add</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body pt-0">
                                    <ul id="js-categories-ul" className="list-unstyled row m-auto">
                                        <li className="list-item col-5 p-3 border mx-auto mb-3">
                                            <div className="input-group mb-3">
                                                <input type="text" className="form-control" defaultValue="Danh mục hoạt động" style={{ fontSize: '13pt' }} disabled />
                                            </div>
                                            <ul className="list-group">
                                                {
                                                    typeChildLv2.map((item, key) => {
                                                        return (
                                                            <li className="list-group-item">
                                                                <div className="input-group">
                                                                    <input type="text" className="form-control" defaultValue={item.label} disabled id={"typeLv2" + item.id} />
                                                                    <div className="input-group-append">
                                                                        <button className="btn btn-white js-edit-input" type="button" onClick={() => this.editTypeLv2(item.id)}>
                                                                            <i className="far fa-edit" />
                                                                        </button>
                                                                        <button className="btn btn-white" type="button" onClick={() => this.onDeleteTypeLv2(item.id)}>
                                                                            <i className="far fa-trash-alt" /> </button>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        );
                                                    })
                                                }
                                                <li className="list-group-item">
                                                    <div className="input-group">
                                                        <input type="text" className="form-control" placeholder="Thêm danh mục mới" aria-label="Add new category" aria-describedby="basic-addon2" id="newTypeLv2" />
                                                        <div className="input-group-append">
                                                            <button className="btn btn-white js-add-subcategory" type="button" onClick={this.addNewTypeLv2.bind(this)}>
                                                                <i className="material-icons">add</i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className="list-item col-5 p-3 border mx-auto mb-3">
                                            <div className="input-group mb-3">
                                                <input type="text" className="form-control" defaultValue="Hoạt động chi tiết" style={{ fontSize: '13pt' }} disabled />
                                            </div>
                                            <ul className="list-group">
                                                {
                                                    typeChildLv3.map((item, key) => {
                                                        return (
                                                            <li className="list-group-item">
                                                                <div className="input-group">
                                                                    <input type="text" className="form-control" defaultValue={item.label} disabled id={"typeLv3" + item.id} />
                                                                    <div className="input-group-append">
                                                                        <button className="btn btn-white js-edit-input" type="button" onClick={() => this.editTypeLv3(item.id)}>
                                                                            <i className="far fa-edit" />
                                                                        </button>
                                                                        <button className="btn btn-white " type="button" onClick={() => this.onDeleteTypeLv3(item.id)}>
                                                                            <i className="far fa-trash-alt" /> </button>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        );
                                                    })
                                                }
                                                <li className="list-group-item">
                                                    <div className="input-group">
                                                        <input type="text" className="form-control" placeholder="Thêm hoạt động chi tiết" aria-label="Add new category" aria-describedby="basic-addon2" id="newTypeLv3" />
                                                        <div className="input-group-append">
                                                            <button className="btn btn-white js-add-subcategory" type="button" onClick={this.addNewTypeLv3.bind(this)}>
                                                                <i className="material-icons">add</i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Category;
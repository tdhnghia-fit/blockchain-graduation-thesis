import React, { Component } from 'react';
import axios from 'axios'
import API from './../API/API.js'
import CONSTANT from './../Constant'
import AppUtils from '../Utils/AppUtils.js';
import { Modal } from 'react-bootstrap'
import ChangePassword from './ChangePassword.js';

class UserInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            showFormAdd: false,
            isDataChange: false,
            username: "",
            isLoading: false
        }

        this.onChangePassword = this.onChangePassword.bind(this);
        this.onUsernameChange = this.onUsernameChange.bind(this)
    }

    onUsernameChange() {
        let username = document.getElementById("name").value + ""
        let isChange = false

        if (username !== this.state.username)
            isChange = true
        this.setState({
            isDataChange: isChange
        })
    }

    onChangePassword() {
        this.handleClose()
    }

    handleClose() {
        this.setState({
            showFormAdd: false
        })
    }

    handleShow() {
        this.setState({
            showFormAdd: true
        })
    }

    componentWillMount() {
        let idUser = localStorage.getItem(CONSTANT.USER_LOGIN_TOKEN)
        let url = API.GET_USER + idUser
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("authorization", authorization)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.setState({
                            data: res.data,
                            isLoading: true,
                            username: res.data.name
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    updateInfo() {
        let idUser = localStorage.getItem(CONSTANT.USER_LOGIN_TOKEN)
        let url = API.UPDATE_INFO + idUser
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log(url)
        axios({
            method: 'put',
            url: url,
            data: {
                password: "",
                name: document.getElementById("name").value + ""
            },
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            // this.props.history.push('admin/userinfo')

            this.setState({
                isDataChange: true
            })

        })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            });
    }

    render() {
        var { data, isDataChange, isLoading } = this.state
        let role = ""
        if (data.roles != undefined) {
            role = AppUtils.getStrRole(data.roles[0].name)
        }
        return (
            isLoading && <div className="section__content section__content--p30">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="card card-user">
                                <div className="image">
                                    <img src="/images/icon/companylogo.png" alt="..." />
                                </div>
                                <div className="card-body">
                                    <p className="description text-center">
                                        "I like the way you work it <br />
                                        No diggity <br />
                                        I wanna bag it up"
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-header">Thông tin người dùng</div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="cc-payment" className="control-label mb-1">Email</label>
                                        <input id="cc-pament" name="cc-payment" type="text" className="form-control" aria-required="true" aria-invalid="false" defaultValue={data.email} disabled />
                                    </div>
                                    {/* <div className="form-group has-success">
                                    <label htmlFor="username" className="control-label mb-1">Username</label>
                                    <input id="username" name="username" type="text" className="form-control cc-name valid"  defaultValue={data.username} onChange={this.onUsernameChange}  />
                                    <span className="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true" />
                                </div> */}
                                    <div className="form-group">
                                        <label htmlFor="cc-number" className="control-label mb-1">Tên</label>
                                        <input id="name" name="name" type="tel" className="form-control cc-number identified visa" defaultValue={data.name} onChange={this.onUsernameChange} />
                                        <span className="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true" />
                                    </div>
                                    <div className="row">
                                        <div className="col-6">
                                            <div className="form-group">
                                                <label htmlFor="cc-exp" className="control-label mb-1">Vai trò</label>
                                                <div className="input-group">
                                                    <input id="x_card_code" className="form-control cc-cvc" defaultValue={role} disabled />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <label htmlFor="x_card_code" className="control-label mb-1">Ngày tham gia</label>
                                            <div className="input-group">
                                                <input id="x_card_code" className="form-control cc-cvc" defaultValue={AppUtils.toDateTime(data.joinedAt)} disabled />
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" className="btn btn-primary btn-sm" onClick={this.handleShow.bind(this)}>
                                        <i className="fa fa-dot-circle-o"></i> Đổi mật khẩu
                                        </button> {'    '}
                                    {
                                        isDataChange && <button type="submit" className="btn btn-success btn-sm" onClick={this.updateInfo}>
                                            <i className="fa fa-check-circle"></i> Chỉnh sửa thông tin
                                    </button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal show={this.state.showFormAdd} onHide={this.handleClose.bind(this)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Đổi mật khẩu</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <ChangePassword onChangePassword={this.onChangePassword} />
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}

export default UserInfo;
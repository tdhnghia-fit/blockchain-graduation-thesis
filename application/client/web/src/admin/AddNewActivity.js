import React, { Component } from 'react';
import FileBase64 from '../component/FileBase64';
import Select from 'react-select';
import { groupStyles, groupBadgeStyles, typeParent } from './../Utils/DataGeneral'
import CONSTANT from './../Constant'
import axios from 'axios'
import API from './../API/API.js'
import AppUtils from '../Utils/AppUtils';
import DatePicker from "react-datepicker";
import swal from '@sweetalert/with-react'
import { Modal } from 'react-bootstrap'
import NumberFormat from 'react-number-format';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusSquare, faSearch } from '@fortawesome/free-solid-svg-icons'

const formatGroupLabel = data => (
    <div style={groupStyles}>
        <span>{data.label}</span>
        <span style={groupBadgeStyles}>{data.options.length}</span>
    </div>
);

class AddNewActivity extends Component {

    constructor(props) {
        super(props);
        this.state = {
            files: [],
            endDate: "",
            end: CONSTANT.NO_FILTER,
            typeParent: "",
            typeLv2: "",
            actorIds: [],
            actor: "",
            showFormAdd: false,
            isShowDes: false,
            disabledBtnAdd: true,
            maxId: CONSTANT.INIT_VALUE,
            typeChildLv2: [],
            typeInputLv2: [],
            typeOutputLv2: [],
            typeAll: [],
            defaultLv2: ""
        }
    }

    handleCloseEdit() {
        this.setState({
            showFormAdd: false
        })
    }

    handleShowEdit(value) {
        this.setState({
            showFormAdd: true
        })
    }

    componentWillMount() {
        this.getNewData()
        this.getActorId()
    };

    getActorId() {
        let url = API.GET_ACTORID
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    let actorIdOption = AppUtils.convertActorIdToSelectModel(res.data, false)
                    this.setState({
                        actorIds: actorIdOption
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    getNewData() {
        this.getTypeChildLv2()
    }

    callApiModify(url, method, data) {
        console.log("callApiModify", data, url)
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios({
            method: method,
            url: url,
            data: data,
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            console.log("success", res)
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            this.getTypeChildLv2()
        }).catch(response => {
            console.log("error", response)

            if (response.error === 'Unauthorized')
                AppUtils.logout()
        });
    }

    getTypeChildLv2() {
        let url = API.GET_TYPE_LV2
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        let maxId = localStorage.getItem(CONSTANT.MAX_ID)

        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    let sortedArr = AppUtils.sortListById(res.data, false)

                    this.setState({
                        typeAll: sortedArr
                    })
                    let arr = AppUtils.convertTypeToSelectModel(sortedArr, false)
                    this.setState({
                        typeChildLv2: arr,
                        isLoadingType2: true,
                        maxId: maxId
                    })
                    this.parseType(sortedArr)
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    parseType(data) {
        let input = data.slice(0, 5)
        let output = data.slice(5, 11)
        this.setState({
            typeInputLv2: input,
            typeOutputLv2: output
        })
    }

    postActivityData() {
        let currentDate = new Date()
        let currentMiliSecs = currentDate.getTime()
        let proof = this.state.files
        // let note = document.getElementById("inputId").value + ""
        let type = this.state.typeParent + ":" + this.state.typeLv2
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        let newItem = {
            id: document.getElementById("inputId").value + "",
            date: this.state.end + "",
            type: type + "",
            amount: document.getElementById("inputAmount").value + "",
            purpose: document.getElementById("inputPurpose").value + "",
            percentage: "",
            description: document.getElementById("inputDes").value + "",
            actorId: this.state.actor + "",
            actorApprovedId: [],
            proofImage: proof,
            createAt: currentMiliSecs + "",
            lastCreateAt: 0
        }
        console.log("newItem", newItem)

        axios({
            method: 'post',
            url: API.POST_ACTIVITY,
            data: newItem,
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            console.log("postActivityData - success", res.data);
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                swal({
                    title: "Thành công",
                    icon: "success",
                })
                    .then((isSave) => {
                        if (isSave) {
                            this.props.history.push('/quanly/hoatdongthuchi')
                        }
                    });
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        });
    }

    onEndSelect = date => {
        let time = date.getTime()
        console.log("end", time)
        this.setState({
            endDate: date,
            end: time,
            enableFilter: this.state.start != CONSTANT.NO_FILTER
        })
    };

    onTypeLv2Select(value) {
        let type = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        this.setState({
            typeLv2: type,
            defaultLv2: value
        })
    }

    getFiles(files) {
        let proofImage = files.map((file, i) => file.base64)
        this.setState({ files: proofImage })
    }

    addNewTypeLv2() {
        let label = document.getElementById("newTypeLv2").value + ""
        let note = document.getElementById("inputNote").value + ""
        let url = API.GET_TYPE_LV2
        let size = this.state.typeChildLv2[0].options.length
        let data = {
            "id": size + 1,
            "label": label,
            "description": note
        }
        this.callApiModify(url, "post", data)
    }

    onRadioChange(e) {
        const { typeAll, typeInputLv2, typeOutputLv2 } = this.state
        let type = e.currentTarget.value
        let typeLv2 = typeAll
        if (type == CONSTANT.TYPE_OUTPUT) {
            typeLv2 = typeOutputLv2
        }
        else if (type == CONSTANT.TYPE_INPUT) {
            typeLv2 = typeInputLv2
        }
        let convertList = AppUtils.convertTypeToSelectModel(typeLv2, true)
        this.setState({
            typeParent: e.currentTarget.value,
            typeLv2: CONSTANT.NO_FILTER,
            typeChildLv2: convertList,
            defaultLv2: ""
        })
    }

    onActorIdSelect(value) {
        let type = value == null || value.length == 0 ? CONSTANT.NO_FILTER : value.value
        console.log("onActorIdSelect", type)
        this.setState({
            actor: type
        })
    }

    onAddActivityChange() {
        let label = document.getElementById('newTypeLv2').value
        let isShowDes = label != ""
        this.setState({
            isShowDes: isShowDes
        })
    }

    onAddActivityDesChange() {
        let label = document.getElementById('newTypeLv2').value
        let note = document.getElementById('inputNote').value

        let disabledBtnAdd = label == "" || note == ""
        this.setState({
            disabledBtnAdd: disabledBtnAdd
        })
    }

    render() {
        const { files, typeChildLv2, typeLv2, actorIds, isShowDes, disabledBtnAdd, maxId, defaultLv2 } = this.state
        let isFileEmpty = files.length == 0
        let genId = parseInt(maxId) + 1
        return (
            <div className="container" style={{ height: '100vh' }}>
                <header className="section-header m-b-50">
                    <h3 className="section-title">Thêm hoạt động mới</h3>
                </header>
                <div className="row">
                    <div className="col-lg-9">
                        <div className="card" >
                            <div className="card-body">
                                <form style={{ width: "100%" }}>
                                    <div className="row form-group">
                                        <div class="col-md-2">
                                            <label htmlFor="input1">ID</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" className="form-control" id="inputId" value={genId} disabled />
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div class="col-md-2">
                                            <label htmlFor="input1">Số tiền</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" className="form-control fs-16" id="inputAmount" />
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div class="col-md-2">
                                            <label htmlFor="input1">Người thực hiện</label>
                                        </div>
                                        <div class="col-md-10">
                                            <Select
                                                onChange={this.onActorIdSelect.bind(this)}
                                                options={actorIds}
                                                formatGroupLabel={formatGroupLabel}
                                                placeholder="Người thực hiện"
                                            />
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div class="col-md-2">
                                            <label htmlFor="input1">Ngày diễn ra</label>
                                        </div>
                                        <div class="col-md-10">
                                            <DatePicker
                                                selected={this.state.endDate}
                                                onChange={this.onEndSelect}
                                            />
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div class="col-md-2">
                                            <label htmlFor="input1">Mục đích</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" className="form-control fs-16" id="inputPurpose" />

                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div class="col-md-2">
                                            <label htmlFor="input1">Mô tả cập nhật</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" className="form-control fs-16" id="inputDes" />
                                        </div>
                                    </div>
                                </form>
                                <p className="card-description"> Hình ảnh minh chứng (hoá đơn, chứng từ, v.v) </p>

                                <div className="col-lg-12">
                                    <div className="form-group row">
                                        <FileBase64
                                            multiple={true}
                                            onDone={this.getFiles.bind(this)} />
                                    </div>
                                </div>
                                {
                                    !isFileEmpty && <div className="container">
                                        <div className="row col-lg-12" style={{ display: 'flex', justifyContent: 'center' }}>
                                            {
                                                files.map((item, i) => {
                                                    return (
                                                        <div className="col-lg-6 col-md-6 wow fadeInUp">
                                                            <figure>
                                                                <a data-lightbox="portfolio" href={item} className="link-preview" title="Preview">
                                                                    <img style={{ width: "100%" }} src={item} className="img-fluid" alt="" /></a>
                                                            </figure>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                }
                                <button className="au-btn au-btn-icon au-btn--green au-btn--small" onClick={() => this.postActivityData()}>
                                    <FontAwesomeIcon className="custom-icon" icon={faPlusSquare} />Thêm mới</button>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="card card-small mb-3">
                            <div className="card-header border-bottom">
                                <h6 className="m-0 fs-16">Loại hoạt động</h6>
                            </div>
                            <div className="card-body p-0">
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item d-flex px-3">
                                        <div className="row col-lg-12" style={{ padding: 'unset' }}>
                                            <div class="col-lg-6" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="optradio" value="input" onChange={this.onRadioChange.bind(this)} />
                                                    <a className="fs-14">Thu</a>
                                                </label>
                                            </div>
                                            <div class="col-lg-6" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="optradio" value="output" onChange={this.onRadioChange.bind(this)} />
                                                    <a className="fs-14">Chi</a>
                                                </label>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="list-group-item d-flex px-3">
                                        <div className="col-lg-12" style={{ padding: 'unset' }}>
                                            <Select
                                                onChange={this.onTypeLv2Select.bind(this)}
                                                options={typeChildLv2}
                                                formatGroupLabel={formatGroupLabel}
                                                placeholder="Chi tiết"
                                                value={defaultLv2}
                                            />
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default AddNewActivity;
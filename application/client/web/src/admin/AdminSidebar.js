import React, { Component } from 'react';
import { NavLink, Redirect } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFilePdf, faUserAlt, faTasks, faHome, faBuilding, faListAlt } from '@fortawesome/free-solid-svg-icons'
import CONSTANT from './../Constant'
import AppUtils from '../Utils/AppUtils';
import API from '../API/API';
import axios from 'axios'
import { Button } from 'react-bootstrap';

class AdminSidebar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {},
            isLoading: false
        }
    }
    logout() {
        AppUtils.logout()

    }

    componentDidMount() {
        let url = API.GET_ME
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        this.setState({
                            data: res.data,
                            isLoading: true
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    render() {
        let userRole = localStorage.getItem(CONSTANT.USER_LOGIN_ROLE)
        let isAdmin = userRole == CONSTANT.ROLE_ADMIN
        let isAccountant = userRole == CONSTANT.ROLE_ACCOUNTANT
        let isAuditor = userRole == CONSTANT.ROLE_AUDITOR

        const { data } = this.state
        return (
            <aside className="menu-sidebar2">
                <div className="logo " style={{ padding: 'unset', margin: 'unset' }}>
                    <a className="text-center" style={{ width: '100%' }}>
                        <img src="/images/icon/logo-admin.png" style={{ width: '175px' }} alt="Cool Admin" />
                    </a>
                </div>


                <div className="menu-sidebar__content js-scrollbar1 m-b-50">
                    <div class="account2">
                        <NavLink to="/quanly/userinfo">
                            <div className="image img-cir img-120">
                                <img src="/images/icon/companylogo.png" alt="John Doe" />
                            </div>
                        </NavLink>
                        <NavLink to="/quanly/userinfo">
                            <h4 className="name">{data.username}</h4>
                        </NavLink>

                        <a onClick={() => this.logout()}>
                            <NavLink to="/dangxuat">Đăng xuất</NavLink>
                        </a>
                    </div>

                    <nav className="navbar-sidebar2">
                        <ul className="list-unstyled navbar__list">
                            {
                                (isAccountant || isAuditor) && <li >
                                    <NavLink to="/quanly/thongke">
                                        <FontAwesomeIcon className="custom-icon" icon={faHome} />Thống kê hoạt động
                                </NavLink>
                                </li>
                            }
                            {
                                (isAccountant || isAuditor) && <li >
                                    <NavLink to="/quanly/hoatdongthuchi">
                                        <FontAwesomeIcon className="custom-icon" icon={faTasks} />Quản lý hoạt động
                                </NavLink>
                                </li>
                            }
                            {
                                isAdmin && <li >
                                    <NavLink to="/quanly/taikhoan">
                                        <FontAwesomeIcon className="custom-icon" icon={faUserAlt} />Quản lý tài khoản
                                    </NavLink>
                                </li>
                            }
                            {/* <li >
                                <NavLink to="/quanly/danhmuchoatdong">
                                    <FontAwesomeIcon className="custom-icon" icon={faListAlt} />Quản lý danh mục
                                </NavLink>
                            </li> */}
                            {/* {
                                isAdmin && <li>
                                    <NavLink to="/quanly/user">
                                        <FontAwesomeIcon className="custom-icon" icon={faBuilding} />Quản lý thông tin công ty
                                    </NavLink>
                                </li>
                            } */}
                            {
                                (isAccountant || isAuditor) && <li>
                                    <NavLink to="/quanly/baocao">
                                        <FontAwesomeIcon className="custom-icon" icon={faFilePdf} />Quản lý báo cáo
                                    </NavLink>
                                </li>
                            }
                            {/* <li className="has-sub">
                                <a className="js-arrow" onClick={() => { this.setState({ ...this.state, isExpanded: !this.state.isExpanded }) }}>
                                    <i className="fas fa-tachometer-alt"></i>Biểu đồ
                                    <span className={`arrow ${this.state.isExpanded ? 'up' : 'down'}`}>
                                        <i className={`fas ${this.state.isExpanded ? 'fa-angle-up' : 'fa-angle-down'}`}></i>
                                    </span>
                                </a>
                                <ul className="list-unstyled navbar__sub-list js-sub-list" style={{ display: this.state.isExpanded ? null : "none" }}>
                                    <li>
                                        <NavLink to="/quanly/financialchart">
                                            <i className="fas fa-tachometer-alt"></i>Biểu đồ tài chính
                                        </NavLink>
                                    </li>
                                    <li>
                                        <   NavLink to="/quanly/incomechart">
                                            <i className="fas fa-tachometer-alt"></i>Biểu đồ thu nhập
                                        </NavLink>
                                    </li>
                                    <li>
                                        <NavLink to="/quanly/cashflowchart">
                                            <i className="fas fa-tachometer-alt"></i>Biểu đồ chuyển lưu tiền tệ
                                        </NavLink>
                                    </li>
                                </ul>
                            </li> */}
                        </ul>
                    </nav>
                </div>
            </aside>
        );
    }
}

export default AdminSidebar;
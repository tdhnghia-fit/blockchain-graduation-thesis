import React, { Component } from 'react';
import axios from 'axios'
import API from './../API/API.js'
import CONSTANT from './../Constant'
import AppUtils from '../Utils/AppUtils.js';
import { typeParent } from './../Utils/DataGeneral'

class ActivityDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            files: [],
            isLoading: false,
            isType3Loading: false,
            isType2Loading: false,
            typeChildLv3: []
        }
    }

    componentWillMount() {
        this.getDetailActivity()
        this.getTypeChildLv2()
    }

    getDetailActivity() {
        let itemId = this.props.match.params.id
        let url = API.GET_ACTIVITY_BY_ID + itemId
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        let proofImage = res.data.proofImage
                        let arr = proofImage
                        if (!Array.isArray(proofImage)) {
                            arr = [proofImage]
                        }
                        this.setState({
                            data: [res.data],
                            files: arr,
                            isLoading: true
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });
    }

    getTypeChildLv2() {
        let url = API.GET_TYPE_LV2
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    this.setState({
                        typeChildLv2: AppUtils.convertTypeToSelectModel(res.data, false),
                        isType2Loading: true
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    render() {
        const { files, data, isLoading, typeChildLv2, isType2Loading } = this.state
        let isFileEmpty = files.length == 0
        return (
            isLoading && isType2Loading && <div className="container">
                <header className="section-header m-b-50">
                    <h3 className="section-title">Chi tiết hoạt động</h3>
                </header>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card" >
                            <div className="card-body">
                                {
                                    data.map((item, key) => {
                                        let parent = ""
                                        let child = ""
                                        let child2 = ""

                                        if (item.type != undefined) {
                                            let type = item.type.split(":")
                                            parent = typeParent.find((item) => {
                                                let label = CONSTANT.TYPE_INPUT
                                                if (item.label == CONSTANT.TYPE_CHI) label = CONSTANT.TYPE_OUTPUT
                                                return label == type[0]
                                            }).label

                                            let type2 = typeChildLv2[0].options.find((item) => {
                                                if (item.value != 0 && item.value + "" == type[1]) {
                                                    return item
                                                }
                                            })

                                            child = type2.label
                                            child2 = type2.description
                                        }
                                        return (
                                            <form style={{ width: "100%" }}>
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <div className="form-group row">
                                                            <label className="col-sm-3 col-form-label">Id</label>
                                                            <div className="col-sm-9">
                                                                <input type="text" className="form-control" id="inputId" defaultValue={item.id} disabled />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className="form-group row">
                                                            <label className="col-sm-3 col-form-label">Số tiền</label>
                                                            <div className="col-sm-9">
                                                                <input type="text" className="form-control" id="inputAmount" defaultValue={item.amount} disabled />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <div className="form-group row">
                                                            <label className="col-sm-3 col-form-label">Loại hoạt động</label>
                                                            <div className="col-sm-9">
                                                                <input type="text" className="form-control" id="inputType" defaultValue={parent} disabled />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className="form-group row">
                                                            <label className="col-sm-3 col-form-label">Mục đích</label>
                                                            <div className="col-sm-9">
                                                                <input type="text" className="form-control" id="inputPurpose" defaultValue={item.purpose} disabled />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <div className="form-group row">
                                                            <label className="col-sm-3 col-form-label">Mô tả loại hoạt động</label>
                                                            <div className="col-sm-9">
                                                                <input type="text" className="form-control" id="inputCategory" defaultValue={child2} disabled />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className="form-group row">
                                                            <label className="col-sm-3 col-form-label">Chi tiết</label>
                                                            <div className="col-sm-9">
                                                                <input type="text" className="form-control" id="inputDetail" defaultValue={child} disabled />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <div className="form-group row">
                                                            <label className="col-sm-3 col-form-label">Người Thực hiện</label>
                                                            <div className="col-sm-9">
                                                                <input type="text" className="form-control" id="inputActorId" defaultValue={item.actorId} disabled />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className="form-group row">
                                                            <label className="col-sm-3 col-form-label">Các bên liên quan</label>
                                                            <div className="col-sm-9">
                                                                <input type="text" className="form-control" id="inputActorApprovedId" defaultValue={item.actorApprovedId} disabled />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <div className="form-group row">
                                                            <label className="col-sm-3 col-form-label">Mô tả thay đổi</label>
                                                            <div className="col-sm-9">
                                                                <input type="text" className="form-control" id="inputDes" defaultValue={item.description} disabled />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-md-6">
                                                        <div className="form-group row">
                                                            <label className="col-sm-3 col-form-label">Ngày thực hiện</label>
                                                            <div className="col-sm-9">
                                                                <input type="text" className="form-control" id="inputDate" defaultValue={AppUtils.toDateTime(item.date)} disabled />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h4 className="m-t-20"> Hình ảnh minh chứng (hoá đơn, chứng từ, v.v) </h4>
                                            </form>
                                        );
                                    })

                                }
                                {
                                    !isFileEmpty &&
                                    <div className="container">
                                        <div className="row col-lg-12" style={{display:'flex', justifyContent: 'center'}}>
                                            {
                                                files.map((item, i) => {
                                                    return (
                                                        <div className="col-lg-6 col-md-6 wow fadeInUp">
                                                            <figure>
                                                                <a data-lightbox="portfolio" href={item} className="link-preview" title="Preview">
                                                                    <img style={{ width: "100%" }} src={item} className="img-fluid" alt="" /></a>
                                                            </figure>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default ActivityDetail;
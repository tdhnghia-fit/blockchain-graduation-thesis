import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <div className="col-md-12" style={{ marginTop: "20px", position: "relative", left: "0", bottom: "0", right: "0" }}>
                <div className="copyright">
                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                </div>
            </div>
        );
    }
}

export default Footer;
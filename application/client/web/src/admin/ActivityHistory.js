import React, { Component } from 'react';
import axios from 'axios'
import API from './../API/API.js'
import CONSTANT from './../Constant'
import AppUtils from '../Utils/AppUtils';
import { Modal } from 'react-bootstrap'
import { Icon } from '@iconify/react';
import detailIcon from '@iconify/icons-zmdi/more';
import { typeParent } from './../Utils/DataGeneral'
import NumberFormat from 'react-number-format';
import HistoryDetail from './HistoryDetail.js';


class ActivityHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: false,
            showFormDetail: false,
            chosenItem: {},
            typeChildLv3: [],
            typeChildLv3: [],
            isType2Loading: false
        }
    }

    handleCloseDetail() {
        this.setState({
            showFormDetail: false
        })
    }

    handleShowDetail(value) {
        this.setState({
            chosenItem: value,
            showFormDetail: true
        })
    }

    componentWillMount() {
        this.getData()
        this.getTypeChildLv2()
    }

    getTypeChildLv2() {
        let url = API.GET_TYPE_LV2
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        }).then(res => {
            if (res.error === 'Unauthorized')
                AppUtils.logout()
            else {
                if (res.data && res.status == 200) {
                    this.setState({
                        typeChildLv2: AppUtils.convertTypeToSelectModel(res.data, false),
                        isType2Loading: true
                    })
                } else {
                    console.log("Không tìm lấy dữ liệu :))")
                }
            }
        }).catch(response => {
            if (response.error === 'Unauthorized')
                AppUtils.logout()
        }).finally(() => { });
    }

    getData() {
        let itemId = this.props.match.params.id
        let url = API.GET_HISTORY_ACTIVITY + itemId
        let authorization = AppUtils.HEADER_AUTHORIZATION()
        console.log("url", url)
        axios.get(url, {
            headers: {
                "Authorization": authorization,
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.error === 'Unauthorized')
                    AppUtils.logout()
                else {
                    if (res.data.length !== 0) {
                        let sortedData = AppUtils.sortListByDate(res.data, true)
                        this.setState({
                            data: sortedData,
                            isLoading: true
                        })
                    } else {
                        console.log("lỗi")
                    }
                }
            })
            .catch(response => {
                if (response.error === 'Unauthorized')
                    AppUtils.logout()
            })
            .finally(() => { });

    }

    render() {
        var { data, isLoading, typeChildLv2, typeChildLv3, isType2Loading } = this.state;
        return (
            isLoading && isType2Loading && <div className="section__content section__content--p30">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            {/* DATA TABLE */}
                            <h3 className="title-5 m-b-35">Lịch sử thay đổi</h3>
                            <div className="table-responsive table-responsive-data2">
                                <table className="table table-data2">
                                    <thead>
                                        <tr>
                                            <th>Ngày diễn ra hoạt động</th>
                                            <th>Người thực hiện</th>
                                            <th>Số tiền</th>
                                            <th>Mục đích</th>
                                            <th>Loại</th>
                                            {/* <th className="large">Chi tiết</th> */}
                                            <th className="large">Mô tả thay đổi</th>
                                            <th>Ngày cập nhật hoạt động</th>
                                            <th>Lịch sử cập nhật hoạt động</th>
                                            <th className="small">Thao tác</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            data.map((item, key) => {
                                                let parent = ""
                                                let child = ""
                                                let note = ""
                                                let state = "role user"
                                                if (item.type != undefined) {
                                                    let type = item.type.split(":")
                                                    parent = typeParent.find((item) => {
                                                        let label = CONSTANT.TYPE_INPUT
                                                        if (item.label == CONSTANT.TYPE_CHI) label = CONSTANT.TYPE_OUTPUT
                                                        return label == type[0]
                                                    }).label

                                                    if (parent == CONSTANT.TYPE_CHI) state = "role admin"

                                                    let type2 = typeChildLv2[0].options.find((item) => {
                                                        if (item.value != 0 && item.value + "" == type[1]) {
                                                            return item
                                                        }
                                                    })

                                                    child = type2.label
                                                    note = type2.description
                                                }
                                                return (
                                                    <tr className="tr-shadow">
                                                        <td>{AppUtils.toDateTime(item.date)}</td>
                                                        <td className="desc">{item.actorId}</td>
                                                        <td>
                                                            <NumberFormat value={item.amount} displayType={'text'} thousandSeparator={true} prefix={''} />{' '}
                                                        </td>
                                                        <td>{item.purpose}</td>
                                                        <td>
                                                            <span className={state}>{parent}</span>
                                                        </td>
                                                        {/* <td>
                                                            <span >{child}</span>
                                                        </td> */}
                                                        <td><span >{item.description}</span></td>
                                                        <td>{AppUtils.toDateTime(item.createAt)}</td>
                                                        <td>{AppUtils.toDateTime(item.lastCreateAt)}</td>
                                                        <td>
                                                            <div className="table-data-feature">
                                                                <button className="item" data-toggle="tooltip" data-placement="top" title="Chi tiết" onClick={() => this.handleShowDetail(item)}>
                                                                    <Icon icon={detailIcon} />
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                )
                                            }
                                            )
                                        }

                                        <Modal show={this.state.showFormDetail} onHide={this.handleCloseDetail.bind(this)}
                                            size="lg"
                                            aria-labelledby="contained-modal-title-vcenter"
                                            centered>
                                            <Modal.Header closeButton>
                                                <Modal.Title>Lịch sử thay đổi</Modal.Title>
                                            </Modal.Header>
                                            <Modal.Body>
                                                <HistoryDetail itemDetail={this.state.chosenItem}/>
                                            </Modal.Body>
                                        </Modal>
                                    </tbody>
                                </table>
                            </div>
                            {/* END DATA TABLE */}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ActivityHistory;
import CONSTANT from "../Constant";
import { date } from "yup";
import axios from 'axios'

const AppUtils = {
    HEADER_AUTHORIZATION: () => localStorage.getItem(CONSTANT.TOKEN_TYPE) + " " + localStorage.getItem(CONSTANT.ACCESS_TOKEN),

    convertYearArrayToOptions: (array) => {
        let arrayOption = []
        array.forEach(element => {
            let obj = {
                value: element, label: element
            }
            arrayOption.push(obj)
        });
        let defaultObj = {
            value: CONSTANT.NO_FILTER, label: "All year"
        }
        arrayOption.push(defaultObj)
        let ops = {
            label: 'year',
            options: arrayOption,
        }
        let res = []
        res.push(ops)
        return res
    },

    convertActorIdToSelectModel: (array, isAddDefault) => {
        let typeOption = []
        array.forEach(element => {
            let obj = {
                value: element, label: element
            }
            typeOption.push(obj)
        });
        if (isAddDefault) {
            let defaultObj = {
                value: CONSTANT.NO_FILTER, label: "Tất cả"
            }
            typeOption.push(defaultObj)
        }
        let ops = {
            label: 'actorId',
            options: typeOption,
        }
        let res = []
        res.push(ops)
        return res
    },

    convertTypeToSelectModel: (array, isAddDefault) => {
        let typeOption = []
        array.forEach(element => {
            let obj = {
                value: element.id, label: element.label, description: element.description
            }
            typeOption.push(obj)
        });
        if (isAddDefault) {
            let defaultObj = {
                value: CONSTANT.NO_FILTER, label: "Tất cả", description: ""
            }
            typeOption.push(defaultObj)
        }
        let ops = {
            label: 'type',
            options: typeOption,
        }
        let res = []
        res.push(ops)
        return res
    },

    convertType3ToSelectModel: (array, isAddDefault) => {
        let typeOption = []
        array.forEach(element => {
            let obj = {
                value: element.idLv3, label: element.label
            }
            typeOption.push(obj)
        });
        if (isAddDefault) {
            let defaultObj = {
                value: CONSTANT.NO_FILTER, label: "Tất cả"
            }
            typeOption.push(defaultObj)
        }
        let ops = {
            label: 'type',
            options: typeOption,
        }
        let res = []
        res.push(ops)
        return res
    },

    convertRoleArrayToOptions: (array, adDefault) => {
        let arrayOption = []
        array.forEach(element => {
            let v = CONSTANT.NGUOI_DUNG
            if (element.name == CONSTANT.ROLE_ADMIN) v = CONSTANT.QUAN_TRI_VIEN
            else if (element.name == CONSTANT.ROLE_SHAREHOLDER_FOUNDER) v = CONSTANT.CO_DONG_SANG_LAP
            else if (element.name == CONSTANT.ROLE_SHAREHOLDER_COMMON) v = CONSTANT.CO_DONG_PHO_THONG
            else if (element.name == CONSTANT.ROLE_SHAREHOLDER_PREFERRED) v = CONSTANT.CO_DONG_UU_DAI
            else if (element.name == CONSTANT.ROLE_USER) v = CONSTANT.NGUOI_DUNG
            else if (element.name == CONSTANT.ROLE_ACCOUNTANT) v = CONSTANT.KE_TOAN
            else if (element.name == CONSTANT.ROLE_AUDITOR) v = CONSTANT.KIEM_TOAN_VIEN

            let obj = {
                value: element.name, label: v
            }
            arrayOption.push(obj)
        });

        if (adDefault) {
            let obj = {
                value: CONSTANT.NO_FILTER, label: "Tất cả"
            }
            arrayOption.push(obj)
        }

        let ops = {
            label: 'role',
            options: arrayOption,
        }
        let res = []
        res.push(ops)
        return res
    },

    getCurrentYear: () => {
        let toDay = Date.now()
        return toDay
    },

    getBeginOfYear: () => {
        let beginyear = new Date(new Date().getFullYear(), 0, 1);
        return beginyear.getMilliseconds()
    },

    getEndOfYear: () => {
        let endyear = new Date(new Date().getFullYear(), 11, 31);
        return endyear.getMilliseconds()
    },

    getBeginOfNYearAgo: (yearAgo) => {
        let today = Date.now()
        let nYearAgo = today - yearAgo * CONSTANT.ONE_YEAR_IN_MILLISECONDS
        var date = new Date(1970, 0, 1);
        date.setMilliseconds(nYearAgo)
        let beginNyear = new Date(date.getFullYear(), 0, 1);
        return beginNyear.getTime()
    },

    getEndOfNYearAgo: (yearAgo) => {
        let today = Date.now()
        let nYearAgo = today - yearAgo * CONSTANT.ONE_YEAR_IN_MILLISECONDS
        var date = new Date(1970, 0, 1);
        date.setMilliseconds(nYearAgo)
        let beginNyear = new Date(date.getFullYear(), 11, 31);
        return beginNyear.getTime()
    },

    formatNumber: (number) => {
        let div = number / 1000
        return div //+ " k"
    },


    toDateTime(miliSecs) {
        if (miliSecs == 0) return "---"
        var t = new Date(1970, 0, 1); // Epoch
        t.setMilliseconds(miliSecs);
        if (t.getFullYear() == 1970) return "---"
        let month = t.getMonth() + 1
        if (month < 10) month = '0' + month
        let date = t.getDate()
        if (date < 10) date = '0' + date
        let strDate = date + "/" + month + "/" + t.getFullYear()
        return strDate;
    },

    sortListByDate(data, state) {
        return data.sort(function (a, b) {
            if (state)
                return b.createAt - a.createAt
            else return a.createAt - b.createAt
        })
    },

    sortListByLastCreateDate(data, state) {
        return data.sort(function (a, b) {
            if (state)
                return b.lastCreateAt - a.lastCreateAt
            else return a.lastCreateAt - b.lastCreateAt
        })
    },

    sortListUserByDate(data, state) {
        return data.sort(function (a, b) {
            if (state)
                return new Date(b.joinedAt) - new Date(a.joinedAt)
            else return new Date(a.joinedAt) - new Date(b.joinedAt)
        })
    },

    sortListByAmount(data, state) {
        return data.sort(function (a, b) {
            if (state)
                return b.amount - a.amount
            else return a.amount - b.amount
        })
    },

    sortListChart(data, state) {
        return data.sort(function (a, b) {
            if (state)
                return ((b.year * 1000) + b.month) - ((a.year * 1000) + a.month)
            else return ((a.year * 1000) + a.month) - ((b.year * 1000) + b.month)
        })
    },

    sortListByActorId(data, state) {
        return data.sort((a, b) => {
            if (state)
                return b.actorId.localeCompare(a.actorId)
            else return a.actorId.localeCompare(b.actorId)
        })
    },

    sortListById(data, state) {
        return data.sort((a, b) => {
            if (state)
                return b.id - a.id
            else return a.id - b.id
        })
    },

    sortListByType(data, state) {
        return data.sort((a, b) => {
            if (state)
                return new Date(b.type) - new Date(a.type)
            else return new Date(a.type) - new Date(b.type)
        })
    },

    sortListByPurpose(data, state) {
        return data.sort((a, b) => {
            if (state)
                return b.purpose.localeCompare(a.purpose)
            else return a.purpose.localeCompare(b.purpose)
        })
    },

    sortListByDescription(data, state) {
        return data.sort((a, b) => {
            if (state)
                return b.description.localeCompare(a.description)
            else return a.description.localeCompare(b.description)
        })
    },

    sortListByYear(data, state) {
        return data.sort((a, b) => {
            if (state)
                return new Date(b.year) - new Date(a.year)
            else return new Date(a.year) - new Date(b.year)
        })
    },

    sortListByQuater(data, state) {
        return data.sort((a, b) => {
            if (state)
                return new Date(b.quarter) - new Date(a.quarter)
            else return new Date(a.quarter) - new Date(b.quarter)
        })
    },

    sortFinancialData(data) {
        return data.sort((a, b) => {
            return new Date(a.createAt) - new Date(b.createAt)
        })
    },

    logout() {
        console.log("logout")
        localStorage.removeItem(CONSTANT.USER_LOGIN_TOKEN)
        localStorage.removeItem(CONSTANT.USER_LOGIN_ROLE)
        localStorage.removeItem(CONSTANT.ACCESS_TOKEN)
        localStorage.removeItem(CONSTANT.TOKEN_TYPE)
    },

    getType(type) {
        if (type == 1) return "Chi phí HĐDN"
        else if (type == 2) return "Chi phí HĐĐT"
        else if (type == 3) return "Chi phí HĐTC"
        else if (type == 4) return "Thu nhập HĐDN"
        else if (type == 5) return "Thu nhập HĐĐT"
        else if (type == 6) return "Thu nhập HĐTC"
        else return "Tất cả"
    },

    getStrType(type) {
        if (type == 1) return CONSTANT.TYPE_INPUT
        else if (type == 2) return CONSTANT.TYPE_OUTPUT
    },

    getStrRole(role) {
        let str = ""
        if (role == CONSTANT.ROLE_ADMIN) {
            str = CONSTANT.QUAN_TRI_VIEN
        } else if (role == CONSTANT.ROLE_SHAREHOLDER_COMMON) {
            str = CONSTANT.CO_DONG_PHO_THONG
        } else if (role == CONSTANT.ROLE_SHAREHOLDER_FOUNDER) {
            str = CONSTANT.CO_DONG_SANG_LAP
        } else if (role == CONSTANT.ROLE_SHAREHOLDER_PREFERRED) {
            str = CONSTANT.CO_DONG_UU_DAI
        } else if (role == CONSTANT.ROLE_ACCOUNTANT) {
            str = CONSTANT.KE_TOAN
        } else if (role == CONSTANT.ROLE_AUDITOR) {
            str = CONSTANT.KIEM_TOAN_VIEN
        } else {
            str = CONSTANT.NGUOI_DUNG
        }
        return str
    },

    getRoleLabel(role) {
        let str = ""
        if (role == CONSTANT.QUAN_TRI_VIEN) {
            str = CONSTANT.ROLE_ADMIN
        } else if (role == CONSTANT.CO_DONG_PHO_THONG) {
            str = CONSTANT.ROLE_SHAREHOLDER_COMMON
        } else if (role == CONSTANT.CO_DONG_SANG_LAP) {
            str = CONSTANT.ROLE_SHAREHOLDER_FOUNDER
        } else if (role == CONSTANT.CO_DONG_UU_DAI) {
            str = CONSTANT.ROLE_SHAREHOLDER_PREFERRED
        } else if (role == CONSTANT.KE_TOAN) {
            str = CONSTANT.ROLE_ACCOUNTANT
        } else if (role == CONSTANT.KIEM_TOAN_VIEN) {
            str = CONSTANT.ROLE_AUDITOR
        } else if (role == CONSTANT.NGUOI_DUNG) {
            str = CONSTANT.ROLE_USER
        }
        return str
    },

    getRoleId(role) {
        if (role == CONSTANT.QUAN_TRI_VIEN || role == CONSTANT.ROLE_ADMIN) {
            return CONSTANT.ROLE_ADMIN_ID
        } else if (role == CONSTANT.CO_DONG_PHO_THONG || role == CONSTANT.ROLE_SHAREHOLDER_COMMON) {
            return CONSTANT.ROLE_SHAREHOLDER_COMMON_ID
        } else if (role == CONSTANT.CO_DONG_SANG_LAP || role == CONSTANT.ROLE_SHAREHOLDER_FOUNDER) {
            return CONSTANT.ROLE_SHAREHOLDER_FOUNDER_ID
        } else if (role == CONSTANT.CO_DONG_UU_DAI || role == CONSTANT.ROLE_SHAREHOLDER_PREFERRED) {
            return CONSTANT.ROLE_SHAREHOLDER_PREFERRED_ID
        } else if (role == CONSTANT.NGUOI_DUNG || role == CONSTANT.ROLE_USER) {
            return CONSTANT.ROLE_USER_ID
        } else if (role == CONSTANT.KE_TOAN || role == CONSTANT.ROLE_ACCOUNTANT) {
            return CONSTANT.ROLE_ACCOUNTANT_ID
        } else if (role == CONSTANT.KIEM_TOAN_VIEN || role == CONSTANT.ROLE_AUDITOR) {
            return CONSTANT.ROLE_AUDITOR_ID
        }
        else return CONSTANT.NO_FILTER
    },

    getRatio(v, total) {
        return (v / total) * 100
    }

}

export default AppUtils

import CONSTANT from "../Constant";

export const groupStyles = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
};

export const groupBadgeStyles = {
    backgroundColor: '#EBECF0',
    borderRadius: '2em',
    color: '#172B4D',
    display: 'inline-block',
    fontSize: 12,
    fontWeight: 'normal',
    lineHeight: '1',
    minWidth: 1,
    padding: '0.16666666666667em 0.5em',
    textAlign: 'center'
};

export const quarterOptions1 = [
    { value: 1, label: 'Q1', type: CONSTANT.TYPE_QUARTER},
    { value: 2, label: 'Q2', type: CONSTANT.TYPE_QUARTER},
    { value: 3, label: 'Q3', type: CONSTANT.TYPE_QUARTER},
    { value: 4, label: 'Q4', type: CONSTANT.TYPE_QUARTER}
];

export const quarterOptions = [
    { value: 1, label: 'Q1', type: CONSTANT.TYPE_QUARTER},
    { value: 2, label: 'Q2', type: CONSTANT.TYPE_QUARTER},
    { value: 3, label: 'Q3', type: CONSTANT.TYPE_QUARTER},
    { value: 4, label: 'Q4', type: CONSTANT.TYPE_QUARTER},
    { value: 5, label: 'Cả năm', type: CONSTANT.TYPE_QUARTER},
    { value: CONSTANT.NO_FILTER, label: 'Bỏ chọn', type: CONSTANT.TYPE_QUARTER},
];

export const monthOptions = [
    { value: 1, label: 'Th1', type: CONSTANT.TYPE_MONTH},
    { value: 2, label: 'Th2', type: CONSTANT.TYPE_MONTH},
    { value: 3, label: 'Th3', type: CONSTANT.TYPE_MONTH},
    { value: 4, label: 'Th4', type: CONSTANT.TYPE_MONTH},
    { value: 5, label: 'Th5', type: CONSTANT.TYPE_MONTH},
    { value: 6, label: 'Th6', type: CONSTANT.TYPE_MONTH},
    { value: 7, label: 'Th7', type: CONSTANT.TYPE_MONTH},
    { value: 8, label: 'Th8', type: CONSTANT.TYPE_MONTH},
    { value: 9, label: 'Th9', type: CONSTANT.TYPE_MONTH},
    { value: 10, label: 'Th10', type: CONSTANT.TYPE_MONTH},
    { value: 11, label: 'Th11', type: CONSTANT.TYPE_MONTH},
    { value: 12, label: 'Th12', type: CONSTANT.TYPE_MONTH},
];

export const yearOptions = [
    { value: 2020, label: '2020', type: CONSTANT.TYPE_YEAR, timeStamp: CONSTANT.TIMESTAMP_2020},
    { value: 2019, label: '2019', type: CONSTANT.TYPE_YEAR, timeStamp: CONSTANT.TIMESTAMP_2019},
    { value: 2018, label: '2018', type: CONSTANT.TYPE_YEAR, timeStamp: CONSTANT.TIMESTAMP_2018},
    { value: 2017, label: '2017', type: CONSTANT.TYPE_YEAR, timeStamp: CONSTANT.TIMESTAMP_2017},
    { value: CONSTANT.NO_FILTER, label: 'Cả năm', type: CONSTANT.TYPE_YEAR, timeStamp: CONSTANT.NO_FILTER},
];

export const chartYearOption = [
    { value: 2020, label: '2020', type: CONSTANT.TYPE_YEAR, timeStamp: CONSTANT.TIMESTAMP_2020},
    { value: 2019, label: '2019', type: CONSTANT.TYPE_YEAR, timeStamp: CONSTANT.TIMESTAMP_2019},
    { value: 2018, label: '2018', type: CONSTANT.TYPE_YEAR, timeStamp: CONSTANT.TIMESTAMP_2018},
    { value: 2017, label: '2017', type: CONSTANT.TYPE_YEAR, timeStamp: CONSTANT.TIMESTAMP_2017}
];

export const chartToYearOption = [
    { value: 2020, label: '2020', type: CONSTANT.TYPE_YEAR, timeStamp: 1609433999000},
    { value: 2019, label: '2019', type: CONSTANT.TYPE_YEAR, timeStamp: 1577811599000},
    { value: 2018, label: '2018', type: CONSTANT.TYPE_YEAR, timeStamp: 1546275599000},
    { value: 2017, label: '2017', type: CONSTANT.TYPE_YEAR, timeStamp: 1514739599000}
];

export const typeParent = [
    { value: 1, label: CONSTANT.TYPE_THU },
    { value: 2, label: CONSTANT.TYPE_CHI },
    { value: 0, label: 'Tất cả' }
]

export const typeParentExcludeAll = [
    { value: 1, label: CONSTANT.TYPE_THU },
    { value: 2, label: CONSTANT.TYPE_CHI }
]

export const typeChildExcludeAll = [
    { value: 1, label: 'Doanh Nghiệp' },
    { value: 2, label: 'Tài Chính' },
    { value: 3, label: 'Đầu tư' }
]

export const typeChild = [
    { value: 0, label: 'Tất cả' },
    { value: 1, label: 'Doanh Nghiệp' },
    { value: 2, label: 'Tài Chính' },
    { value: 3, label: 'Đầu tư' }
]

export const typeChildLv2ExcludeAll = [
    { value: 1, label: 'Đầu tư công ty con' },
    { value: 2, label: 'Mua sắm thiết bị' },
    { value: 3, label: 'Thuế Doanh nghiệp' },
    { value: 4, label: 'Tiền lương nhân viên' },
    { value: 5, label: 'Bảo hiểm cho nhân viên' },
    { value: 6, label: 'Chứng khoán' },
    { value: 7, label: 'Cổ phiếu' },
    { value: 8, label: 'Công việc kinh doanh' },
]

export const typeChildLv2 = [
    { value: 0, label: 'Tất cả' },
    { value: 1, label: 'Đầu tư công ty con' },
    { value: 2, label: 'Mua sắm thiết bị' },
    { value: 3, label: 'Thuế Doanh nghiệp' },
    { value: 4, label: 'Tiền lương nhân viên' },
    { value: 5, label: 'Bảo hiểm cho nhân viên' },
    { value: 6, label: 'Chứng khoán' },
    { value: 7, label: 'Cổ phiếu' },
    { value: 8, label: 'Công việc kinh doanh' },
]

export const typeOptions = [
    { value: 0, label: 'Tất cả' },
    { value: 1, label: 'Chi phí HĐDN' },
    { value: 2, label: 'Chi phí HĐĐT' },
    { value: 3, label: 'Chi phí HĐTC' },
    { value: 4, label: 'Thu nhập HĐDN' },
    { value: 5, label: 'Thu nhập HĐĐT' },
    { value: 6, label: 'Thu nhập HĐTC' }
]

export const typeOptions1 = [
    { value: 1, label: 'Chi phí HĐDN' },
    { value: 2, label: 'Chi phí HĐĐT' },
    { value: 3, label: 'Chi phí HĐTC' },
    { value: 4, label: 'Thu nhập HĐDN' },
    { value: 5, label: 'Thu nhập HĐĐT' },
    { value: 6, label: 'Thu nhập HĐTC' }
]

export const roleOptions = [
    { value: CONSTANT.ROLE_ADMIN, label: CONSTANT.QUAN_TRI_VIEN },
    { value: CONSTANT.ROLE_SHAREHOLDER_FOUNDER, label: CONSTANT.CO_DONG_SANG_LAP },
    { value: CONSTANT.ROLE_SHAREHOLDER_COMMON, label: CONSTANT.CO_DONG_PHO_THONG },
    { value: CONSTANT.ROLE_SHAREHOLDER_PREFERRED, label: CONSTANT.CO_DONG_UU_DAI },
    { value: CONSTANT.ROLE_USER, label: CONSTANT.NGUOI_DUNG },
    { value: CONSTANT.ROLE_ACCOUNTANT, label: CONSTANT.KE_TOAN },
    { value: CONSTANT.ROLE_AUDITOR, label: CONSTANT.KIEM_TOAN_VIEN }

]

export const groupTypeOptions = [
    {
        label: 'Type',
        options: typeOptions
    }
];

export const groupTypeOptions1 = [
    {
        label: 'Type',
        options: typeOptions1
    }
];
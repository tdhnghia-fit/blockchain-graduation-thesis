About Hyperledger-Fabric
===

## Introduction [Link](https://hyperledger-fabric.readthedocs.io/en/release-1.4/whatis.html)

### Quan trọng
- The Fabric platform is also permissioned, meaning that, unlike with a public permissionless network, `the participants are known to each other`, rather than anonymous and therefore fully untrusted. This means that while the participants may not fully trust one another (they may, for example, be competitors in the same industry), a network can be operated under a governance model that is built off of what trust does exist between participants, such as a legal agreement or framework for handling disputes.
- One of the most important of the platform’s differentiators is its support for `pluggable consensus protocols` that enable the platform to be more effectively customized to fit particular use cases and trust models. For instance, when deployed within a single enterprise, or operated by a trusted authority, fully byzantine fault tolerant consensus might be considered unnecessary and an excessive drag on performance and throughput. In situations such as that, a crash fault-tolerant (CFT) consensus protocol might be more than adequate whereas, in a multi-party, decentralized use case, a more traditional byzantine fault tolerant (BFT) consensus protocol might be required.
- Fabric can leverage consensus protocols that do not require `a native cryptocurrency` to incent costly mining or to fuel smart contract execution. Avoidance of a cryptocurrency reduces some significant risk/attack vectors, and absence of cryptographic mining operations means that the platform can be deployed with roughly the same operational cost as any other distributed system.


## Reference

- https://viblo.asia/p/hyperledger-la-gi-gioi-thieu-tong-quan-ve-cac-du-an-cua-he-sinh-thai-hyperledger-YWOZrw4YlQ0
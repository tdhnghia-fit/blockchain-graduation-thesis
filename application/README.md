Kế hoạch thực hiện và công việc thực hiện
===

## 17/02 - 02/03

Tạ Đăng Hiếu Nghĩa

- Tìm hiểu Hyperledger Fabric
- Cấu trúc một mạng lưới Hyperledger Fabric
- Demo một ứng dụng mẫu

Trương Hổ Phong

- Tìm hiểu về Hyperledger Fabric
- Tìm hiểu cách thức triển khai ứng dụng phía client

Folder báo cáo: [Report - 1](./Report-1/README.md)

## 3/3 - 14/3

Tạ Đăng Hiếu Nghĩa

- Bài viết về Smart Contract của Hyperledger
- Bài viết về cách lưu trữ dữ liệu của Hyperledger
- Triển khai lại hệ thống lưu trữ dữ liệu (API Server)
- Thiết kế sơ đồ use case


Trương Hổ Phong

- Tìm hiểu các dữ liệu cần để lưu trữ, xử lý việc quản lý vốn
- Tiếp tục tìm hiểu cách thức triển khai ứng dụng phía client
- Thiết kế sơ đồ use case

Folder báo cáo: [Report - 2](./Report-2/README.md)

## 15/3 - 22/3

Tạ Đăng Hiếu Nghĩa

- Sau khi thống nhất cơ sở dữ liệu cần  lưu trữ tiến hành sửa đổi  chaincode của server API hiện tại   

- Điều chỉnh và thêm các business logic của Server dựa trên usecase design  

- Tự tạo script khởi tạo network với số lượng node theo ý muốn ( hiện tại đang sử dụng network mẫu của Open Source) 

Trương Hổ Phong

Tìm hiểu về vấn đề tài chính doanh nghiệp
1. Báo cáo tài chính
2. Báo cáo kết quả hoạt động kinh doanh
3. Báo cáo lưu chuyển tiền tệ

Code front end admin cho web client

Folder báo cáo: [Report - 3](./Report-3)

## 23/3 - 29/3

Tạ  Đăng  Hiếu  Nghĩa: 

- Sau khi thống nhất cơ sở dữ liệu cần  lưu trữ tiến hành sửa đổi  chaincode của server API hiện tại   
- Điều chỉnh và thêm các business logic của Server dựa trên usecase design  
- Từ  phương án lựa chọn business code của project  để triển khai  

Trương Hổ Phong 

- Chuyển code front end phần web (html) sang reactjs 
- Code giao diện cho mobile client

Folder báo cáo: [Report - 4](./Report-4)

## 30/3 - 5/4

Tạ Đăng Hiếu Nghĩa:

- Tiếp tục viết các api cho hệ thống trên các dữ liệu đang thay
đổi
- Nếu có thể tiến hành deploy lên server để sau này phía
Front-end tiện sử dụng API trong quá trình code.
- Tìm hiểu cơ chế lưu trữ của hệ thống và thử các scenarios
thay đổi dữ liệu xem hành vi của hệ thống mình như thế nào
(có đảm bảo bất biến dữ liệu không?, có lưu lại các lịch sử
changes dữ liệu không?)

Trương Hổ Phong:

- Tiếp tục khảo sát, lấy ý kiến update model database (nếu có
thay đổi)
- Chuẩn bị các câu query cho các usecase
- Học thêm cách getdata hiển thị lên UI (backend)
- Tiếp tục hoàn thiện Front-end

